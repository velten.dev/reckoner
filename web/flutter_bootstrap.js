{{flutter_js}}
{{flutter_build_config}}

//Modified from https://gist.github.com/bizz84/08e6608cf7289a77c335e70122308857

const loadingDiv = document.getElementsByClassName('loading')[0];

// Customize the app initialization process
_flutter.loader.load({
  onEntrypointLoaded: async function(engineInitializer) {
    const appRunner = await engineInitializer.initializeEngine({renderer: "canvaskit"});

    // Remove the loading spinner when the app runner is ready
    if (document.body.contains(loadingDiv)) document.body.removeChild(loadingDiv);

    await appRunner.runApp();
  }
});