#!/bin/bash

APP=reckoner
export ARCH=$(uname -m)

mkdir -p ./$APP.AppDir
cd $APP.AppDir/
rm -rf *
cp ../reckoner.png .
pwd

cat > $APP.desktop <<EOF
[Desktop Entry]
Type=Application
Name=Reckoner
Terminal=false
StartupNotify=true
Icon=reckoner
Categories=Office;Finance;
EOF

cp -r ../../build/linux/*64/release/bundle/* .
chmod a+x reckoner

cat > AppRun <<EOF
#!/bin/sh
cd "\$(dirname "\$0")"
HERE=\`pwd\`
export LD_LIBRARY_PATH="\${HERE}/usr/lib/:\${LD_LIBRARY_PATH:+:\$LD_LIBRARY_PATH}"
exec ./reckoner
EOF
chmod a+x AppRun

cd ..
wget -c https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-${ARCH}.AppImage -O appimagetool.AppImage
chmod a+x appimagetool.AppImage
./appimagetool.AppImage -v -n $APP.AppDir ../release/reckoner-$1-$ARCH.AppImage
rm appimagetool.AppImage

if [ -d "./$APP.AppDir/" ] ; then
  rm -rf ./$APP.AppDir
fi