#!/bin/bash

# if [ -n "$(command -v pacman)" ]; then
#     pacman -S clang cmake ninja pkg-config gtk3
#     pacman -S sqlite libsecret openssl flatpak flatpak-builder file
# elif [ -n "$(command -v apt)" ]; then
#     apt update
#     apt install -y clang cmake ninja-build pkg-config libgtk-3-dev liblzma-dev libstdc++-12-dev
#     apt install -y libsqlite3-dev libsecret-1-0 libjsoncpp-dev libjsoncpp25 libsecret-1-dev libssl-dev libfuse2 flatpak flatpak-builder file

# elif [ -n "$(command -v dnf)" ]; then
#     dnf update
#     dnf install -y clang cmake ninja-build pkg-config gtk3-devel lzma-sdk-devel libstdc++-devel
#     dnf install -y sqlite-devel libsecret libsecret-devel jsoncpp jsoncpp-devel openssl-devel fuse-devel flatpak flatpak-builder file libappindicator
# else
#     echo "Not supported"
#     exit 1
# fi

sudo apt update
# development dependencies
sudo apt install -y libsqlite3-dev libjsoncpp-dev libsecret-1-dev libssl-dev libayatana-appindicator3-dev
# production dependencies
sudo apt install -y libsqlite3-0 libsecret-1-0 libjsoncpp25 libsecret-1-0 libssl3 libfuse2 flatpak flatpak-builder file aapt

# snap install snapcraft --classic
# snap install multipass
# multipass start
# sudo snap set snapcraft provider=multipass
