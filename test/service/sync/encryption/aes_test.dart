import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:reckoner/model/encrypted.dart';
import 'package:reckoner/service/sync/encryption/aes.dart';

void main() {
  group('AES Encryption Tests', () {
    setUp(() async {
      await Process.run('flutter', ['pub', 'run', 'webcrypto:setup']);
    });

    test('Encrypts Data', () async {
      final object = {'foo': 'bar'};
      const secret = 'SuperSecret';
      final data = await aesEncrypt(secret, object);

      expect(data.algorithm, EncryptionAlgorithm.aes);
    });

    test('Decrypts Data', () async {
      final object = {'foo': 'bar'};
      const secret = 'SuperSecret';
      final data = await aesEncrypt(secret, object);
      final object2 = await aesDecrypt(secret, data.aes!);
      expect(object, object2);
    });

    test('Return null if wrong key', () async {
      final object = {'foo': 'bar'};
      const secret = 'SuperSecret';
      final data = await aesEncrypt(secret, object);
      final object2 = await aesDecrypt('I FORGOT!', data.aes!);
      expect(object2, null);
    });
  });
}
