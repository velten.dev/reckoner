import 'package:flutter_test/flutter_test.dart';
import 'package:reckoner/model/currency.dart';
import 'package:reckoner/view/model/currency_input_formatter.dart';

void main() {
  group('Currency Formatter Tests', () {
    test('Test with symbol to DB', () async {
      final formatter = CurrencyInputFormatter(
        currency: Currency(decimalDigits: 2, symbol: '\$'),
      );

      expect(formatter.toDB('\$10.21'), 1021);
      expect(formatter.toDB('\$10'), 1000);
      expect(formatter.toDB('💶10 Some Other. Stuff34'), 1034);
      expect(formatter.toDB('💶10 Some Other. Stuff.34'), 0);
      expect(formatter.toDB('\$10.21999999'), 1021);
      expect(formatter.toDB('This is bananas'), 0);
    });

    test('Test without symbol to DB', () async {
      final formatter = CurrencyInputFormatter(
        currency: Currency(decimalDigits: 2, symbol: '\$'),
        showSymbol: false,
      );

      expect(formatter.toDB('\$10.21'), 1021);
      expect(formatter.toDB('\$10'), 1000);
      expect(formatter.toDB('💶10 Some Other. Stuff34'), 1034);
      expect(formatter.toDB('💶10 Some Other. Stuff.34'), 0);
      expect(formatter.toDB('\$10.21999999'), 1021);
      expect(formatter.toDB('This is bananas'), 0);
    });

    test('Test with large number of decimal places', () async {
      final formatter = CurrencyInputFormatter(
        currency: Currency(
          name: 'Etherium',
          code: 'ETH',
          symbol: 'Ξ',
          decimalDigits: 18,
        ),
        showSymbol: false,
      );

      expect(formatter.fromDB(1), 'Ξ0.000000000000000001');
      // ignore: avoid_js_rounded_ints
      expect(formatter.fromDB(1000000001234567890), 'Ξ1.000000001234567890');
    });
  });
}
