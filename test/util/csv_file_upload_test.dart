import 'package:flutter_test/flutter_test.dart';
import 'package:reckoner/model/account.dart';
import 'package:reckoner/model/currency.dart';
import 'package:reckoner/view/util/csv_file_upload.dart';

void main() {
  final currency = Currency();
  final org = Organization();
  final account = Account()..organization = org;

  group('CSV Tests', () {
    test('Handles missing data', () {
      void handler(final dynamic error) =>
          expect(error, 'CSV does not contain data');
      expect(csvToTransaction('', currency, account, handler), null);
    });

    test('Handles wrong header', () {
      void handler(final dynamic error) =>
          expect(error, 'CSV header not formatted correctly');
      expect(
        csvToTransaction('Some,Wrong,Data\n1,2,3', currency, account, handler),
        null,
      );
    });

    test('Error on certain row', () {
      void handler(final dynamic error) =>
          expect(error, 'Error importing row: 1. Check the row for errors.');
      expect(
        csvToTransaction(
          'Description,Date,Amount\n1,2,3',
          currency,
          account,
          handler,
        ),
        null,
      );
    });

    test('Converts Row', () {
      Never handler(final dynamic error) => throw Exception(error);
      final result = csvToTransaction(
        'Description,Date,Amount\nTest,2022-01-01,10.00',
        currency,
        account,
        handler,
      );
      expect(result?.length, 1);
    });
  }, skip: true);
}
