import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:reckoner/model/currency.dart';
import 'package:reckoner/view/l10n/localizations.dart';
import 'package:reckoner/view/widgets/form/input_form.dart';
import 'package:reckoner/view/widgets/form/list_search_input.dart';

ReckonerLocalizations enText = ReckonerLocalizations(null);

var ddList = [
  Currency(
    updateDate: DateTime.now().toUtc(),
    name: 'name',
    code: 'NAM',
    symbol: '\$N',
    decimalDigits: 2,
  ),
  Currency(
    updateDate: DateTime.now().toUtc(),
    name: 'blah',
    code: 'BLA',
    symbol: '\$B',
    decimalDigits: 2,
  ),
];

var title = 'Test Form';
var input1Title = 'String value (unlimited)';
var input1Iv = 'Unlimited power!';
var input2Title = 'String value (3)';
var input3Title = 'Drop down item list';
var input4Title = 'Number';

void main() {
  group('Input Form Tests:', () {
    testWidgets('Input Form Loads', (final tester) async {
      await testSetup(
        tester,
        name: input1Iv,
        currency: ddList[0],
        decimalDigits: 0,
      );
      expect(find.text(title), findsOneWidget);
      expect(find.text(input1Title), findsOneWidget);
      expect(find.text(input1Iv), findsOneWidget);
      expect(find.text(input2Title), findsOneWidget);
      expect(find.text(input3Title), findsOneWidget);
      expect(find.text(ddList[0].symbol), findsOneWidget);
      expect(find.text(input4Title), findsOneWidget);
      expect(find.text('0'), findsOneWidget);
    });

    testWidgets('Edit data and verify database call', (final tester) async {
      final expectedCurrency = Currency(
        name: input1Iv,
        code: 'ABC',
        symbol: ddList[0].symbol,
        decimalDigits: 0,
      );

      void onSaved(final Currency cur) => expect(cur, expectedCurrency);

      await testSetup(
        tester,
        name: input1Iv,
        currency: ddList[0],
        decimalDigits: 0,
        onSave: onSaved,
      );

      await tester.enterText(
        find.widgetWithText(TextFormField, input2Title),
        'abc',
      );
      await tester.pump();
      expect(find.text('ABCD'), findsNothing);
      expect(find.text('abc'), findsOneWidget);
      await tester.tap(find.text(enText.save));
      await tester.pumpAndSettle();
    });

    testWidgets('Open dropdown to verify list', (final tester) async {
      await testSetup(
        tester,
        name: input1Iv,
        currency: ddList[0],
        decimalDigits: 0,
      );
      await tester.tap(find.text(ddList[0].symbol));
      await tester.pumpAndSettle();
      expect(find.text(ddList[0].symbol), findsNWidgets(2));
      expect(find.text(ddList[1].symbol), findsOneWidget);
    });

    testWidgets('Test form missing input', (final WidgetTester tester) async {
      await testSetup(tester);
      await tester.enterText(
        find.widgetWithText(TextFormField, input2Title),
        'abc',
      );
      await tester.pump();
      await tester.tap(find.text(enText.save));
      await tester.pumpAndSettle();
      expect(find.text(input3Title), findsOneWidget);
      //expect(find.text(enText.missingValue), findsWidgets);
    });
  });
}

Future<void> testSetup(
  final WidgetTester tester, {
  final String? name,
  final Currency? currency,
  final int? decimalDigits,
  final void Function(Currency)? onSave,
}) async {
  final Currency model = Currency();

  final form = InputForm(
    title: title,
    inputs: [
      TextFormField(
        decoration: InputDecoration(labelText: input1Title),
        initialValue: name,
        onSaved: (final value) => model.name = value!,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: input2Title),
        onSaved: (final value) => model.code = value!.toUpperCase(),
        maxLength: 3,
      ),
      ListSearchFormField<Currency>(
        decoration: InputDecoration(label: Text(input3Title)),
        onSaved: (final Currency? value) => model.symbol = value?.symbol ?? '',
        initialValue: currency,
        itemToDisplayString: (final Currency item) => item.symbol,
        searchItems: ddList,
        validator: (final value) => value == null ? enText.missingValue : null,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: input4Title),
        onSaved: (final value) => model.decimalDigits = int.parse(value!),
        initialValue: decimalDigits?.toString(),
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      ),
    ],
  );

  final widget = MaterialApp(
    home: form,
    localizationsDelegates: const [
      ReckonerLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ],
    supportedLocales: const [
      Locale('en', ''), // English, no country code
    ],
  );
  await tester.pumpWidget(widget);
  await tester.pumpAndSettle();
}
