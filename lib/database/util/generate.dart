import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';

import '../../config/user_pref_const.dart';
import '../../model/account.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/model.dart';
import '../../model/report.dart';
import '../../model/settings.dart';
import '../../model/transaction.dart';
import '../../view/model/transaction_type.dart';
import '../reckoner_db.dart';

Future<D> _saveItem<D extends Tracked<D>>(final D item) => switch (D) {
  const (Transaction) =>
    ReckonerDb.I.transactionDao.saveDbTransaction(item as Transaction)
        as Future<D>,
  _ => ReckonerDb.I.commonDao.saveTrackedDbItem(item, false),
};

Future<void> _generateTransfer(
  final String srcAccountId,
  final String destAccountId,
  final String name,
  final int amount,
  final DateTime date,
) async {
  await ReckonerDb.I.transactionDao.saveTransfer(
    TransactionTransfer()
      ..source = Transaction(
        accountUuid: srcAccountId,
        name: name,
        date: date,
        currencyCode: 'USD',
        amount: -amount,
      )
      ..destination = Transaction(
        accountUuid: destAccountId,
        name: name,
        date: date,
        currencyCode: 'USD',
        amount: amount,
      ),
    false,
  );
}

Future<void> _generateSimpleTx(
  final String accountId,
  final String name,
  final int amount,
  final DateTime date,
  final String? merchantName,
  final List<(String?, int, Category?, String?)>? splits,
) async {
  final tx = Transaction(
    name: name,
    date: date,
    amount: amount,
    currencyCode: 'USD',
    accountUuid: accountId,
    merchantName: merchantName ?? '',
  );
  if (splits != null) {
    tx.splits =
        splits.map((final s) {
          final split = TransactionSplit(
            uuid: tx.uuid,
            name: s.$1 ?? tx.name,
            amount: s.$2,
          );
          if (s.$3 != null) split.categories[s.$3!.groupUuid] = s.$3!;
          if (s.$4 != null) split.tags = {s.$4!};
          return split;
        }).toList();
  }

  await _saveItem(tx);
}

Future<Category> _generateCatWithLimit(
  final String groupId,
  final String name,
  final int? amount, {
  final String? parentId,
}) async {
  final cat = Category(groupUuid: groupId, name: name, parentUuid: parentId);

  if (amount != null) {
    cat.limits.add(
      CategoryLimit(amount: amount, uuid: cat.uuid, currencyCode: 'USD'),
    );
  }

  return _saveItem(cat);
}

Future<void> _genData() async {
  // ACCOUNTS
  final org = await _saveItem(Organization(name: 'Big Bank'));
  final checking =
      (await _saveItem(
        Account(
          name: 'Checking',
          orgUuid: org.uuid,
          defaultCurrencyCode: 'USD',
        ),
      )).uuid;
  final saving =
      (await _saveItem(
        Account(name: 'Savings', orgUuid: org.uuid, defaultCurrencyCode: 'USD')
          ..alerts.add(
            AccountAlert(
              id: 0,
              uuid: '',
              amount: 5000,
              isMax: false,
              message: 'Low Balance',
              // ignore: deprecated_member_use
              color: Colors.red.value,
              currencyCode: 'USD',
            ),
          ),
      )).uuid;
  final creditCard =
      (await _saveItem(
        Account(
          name: 'Preferred Platinum',
          orgUuid: org.uuid,
          defaultCurrencyCode: 'USD',
        ),
      )).uuid;

  //BUDGETS
  final budgetGroup =
      (await _saveItem(
        CategoryGroup(
          name: 'Budgets',
          requireLimits: true,
          datePeriod: CategoryLimitPeriod.monthly,
          datePeriodInterval: 1,
          isRequired: true,
          enableAutoBalance: kDebugMode,
        ),
      )).uuid;

  final rentCat = await _generateCatWithLimit(budgetGroup, 'Rent', -75000);
  final billsCat = await _generateCatWithLimit(budgetGroup, 'Bills', -30000);
  final foodCat = await _generateCatWithLimit(budgetGroup, 'Food', -45000);
  final groceriesCat = await _generateCatWithLimit(
    budgetGroup,
    'Groceries',
    -30000,
    parentId: foodCat.uuid,
  );
  final restCat = await _generateCatWithLimit(
    budgetGroup,
    'Restaurants',
    -10000,
    parentId: foodCat.uuid,
  );
  final shoppingCat = await _generateCatWithLimit(
    budgetGroup,
    'Shopping',
    -50000,
  );
  final entertainmentCat = await _generateCatWithLimit(
    budgetGroup,
    'Entertainment',
    -10000,
  );
  final miscCat = await _generateCatWithLimit(budgetGroup, 'Misc', -10000);

  //Report Group
  var reportGroup = ReportGroup(name: 'Reports');

  final txReport = await _saveItem(
    Report(name: 'Daily Total Balance', invert: false)
      ..filterObj = ReportTransactionFilter(currencyCodes: ['USD']),
  );
  final accountReport = await _saveItem(
    Report(
      name: 'Account Daily Balances',
      invert: false,
      grouping: TransactionReportGrouping.account,
    )..filterObj = ReportTransactionFilter(currencyCodes: ['USD']),
  );
  final merchantReport = await _saveItem(
    Report(
        name: 'Merchant Balances',
        grouping: TransactionReportGrouping.merchant,
        loadOverTime: false,
        invert: true,
        type: ReportType.bar,
      )
      ..filterObj = ReportTransactionFilter(
        currencyCodes: ['USD'],
        type: TransactionTypeFilter.expense,
      ),
  );
  final merchantPieReport = await _saveItem(
    Report(
        name: 'Merchant Pie Balances',
        grouping: TransactionReportGrouping.merchant,
        loadOverTime: false,
        invert: true,
        type: ReportType.pie,
      )
      ..filterObj = ReportTransactionFilter(
        currencyCodes: ['USD'],
        type: TransactionTypeFilter.expense,
      ),
  );
  final budgetReport = await _saveItem(
    Report(
        name: 'Monthly Budget Balances',
        grouping: TransactionReportGrouping.category,
        loadOverTime: false,
        invert: true,
        type: ReportType.bar,
        relativeEndOffset: 0,
        relativeStartOffset: 0,
        relativeRange: ReportTimeRange.month,
      )
      ..filterObj = ReportTransactionFilter(
        currencyCodes: ['USD'],
        type: TransactionTypeFilter.expense,
        categoryGroupIds: [budgetGroup],
      ),
  );
  final tagReport = await _saveItem(
    Report(
      name: 'Tag Balances',
      grouping: TransactionReportGrouping.tag,
      loadOverTime: false,
      invert: false,
      type: ReportType.bar,
    )..filterObj = ReportTransactionFilter(currencyCodes: ['USD']),
  );
  final netWorth = await _saveItem(
    Report(
      name: 'Net Worth',
      loadOverTime: false,
      type: ReportType.text,
      reportText: 'Net Worth',
    )..filterObj = ReportTransactionFilter(currencyCodes: ['USD']),
  );
  final monthlyCashflow = await _saveItem(
    Report(
      name: 'Cashflow',
      loadOverTime: false,
      type: ReportType.text,
      relativeRange: ReportTimeRange.current,
    )..filterObj = ReportTransactionFilter(currencyCodes: ['USD']),
  );

  final reportGroupLayout =
      ReportGroupLayout()
        ..rows = [
          ReportGroupRow()..reports = [netWorth, monthlyCashflow],
          ReportGroupRow()..reports = [txReport],
          ReportGroupRow()..reports = [accountReport],
          ReportGroupRow()..reports = [merchantReport, merchantPieReport],
          ReportGroupRow()..reports = [budgetReport, tagReport],
        ];
  reportGroup.layouts.add(reportGroupLayout);

  reportGroup = await _saveItem(reportGroup);

  final now = DateTime.now();
  final year = now.year;
  final month = now.month;

  // Transfers
  await _generateTransfer(
    checking,
    creditCard,
    'Credit Card Payment',
    200000,
    DateTime(year, month, 15),
  );
  await _generateTransfer(
    checking,
    saving,
    'Saving Transfer',
    25000,
    DateTime(year, month, 15),
  );

  // Checking Account
  await _generateSimpleTx(
    checking,
    'Initial Balance',
    100000,
    DateTime(year, month - 1, 1),
    null,
    null,
  );
  await _generateSimpleTx(
    checking,
    'Paycheck',
    150000,
    DateTime(year, month, 1),
    'Uber',
    [(null, 150000, null, '💵 Income')],
  );
  await _generateSimpleTx(
    checking,
    'Rent',
    -75000,
    DateTime(year, month, 2),
    'Landlord',
    [(null, -75000, rentCat, '🏠 Home')],
  );
  await _generateSimpleTx(
    checking,
    'Paycheck',
    150000,
    DateTime(year, month, 15),
    'Uber',
    [(null, 150000, null, '💵 Income')],
  );
  await _generateSimpleTx(
    checking,
    'Electric Bill',
    -15000,
    DateTime(year, month, 15),
    'Neighborhood Electric',
    [(null, -15000, billsCat, '🏠 Home')],
  );
  await _generateSimpleTx(
    checking,
    'Water Bill',
    -7000,
    DateTime(year, month, 15),
    'City Water',
    [(null, -7000, billsCat, '🏠 Home')],
  );
  await _generateSimpleTx(
    checking,
    'Cash',
    -5000,
    DateTime(year, month, 25),
    null,
    [(null, -5000, miscCat, null)],
  );

  // Credit Card
  await _generateSimpleTx(
    creditCard,
    'Initial Balance',
    -200000,
    DateTime(year, month - 1, 1),
    null,
    null,
  );
  await _generateSimpleTx(
    creditCard,
    'Bulk Groceries',
    -14456,
    DateTime(year, month, 5),
    'Costco',
    [(null, -14456, groceriesCat, '🥖 Food')],
  );
  await _generateSimpleTx(
    creditCard,
    'Groceries',
    -10589,
    DateTime(year, month, 15),
    'Trader Joe\'s',
    [(null, -10589, groceriesCat, '🥖 Food')],
  );
  await _generateSimpleTx(
    creditCard,
    'Restocking veggies and fruit',
    -6858,
    DateTime(year, month, 25),
    'Kroger',
    [(null, -6858, groceriesCat, '🥖 Food')],
  );
  await _generateSimpleTx(
    creditCard,
    'Dinner out',
    -2344,
    DateTime(year, month, 2),
    'Tijuana Tacos',
    [(null, -2344, restCat, '🥖 Food')],
  );
  await _generateSimpleTx(
    creditCard,
    'Movie Ticket',
    -800,
    DateTime(year, month, 10),
    'Regal Theatres',
    [(null, -800, entertainmentCat, null)],
  );
  await _generateSimpleTx(
    creditCard,
    'Movie Snacks',
    -1534,
    DateTime(year, month, 10),
    'Regal Theatres',
    [(null, -1534, shoppingCat, null)],
  );
  await _generateSimpleTx(
    creditCard,
    'Amazing Phone S50 Pro Plus',
    -50000,
    DateTime(year, month, 16),
    'Amazing Tech',
    [
      ('Phone', -45000, shoppingCat, '📱 Phone'),
      ('Warranty', -5000, miscCat, '📱 Phone'),
    ],
  );
  await _generateSimpleTx(
    creditCard,
    'Must have app bundle',
    -2000,
    DateTime(year, month, 16),
    'Amazing Tech',
    [(null, -2000, shoppingCat, '📱 Phone')],
  );
  await _generateSimpleTx(
    creditCard,
    'Dinner with Friends',
    -5000,
    DateTime(year, month, 27),
    'Steak House',
    [(null, -5000, restCat, '🥖 Food')],
  );
  await _generateSimpleTx(
    creditCard,
    'Out on the town',
    -7548,
    DateTime(year, month, 27),
    'Breakout Bar',
    [
      (null, -2421, entertainmentCat, '🍻 Bar'),
      (null, -5127, restCat, '🥖 Food'),
    ],
  );

  // Update Settings
  final settings =
      (await ReckonerDb.I.settingsDao.getUserSetting())
        ..transactionReportUuid = monthlyCashflow.uuid
        ..accountReportUuid = accountReport.uuid
        ..merchantReportUuid = merchantReport.uuid
        ..navList.insert(
          0,
          NavigationEntry(
            icon: Icons.pie_chart,
            type: NavigationEntryType.report,
            uuid: reportGroup.uuid,
            name: reportGroup.name,
          ),
        )
        ..navList.insert(
          1,
          NavigationEntry(
            icon: Icons.sort,
            type: NavigationEntryType.category,
            uuid: budgetGroup,
            name: 'Budgets',
          ),
        )
        ..currency = Currency(code: 'USD');

  await _saveItem(settings);
}

Future<void> generateExampleData() async {
  await ReckonerDb.I.transaction(() async {
    await _genData();
  });
  await ReckonerDb.I.categoryDao.generateDates();
  await getPreferences().setBool(generateData, true);
}
