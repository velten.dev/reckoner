import 'dart:js_interop' show StringToJSString;

import 'package:drift/drift.dart';
import 'package:drift/wasm.dart';
import 'package:sqlite3/wasm.dart';
import 'package:web/web.dart' show console;

import '../../config/const.dart';
import '../../config/environment_const.dart';
import '../reckoner_db.dart';

const _unsafeImplementation = [
  WasmStorageImplementation.inMemory,
  WasmStorageImplementation.unsafeIndexedDb,
];

QueryExecutor getExecutor() {
  if (isDemo) {
    return LazyDatabase(() async {
      final sqlite3 = await WasmSqlite3.loadFromUrl(Uri.parse('/sqlite3.wasm'));
      sqlite3.registerVirtualFileSystem(
        InMemoryFileSystem(),
        makeDefault: true,
      );
      return WasmDatabase.inMemory(sqlite3, logStatements: logStatements);
    });
  }
  return LazyDatabase(() async {
    return DatabaseConnection.delayed(
      Future(() async {
        final result = await WasmDatabase.open(
          databaseName: dbName,
          sqlite3Uri: Uri.parse('/sqlite3.wasm'),
          driftWorkerUri: Uri.parse('/worker.dart.js'),
        );
        return result.resolvedExecutor;
      }),
    );
  });
}

Future<String> checkDatabase() async {
  if (isDemo) {
    console.warn('Using memory database. Data will not persist.'.toJS);
    return '';
  }

  final result = await WasmDatabase.open(
    databaseName: dbName, // prefer to only use valid identifiers here
    sqlite3Uri: Uri.parse('/sqlite3.wasm'),
    driftWorkerUri: Uri.parse('/worker.dart.js'),
  );
  final missingFeatures = result.missingFeatures;
  final chosenImplementation = result.chosenImplementation;
  await result.resolvedExecutor.close();

  if (_unsafeImplementation.contains(chosenImplementation)) {
    // Depending how central local persistence is to your app, you may want
    // to show a warning to the user if only unreliable implementations
    // are available.
    return 'Using $chosenImplementation due to missing browser features: $missingFeatures';
  } else {
    console.warn(
      'Using $chosenImplementation for database; missing browser features: $missingFeatures'
          .toJS,
    );
  }

  return '';
}

Future<void> clearDatabase() async {
  final db = ReckonerDb.I;
  await Future.wait(db.allTables.map((final t) => db.delete(t).go()));
  await db.customStatement('VACUUM');
}

Future<bool> dbBackup(final String path, final String encryptionKey) =>
    throw UnimplementedError();

Future<bool> dbRestore([final String encryptionKey = '']) =>
    throw UnimplementedError();

Future<void> dbCronBackup() => Future.value();

Future<void> changeDbEncryption(final String password) => Future.value();
