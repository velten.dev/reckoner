import 'dart:ffi';
import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:sqlcipher_flutter_libs/sqlcipher_flutter_libs.dart';
import 'package:sqlite3/open.dart';

import '../../config/const.dart';
import '../../config/environment_const.dart';
import '../../config/platform.dart';
import '../../model/secure_storage.dart';
import '../../service/logger.dart';
import '../reckoner_db.dart';

Future<String> checkDatabase() => Future.value('');

QueryExecutor getExecutor() {
  if (isDemo) {
    _setLibraryOverrides();
    return NativeDatabase.memory(logStatements: logStatements);
  }

  return LazyDatabase(() async {
    final (file, setup) = await _dbSetup();
    return NativeDatabase.createInBackground(
      file,
      setup: setup,
      isolateSetup: _setLibraryOverrides,
    );
  });
}

Future<(File, DatabaseSetup?)> _dbSetup() async {
  final password = await SecureStorage.dbPassword;
  final dataDir = await getApplicationSupportDirectory();
  final dbFile = File(join(dataDir.path, dbName));
  DatabaseSetup? setup;
  if (password.isNotEmpty) {
    setup = (final db) => db.execute("PRAGMA key = '$password';");
  }
  return (dbFile, setup);
}

void _setLibraryOverrides() {
  open.overrideFor(OperatingSystem.android, openCipherOnAndroid);
  if (!kDebugMode && Platform.isLinux && isAppImage) {
    final exePath = (Platform.resolvedExecutable.split('/')
      ..removeLast()).join('/');
    open.overrideFor(
      OperatingSystem.linux,
      () => DynamicLibrary.open('$exePath/lib/libsqlite3.so'),
    );
  }
}

Future<void> clearDatabase() async {
  final dataDir = await getApplicationSupportDirectory();
  final dbFile = File(join(dataDir.path, dbName));
  await dbFile.delete();
}

Future<bool> dbBackup(final String path, final String encryptionKey) async {
  if (isDemo) return false;
  final file = File(path);
  final db = ReckonerDb.I;
  bool detach = false;
  bool success = false;
  try {
    // Make sure the directory of the target file exists
    await file.parent.create(recursive: true);

    // Override an existing backup, sqlite expects the target file to be empty
    if (await file.exists()) await file.delete();

    await db.customStatement('ATTACH ? as backup_db KEY ?', [
      path,
      encryptionKey,
    ]);
    detach = true;
    await db.customStatement("SELECT sqlcipher_export('backup_db');");

    Future(() async {
      final backupSettings = await SecureStorage.backupSettings;
      backupSettings.lastBackupDate = DateTime.now();
      SecureStorage.backupSettings = Future.value(backupSettings);
    }).ignore();

    success = true;
  } catch (err, stack) {
    logError('Reckoner', err, stack);
    success = false;
  } finally {
    if (detach) await db.customStatement('DETACH backup_db');
  }
  return success;
}

Future<bool> dbRestore([final String encryptionKey = '']) async {
  final backupSettings = await SecureStorage.backupSettings;
  final result = await FilePicker.platform.pickFiles(
    initialDirectory: backupSettings.path,
    allowedExtensions: isMobile ? null : ['db', 'db3', 'sqlite', 'sqlite3'],
    type: isMobile ? FileType.any : FileType.custom,
    allowMultiple: false,
    lockParentWindow: true,
    withData: false,
  );
  if (result == null) return false;
  final file = File(result.files.first.path!);
  if (await file.exists()) {
    try {
      // Need to close the database first to prevent data cache from throwing
      // errors due to differing schema versions
      await ReckonerDb.dispose();
      final db = ReckonerDb.I;

      await db.customStatement('ATTACH ? as restore_db KEY ?', [
        file.path,
        encryptionKey,
      ]);

      // Check version
      final result =
          await db.customSelect('PRAGMA restore_db.user_version').getSingle();
      int version = result.read('user_version');
      if (version == 0) {
        version = 1;
      } else if (version > db.schemaVersion) {
        logError(
          'Reckoner',
          'Schema version mismatch. Maximum supported version: ${db.schemaVersion}, import database version: $version',
          null,
        );
        await db.customStatement('DETACH restore_db');
        return false;
      }

      //https://stackoverflow.com/questions/21547616/how-do-i-completely-clear-a-sqlite3-database-without-deleting-the-database-file
      await db.customStatement('PRAGMA writable_schema = 1');
      await db.customStatement('DELETE FROM sqlite_master');
      await db.customStatement('PRAGMA writable_schema = 0');
      await db.customStatement('VACUUM');
      await db.customStatement('PRAGMA integrity_check');
      await db.customStatement("SELECT sqlcipher_export('main', 'restore_db')");

      // Set the proper version so that migrations are run
      await db.customStatement('PRAGMA user_version = $version');

      await db.customStatement('DETACH restore_db');
      return true;
    } catch (_) {}
  }
  return false;
}

Future<void> dbCronBackup() async {
  final now = DateTime.now();
  final backupSettings = await SecureStorage.backupSettings;

  if (backupSettings.cronJob) {
    if (backupSettings.appendDate) {
      final files =
          Directory(backupSettings.path)
              .listSync()
              .where(
                (final element) =>
                    element.path.contains(backupSettings.filename),
              )
              .toList();
      if (files.length > backupSettings.backupCount) {
        files.sort((final a, final b) => b.path.compareTo(a.path));
        await Future.wait(
          files
              .sublist(backupSettings.backupCount)
              .map((final f) => f.delete()),
        );
      }
    }

    final lastBackup = backupSettings.lastBackupDate;

    final difference = switch (backupSettings.cadence) {
      BackupCadence.daily => const Duration(days: 1),
      BackupCadence.weekly => const Duration(days: 7),
      BackupCadence.monthly => const Duration(days: 30),
    };

    if (now.difference(lastBackup).compareTo(difference) > 0) {
      final backupSettings = await SecureStorage.backupSettings;
      await dbBackup(
        backupSettings.filePath,
        await backupSettings.encryptionSettings.encryptionKey,
      );
    }
  }
}

Future<void> changeDbEncryption(final String password) async {
  await ReckonerDb.I.customStatement("PRAGMA rekey = '$password';");
  SecureStorage.dbPassword = Future.value(password);
}
