import 'account_dao.dart';
import 'balance_dao.dart';
import 'category_dao.dart';
import 'common_dao.dart';
import 'currency_dao.dart';
import 'device_dao.dart';
import 'merchant_dao.dart';
import 'report_dao.dart';
import 'settings_dao.dart';
import 'transaction_dao.dart';
import 'transaction_merchant_dao.dart';
import 'transaction_tag_dao.dart';

export 'account_dao.dart';
export 'balance_dao.dart';
export 'category_dao.dart';
export 'common_dao.dart';
export 'currency_dao.dart';
export 'device_dao.dart';
export 'merchant_dao.dart';
export 'report_dao.dart';
export 'settings_dao.dart';
export 'transaction_dao.dart';
export 'transaction_merchant_dao.dart';
export 'transaction_tag_dao.dart';

const daoList = [
  AccountDao,
  BalanceDao,
  CategoryDao,
  CommonDao,
  CurrencyDao,
  DeviceDao,
  MerchantDao,
  ReportDao,
  SettingsDao,
  TransactionDao,
  TransactionMerchantDao,
  TransactionTagDao,
];
