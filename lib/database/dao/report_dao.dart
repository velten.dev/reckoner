import 'package:collection/collection.dart';
import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../model/currency.dart';
import '../../../model/report.dart';
import '../../../model/tz_date_time.dart';
import '../../../view/model/report_transactions.dart';
import '../../../view/model/transaction_type.dart';
import '../reckoner_db.dart';
import '../tables/account_tables.dart';
import '../tables/category_tables.dart';
import '../tables/currency_tables.dart';
import '../tables/report_tables.dart';
import '../tables/setting_tables.dart';
import '../tables/transaction_tables.dart';

part 'report_dao.g.dart';

@DriftAccessor(
  tables: [
    Accounts,
    Categories,
    CategoryGroups,
    Currencies,
    Organizations,
    Reports,
    ReportGroups,
    ReportGroupLayouts,
    ReportGroupRows,
    Transactions,
    TransactionCategories,
    TransactionSplits,
    TransactionTags,
    TransactionTransfers,
    UserSettings,
  ],
)
class ReportDao extends DatabaseAccessor<ReckonerDb> with _$ReportDaoMixin {
  ReportDao(super.db);

  Stream<ReportGroupMap> loadReportGroups() {
    final query = select(reportGroups)
      ..where((final tbl) => tbl.deleteDate.isNull());

    return query.watch().switchMap((final groups) {
      return select(reportGroupLayouts).watch().switchMap((final layouts) {
        return select(reportGroupRows).watch().switchMap((final groupRows) {
          return (select(reports)..where(
            (final tbl) => tbl.deleteDate.isNull(),
          )).watch().map((final reports) {
            return _combineData(
              groups,
              layouts,
              groupRows,
              reports,
            ).asMap().map((final _, final g) => MapEntry(g.uuid, g));
          });
        });
      });
    });
  }

  Future<List<ReportGroup>> getSyncReportGroups([
    final DateTime? syncDate,
  ]) async {
    final query = select(reportGroups);
    if (syncDate != null) {
      query.where((final tbl) => tbl.updateDate.isBiggerThanValue(syncDate));
    }

    return _combineData(
      await query.get(),
      await select(reportGroupLayouts).get(),
      await select(reportGroupRows).get(),
      [],
    );
  }

  List<ReportGroup> _combineData(
    final List<ReportGroup> groups,
    final List<ReportGroupLayout> layouts,
    final List<ReportGroupRow> groupRows,
    final List<Report> reports,
  ) {
    final rptMap = reports.asMap().map(
      (final _, final rpt) => MapEntry(rpt.uuid, rpt),
    );

    final Map<(String, int), List<ReportGroupRow>> rowMap = groupRows.fold({}, (
      final map,
      final row,
    ) {
      row.reports = [
        if (rptMap.containsKey(row.primaryReport)) rptMap[row.primaryReport]!,
        if (rptMap.containsKey(row.secondaryReport))
          rptMap[row.secondaryReport]!,
        if (rptMap.containsKey(row.tertiaryReport)) rptMap[row.tertiaryReport]!,
      ];
      map.putIfAbsent((row.uuid, row.layoutOrder), () => []).add(row);
      return map;
    });

    final Map<String, List<ReportGroupLayout>> layoutMap = layouts.fold({}, (
      final map,
      final layout,
    ) {
      map
          .putIfAbsent(layout.uuid, () => [])
          .add(layout..rows = rowMap[(layout.uuid, layout.order)] ?? []);
      return map;
    });

    return groups
        .map((final g) => g..layouts = layoutMap[g.uuid] ?? [])
        .toList();
  }

  Expression<bool> _txQueryParamsByGroup({
    required final List<Join<HasResultSet, dynamic>> joins,
    required final List<Expression> addColumns,
    required final List<Expression> groupBy,
    required final Report report,
    final DateTimeRange? range,
    final DateTime? endDate,
  }) {
    Expression<bool> whereExpression = transactions.deleteDate.isNull();
    void addWhereExp<T extends Object>(
      final GeneratedColumn<T> column,
      final List<T> filter,
    ) {
      final tempExpression = column.isIn(filter);
      whereExpression = whereExpression & tempExpression;
    }

    addColumns.addAll([
      transactions.currencyCode,
      currencies.decimalDigits,
      currencies.symbol,
    ]);

    groupBy.add(transactions.currencyCode);

    joins.addAll([
      leftOuterJoin(
        transactions,
        transactions.uuid.equalsExp(transactionSplits.uuid),
      ),
      leftOuterJoin(
        currencies,
        currencies.code.equalsExp(transactions.currencyCode),
      ),
      leftOuterJoin(
        accounts,
        accounts.uuid.equalsExp(transactions.accountUuid),
      ),
      leftOuterJoin(
        transactionCategories,
        transactionCategories.splitId.equalsExp(transactionSplits.id) &
            transactionCategories.uuid.equalsExp(transactionSplits.uuid),
      ),
      leftOuterJoin(
        categories,
        categories.uuid.equalsExp(transactionCategories.categoryUuid),
      ),
      leftOuterJoin(
        categoryGroups,
        categoryGroups.uuid.equalsExp(categories.groupUuid),
      ),
      leftOuterJoin(
        transactionTags,
        transactionTags.splitId.equalsExp(transactionSplits.id) &
            transactionTags.uuid.equalsExp(transactionSplits.uuid),
      ),
    ]);

    switch (report.grouping) {
      case TransactionReportGrouping.account:
        groupBy.add(transactions.accountUuid);
        joins.addAll([
          leftOuterJoin(
            organizations,
            organizations.uuid.equalsExp(accounts.orgUuid),
          ),
        ]);
        addColumns.addAll([
          accounts.uuid,
          accounts.name,
          organizations.uuid,
          organizations.name,
        ]);
        break;
      case TransactionReportGrouping.category:
        groupBy.add(transactionCategories.categoryUuid);
        addColumns.addAll([categories.uuid, categories.name]);
        break;
      case TransactionReportGrouping.currency:
        break;
      case TransactionReportGrouping.merchant:
        groupBy.add(transactions.merchantName);
        addColumns.addAll([transactions.merchantName]);
        break;
      case TransactionReportGrouping.tag:
        groupBy.add(transactionTags.tag);
        addColumns.addAll([transactionTags.tag]);
        break;
    }

    if (range != null) {
      whereExpression =
          whereExpression &
          transactions.date.isBetweenValues(range.start, range.end);
    } else if (endDate != null) {
      whereExpression =
          whereExpression & transactions.date.isSmallerThanValue(endDate);
    }

    if (report.filterObj.type == TransactionTypeFilter.expense) {
      whereExpression =
          whereExpression & transactions.amount.isSmallerThanValue(0);
    } else if (report.filterObj.type == TransactionTypeFilter.income) {
      whereExpression =
          whereExpression & transactions.amount.isBiggerOrEqualValue(0);
    }

    final filters = report.filterObj;

    if (filters.hasFilter()) {
      if (filters.accountIds.isNotEmpty) {
        whereExpression =
            whereExpression & transactions.accountUuid.isIn(filters.accountIds);
      }
      if (filters.categoryGroupIds.isNotEmpty) {
        addWhereExp(categoryGroups.uuid, filters.categoryGroupIds);
      }
      if (filters.categoryIds.isNotEmpty) {
        addWhereExp(transactionCategories.categoryUuid, filters.categoryIds);
      }
      if (filters.currencyCodes.isNotEmpty) {
        addWhereExp(transactions.currencyCode, filters.currencyCodes);
      }
      if (filters.merchants.isNotEmpty) {
        addWhereExp(transactions.merchantName, filters.merchants);
      }
      if (filters.tags.isNotEmpty) {
        addWhereExp(transactionTags.tag, filters.tags);
      }
    }

    return whereExpression;
  }

  TransactionCurrencyGroup? _rowToGroup({
    required final TransactionReportGrouping grouping,
    required final TypedResult row,
  }) {
    final curCode = row.read(transactions.currencyCode)!;
    final defCur = defaultCurrencies[curCode];
    final curDD =
        row.read(currencies.decimalDigits) ?? defCur?.decimalDigits ?? 2;
    final curSym = row.read(currencies.symbol) ?? defCur?.symbol ?? '\$';
    try {
      return switch (grouping) {
        TransactionReportGrouping.account => TransactionAccountGroup(
          currencyCode: curCode,
          currencyDigits: curDD,
          currencySymbol: curSym,
          accountId: row.read(accounts.uuid)!,
          accountName: row.read(accounts.name)!,
          organizationId: row.read(organizations.uuid)!,
          organizationName: row.read(organizations.name)!,
        ),
        TransactionReportGrouping.category => TransactionCategoryGroup(
          currencyCode: curCode,
          currencyDigits: curDD,
          currencySymbol: curSym,
          categoryId: row.read(categories.uuid)!,
          categoryName: row.read(categories.name)!,
        ),
        TransactionReportGrouping.currency => TransactionCurrencyGroup(
          currencyCode: curCode,
          currencyDigits: curDD,
          currencySymbol: curSym,
        ),
        TransactionReportGrouping.merchant => TransactionMerchantGroup(
          currencyCode: curCode,
          currencyDigits: curDD,
          currencySymbol: curSym,
          merchantName: row.read(transactions.merchantName)!,
        ),
        TransactionReportGrouping.tag => TransactionTagGroup(
          currencyCode: curCode,
          currencyDigits: curDD,
          currencySymbol: curSym,
          tagName: row.read(transactionTags.tag)!,
        ),
      };
    } catch (err) {
      return null;
    }
  }

  Stream<TransactionReportChartData> _loadSums({
    required final Report report,
    final DateTimeRange? range,
    final DateTime? endDate,
  }) {
    final txSplitSum = transactionSplits.amount.sum();

    final List<Join<HasResultSet, dynamic>> joins = [];
    final List<Expression> groupBy = [];
    final List<Expression> addColumns = [txSplitSum];
    final grouping = report.grouping;
    final invert = report.invert;

    final whereStatement = _txQueryParamsByGroup(
      joins: joins,
      addColumns: addColumns,
      groupBy: groupBy,
      report: report,
      range: range,
      endDate: endDate,
    );

    final query =
        selectOnly(transactionSplits).join(joins)
          ..groupBy(groupBy)
          ..addColumns(addColumns)
          ..where(whereStatement);

    return query.watch().map((final rows) {
      final chartData = TransactionReportChartData(report);
      for (final row in rows) {
        final group = _rowToGroup(grouping: grouping, row: row);
        if (group == null) continue;
        final int amount = (invert ? -1 : 1) * row.read(txSplitSum)!;
        chartData.add(TransactionReportData(amount: amount, group: group));
      }
      return chartData;
    });
  }

  Stream<TransactionReportChartData> _loadOverTimeTotals(
    final Report report,
    final DateTimeRange? range,
  ) {
    final txSplitSum = transactionSplits.amount.sum();
    final List<Join<HasResultSet, dynamic>> joins = [];
    final List<Expression> groupBy = [transactions.date];
    final List<Expression> addColumns = [txSplitSum, transactions.date];

    final whereStatement = _txQueryParamsByGroup(
      joins: joins,
      addColumns: addColumns,
      groupBy: groupBy,
      report: report,
      range: range,
    );

    final query =
        selectOnly(transactionSplits).join(joins)
          ..groupBy(groupBy)
          ..addColumns(addColumns)
          ..where(whereStatement);

    final grouping = report.grouping;
    final invert = report.invert;

    if (range == null) {
      return query.watch().map((final rows) {
        final otTransform = TransactionReportOverTime(report: report);
        for (final row in rows) {
          final group = _rowToGroup(grouping: grouping, row: row);
          if (group == null) continue;
          otTransform.add(
            group: group,
            date: row.read(transactions.date)!,
            amount: (invert ? -1 : 1) * row.read(txSplitSum)!,
          );
        }
        return otTransform.toChartData();
      });
    }

    return _loadSums(report: report, endDate: range.start).switchMap((
      final totals,
    ) {
      return query.watch().map((final rows) {
        final otTransform = TransactionReportOverTime(
          report: report,
          range: TZDateTimeRange.from(range),
          totals: totals.data,
        );
        for (final row in rows) {
          final group = _rowToGroup(grouping: grouping, row: row);
          if (group == null) continue;
          otTransform.add(
            group: group,
            date: row.read(transactions.date)!,
            amount: (invert ? -1 : 1) * row.read(txSplitSum)!,
          );
        }
        return otTransform.toChartData();
      });
    });
  }

  Stream<TransactionReportChartData> loadTxTotals(
    final Report report,
    final TZDateTimeRange? loaderRange,
  ) {
    DateTimeRange? range = report.range?.native;
    if (report.loadOverTime ||
        report.relativeRange == ReportTimeRange.current) {
      range ??= loaderRange?.native;
    }

    if (report.loadOverTime) {
      return _loadOverTimeTotals(report, range);
    }
    return _loadSums(report: report, range: range, endDate: report.endDate);
  }

  Future<int> deleteReport(final Report report) async {
    bool settingsUpdated = false;
    final deleteDate = DateTime.now();
    report
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;
    final deleteDateValue = Value(deleteDate);

    await update(reports).replace(report);

    var cnt = 0;

    final catGroupQuery = update(categoryGroups)
      ..where((final tbl) => tbl.reportUuid.equals(report.uuid));
    cnt += await catGroupQuery.write(
      CategoryGroupsCompanion(
        reportUuid: const Value(null),
        updateDate: deleteDateValue,
      ),
    );

    final settings = await db.settingsDao.getUserSetting();

    if (settings.accountReportUuid == report.uuid) {
      settings
        ..accountReportUuid = null
        ..updateDate = deleteDate;
      settingsUpdated = true;
    }

    if (settings.transactionReportUuid == report.uuid) {
      settings
        ..transactionReportUuid = null
        ..updateDate = deleteDate;
      settingsUpdated = true;
    }

    if (settingsUpdated) {
      await update(userSettings).replace(settings);
    }

    return cnt;
  }

  Future<ReportGroup> saveDbReportGroup(
    final ReportGroup group,
    final bool update,
  ) async {
    await db.commonDao.deleteChildDbByUuid(db.reportGroupLayouts, group.uuid);
    await db.commonDao.deleteChildDbByUuid(db.reportGroupRows, group.uuid);
    final newGroup = await db.commonDao.saveDbItem(group);
    int order = 0;
    for (final layout in group.layouts) {
      final rows = layout.rows.where((final r) => r.hasData).toList();
      layout
        ..uuid = newGroup.uuid
        ..order = order++;
      final newLayout = await db.commonDao.saveDbItem(layout);
      if (rows.isNotEmpty) {
        Iterable<ReportGroupRow> rowsToSave = rows;
        rowsToSave = rows.mapIndexed((final index, final row) {
          if (row.reports.isNotEmpty) {
            row
              ..primaryReport = row.reports.first.uuid
              ..secondaryReport =
                  row.reports.length > 1 ? row.reports[1].uuid : null
              ..tertiaryReport =
                  row.reports.length > 2 ? row.reports[2].uuid : null;
          }
          return row
            ..order = index
            ..layoutOrder = newLayout.order
            ..uuid = newGroup.uuid;
        });
        newLayout.rows = await db.commonDao.saveChildList(rowsToSave);
      }
      newGroup.layouts.add(newLayout);
    }

    if (update) await db.settingsDao.handleUserSettingUpdate();

    return newGroup;
  }

  Future<DateTime?> saveSyncReportList(
    final ReckonerDb db,
    final List<Report> externalReports,
    final bool firstSync,
  ) async {
    final dbReports = await db.commonDao.getDbItems<Report>(true);
    DateTime? latestDate;
    for (final extReport in externalReports) {
      final intReport = dbReports.firstWhereOrNull(
        (final rpt) => rpt == extReport,
      );
      if (intReport != null &&
          intReport.updateDate.isAfter(extReport.updateDate) &&
          !firstSync) {
        continue;
      }
      final newItem = await db.commonDao.saveSyncItem<Report>(
        extReport,
        firstSync,
      );
      if (newItem != null) {
        if (latestDate == null || newItem.updateDate.isAfter(latestDate)) {
          latestDate = newItem.updateDate;
        }
        //remove duplicate
        if (intReport != null && intReport.uuid != extReport.uuid) {
          await db.commonDao.hardDeleteDbItem(intReport);
        }
      }
    }
    return latestDate;
  }

  Future<int> softDeleteReportGroup(final ReportGroup group) async {
    group
      ..deleteDate = DateTime.now()
      ..updateDate = DateTime.now();
    final cnt = await update(reportGroups).write(group);
    await db.settingsDao.handleUserSettingUpdate();
    return cnt;
  }
}
