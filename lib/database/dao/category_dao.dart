import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../model/category.dart';
import '../../../model/tz_date_time.dart';
import '../reckoner_db.dart';
import '../tables/category_tables.dart';
import '../tables/currency_tables.dart';
import '../tables/setting_tables.dart';
import '../tables/transaction_tables.dart';

part 'category_dao.g.dart';

@DriftAccessor(
  tables: [
    Categories,
    CategoryAlerts,
    CategoryGroups,
    CategoryLimits,
    CategoryBudgets,
    Currencies,
    Transactions,
    TransactionSplits,
    TransactionCategories,
    UserSettings,
  ],
)
class CategoryDao extends DatabaseAccessor<ReckonerDb> with _$CategoryDaoMixin {
  CategoryDao(super.db);

  Future<Map<String, int>> _getRolloverAmountMap(
    final Category category,
    final TZDateTimeRange range,
  ) async {
    final List<String> allUuids = [];
    _categoryToAllUuids(category, allUuids);
    final dates =
        await (select(categoryBudgets)..where(
          (final tbl) =>
              tbl.uuid.equals(category.uuid) &
              tbl.startDate.equals(range.start) &
              tbl.endDate.equals(range.end),
        )).get();
    if (dates.isEmpty) return {};
    final dateMap = dates.asMap().map(
      (final key, final value) => MapEntry(value.currencyCode, value),
    );

    final txQuery =
        selectOnly(transactionCategories).join([
            leftOuterJoin(
              transactionSplits,
              transactionSplits.uuid.equalsExp(transactionCategories.uuid) &
                  transactionSplits.id.equalsExp(transactionCategories.splitId),
            ),
            leftOuterJoin(
              transactions,
              transactions.uuid.equalsExp(transactionCategories.uuid),
            ),
          ])
          ..where(
            transactionCategories.categoryUuid.isIn(allUuids) &
                transactions.date.isBetweenValues(range.start, range.end),
          )
          ..groupBy([transactions.currencyCode])
          ..addColumns([
            transactions.currencyCode,
            transactionSplits.amount.sum(),
          ]);
    final amntMap = (await txQuery.get()).asMap().map(
      (final _, final row) => MapEntry(
        row.read(transactions.currencyCode)!,
        row.read(transactionSplits.amount.sum())!,
      ),
    );

    final Map<String, int> rolloverMap = {};
    for (final code in dateMap.keys) {
      rolloverMap[code] = (dateMap[code]?.amount ?? 0) - (amntMap[code] ?? 0);
    }
    return rolloverMap;
  }

  void _categoryToAllUuids(final Category cat, final List<String> ids) {
    ids.add(cat.uuid);
    for (final child in cat.children) {
      _categoryToAllUuids(child, ids);
    }
  }

  Stream<List<CategoryGroup>> loadCategoryGroups() {
    final catGrpQuery = select(categoryGroups)
      ..where((final tbl) => tbl.deleteDate.isNull());
    final catQuery = select(categories)
      ..where((final tbl) => tbl.deleteDate.isNull());

    final Stream<List<Object>> stream = catGrpQuery.watch();

    return stream
        .combineLatestAll([
          catQuery.watch(),
          select(categoryAlerts).watch(),
          _limitQuery().watch(),
        ])
        .map((final event) {
          final groups = event[0] as List<CategoryGroup>;
          final categories = event[1] as List<Category>;
          final alerts = event[2] as List<CategoryAlert>;
          final limits = event[3] as List<TypedResult>;

          final CategoryGroupMap groupMap = groups.asMap().map(
            (final key, final value) =>
                MapEntry(value.uuid, value..categories = []),
          );
          final alertMap = _mapAlerts(alerts);
          final limitMap = _mapLimits(limits);
          for (final cat in categories) {
            final group = groupMap[cat.groupUuid];

            group?.categories.add(cat);
            cat
              ..alerts = alertMap[cat.uuid] ?? []
              ..limits = limitMap[cat.uuid] ?? [];
            if (group != null) {
              cat.group = group;
            } else {
              debugPrint('No group');
            }
          }

          for (final group in groupMap.values) {
            group.categories = _mapToParents(group.categories);
          }

          return groups;
        });
  }

  Stream<CategoryBudgetDateMap> loadCategoryBudgets(
    final DateTimeRange? range,
  ) {
    final query = select(categoryBudgets).join([
      leftOuterJoin(
        currencies,
        currencies.code.equalsExp(categoryBudgets.currencyCode),
      ),
      leftOuterJoin(
        categories,
        categories.uuid.equalsExp(categoryBudgets.uuid),
        useColumns: false,
      ),
      leftOuterJoin(
        categoryGroups,
        categoryGroups.uuid.equalsExp(categories.groupUuid),
        useColumns: false,
      ),
    ])..addColumns([categoryGroups.uuid]);
    Expression<bool> expression = categoryBudgets.deleteDate.isNull();
    if (range != null) {
      expression =
          expression &
          categoryBudgets.startDate.isSmallerOrEqualValue(range.end) &
          categoryBudgets.endDate.isBiggerOrEqualValue(range.end);
    }
    query.where(expression);

    return query.watch().map((final rows) {
      final CategoryBudgetDateMap map = {};
      for (final row in rows) {
        final date = row.readTable(categoryBudgets);
        date.currency = db.currencyDao.readCurrency(row, date.currencyCode);
        final groupUuid = row.read(categoryGroups.uuid)!;
        date.category.group.uuid = groupUuid;

        final existingDate = map[groupUuid]?[date.uuid]?[date.currencyCode];
        if (existingDate == null ||
            existingDate.endDate.isBefore(date.endDate)) {
          map
                  .putIfAbsent(groupUuid, () => {})
                  .putIfAbsent(date.uuid, () => {})[date.currencyCode] =
              date;
        }
      }
      return map;
    });
  }

  Future<void> _updateBudgetAmounts(final Category category) async {
    final now = DateTime.now();
    final uuid = category.uuid;
    final newAmounts = category.limits.asMap().map(
      (final key, final value) => MapEntry(value.currencyCode, value.amount),
    );
    for (final map in newAmounts.entries) {
      final limitQuery =
          selectOnly(categoryLimits)
            ..where(
              categoryLimits.uuid.equals(uuid) &
                  categoryLimits.amount.equals(map.value).not() &
                  categoryLimits.currencyCode.equals(map.key),
            )
            ..addColumns([categoryLimits.amount]);
      final query = update(categoryBudgets)..where(
        (final tbl) =>
            tbl.deleteDate.isNull() &
            tbl.uuid.equals(uuid) &
            tbl.amount.isInQuery(limitQuery) &
            tbl.currencyCode.equals(map.key) &
            tbl.startDate.isSmallerOrEqualValue(now) &
            tbl.endDate.isBiggerOrEqualValue(now),
      );
      await query.write(
        CategoryBudgetsCompanion(
          amount: Value(map.value),
          updateDate: Value(now),
        ),
      );
    }
  }

  Future<List<CategoryGroup>> getSyncCategoryGroups([
    final DateTime? syncDate,
  ]) async {
    final catGroupQuery = select(categoryGroups);
    if (syncDate != null) {
      catGroupQuery.where(
        (final tbl) => tbl.updateDate.isBiggerThanValue(syncDate),
      );
    }
    final catGroups = await catGroupQuery.get();

    final catGroupMap = catGroups.asMap().map(
      (final key, final value) => MapEntry(value.uuid, value),
    );

    final query = select(categories)
      ..where((final tbl) => tbl.groupUuid.isIn(catGroupMap.keys));
    final catList = await query.get();
    final uuidList = catList.map((final e) => e.uuid);
    final alertMap = _mapAlerts(
      await (select(categoryAlerts)
        ..where((final tbl) => tbl.uuid.isIn(uuidList))).get(),
    );

    final limits =
        await (select(categoryLimits)
          ..where((final tbl) => tbl.uuid.isIn(uuidList))).get();

    final Map<String, List<CategoryLimit>> limitMap = {};

    for (final limit in limits) {
      limitMap.putIfAbsent(limit.uuid, () => []).add(limit);
    }

    for (final cat in catList) {
      cat
        ..alerts = alertMap[cat.uuid] ?? []
        ..limits = limitMap[cat.uuid] ?? [];
      catGroupMap[cat.groupUuid]?.categories.add(cat);
    }

    return catGroups;
  }

  List<Category> _mapToParents(final List<Category> allCategories) {
    final List<Category> parents = [];
    final List<Category> potentialChildren = [];

    for (final cat in allCategories) {
      if (cat.parentUuid == null) {
        parents.add(cat);
      } else {
        potentialChildren.add(cat);
      }
    }

    for (final cat in parents) {
      _mapChildren(cat, potentialChildren, null);
    }

    return parents;
  }

  Category _mapChildren(
    final Category parent,
    final List<Category> potentialChildren,
    final Category? progenitor,
  ) {
    final List<Category> children = [];
    final List<Category> otherChildren = [];

    for (final cat in potentialChildren) {
      if (cat.parentUuid == parent.uuid) {
        children.add(cat);
        cat.parent = parent;
      } else {
        otherChildren.add(cat);
      }
    }

    parent
      ..children =
          children
              .map((final e) => _mapChildren(e, otherChildren, parent))
              .toList()
      ..progenitor = progenitor;

    return parent;
  }

  Map<String, List<CategoryAlert>> _mapAlerts(
    final List<CategoryAlert> alerts,
  ) {
    final Map<String, List<CategoryAlert>> map = {};
    for (final warn in alerts) {
      map.putIfAbsent(warn.uuid, () => []).add(warn);
    }
    return map;
  }

  JoinedSelectStatement<HasResultSet, dynamic> _limitQuery([
    final Iterable<String>? uuids,
  ]) {
    final query = select(categoryLimits).join([
      leftOuterJoin(
        currencies,
        currencies.code.equalsExp(categoryLimits.currencyCode),
      ),
    ]);
    if (uuids != null) query.where(categoryLimits.uuid.isIn(uuids));
    return query;
  }

  Map<String, List<CategoryLimit>> _mapLimits(final List<TypedResult> rows) {
    final Map<String, List<CategoryLimit>> map = {};
    for (final row in rows) {
      final limit = row.readTable(categoryLimits);
      limit.currency = db.currencyDao.readCurrency(row, limit.currencyCode);
      map.putIfAbsent(limit.uuid, () => []).add(limit);
    }
    return map;
  }

  Future<Map<int, List<CategoryBudget>>> getSyncLimitDates(
    final DateTime? syncDate,
  ) async {
    List<CategoryBudget> catList;

    if (syncDate == null) {
      catList = await select(categoryBudgets).get();
    } else {
      final yearQuery =
          selectOnly(categoryBudgets)
            ..where(categoryBudgets.updateDate.isBiggerThanValue(syncDate))
            ..addColumns([categoryBudgets.startDate.year]);
      final monthQuery =
          selectOnly(categoryBudgets)
            ..where(categoryBudgets.updateDate.isBiggerThanValue(syncDate))
            ..addColumns([categoryBudgets.startDate.month]);

      catList =
          await (select(categoryBudgets)..where(
            (final tbl) =>
                tbl.startDate.year.isInQuery(yearQuery) &
                tbl.startDate.month.isInQuery(monthQuery),
          )).get();
    }

    final Map<int, List<CategoryBudget>> map = {};
    for (final cat in catList) {
      final utcDate = cat.startDate.toUtc();
      map.putIfAbsent(utcDate.year * 100 + utcDate.month, () => []).add(cat);
    }
    return map;
  }

  Future<int> deleteCategory(final Category category) async {
    final deleteDate = DateTime.now();
    category
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;
    final deleteDateValue = Value(deleteDate);

    final catDateQuery = update(categoryBudgets)
      ..where((final tbl) => tbl.uuid.equals(category.uuid));
    await catDateQuery.write(
      CategoryBudgetsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );

    await update(categories).replace(category);

    return transaction(() async {
      final txUuids =
          selectOnly(transactionCategories)
            ..addColumns([transactionCategories.uuid])
            ..where(transactionCategories.categoryUuid.equals(category.uuid));

      final txQuery = update(transactions)
        ..where((final tbl) => tbl.uuid.isInQuery(txUuids));

      await txQuery.write(TransactionsCompanion(updateDate: deleteDateValue));

      final txCatQuery = delete(transactionCategories)
        ..where((final tbl) => tbl.categoryUuid.equals(category.uuid));

      return txCatQuery.go();
    });
  }

  Future<int> deleteCategoryGroup(final CategoryGroup group) async {
    final deleteDate = DateTime.now();
    final deleteDateValue = Value(deleteDate);
    group
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;

    final catUuids =
        selectOnly(categories)
          ..addColumns([categories.uuid])
          ..where(categories.groupUuid.equals(group.uuid));

    final catDateQuery = update(categoryBudgets)
      ..where((final tbl) => tbl.uuid.isInQuery(catUuids));
    await catDateQuery.write(
      CategoryBudgetsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );

    final catQuery = update(categories)
      ..where((final tbl) => tbl.groupUuid.equals(group.uuid));
    await catQuery.write(
      CategoriesCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );

    await update(categoryGroups).replace(group);
    await db.settingsDao.handleUserSettingUpdate();

    return transaction(() async {
      final txUuids =
          selectOnly(transactionCategories)
            ..addColumns([transactionCategories.uuid])
            ..where(transactionCategories.categoryUuid.isInQuery(catUuids));

      final txQuery = update(transactions)
        ..where((final tbl) => tbl.uuid.isInQuery(txUuids));

      await txQuery.write(TransactionsCompanion(updateDate: deleteDateValue));

      final txCatQuery = delete(transactionCategories)
        ..where((final tbl) => tbl.categoryUuid.isInQuery(catUuids));

      return txCatQuery.go();
    });
  }

  Stream<CatBudgetAmounts> loadBudgetAmounts(
    final List<Category> cats,
    final TZDateTime? endDate,
  ) async* {
    await for (final _ in (select(transactions)..limit(1)).watch()) {
      yield await getBudgetAmounts(cats, endDate);
    }
  }

  Future<CatBudgetAmounts> getBudgetAmounts(
    final List<Category> cats,
    final TZDateTime? endDate,
  ) async {
    final Map<String, Map<String, int>> map = {};
    for (final cat in cats) {
      final range = cat.getRange(endDate);
      map[cat.uuid] = await getBudgetAmount(cat, range);
      map.addAll(await getBudgetAmounts(cat.children, endDate));
    }
    return map;
  }

  Future<BudgetAmounts> getBudgetAmount(
    final Category cat,
    final TZDateTimeRange range,
  ) async {
    final familyUuids = cat.familyUuids;
    final query =
        selectOnly(transactionSplits).join([
            leftOuterJoin(
              transactionCategories,
              transactionCategories.uuid.equalsExp(transactionSplits.uuid) &
                  transactionCategories.splitId.equalsExp(transactionSplits.id),
            ),
            leftOuterJoin(
              transactions,
              transactions.uuid.equalsExp(transactionSplits.uuid),
            ),
          ])
          ..where(
            transactions.date.isBetweenValues(range.start, range.end) &
                transactionCategories.categoryUuid.isIn(familyUuids),
          )
          ..groupBy([transactions.currencyCode])
          ..addColumns([
            transactions.currencyCode,
            transactionSplits.amount.sum(),
          ]);

    return (await query.get()).asMap().map(
      (final key, final value) => MapEntry(
        value.read(transactions.currencyCode)!,
        value.read(transactionSplits.amount.sum())!,
      ),
    );
  }

  Future<CategoryGroup> saveDbCategoryGroup(
    final CategoryGroup group,
    final bool update,
  ) async {
    final newGroup = await db.commonDao.saveDbItem(group);
    if (update) await db.settingsDao.handleUserSettingUpdate();

    return newGroup;
  }

  Future<Category> saveDbCategory(
    final Category category,
    final bool update,
  ) async {
    final uuid = category.uuid;
    if (update) {
      if (category.group.requireLimits) {
        await _deleteChangedLimits(category);
        await _updateBudgetAmounts(category);
      }
      final query = db.update(db.categoryGroups)
        ..whereSamePrimaryKey(category.group);
      await query.write(
        CategoryGroupsCompanion(updateDate: Value(DateTime.now())),
      );
    }
    await db.commonDao.deleteChildDbByUuid(db.categoryAlerts, uuid);
    await db.commonDao.deleteChildDbByUuid(db.categoryLimits, uuid);
    final newCategory = await db.commonDao.saveDbItem(category);
    db.commonDao.setIdItemList(category.alerts, newCategory.uuid);
    for (final limit in category.limits) {
      limit.uuid = newCategory.uuid;
    }
    newCategory
      ..alerts = await db.commonDao.saveChildList(category.alerts)
      ..limits = await db.commonDao.saveChildList(category.limits)
      ..parent = category.parent
      ..children = category.children
      ..progenitor = category.progenitor
      ..group = category.group;

    if (update && category.group.requireLimits) {
      await _generateCatLimitDates(
        newCategory.copy()..firstDateOffset = category.firstDateOffset,
        null,
      );
    }
    return newCategory;
  }

  Future<void> _deleteChangedLimits(final Category category) async {
    final uuid = category.uuid;
    final now = DateTime.now();
    final timeValue = Value(now);
    final prevCat =
        await (db.select(db.categories)
          ..whereSamePrimaryKey(category)).getSingleOrNull();
    if (prevCat == null) return;

    if (prevCat.isSpendLimit != category.isSpendLimit ||
        prevCat.datePeriodInterval != category.datePeriodInterval ||
        prevCat.datePeriod != category.datePeriod) {
      await (db.update(db.categoryBudgets)
        ..where((final tbl) => tbl.uuid.equals(uuid))).write(
        CategoryBudgetsCompanion(deleteDate: timeValue, updateDate: timeValue),
      );
      return;
    }

    final prevCurrencies =
        (await (db.select(db.categoryLimits)
              ..where((final tbl) => tbl.uuid.equals(category.uuid))).get())
            .map((final e) => e.currencyCode)
            .toSet();

    for (final limit in category.limits) {
      prevCurrencies.remove(limit.currencyCode);
    }

    if (prevCurrencies.isNotEmpty) {
      await (db.update(db.categoryBudgets)..where(
        (final tbl) =>
            tbl.uuid.equals(uuid) & tbl.currencyCode.isIn(prevCurrencies),
      )).write(
        CategoryBudgetsCompanion(deleteDate: timeValue, updateDate: timeValue),
      );
    }
  }

  Future<void> generateDates([final TZDateTime? date]) async {
    final groups = await db.categoryDao.loadCategoryGroups().first;

    for (final group in groups) {
      if (group.requireLimits) {
        for (final category in group.categories) {
          await _generateCatLimitDates(category, date);
        }
      }
    }
  }

  Future<void> _generateCatLimitDates(
    final Category category,
    final TZDateTime? date,
  ) async {
    final range = category.getRange(date);
    final prevRange = category.getRange(
      range.start.add(const Duration(days: -1)),
    );

    final dateQuery = select(categoryBudgets)..where(
      (final tbl) =>
          tbl.deleteDate.isNull() &
          tbl.uuid.equals(category.uuid) &
          tbl.startDate.equals(range.start) &
          tbl.endDate.equals(range.end),
    );
    final dateMap = (await dateQuery.get()).asMap().map(
      (final key, final value) => MapEntry(value.currencyCode, value),
    );

    Map<String, int> rolloverMap = {};
    if (category.limits.fold(
      false,
      (final previousValue, final element) =>
          previousValue || element.isRollover,
    )) {
      rolloverMap = await _getRolloverAmountMap(category, prevRange);
    }

    for (final limit in category.limits) {
      if (dateMap[limit.currencyCode] == null) {
        var amount = limit.amount;
        if (limit.isRollover) amount += rolloverMap[limit.currencyCode] ?? 0;
        await db.commonDao.saveDbItem(
          CategoryBudget(
            amount: amount,
            currencyCode: limit.currencyCode,
            startDate: range.start,
            endDate: range.end,
            uuid: category.uuid,
          ),
        );
      }
    }
    for (final child in category.children) {
      await _generateCatLimitDates(child, date);
    }
  }

  Future<void> balanceLimitDates(
    final CategoryBudget date,
    final CatBudgetAmounts amounts,
    final CategoryBudgetMap budgetMap,
  ) async {
    int spent(final CategoryBudget budget) =>
        amounts[budget.uuid]?[budget.currencyCode] ?? 0;

    int freeCash(final CategoryBudget budget) {
      if (budget.amount > 0) return 0;
      return budget.amount.abs() + spent(budget);
    }

    bool needsCash(final CategoryBudget budget) {
      if (budget.amount > 0) return false;
      return budget.amount > spent(budget);
    }

    final groupDates = budgetMap[date.category.groupUuid]!;
    if (!needsCash(date)) return;
    final familyUuids = date.category.familyUuids;

    final List<CategoryBudget> freeDates = [];
    for (final key in groupDates.keys) {
      if (familyUuids.contains(key)) continue;
      final dateMap = groupDates[key]!;
      final targetDate = dateMap[date.currencyCode];
      if (targetDate != null &&
          targetDate.category.isSpendLimit == date.category.isSpendLimit &&
          !needsCash(targetDate)) {
        freeDates.add(targetDate);
      }
    }

    if (freeDates.isEmpty) return;

    freeDates.sort(
      (final a, final b) => freeCash(b).abs().compareTo(freeCash(a).abs()),
    );

    await db.transaction(() async {
      for (final freeDate in freeDates) {
        final needed = spent(date) - date.amount;
        if (needed.abs() <= freeCash(freeDate).abs()) {
          date.amount += needed;
          freeDate.amount -= needed;
        } else {
          date.amount += freeCash(freeDate);
          freeDate.amount -= freeCash(freeDate);
        }
        freeDate.edited = true;
        await db.commonDao.saveDbItem(freeDate);
      }
      date.edited = true;
      return db.commonDao.saveDbItem(date);
    });
  }

  Future<void> setBudgetSyncId(
    final Iterable<String> ids,
    final String syncId,
  ) => (update(categoryBudgets)..where(
    (final tbl) => tbl.uuid.isIn(ids),
  )).write(CategoryBudgetsCompanion(syncId: Value(syncId)));
}
