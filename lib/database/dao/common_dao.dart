import 'package:drift/drift.dart';

import '../../model/account.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/device.dart';
import '../../model/merchant.dart';
import '../../model/model.dart';
import '../../model/report.dart';
import '../../model/settings.dart';
import '../../model/transaction.dart';
import '../reckoner_db.dart';
import '../tables/all_tables.dart';

part 'common_dao.g.dart';

@DriftAccessor(tables: tableList)
class CommonDao extends DatabaseAccessor<ReckonerDb> with _$CommonDaoMixin {
  CommonDao(super.db);

  TableInfo<Table, D> _dataTypeToTable<D>(final ReckonerDb db) {
    dynamic tableInfo;
    tableInfo = switch (D) {
      const (Account) => db.accounts,
      const (AccountIdentifier) => db.accountIdentifiers,
      const (AccountAlert) => db.accountAlerts,
      const (Category) => db.categories,
      const (CategoryAlert) => db.categoryAlerts,
      const (CategoryGroup) => db.categoryGroups,
      const (CategoryLimit) => db.categoryLimits,
      const (CategoryBudget) => db.categoryBudgets,
      const (Currency) => db.currencies,
      const (Device) => db.devices,
      const (Location) => db.locations,
      // ignore: deprecated_member_use_from_same_package
      const (Merchant) => db.merchants,
      const (Organization) => db.organizations,
      const (Report) => db.reports,
      const (ReportGroup) => db.reportGroups,
      const (ReportGroupLayout) => db.reportGroupLayouts,
      const (ReportGroupRow) => db.reportGroupRows,
      const (Transaction) => db.transactions,
      const (TransactionCategory) => db.transactionCategories,
      const (TransactionSplit) => db.transactionSplits,
      const (TransactionTag) => db.transactionTags,
      const (TransactionTransfer) => db.transactionTransfers,
      const (UserSetting) => db.userSettings,
      _ => throw UnsupportedError('Unsupported data type: $D'),
    };
    return tableInfo as TableInfo<Table, D>;
  }

  TableInfo<TrackedTable, D> _dataTypeToTrackedTable<D extends Tracked<D>>(
    final ReckonerDb db,
  ) {
    dynamic tableInfo;
    tableInfo = switch (D) {
      const (Account) => db.accounts,
      const (Category) => db.categories,
      const (CategoryGroup) => db.categoryGroups,
      const (CategoryBudget) => db.categoryBudgets,
      const (Currency) => db.currencies,
      const (Device) => db.devices,
      const (Organization) => db.organizations,
      // ignore: deprecated_member_use_from_same_package
      const (Merchant) => db.merchants,
      const (Report) => db.reports,
      const (ReportGroup) => db.reportGroups,
      const (Transaction) => db.transactions,
      const (UserSetting) => db.userSettings,
      _ => throw UnsupportedError('Unsupported data type: $D'),
    };
    return tableInfo as TableInfo<TrackedTable, D>;
  }

  Future<D> saveTrackedDbItem<D extends Tracked<D>>(
    final D item, [
    final bool update = true,
  ]) async {
    if (update) item.updateDate = DateTime.now();
    return switch (D) {
          const (Account) => await db.accountDao.saveDbAccount(
            item as Account,
            update,
          ),
          const (Category) => await db.categoryDao.saveDbCategory(
            item as Category,
            update,
          ),
          const (CategoryGroup) => await db.categoryDao.saveDbCategoryGroup(
            item as CategoryGroup,
            update,
          ),
          const (Transaction) => await db.transactionDao.saveDbTransaction(
            item as Transaction,
            update,
          ),
          const (ReportGroup) => await db.reportDao.saveDbReportGroup(
            item as ReportGroup,
            update,
          ),
          const (UserSetting) => await db.settingsDao.saveDbUserSettings(
            item as UserSetting,
          ),
          _ => await saveDbItem(item),
        }
        as D;
  }

  Future<D> saveDbItem<D extends Common<D>>(final D item) => db
      .into(_dataTypeToTable<D>(db))
      .insertReturning(item, onConflict: DoUpdate((final _) => item));

  Future<int> softDeleteDbItem<D extends Tracked<D>>(
    final D item,
  ) => switch (D) {
    const (Account) => db.accountDao.deleteAccount(item as Account),
    const (Category) => db.categoryDao.deleteCategory(item as Category),
    const (CategoryGroup) => db.categoryDao.deleteCategoryGroup(
      item as CategoryGroup,
    ),
    const (Currency) => db.currencyDao.deleteCurrency(item as Currency),
    const (Device) => db.deviceDao.deleteDevice(item as Device),
    const (Organization) => db.accountDao.deleteOrganization(
      item as Organization,
    ),
    const (Report) => db.reportDao.deleteReport(item as Report),
    const (ReportGroup) => db.reportDao.softDeleteReportGroup(
      item as ReportGroup,
    ),
    const (Transaction) => db.transactionDao.softDeleteTransaction(item.uuid),
    _ => _generalSoftDelete(item),
  };

  Future<int> _generalSoftDelete<D extends Tracked<D>>(final D item) {
    final table = _dataTypeToTable<D>(db);
    final date = DateTime.now();
    item
      ..deleteDate = date
      ..updateDate = date;
    final query = db.update(table)..whereSamePrimaryKey(item);
    return query.write(item);
  }

  Future<int> hardDeleteDbItem<D extends Common<D>>(final D item) async {
    final table = _dataTypeToTable<D>(db);
    return db.delete(table).delete(item);
  }

  Future<List<D>> getDbItems<D extends Tracked<D>>([
    final bool loadDeleted = false,
  ]) {
    try {
      final table = _dataTypeToTrackedTable<D>(db);
      final query = db.select(table);
      if (!loadDeleted) query.where((final tbl) => tbl.deleteDate.isNull());
      return db.select(table).get();
    } on UnsupportedError {
      return db.select(_dataTypeToTable<D>(db)).get();
    }
  }

  Stream<List<D>> loadAllDbItems<D extends Tracked<D>>([
    final bool loadDeleted = false,
  ]) {
    try {
      final table = _dataTypeToTrackedTable<D>(db);
      final query = db.select(table);
      if (!loadDeleted) query.where((final tbl) => tbl.deleteDate.isNull());
      return query.watch();
    } on UnsupportedError {
      return db.select(_dataTypeToTable<D>(db)).watch();
    }
  }

  Future<void> deleteChildDbByUuid<D extends Common<D>>(
    final TableInfo<UuidTable, D> tableInfo,
    final String uuid,
  ) async {
    await (db.delete(tableInfo)
      ..where((final tbl) => tbl.uuid.equals(uuid))).go();
  }

  Future<List<D>> saveChildList<D extends Common<D>>(
    final Iterable<D> itemList,
  ) async {
    final List<D> newItems = [];
    for (final item in itemList) {
      newItems.add(await saveDbItem(item));
    }
    return newItems;
  }

  void setIdItemList<T>(final List<IdItem<T>> list, final String uuid) {
    for (var i = 0; i < list.length; i++) {
      list[i]
        ..id = i
        ..uuid = uuid;
    }
  }

  Future<D?> saveSyncItem<D extends Tracked<D>>(
    final D item,
    final bool firstSync,
  ) async {
    Future<bool> isItemNewer(final ReckonerDb db, final D item) async {
      if (firstSync) return true;

      final existingItem = switch (D) {
        const (UserSetting) => await db.settingsDao.getUserSetting() as D,
        _ =>
          await (db.select(_dataTypeToTrackedTable<D>(db))
            ..whereSamePrimaryKey(item)).getSingleOrNull(),
      };

      if (existingItem == null) return item.isDeleted ? false : true;

      if (D is CategoryBudget &&
          (item as CategoryBudget).edited &&
          !(existingItem as CategoryBudget).edited) {
        return true;
      }

      return item.updateDate.isAfter(existingItem.updateDate);
    }

    if (!await isItemNewer(db, item)) return null;
    return saveTrackedDbItem(item, false);
  }

  Future<DateTime?> saveSyncItemList<D extends Tracked<D>>(
    final List<D> items,
    final bool firstSync,
  ) async {
    DateTime? latestDate;
    if (D == Report) {
      return db.reportDao.saveSyncReportList(
        db,
        items as List<Report>,
        firstSync,
      );
    }

    for (final item in items) {
      final newItem = await saveSyncItem(item, firstSync);
      if (newItem != null) {
        if (latestDate == null || newItem.updateDate.isAfter(latestDate)) {
          latestDate = newItem.updateDate;
        }
      }
    }
    return latestDate;
  }

  Future<List<D>> getDbSyncItems<D extends Tracked<D>>(
    final DateTime? syncDate,
  ) {
    final query = db.select(_dataTypeToTrackedTable<D>(db));
    if (syncDate != null) {
      query.where((final tbl) => tbl.updateDate.isBiggerThanValue(syncDate));
    }
    return query.get();
  }

  Future<int> getDbSyncCount<D extends Tracked<D>>(
    final DateTime? syncDate,
  ) async {
    final table = _dataTypeToTrackedTable<D>(db);
    final count = (table as TrackedTable).updateDate.count();
    final query = db.selectOnly(_dataTypeToTrackedTable<D>(db))
      ..addColumns([count]);
    if (syncDate != null) {
      query.where(
        (table as TrackedTable).updateDate.isBiggerThanValue(syncDate),
      );
    }
    final result = await query.getSingleOrNull();
    return result?.read(count) ?? 0;
  }

  Future<void> deleteData([DateTime? deleteDate]) async {
    deleteDate ??= DateTime.now();

    Future<int> deleteType<T extends Tracked<T>>() {
      final query = db.delete(_dataTypeToTrackedTable<T>(db))..where(
        (final tbl) => tbl.deleteDate.isSmallerOrEqualValue(deleteDate!),
      );
      return query.go();
    }

    await db.transaction(() async {
      int deleteCount = 0;
      deleteCount += await deleteType<Device>();
      deleteCount += await deleteType<Organization>();
      deleteCount += await deleteType<Account>();
      deleteCount += await deleteType<Transaction>();
      deleteCount += await deleteType<CategoryGroup>();
      deleteCount += await deleteType<Category>();
      deleteCount += await deleteType<Currency>();
      deleteCount += await deleteType<Report>();
      deleteCount += await deleteType<ReportGroup>();

      if (deleteCount > 0) {
        await db.customStatement('VACUUM');
      }
      return deleteCount;
    });
  }
}
