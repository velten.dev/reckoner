import 'package:drift/drift.dart';

import '../reckoner_db.dart';
import '../tables/transaction_tables.dart';

part 'transaction_tag_dao.g.dart';

@DriftAccessor(tables: [Transactions, TransactionTags, TransactionTransfers])
class TransactionTagDao extends DatabaseAccessor<ReckonerDb>
    with _$TransactionTagDaoMixin {
  TransactionTagDao(super.db);

  Stream<List<String>> loadTags() {
    final query =
        selectOnly(transactionTags)
          ..addColumns([transactionTags.tag])
          ..groupBy([transactionTags.tag]);
    return query.watch().map((final rows) {
      return rows.map((final row) => row.read(transactionTags.tag)!).toList();
    });
  }

  Future<int> updateTag(final String oldTag, final String newTag) =>
      (update(transactionTags)..where(
        (final tbl) => tbl.tag.equals(oldTag),
      )).write(TransactionTagsCompanion(tag: Value(newTag)));

  Future<int> deleteTag(final String tag) async {
    final uuids =
        selectOnly(transactionTags)
          ..addColumns([transactionTags.uuid])
          ..where(transactionTags.tag.equals(tag));

    final txQuery = update(transactions)
      ..where((final tbl) => tbl.uuid.isInQuery(uuids));

    int txRows = 0, tagRows = 0;

    await transaction(() async {
      txRows = await txQuery.write(
        TransactionsCompanion(updateDate: Value(DateTime.now())),
      );

      tagRows =
          await (delete(transactionTags)
            ..where((final tbl) => tbl.tag.equals(tag))).go();
    });

    assert(txRows == tagRows);

    return tagRows;
  }
}
