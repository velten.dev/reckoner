import 'package:drift/drift.dart';

import '../../model/device.dart';
import '../reckoner_db.dart';
import '../tables/device_tables.dart';
import '../tables/setting_tables.dart';
import 'device_name/native.dart'
    if (dart.library.js_interop) 'device_name/web.dart';

part 'device_dao.g.dart';

@DriftAccessor(tables: [Devices, UserSettings])
class DeviceDao extends DatabaseAccessor<ReckonerDb> with _$DeviceDaoMixin {
  DeviceDao(super.db);

  Future<int> deleteDevice(final Device device) async {
    final updateDate = Value(DateTime.now());
    var cnt = await db
        .update(devices)
        .write(
          DevicesCompanion(deleteDate: updateDate, updateDate: updateDate),
        );
    cnt += await db
        .update(db.userSettings)
        .write(UserSettingsCompanion(updateDate: updateDate));
    return cnt;
  }

  Future<void> resetSyncData() async {
    final query = db.delete(db.devices)
      ..where((final tbl) => tbl.thisDevice.equals(false));
    await query.go();
    await db
        .update(db.devices)
        .write(const DevicesCompanion(syncDate: Value(null)));
    await db
        .update(db.merchants)
        .write(const MerchantsCompanion(syncId: Value(null)));
    await db
        .update(db.categoryBudgets)
        .write(const CategoryBudgetsCompanion(syncId: Value(null)));
  }

  Future<Device> getCurrentDevice() async {
    Device thisDevice;
    final thisDeviceList =
        await (db.select(db.devices)
          ..where((final tbl) => tbl.thisDevice.equals(true))).get();
    if (thisDeviceList.isEmpty) {
      thisDevice = Device(name: await getDeviceName(), thisDevice: true);
      await db.commonDao.saveDbItem(thisDevice);
    } else if (thisDeviceList.length > 1) {
      thisDevice = thisDeviceList.removeLast();
      await Future.wait(
        thisDeviceList.map(
          (final e) => ReckonerDb.I.commonDao.hardDeleteDbItem(e),
        ),
      );
    } else {
      thisDevice = thisDeviceList.first;
    }
    return thisDevice;
  }
}
