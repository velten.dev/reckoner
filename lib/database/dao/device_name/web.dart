import 'package:device_info_plus/device_info_plus.dart';

Future<String> getDeviceName() async {
  final deviceInfoPlugin = DeviceInfoPlugin();
  return (await deviceInfoPlugin.webBrowserInfo).browserName.name;
}
