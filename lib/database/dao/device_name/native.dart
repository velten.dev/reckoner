import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

Future<String> getDeviceName() async {
  final deviceInfoPlugin = DeviceInfoPlugin();

  if (Platform.isAndroid) {
    final androidInfo = await deviceInfoPlugin.androidInfo;
    return '${androidInfo.brand} ${androidInfo.model}';
  } else if (Platform.isLinux) {
    final linuxInfo = await deviceInfoPlugin.linuxInfo;
    return linuxInfo.prettyName;
  } else if (Platform.isIOS) {
    final iosInfo = await deviceInfoPlugin.iosInfo;
    return iosInfo.name;
  } else if (Platform.isMacOS) {
    final macInfo = await deviceInfoPlugin.macOsInfo;
    return macInfo.hostName;
  }
  throw UnimplementedError();
}
