import 'package:drift/drift.dart';

import '../reckoner_db.dart';
import '../tables/transaction_tables.dart';

part 'transaction_merchant_dao.g.dart';

@DriftAccessor(tables: [Transactions])
class TransactionMerchantDao extends DatabaseAccessor<ReckonerDb>
    with _$TransactionMerchantDaoMixin {
  TransactionMerchantDao(super.db);

  Future<void> deleteMerchant(final String merchant) =>
      (update(transactions)
        ..where((final t) => t.merchantName.equals(merchant))).write(
        TransactionsCompanion(
          updateDate: Value(DateTime.now()),
          merchantName: const Value(''),
        ),
      );

  Future<void> updateMerchant(
    final String oldMerchant,
    final String newMerchant,
  ) async {
    if (oldMerchant == newMerchant) return;
    await (update(transactions)
      ..where((final t) => t.merchantName.equals(oldMerchant))).write(
      TransactionsCompanion(
        updateDate: Value(DateTime.now()),
        merchantName: Value(newMerchant),
      ),
    );
  }

  Stream<List<String>> loadMerchants({
    final int? limit,
    final bool nameOrder = true,
  }) {
    final query =
        selectOnly(transactions, distinct: true)
          ..addColumns([transactions.merchantName])
          ..orderBy([
            if (nameOrder) OrderingTerm.asc(transactions.merchantName),
            if (!nameOrder) OrderingTerm.desc(transactions.date),
          ])
          ..where(transactions.merchantName.isNotNull());
    if (limit != null && limit > 1) query.limit(limit);
    return query.watch().map(
      (final rows) =>
          rows.map((final r) => r.read(transactions.merchantName)!).toList(),
    );
  }
}
