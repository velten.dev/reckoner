import 'package:drift/drift.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:nanoid2/nanoid2.dart';

import '../../model/account.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/device.dart';
import '../../model/merchant.dart';
import '../../model/report.dart';
import '../../model/settings.dart';
import '../../model/transaction.dart';
import '../config/environment_const.dart';
import 'dao/all_daos.dart';
import 'instance/native.dart' if (dart.library.js_interop) 'instance/web.dart';
import 'migrations/migrator.dart';
import 'tables/all_tables.dart';
import 'util/generate.dart';

part 'reckoner_db.g.dart';

String generateUuid() => nanoid(length: 15, alphabet: Alphabet.alphanumeric);

@DriftDatabase(tables: tableList, daos: daoList)
class ReckonerDb extends _$ReckonerDb {
  ReckonerDb._(super.e);

  factory ReckonerDb.isolate(final DatabaseConnection con) {
    _instance ??= ReckonerDb._(con);
    return _instance!;
  }

  static ReckonerDb? _instance;

  static ReckonerDb get instance {
    if (_instance == null) {
      _instance = ReckonerDb._(getExecutor());
      if (!isDemo) {
        dbCronBackup().then(
          // Also a performance tunning to be run once per connection
          (final _) => _instance!.customSelect('PRAGMA optimize'),
        );
      }
    }
    return _instance!;
  }

  static ReckonerDb get I => instance;

  static Future<void> dispose() => _instance?.close() ?? Future.value();

  static Future<void> clear() => clearDatabase();
  static Future<String> check() => checkDatabase();
  static Future<bool> backup(final String path, final String encryptionKey) =>
      dbBackup(path, encryptionKey);
  static Future<bool> restore(final String encryptionKey) =>
      dbRestore(encryptionKey);
  static Future<void> generateData() =>
      (kDebugMode || isDemo) ? generateExampleData() : Future.value();
  static Future<void> changeEncryption(final String password) =>
      changeDbEncryption(password);

  @override
  Future<void> close() {
    _instance = null;
    return super.close();
  }

  @override
  int get schemaVersion => 2;

  @override
  MigrationStrategy get migration => MigrationStrategy(
    beforeOpen: (final _) async {
      // https://phiresky.github.io/blog/2020/sqlite-performance-tuning/
      await customStatement('PRAGMA foreign_keys = ON');
      await customStatement('PRAGMA temp_store = MEMORY');
      await customStatement('PRAGMA synchronous = NORMAL');
      await customStatement('PRAGMA journal_mode = WAL');
    },
    onUpgrade: migrator(this),
  );
}
