import 'package:drift/drift.dart';

class TrackedTable extends Table {
  DateTimeColumn get createDate => dateTime().withDefault(currentDateAndTime)();
  DateTimeColumn get updateDate => dateTime().withDefault(currentDateAndTime)();
  DateTimeColumn get deleteDate => dateTime().nullable()();
  TextColumn get uuid => text()();

  @override
  Set<Column>? get primaryKey => {uuid};
}

class NamedTable extends TrackedTable with NamedTableMixin {}

mixin NamedTableMixin on Table {
  TextColumn get name => text().withLength(min: 1)();
}

mixin IdTableMixin on Table {
  IntColumn get id => integer()();
}

mixin SyncIdTableMixin on Table {
  TextColumn get syncId => text().nullable()();
}

abstract interface class UuidTable extends Table {
  TextColumn get uuid;
}
