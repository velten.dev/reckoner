import 'package:drift/drift.dart';

import '../../../model/category.dart';
import 'model_tables.dart';
import 'report_tables.dart';

@UseRowClass(CategoryGroup)
class CategoryGroups extends NamedTable {
  BoolColumn get isRequired => boolean().withDefault(const Constant(false))();
  BoolColumn get enableAutoBalance =>
      boolean().withDefault(const Constant(false))();
  BoolColumn get requireLimits =>
      boolean().withDefault(const Constant(false))();
  IntColumn get defaultColor => integer()();
  IntColumn get overColor => integer()();
  TextColumn get datePeriod => textEnum<CategoryLimitPeriod>().nullable()();
  IntColumn get datePeriodInterval => integer().nullable()();
  TextColumn get reportUuid =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
}

@UseRowClass(Category)
class Categories extends NamedTable {
  TextColumn get groupUuid =>
      text().references(CategoryGroups, #uuid, onDelete: KeyAction.cascade)();
  TextColumn get parentUuid =>
      text().nullable().references(
        Categories,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  TextColumn get progenitorUuid =>
      text().nullable().references(
        Categories,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  BoolColumn get isSpendLimit => boolean().withDefault(const Constant(true))();
  BoolColumn get excludeFromAutoBalance =>
      boolean().withDefault(const Constant(false))();
  TextColumn get datePeriod => textEnum<CategoryLimitPeriod>().nullable()();
  IntColumn get datePeriodInterval => integer().nullable()();
}

@UseRowClass(CategoryAlert)
class CategoryAlerts extends Table with IdTableMixin implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Categories, #uuid, onDelete: KeyAction.cascade)();
  IntColumn get color => integer()();
  RealColumn get percentDone => real()();

  @override
  Set<Column>? get primaryKey => {uuid, id};
}

@UseRowClass(CategoryLimit)
class CategoryLimits extends Table implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Categories, #uuid, onDelete: KeyAction.cascade)();
  IntColumn get amount => integer()();
  BoolColumn get isRollover => boolean()();
  TextColumn get currencyCode => text().withLength(min: 3, max: 3)();

  @override
  Set<Column>? get primaryKey => {uuid, currencyCode};
}

@UseRowClass(CategoryBudget)
class CategoryBudgets extends TrackedTable
    with SyncIdTableMixin
    implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Categories, #uuid, onDelete: KeyAction.cascade)();
  TextColumn get currencyCode => text().withLength(min: 3, max: 3)();
  DateTimeColumn get startDate => dateTime()();
  DateTimeColumn get endDate => dateTime()();
  IntColumn get amount => integer()();
  BoolColumn get edited => boolean()();

  @override
  Set<Column>? get primaryKey => {uuid, currencyCode, startDate};
}
