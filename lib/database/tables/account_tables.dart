import 'package:drift/drift.dart';

import '../../../model/account.dart';
import 'model_tables.dart';

@UseRowClass(Organization)
class Organizations extends NamedTable {}

@UseRowClass(Account)
class Accounts extends NamedTable {
  TextColumn get orgUuid =>
      text().references(Organizations, #uuid, onDelete: KeyAction.cascade)();
  TextColumn get defaultCurrencyCode =>
      text().withLength(min: 3, max: 3).nullable()();
  BoolColumn get isMultiCurrency =>
      boolean().withDefault(const Constant(false))();
}

@UseRowClass(AccountIdentifier)
class AccountIdentifiers extends Table
    with IdTableMixin, NamedTableMixin
    implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Accounts, #uuid, onDelete: KeyAction.cascade)();
  TextColumn get identifier => text()();

  @override
  Set<Column>? get primaryKey => {uuid, id};
}

@UseRowClass(AccountAlert)
class AccountAlerts extends Table with IdTableMixin implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Accounts, #uuid, onDelete: KeyAction.cascade)();
  BoolColumn get isMax => boolean()();
  IntColumn get amount => integer()();
  TextColumn get message => text()();
  IntColumn get color => integer()();
  TextColumn get currencyCode => text().withLength(min: 3, max: 3)();

  @override
  Set<Column>? get primaryKey => {uuid, id};
}
