import 'package:drift/drift.dart';

import '../../../model/merchant.dart';
import 'model_tables.dart';
import 'transaction_tables.dart';

// ignore: deprecated_member_use_from_same_package
@UseRowClass(Merchant)
class Merchants extends NamedTable with SyncIdTableMixin {
  TextColumn get locationUuid =>
      text()
          .references(Locations, #uuid, onDelete: KeyAction.setNull)
          .nullable()();
}
