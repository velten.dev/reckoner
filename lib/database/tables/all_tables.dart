import 'account_tables.dart';
import 'category_tables.dart';
import 'currency_tables.dart';
import 'device_tables.dart';
import 'merchant_tables.dart';
import 'report_tables.dart';
import 'setting_tables.dart';
import 'transaction_tables.dart';

export 'account_tables.dart';
export 'category_tables.dart';
export 'currency_tables.dart';
export 'device_tables.dart';
export 'merchant_tables.dart';
export 'model_tables.dart';
export 'report_tables.dart';
export 'setting_tables.dart';
export 'transaction_tables.dart';

const tableList = [
  Accounts,
  AccountIdentifiers,
  AccountAlerts,
  Categories,
  CategoryAlerts,
  CategoryGroups,
  CategoryLimits,
  CategoryBudgets,
  Currencies,
  Devices,
  Locations,
  Merchants,
  Organizations,
  Reports,
  ReportGroups,
  ReportGroupLayouts,
  ReportGroupRows,
  Transactions,
  TransactionCategories,
  TransactionSplits,
  TransactionTags,
  TransactionTransfers,
  UserSettings,
];
