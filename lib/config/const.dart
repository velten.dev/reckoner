import 'package:flutter/foundation.dart';

const String dbName = '${kDebugMode ? 'debug-' : ''}reckoner.db';
const bool logStatements = false;
