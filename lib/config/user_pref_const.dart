import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences? _instance;

Future<void> initPreferences() async =>
    _instance ??= await SharedPreferences.getInstance();

SharedPreferences getPreferences() => _instance!;

const String _debugPrefix = kDebugMode ? 'debug-' : '';

const String collapseKey = '${_debugPrefix}collapse';
const String transactionRange = '${_debugPrefix}transactionRange';
const String themeKey = '${_debugPrefix}theme';
const String hideHelpIconKey = '${_debugPrefix}hideHelpIcon';
const String useBlackThemeKey = '${_debugPrefix}useBlackTheme';
const String generateData = '${_debugPrefix}generateData';
const String optionsTab = '${_debugPrefix}optionsTab';
const String tableColumnOrder = '${_debugPrefix}tableColumnOrder';

// Secure constants
const String dbPasswordSec = '${_debugPrefix}dbPassword';
const String appLockSettingsSec = '${_debugPrefix}appLockSettings';
const String backupSettingsSec = '${_debugPrefix}backupSettings';
const String syncManagerSettingsSec = '${_debugPrefix}syncManagerSettings';
const String pbSyncSettingsSec = '${_debugPrefix}pbSyncSettings';
