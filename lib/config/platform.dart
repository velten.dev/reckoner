import 'package:flutter/foundation.dart';

final isDesktop =
    !kIsWeb &&
    [
      TargetPlatform.linux,
      TargetPlatform.windows,
      TargetPlatform.macOS,
    ].contains(defaultTargetPlatform);
final isMobile =
    !kIsWeb &&
    [
      TargetPlatform.android,
      TargetPlatform.iOS,
    ].contains(defaultTargetPlatform);
const isWeb = kIsWeb;

double? appBarToolbarHeight =
    defaultTargetPlatform == TargetPlatform.linux ? 72 : null;
