const bool isDemo = bool.fromEnvironment('DEMO', defaultValue: false);
const bool isAppImage = bool.fromEnvironment('APPIMAGE', defaultValue: false);
