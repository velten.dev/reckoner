import 'dart:io';

import '../platform.dart';
import 'desktop.dart';
import 'mobile.dart';

Future<void> appSetup() async {
  if (isDesktop) {
    await desktopSetup();
  } else if (isMobile) {
    await mobileSetup();
  }
}

Future<void> appOnClose() =>
    Platform.isAndroid ? androidOnClose() : Future.value();
