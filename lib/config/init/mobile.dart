import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/foundation.dart' show debugPrint;

import '../../database/reckoner_db.dart';
import '../../model/secure_storage.dart';
import '../../service/sync_service_manager.dart';
import '../user_pref_const.dart';

Future<void> mobileSetup() async {
  await BackgroundFetch.configure(
    BackgroundFetchConfig(
      startOnBoot: true,
      minimumFetchInterval: 60,
      stopOnTerminate: false,
      enableHeadless: true,
      requiresBatteryNotLow: true,
      requiredNetworkType: NetworkType.NOT_ROAMING,
    ),
    (final String taskId) async {
      debugPrint('Background bulk syncing');
      final manager = await SyncServiceManager.instance;
      await manager.start(useIsolate: false, force: true);
      await BackgroundFetch.finish(taskId);
    },
    (final String taskId) async {
      debugPrint('Background bulk sync timeout');
      final manager = await SyncServiceManager.instance;
      manager.stop();
      await BackgroundFetch.finish(taskId);
    },
  );

  final settings = await SecureStorage.syncManagerSettings;
  if (settings.backgroundEnabled) await BackgroundFetch.start();
}

Future<void> androidOnClose() async {
  @pragma('vm:entry-point')
  Future<void> backgroundFetchHeadlessTask(final HeadlessTask task) async {
    await initPreferences();
    debugPrint('Headless bulk syncing');
    final manager = await SyncServiceManager.instance;
    if (task.timeout) {
      debugPrint('Headless bulk sync timeout');
      manager.stop();
      await BackgroundFetch.finish(task.taskId);
    } else {
      //Good practice to properly close/dispose of resources
      await manager.start(useIsolate: false, force: true);
      manager.dispose();
      await ReckonerDb.I.close();
      await BackgroundFetch.finish(task.taskId);
    }
  }

  final settings = await SecureStorage.syncManagerSettings;

  if (settings.backgroundEnabled) {
    await BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
  }
}
