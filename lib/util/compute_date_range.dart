import '../model/category.dart';
import '../model/tz_date_time.dart';

TZDateTimeRange computeDateRange({
  final CategoryLimitPeriod period = CategoryLimitPeriod.monthly,
  TZDateTime? today,
  final int periodDuration = 1,
  final int startOffset = 0,
}) {
  today ??= tzNow();
  TZDateTime start, end;
  final endOffset = startOffset + periodDuration;

  switch (period) {
    case CategoryLimitPeriod.daily:
      start = TZDateTime(
        local,
        today.year,
        today.month,
        today.day + startOffset,
      );
      end = TZDateTime(
        local,
        today.year,
        today.month,
        today.day + endOffset,
        0,
        0,
        -1,
      );
      break;
    case CategoryLimitPeriod.weekly:
      start = TZDateTime(
        local,
        today.year,
        today.month,
        today.day - today.weekday + 1 + 7 * startOffset,
      );
      end = TZDateTime(
        local,
        today.year,
        today.month,
        today.day - today.weekday + 1 + 7 * endOffset,
        0,
        0,
        -1,
      );
      break;
    case CategoryLimitPeriod.yearly:
      start = TZDateTime(local, today.year + startOffset, DateTime.january);
      end = TZDateTime(
        local,
        today.year + endOffset,
        DateTime.january,
        1,
        0,
        0,
        -1,
      );
      break;
    case CategoryLimitPeriod.monthly:
      start = TZDateTime(local, today.year, today.month + startOffset);
      end = TZDateTime(local, today.year, today.month + endOffset, 1, 0, 0, -1);
  }

  return TZDateTimeRange(start: start, end: end);
}
