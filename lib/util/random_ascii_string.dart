import 'package:nanoid2/nanoid2.dart';

String randomAsciiString([final int length = 32]) =>
    nanoid(length: length, alphabet: Alphabet.alphanumeric);
