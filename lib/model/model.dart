import 'package:drift/drift.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart' as j;

import '../database/reckoner_db.dart';

abstract class Common<T> implements Insertable<T> {
  Object get key;

  Object? get parentKey => null;

  UpdateCompanion<T> toUpdateCompanion();

  T copy([final bool newId = false]);

  @override
  Map<String, Expression> toColumns(final bool nullToAbsent) =>
      toUpdateCompanion().toColumns(nullToAbsent);

  bool get hasData;
}

abstract class Tracked<T> extends Common<T> {
  final DateTime? createDate;
  DateTime? deleteDate;
  DateTime? _updateDate;
  String? _uuid;

  DateTime get updateDate => _updateDate ?? DateTime.now();
  set updateDate(final DateTime value) => _updateDate = value;

  String get uuid {
    if (_uuid?.isEmpty ?? true) _uuid = generateUuid();
    return _uuid!;
  }

  set uuid(final String val) => _uuid = val;

  bool get isNew => createDate == null;

  Tracked({
    this.createDate,
    final DateTime? updateDate,
    this.deleteDate,
    final String? uuid,
  }) : _updateDate = updateDate,
       _uuid = uuid;

  Tracked.from(final Tracked<T> other, final bool newId)
    : createDate = newId ? null : other.createDate,
      _uuid = newId ? null : other.uuid;

  @override
  Object get key => uuid;

  bool get isDeleted => deleteDate != null;
}

abstract class Named<T> extends Tracked<T>
    with EquatableMixin
    implements NamedInterface {
  @override
  String name;

  Named({
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.uuid,
    this.name = '',
  });

  Named.from(Named<T> super.other, super.newId)
    : name = other.name,
      super.from();

  @override
  String get displayName => name;

  @override
  List<Object?> get props => [name];

  @override
  bool get hasData => name.isNotEmpty;
}

abstract interface class NamedInterface {
  late String name;
  String get displayName;
}

abstract class IdItem<T> implements Insertable<T> {
  late int id;
  late String uuid;
}

mixin class SyncIdItem {
  @j.JsonKey(includeFromJson: false, includeToJson: false)
  String? syncId;
}

abstract interface class AmountInterface {
  int amount = 0;
}

int copyIdItemId<T>(
  final IdItem<T> source,
  final IdItem<T> target,
  final bool newId,
) => target.id = newId ? -1 : source.id;

List<T> copyList<T>(final List<Common<T>> list) =>
    list.map((final e) => e.copy()).toList();

typedef IdKey = (String, int);
