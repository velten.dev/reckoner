import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:timezone/timezone.dart';

class IconDataConverter extends JsonConverter<IconData, Map<String, dynamic>> {
  const IconDataConverter();

  @override
  Map<String, dynamic> toJson(final IconData object) {
    return {
      'codePoint': object.codePoint,
      'matchTextDirection': object.matchTextDirection,
      if (object.fontFamily != null) 'fontFamily': object.fontFamily,
      if (object.fontPackage != null) 'fontPackage': object.fontPackage,
    };
  }

  @override
  IconData fromJson(final Map<String, dynamic> json) {
    return IconData(
      json['codePoint'] as int,
      matchTextDirection: json['matchTextDirection'] as bool,
      fontFamily: json['fontFamily'] as String?,
      fontPackage: json['fontPackage'] as String?,
    );
  }
}

class TZDateTimeConverter extends JsonConverter<TZDateTime, String> {
  const TZDateTimeConverter();

  @override
  String toJson(final TZDateTime object) => object.toIso8601String();

  @override
  TZDateTime fromJson(final String json) => TZDateTime.parse(local, json);
}
