import 'dart:convert';

import 'package:biometric_storage/biometric_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import '../config/platform.dart';
import '../config/user_pref_const.dart';
import '../util/random_ascii_string.dart';

part 'secure_storage.g.dart';

class SecureStorage {
  static final _storage = BiometricStorage();
  static bool _initialized = false;
  static bool _useSecure = false;

  SecureStorage._();

  static bool get isSecure => _useSecure;

  static Future<void> _initialize() async {
    if (_initialized) return;
    try {
      _useSecure = !await BiometricStorage().linuxCheckAppArmorError();
    } catch (e) {
      debugPrint(e.toString());
    }

    debugPrint(_useSecure ? '✅Storage Secure✅' : '🚧Storage Insecure🚧');
    _initialized = true;
  }

  static Future<void> clear() async {
    await _initialize();
    for (final file in [
      dbPasswordSec,
      appLockSettingsSec,
      backupSettingsSec,
      syncManagerSettingsSec,
      pbSyncSettingsSec,
    ]) {
      await _delete(file);
    }
  }

  static Future<String?> _read(final String name) async {
    await _initialize();
    if (_useSecure) {
      return (await _storage.getStorage(
        name,
        options: StorageFileInitOptions(authenticationRequired: false),
      )).read();
    }
    return getPreferences().getString(name);
  }

  static Future<void> _write(final String name, final String? value) async {
    if (value == null) {
      await _delete(name);
      return;
    }
    await _initialize();
    if (_useSecure) {
      final fileStorage = await _storage.getStorage(
        name,
        options: StorageFileInitOptions(authenticationRequired: false),
      );
      await fileStorage.write(value);
      return;
    }
    await getPreferences().setString(name, value);
  }

  static Future<void> _delete(final String name) async {
    await _initialize();
    if (_useSecure) {
      final fileStorage = await _storage.getStorage(
        name,
        options: StorageFileInitOptions(authenticationRequired: false),
      );
      await fileStorage.delete();
      return;
    }
    await getPreferences().remove(name);
  }

  static Future<String> get dbPassword async {
    var password = await _read(dbPasswordSec);
    if (password == null) {
      password = randomAsciiString();
      await _write(dbPasswordSec, password);
    }
    return password;
  }

  static set dbPassword(final Future<String> value) =>
      value.then((final v) => _write(dbPasswordSec, v));

  static Future<AppLockSettings> get appLockSettings async {
    final json = await _read(appLockSettingsSec) ?? '';
    return json.isEmpty
        ? AppLockSettings()
        : AppLockSettings.fromJson(jsonDecode(json) as Map<String, dynamic>);
  }

  static set appLockSettings(final Future<AppLockSettings> value) =>
      value.then((final v) => _write(appLockSettingsSec, jsonEncode(v)));

  static Future<BackupSettings> get backupSettings async {
    final json = await _read(backupSettingsSec) ?? '';
    if (json.isNotEmpty) {
      return BackupSettings.fromJson(jsonDecode(json) as Map<String, dynamic>);
    } else if (isWeb) {
      return BackupSettings();
    } else {
      return BackupSettings(
        path: (await getApplicationDocumentsDirectory()).path,
      );
    }
  }

  static set backupSettings(final Future<BackupSettings> value) =>
      value.then((final v) => _write(backupSettingsSec, jsonEncode(v)));

  static Future<SyncManagerSettings> get syncManagerSettings async {
    final json = await _read(syncManagerSettingsSec) ?? '';
    return json.isEmpty
        ? SyncManagerSettings()
        : SyncManagerSettings.fromJson(
          jsonDecode(json) as Map<String, dynamic>,
        );
  }

  static set syncManagerSettings(final Future<SyncManagerSettings> value) =>
      value.then((final v) => _write(syncManagerSettingsSec, jsonEncode(v)));

  static Future<PocketBaseSyncSettings> get pbSyncSettings async {
    final json = await _read(pbSyncSettingsSec) ?? '';
    return json.isEmpty
        ? PocketBaseSyncSettings()
        : PocketBaseSyncSettings.fromJson(
          jsonDecode(json) as Map<String, dynamic>,
        );
  }

  static set pbSyncSettings(final Future<PocketBaseSyncSettings> value) =>
      value.then((final v) => _write(pbSyncSettingsSec, jsonEncode(v)));
}

@JsonSerializable()
class AppLockSettings {
  String password;
  DateTime? canUnlockTime;
  int unlockFailCount;
  bool useBiometrics;

  AppLockSettings({
    this.password = '',
    this.canUnlockTime,
    this.unlockFailCount = 0,
    this.useBiometrics = false,
  });

  bool get hasPassword => password.isNotEmpty || useBiometrics;

  factory AppLockSettings.fromJson(final Map<String, dynamic> json) =>
      _$AppLockSettingsFromJson(json);
  Map<String, dynamic> toJson() => _$AppLockSettingsToJson(this);
}

enum BackupCadence { daily, weekly, monthly }

@JsonSerializable()
class BackupSettings {
  String path;
  String filename;
  bool appendDate;
  int backupCount;
  BackupCadence cadence;
  bool cronJob;
  DateTime lastBackupDate;
  EncryptionSettings encryptionSettings;

  BackupSettings({
    this.appendDate = true,
    this.cadence = BackupCadence.monthly,
    this.cronJob = false,
    this.filename = 'reckoner-backup',
    final DateTime? lastBackupDate,
    this.path = '',
    final EncryptionSettings? encryptionSettings,
    this.backupCount = 5,
  }) : lastBackupDate = lastBackupDate ?? DateTime(0),
       encryptionSettings = encryptionSettings ?? EncryptionSettings();

  String get filePath => join(path, fullFileName);

  String get fullFileName =>
      '$filename${appendDate ? DateFormat('-yyyy-MM-dd').format(DateTime.now()) : ''}.db';

  factory BackupSettings.fromJson(final Map<String, dynamic> json) =>
      _$BackupSettingsFromJson(json);
  Map<String, dynamic> toJson() => _$BackupSettingsToJson(this);
}

enum SyncType { pocketBase }

@JsonSerializable()
class SyncManagerSettings {
  bool enabled;
  SyncType type;
  EncryptionSettings encryptionSettings;
  bool enableBackgroundSync;

  @JsonKey(includeFromJson: false, includeToJson: false)
  bool get backgroundEnabled => enabled && enableBackgroundSync;

  SyncManagerSettings({
    this.enabled = false,
    this.type = SyncType.pocketBase,
    final EncryptionSettings? encryptionSettings,
    this.enableBackgroundSync = false,
  }) : encryptionSettings = encryptionSettings ?? EncryptionSettings();

  factory SyncManagerSettings.fromJson(final Map<String, dynamic> json) =>
      _$SyncManagerSettingsFromJson(json);
  Map<String, dynamic> toJson() => _$SyncManagerSettingsToJson(this);
}

@JsonSerializable()
class PocketBaseSyncSettings {
  Uri url;
  String username;
  String password;

  PocketBaseSyncSettings({
    final Uri? url,
    this.username = '',
    this.password = '',
  }) : url = url ?? Uri();

  bool get loggedIn =>
      url.hasScheme && username.isNotEmpty && password.isNotEmpty;

  factory PocketBaseSyncSettings.fromJson(final Map<String, dynamic> json) =>
      _$PocketBaseSyncSettingsFromJson(json);
  Map<String, dynamic> toJson() => _$PocketBaseSyncSettingsToJson(this);
}

enum EncryptionType { none, db, custom }

@JsonSerializable()
class EncryptionSettings {
  EncryptionType type;
  String customKey;
  @JsonKey(includeFromJson: false, includeToJson: false)
  String _loadedKey = '';

  EncryptionSettings({this.type = EncryptionType.db, this.customKey = ''});

  Future<String> get encryptionKey async {
    if (type == EncryptionType.none) return '';
    if (_loadedKey.isEmpty) {
      _loadedKey = switch (type) {
        EncryptionType.none => '',
        EncryptionType.db => await SecureStorage.dbPassword,
        EncryptionType.custom => customKey,
      };
    }
    return _loadedKey;
  }

  Future<void> cacheForIsolate() async => _loadedKey = await encryptionKey;

  factory EncryptionSettings.fromJson(final Map<String, dynamic> json) =>
      _$EncryptionSettingsFromJson(json);
  Map<String, dynamic> toJson() => _$EncryptionSettingsToJson(this);
}
