import 'dart:convert';

import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:timezone/timezone.dart' as tz;

import '../config/routes_const.dart';
import '../database/reckoner_db.dart';
import 'currency.dart';
import 'model.dart';
import 'util/json_converters.dart';

part 'settings.g.dart';

enum NavigationEntryType {
  account,
  category,
  transaction,
  report,
  options,
  merchant,
  tag,
}

@immutable
@JsonSerializable(converters: [IconDataConverter()])
class NavigationEntry extends Equatable {
  final IconData icon;
  final NavigationEntryType type;
  final String name;
  final String? uuid;
  final bool hide;

  String get key => type.toString() + (uuid ?? '');

  const NavigationEntry({
    required this.icon,
    required this.type,
    required this.name,
    this.hide = false,
    this.uuid,
  }) : assert(
         (type != NavigationEntryType.report &&
                 type != NavigationEntryType.category) ||
             (uuid?.length ?? 0) > 0,
       );

  NavigationEntry copyWith(final bool? hide) => NavigationEntry(
    icon: icon,
    type: type,
    name: name,
    hide: hide ?? this.hide,
    uuid: uuid,
  );

  String get route => switch (type) {
    NavigationEntryType.account => accountPath,
    NavigationEntryType.category => categoryGroupPath(uuid!),
    NavigationEntryType.options => optionsPath,
    NavigationEntryType.report => reportGroupPath(uuid!),
    NavigationEntryType.transaction => transactionPath,
    NavigationEntryType.merchant => merchantPath,
    NavigationEntryType.tag => tagPath,
  };

  factory NavigationEntry.fromJson(final Map<String, dynamic> json) =>
      _$NavigationEntryFromJson(json);
  Map<String, dynamic> toJson() => _$NavigationEntryToJson(this);

  bool get canDelete => (uuid ?? '').isNotEmpty;

  @override
  List<Object?> get props => [type, name, uuid];
}

const List<NavigationEntry> _defaultEntries = [
  NavigationEntry(
    icon: Icons.sync_alt,
    type: NavigationEntryType.transaction,
    name: 'Transactions',
  ),
  NavigationEntry(
    icon: Icons.account_balance,
    type: NavigationEntryType.account,
    name: 'Accounts',
  ),
  NavigationEntry(
    icon: Icons.store,
    type: NavigationEntryType.merchant,
    name: 'Merchants',
  ),
  NavigationEntry(
    icon: Icons.tune,
    type: NavigationEntryType.options,
    name: 'Options',
  ),
  NavigationEntry(
    icon: Icons.label,
    type: NavigationEntryType.tag,
    name: 'Tags',
  ),
];

@JsonSerializable(explicitToJson: true)
class UserSetting extends Tracked<UserSetting> with DefaultCurrencyMixin {
  late List<NavigationEntry> navList;
  String navListJson;
  String? accountReportUuid;
  String? transactionReportUuid;
  String? merchantReportUuid;
  String? tagReportUuid;
  late String timezone;

  UserSetting({
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.uuid,
    this.navListJson = '',
    final String? defaultCurrencyCode,
    this.accountReportUuid,
    this.transactionReportUuid,
    this.merchantReportUuid,
    this.tagReportUuid,
    final String? localEncryptionKey,
    final String? timezone,
  }) : timezone = timezone ?? tz.local.name {
    if (defaultCurrencyCode?.isNotEmpty ?? false) {
      currency = Currency(code: defaultCurrencyCode!);
    }

    if (navListJson.isNotEmpty) {
      try {
        navList =
            (jsonDecode(navListJson) as List<dynamic>)
                .map(
                  (final e) =>
                      NavigationEntry.fromJson(e as Map<String, dynamic>),
                )
                .toList();
        for (final entry in _defaultEntries) {
          if (!navList.contains(entry)) navList.add(entry);
        }
      } catch (err) {
        navList = List.from(_defaultEntries);
      }
    } else {
      navList = List.from(_defaultEntries);
    }
  }

  UserSetting.from(final UserSetting other)
    : navListJson = other.navListJson,
      accountReportUuid = other.accountReportUuid,
      transactionReportUuid = other.transactionReportUuid,
      merchantReportUuid = other.merchantReportUuid,
      tagReportUuid = other.tagReportUuid,
      super.from(other, false) {
    currency = other.currency;
    if (navListJson.isNotEmpty) {
      try {
        navList =
            (jsonDecode(navListJson) as List<dynamic>)
                .map(
                  (final e) =>
                      NavigationEntry.fromJson(e as Map<String, dynamic>),
                )
                .toList();
        for (final entry in _defaultEntries) {
          if (!navList.contains(entry)) navList.add(entry);
        }
      } catch (err) {
        navList = List.from(_defaultEntries);
      }
    } else {
      navList = List.from(_defaultEntries);
    }
  }

  bool setIcon(final NavigationEntry entry, final IconData icon) {
    if (!entry.canDelete) return false;
    final index = navList.indexOf(entry);
    if (index < 0) return false;
    navList[index] = NavigationEntry(
      icon: icon,
      type: entry.type,
      name: entry.name,
      uuid: entry.uuid,
    );
    return true;
  }

  bool moveEntry(final int oldIndex, final int newIndex) {
    if (oldIndex < 0 || oldIndex >= navList.length) return false;
    final entry = navList.removeAt(oldIndex);
    navList.insert(newIndex, entry);
    return true;
  }

  @override
  UpdateCompanion<UserSetting> toUpdateCompanion() => UserSettingsCompanion(
    updateDate: Value(updateDate),
    navListJson: Value(jsonEncode(navList)),
    defaultCurrencyCode: Value(defaultCurrencyCode),
    accountReportUuid: Value(accountReportUuid),
    transactionReportUuid: Value(transactionReportUuid),
    merchantReportUuid: Value(merchantReportUuid),
    tagReportUuid: Value(tagReportUuid),
    uuid: Value(uuid),
  );

  factory UserSetting.fromJson(final Map<String, dynamic> json) =>
      _$UserSettingFromJson(json);
  Map<String, dynamic> toJson() => _$UserSettingToJson(this);

  @override
  UserSetting copy([final bool newId = false]) => UserSetting.from(this);

  @override
  bool get hasData => true;
}
