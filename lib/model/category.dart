import 'package:collection/collection.dart';
import 'package:drift/drift.dart' show Value, UpdateCompanion;
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import '../util/compute_date_range.dart';
import 'currency.dart';
import 'model.dart';
import 'tz_date_time.dart';
import 'util/json_converters.dart';

part 'category.g.dart';

typedef CategoryGroupMap = Map<String, CategoryGroup>;
typedef BudgetAmounts = Map<String, int>;
typedef CatBudgetAmounts = Map<String, BudgetAmounts>;
// Group UUID, Category UUID, Currency Code
typedef CategoryBudgetMap =
    Map<String, Map<String, CurrencyToCategoryBudgetMap>>;
typedef CategoryBudgetDateMap =
    Map<String, Map<String, CurrencyToCategoryBudgetMap>>;
typedef CurrencyToCategoryBudgetMap = Map<String, CategoryBudget>;
typedef CategoryAmountMap = Map<Currency, int>;
typedef CategoryGroupExtras = Map<bool, CategoryAmountMap>;

enum CategoryLimitPeriod { yearly, monthly, weekly, daily }

abstract interface class CategoryDatePeriodInterface {
  CategoryLimitPeriod? datePeriod;
  int? datePeriodInterval;
}

@JsonSerializable()
class CategoryGroup extends Named<CategoryGroup>
    with EquatableMixin
    implements CategoryDatePeriodInterface {
  bool isRequired;
  bool requireLimits;
  int defaultColor;
  int overColor;
  bool enableAutoBalance;
  @override
  CategoryLimitPeriod? datePeriod;
  @override
  int? datePeriodInterval;
  String? reportUuid;

  List<Category> categories = [];

  CategoryGroup({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    this.isRequired = false,
    this.requireLimits = false,
    this.enableAutoBalance = false,
    this.defaultColor = 0xFF4CAF50, //Green default
    this.overColor = 0xFFF44336, //Red default
    this.datePeriod,
    this.datePeriodInterval,
    this.reportUuid,
  });

  CategoryGroup.from(CategoryGroup super.other, super.newId)
    : isRequired = other.isRequired,
      enableAutoBalance = other.enableAutoBalance,
      requireLimits = other.requireLimits,
      defaultColor = other.defaultColor,
      overColor = other.overColor,
      datePeriod = other.datePeriod,
      datePeriodInterval = other.datePeriodInterval,
      super.from();

  @override
  UpdateCompanion<CategoryGroup> toUpdateCompanion() => CategoryGroupsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    isRequired: Value(isRequired),
    enableAutoBalance: Value(enableAutoBalance),
    requireLimits: Value(requireLimits),
    defaultColor: Value(defaultColor),
    overColor: Value(overColor),
    datePeriod: Value(datePeriod),
    datePeriodInterval: Value(datePeriodInterval),
    reportUuid: Value(reportUuid),
  );

  @override
  List<Object?> get props =>
      super.props..addAll([
        isRequired,
        enableAutoBalance,
        requireLimits,
        defaultColor,
        overColor,
        datePeriod,
        datePeriodInterval,
      ]);

  @override
  CategoryGroup copy([final bool newId = false]) =>
      CategoryGroup.from(this, newId);

  factory CategoryGroup.fromJson(final Map<String, dynamic> json) =>
      _$CategoryGroupFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryGroupToJson(this);

  @override
  String get key => uuid;

  List<Category> get allCategories => categories
      .map((final e) => allCategoryChildren(e))
      .fold(
        [],
        (final previousValue, final element) => previousValue + element,
      );

  List<Category> allCategoryChildren(final Category parent) {
    final List<Category> catList = [parent];
    for (final child in parent.children) {
      catList.addAll(allCategoryChildren(child));
    }
    return catList;
  }

  Category? findCategory(final String catUuid) =>
      _findCategoryRecursive(catUuid, categories);

  Category? _findCategoryRecursive(
    final String catUuid,
    final List<Category> catList,
  ) =>
      catList.singleWhereOrNull((final e) => e.uuid == catUuid) ??
      catList
          .map((final e) => _findCategoryRecursive(catUuid, e.children))
          .fold(
            null,
            (final previousValue, final element) => previousValue ?? element,
          );
}

@JsonSerializable()
class Category extends Named<Category>
    with EquatableMixin
    implements CategoryDatePeriodInterface {
  bool isSpendLimit;
  bool excludeFromAutoBalance;
  @override
  CategoryLimitPeriod? datePeriod;
  @override
  int? datePeriodInterval;

  String get groupUuid => group.uuid;
  String? get progenitorUuid => progenitor?.uuid;
  String? get parentUuid => parent?.uuid;

  List<CategoryLimit> limits = [];
  List<CategoryAlert> alerts = [];

  @JsonKey(includeFromJson: false, includeToJson: false)
  List<Category> children = [];
  @JsonKey(includeFromJson: false, includeToJson: false)
  CategoryGroup group = CategoryGroup();
  @JsonKey(includeFromJson: false, includeToJson: false)
  Category? progenitor;
  @JsonKey(includeFromJson: false, includeToJson: false)
  Category? parent;
  @JsonKey(includeFromJson: false, includeToJson: false)
  int firstDateOffset = 0;

  Category({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    final String groupUuid = '',
    final String? parentUuid,
    final String? progenitorUuid,
    this.isSpendLimit = true,
    this.excludeFromAutoBalance = false,
    this.datePeriod,
    this.datePeriodInterval,
  }) {
    group.uuid = groupUuid;
    if (parentUuid?.isNotEmpty ?? false) {
      parent = Category(
        groupUuid: groupUuid,
        uuid: parentUuid!,
        parentUuid: progenitorUuid,
      );

      if (progenitorUuid?.isNotEmpty ?? false) {
        progenitor = Category(groupUuid: groupUuid, uuid: progenitorUuid!);
      }
    }
  }

  Category.from(Category super.other, super.newId)
    : isSpendLimit = other.isSpendLimit,
      excludeFromAutoBalance = other.excludeFromAutoBalance,
      datePeriod = other.datePeriod,
      datePeriodInterval = other.datePeriodInterval,
      parent = other.parent,
      progenitor = other.progenitor,
      group = other.group,
      children = copyList(other.children),
      limits = copyList(other.limits),
      alerts = copyList(other.alerts),
      super.from();

  @override
  UpdateCompanion<Category> toUpdateCompanion() => CategoriesCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    groupUuid: Value(groupUuid),
    parentUuid: Value(parentUuid),
    progenitorUuid: Value(progenitorUuid),
    isSpendLimit: Value(isSpendLimit),
    datePeriod: Value(datePeriod),
    datePeriodInterval: Value(datePeriodInterval),
  );

  @override
  List<Object?> get props =>
      super.props..addAll([
        groupUuid,
        parentUuid,
        progenitorUuid,
        isSpendLimit,
        datePeriod,
        datePeriodInterval,
      ]);

  @override
  Category copy([final bool newId = false]) => Category.from(this, newId);

  factory Category.fromJson(final Map<String, dynamic> json) =>
      _$CategoryFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  @override
  String get key => uuid;

  @override
  String get parentKey => groupUuid;

  List<String> get familyUuids {
    final List<String> ids = [];
    _categoryToAllUuids(this, ids);
    return ids;
  }

  void _categoryToAllUuids(final Category cat, final List<String> ids) {
    ids.add(cat.uuid);
    for (final child in cat.children) {
      _categoryToAllUuids(child, ids);
    }
  }

  @override
  String get displayName => parent == null ? name : '${parent!.name}: $name';

  TZDateTimeRange getRange([final TZDateTime? today]) => computeDateRange(
    period: datePeriod ?? group.datePeriod ?? CategoryLimitPeriod.monthly,
    today: today,
    periodDuration: datePeriodInterval ?? group.datePeriodInterval ?? 1,
    startOffset: firstDateOffset,
  );
}

@JsonSerializable()
class CategoryAlert extends Common<CategoryAlert>
    with EquatableMixin
    implements IdItem<CategoryAlert> {
  @override
  int id;
  @override
  String uuid;
  int color;
  double percentDone;

  CategoryAlert({
    required this.uuid,
    this.id = -1,
    this.color = 0,
    this.percentDone = 0.5,
  });

  CategoryAlert.from(final CategoryAlert other, final bool newId)
    : color = other.color,
      percentDone = other.percentDone,
      uuid = other.uuid,
      id = 0 {
    copyIdItemId(other, this, newId);
  }

  @override
  CategoryAlert copy([final bool newId = false]) =>
      CategoryAlert.from(this, newId);

  @override
  UpdateCompanion<CategoryAlert> toUpdateCompanion() => CategoryAlertsCompanion(
    id: Value(id),
    uuid: Value(uuid),
    color: Value(color),
    percentDone: Value(percentDone),
  );

  @override
  List<Object?> get props => [uuid, color, percentDone];

  factory CategoryAlert.fromJson(final Map<String, dynamic> json) =>
      _$CategoryAlertFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryAlertToJson(this);

  @override
  (String, int) get key => (uuid, id);

  @override
  String get parentKey => uuid;

  @override
  bool get hasData => true;
}

@JsonSerializable()
class CategoryLimit extends Common<CategoryLimit>
    with EquatableMixin, CurrencyMixin
    implements AmountInterface {
  String uuid;
  @override
  int amount;
  bool isRollover;

  CategoryLimit({
    this.uuid = '',
    this.amount = 0,
    final String currencyCode = '',
    this.isRollover = false,
  }) {
    currency.code = currencyCode;
  }

  @override
  CategoryLimit.from(final CategoryLimit other, final newId)
    : amount = other.amount,
      isRollover = other.isRollover,
      uuid = other.uuid {
    currency = other.currency;
  }

  @override
  UpdateCompanion<CategoryLimit> toUpdateCompanion() => CategoryLimitsCompanion(
    amount: Value(amount),
    isRollover: Value(isRollover),
    currencyCode: Value(currencyCode),
    uuid: Value(uuid),
  );

  @override
  List<Object?> get props => [amount, isRollover, currencyCode, uuid];

  @override
  CategoryLimit copy([final bool newId = false]) =>
      CategoryLimit.from(this, newId);

  factory CategoryLimit.fromJson(final Map<String, dynamic> json) =>
      _$CategoryLimitFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryLimitToJson(this);

  @override
  (String, String) get key => (uuid, currencyCode);

  @override
  String get parentKey => uuid;

  @override
  bool get hasData => amount != 0;
}

@JsonSerializable(converters: [TZDateTimeConverter()])
class CategoryBudget extends Tracked<CategoryBudget>
    with EquatableMixin, SyncIdItem
    implements AmountInterface {
  TZDateTime startDate;
  TZDateTime endDate;
  bool edited;
  @override
  int amount;

  @override
  String get uuid => category.uuid;
  String get currencyCode => currency.code;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Category category = Category();
  @JsonKey(includeFromJson: false, includeToJson: false)
  Currency currency = Currency();

  CategoryBudget({
    super.createDate,
    super.updateDate,
    super.deleteDate,
    final String uuid = '',
    this.amount = 0,
    this.edited = false,
    final DateTime? startDate,
    final DateTime? endDate,
    final String currencyCode = '',
    final String? syncId,
  }) : startDate = tzFromDtNow(startDate),
       endDate = tzFromDtNow(endDate) {
    category.uuid = uuid;
    currency.code = currencyCode;
    this.syncId = syncId;
  }

  CategoryBudget.from(final CategoryBudget other, final bool newId)
    : startDate = other.startDate,
      endDate = other.endDate,
      amount = other.amount,
      edited = other.edited,
      super.from(other, newId) {
    category = other.category;
    currency = other.currency;
  }

  @override
  CategoryBudget copy([final bool newId = false]) =>
      CategoryBudget.from(this, newId);

  @override
  UpdateCompanion<CategoryBudget> toUpdateCompanion() =>
      CategoryBudgetsCompanion(
        deleteDate: Value(deleteDate),
        updateDate: Value(updateDate),
        uuid: Value(uuid),
        currencyCode: Value(currencyCode),
        startDate: Value(startDate),
        endDate: Value(endDate),
        amount: Value(amount),
        edited: Value(edited),
        syncId: syncId == null ? const Value.absent() : Value(syncId),
      );

  @override
  List<Object?> get props => [uuid, currencyCode, startDate, endDate, amount];

  factory CategoryBudget.fromJson(final Map<String, dynamic> json) =>
      _$CategoryBudgetFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryBudgetToJson(this);

  TZDateTimeRange get range => TZDateTimeRange(start: startDate, end: endDate);

  @override
  (String, String, DateTime) get key => (uuid, currencyCode, startDate);

  @override
  (String, String) get parentKey => (uuid, currencyCode);

  @override
  bool get hasData => amount != 0;
}
