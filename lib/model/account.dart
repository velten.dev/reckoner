import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import 'currency.dart';
import 'model.dart';

part 'account.g.dart';

typedef AccountMap = Map<String, Account>;

@JsonSerializable()
class Organization extends Named<Organization> with EquatableMixin {
  List<Account> accounts = [];

  Organization({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
  });

  Organization.from(Organization super.other, super.newId) : super.from();

  @override
  UpdateCompanion<Organization> toUpdateCompanion() => OrganizationsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
  );

  @override
  Organization copy([final newId = false]) => Organization.from(this, newId);

  factory Organization.fromJson(final Map<String, dynamic> json) =>
      _$OrganizationFromJson(json);
  Map<String, dynamic> toJson() => _$OrganizationToJson(this);

  @override
  String get key => uuid;
}

@JsonSerializable()
class Account extends Named<Account> with EquatableMixin, DefaultCurrencyMixin {
  bool isMultiCurrency;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Organization organization = Organization();

  List<AccountAlert> alerts = [];
  List<AccountIdentifier> identifiers = [];

  Account({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    final String orgUuid = '',
    final String? defaultCurrencyCode,
    this.isMultiCurrency = false,
  }) {
    organization.uuid = orgUuid;
    if (defaultCurrencyCode != null) {
      currency = Currency(code: defaultCurrencyCode);
    }
  }

  Account.from(final Account other, final bool newId)
    : isMultiCurrency = other.isMultiCurrency,
      organization = other.organization.copy(newId),
      alerts = copyList(other.alerts),
      identifiers = copyList(other.identifiers),
      super.from(other, newId) {
    currency = other.currency?.copy(newId);
  }

  String get orgUuid => organization.uuid;

  @override
  UpdateCompanion<Account> toUpdateCompanion() => AccountsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    orgUuid: Value(orgUuid),
    defaultCurrencyCode: Value(defaultCurrencyCode),
    isMultiCurrency: Value(isMultiCurrency),
  );

  @override
  List<Object?> get props =>
      super.props..addAll([
        orgUuid,
        defaultCurrencyCode,
        isMultiCurrency,
        alerts,
        identifiers,
      ]);

  @override
  Account copy([final newId = false]) => Account.from(this, newId);

  factory Account.fromJson(final Map<String, dynamic> json) =>
      _$AccountFromJson(json);
  Map<String, dynamic> toJson() => _$AccountToJson(this);

  @override
  String get key => uuid;

  @override
  String get parentKey => orgUuid;

  @override
  String get displayName => '${organization.name} $name';
}

@JsonSerializable()
class AccountIdentifier extends Common<AccountIdentifier>
    with EquatableMixin
    implements IdItem<AccountIdentifier>, NamedInterface {
  @override
  int id;
  @override
  String uuid;
  String identifier;
  @override
  String name;

  AccountIdentifier({
    required this.uuid,
    this.name = '',
    this.id = -1,
    this.identifier = '',
  });

  AccountIdentifier.from(final AccountIdentifier other, final bool newId)
    : id = other.id,
      uuid = other.uuid,
      identifier = other.identifier,
      name = other.name;

  @override
  AccountIdentifier copy([final bool newId = false]) =>
      AccountIdentifier.from(this, newId);

  @override
  UpdateCompanion<AccountIdentifier> toUpdateCompanion() =>
      AccountIdentifiersCompanion(
        id: Value(id),
        name: Value(name),
        uuid: Value(uuid),
        identifier: Value(identifier),
      );

  @override
  List<Object?> get props => [uuid, identifier, name];

  factory AccountIdentifier.fromJson(final Map<String, dynamic> json) =>
      _$AccountIdentifierFromJson(json);
  Map<String, dynamic> toJson() => _$AccountIdentifierToJson(this);

  @override
  (String, int) get key => (uuid, id);

  @override
  String get parentKey => uuid;

  @override
  bool get hasData => name.isNotEmpty;

  @override
  String get displayName => name;
}

@JsonSerializable()
class AccountAlert extends Common<AccountAlert>
    with EquatableMixin
    implements IdItem<AccountAlert>, AmountInterface {
  @override
  int id;
  @override
  String uuid;
  bool isMax;
  @override
  int amount;
  String message;
  int color;

  String get currencyCode => currency.code;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Currency currency = Currency();

  AccountAlert({
    required this.uuid,
    this.id = -1,
    this.isMax = true,
    this.amount = 0,
    this.message = '',
    this.color = 0,
    final String currencyCode = '',
  }) {
    currency.code = currencyCode;
  }

  AccountAlert.from(final AccountAlert other, final bool newId)
    : id = other.id,
      uuid = other.uuid,
      isMax = other.isMax,
      amount = other.amount,
      message = other.message,
      color = other.color {
    currency = other.currency;
  }

  @override
  AccountAlert copy([final bool newId = false]) =>
      AccountAlert.from(this, newId);

  @override
  UpdateCompanion<AccountAlert> toUpdateCompanion() => AccountAlertsCompanion(
    id: Value(id),
    uuid: Value(uuid),
    isMax: Value(isMax),
    amount: Value(amount),
    message: Value(message),
    color: Value(color),
    currencyCode: Value(currencyCode),
  );

  @override
  List<Object?> get props => [
    uuid,
    isMax,
    amount,
    message,
    color,
    currencyCode,
  ];

  factory AccountAlert.fromJson(final Map<String, dynamic> json) =>
      _$AccountAlertFromJson(json);
  Map<String, dynamic> toJson() => _$AccountAlertToJson(this);

  @override
  (String, int) get key => (uuid, id);

  @override
  String get parentKey => uuid;

  @override
  bool get hasData => true;
}
