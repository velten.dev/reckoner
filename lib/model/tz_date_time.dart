import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';

export 'package:timezone/timezone.dart' show TZDateTime, local;

extension TZDateTimeExtension on TZDateTime {
  DateTime get native =>
      DateTime.fromMicrosecondsSinceEpoch(microsecondsSinceEpoch);
}

TZDateTime tzNow() => TZDateTime.now(local);

TZDateTime? tzFromDt(final DateTime? other) =>
    other == null ? null : TZDateTime.from(other, local);

TZDateTime tzFromDtNow(final DateTime? other) =>
    other == null ? tzNow() : TZDateTime.from(other, local);

TZDateTime tzParse(final String formattedString) =>
    TZDateTime.parse(local, formattedString);

TZDateTime tzParseDate(final DateTime other) => TZDateTime(
  local,
  other.year,
  other.month,
  other.day,
  other.hour,
  other.minute,
  other.second,
  other.millisecond,
  other.microsecond,
);

class TZDateTimeRange {
  /// Creates a date range for the given start and end [DateTime].
  TZDateTimeRange({required this.start, required this.end})
    : assert(!start.isAfter(end));

  TZDateTimeRange.from(final DateTimeRange range)
    : start = TZDateTime.from(range.start, local),
      end = TZDateTime.from(range.end, local);

  /// The start of the range of dates.
  final TZDateTime start;

  /// The end of the range of dates.
  final TZDateTime end;

  /// Returns a [Duration] of the time between [start] and [end].
  ///
  /// See [DateTime.difference] for more details.
  Duration get duration => end.difference(start);

  @override
  bool operator ==(final Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is TZDateTimeRange && other.start == start && other.end == end;
  }

  @override
  int get hashCode => Object.hash(start, end);

  @override
  String toString() => '$start - $end';

  DateTimeRange get native =>
      DateTimeRange(start: start.native, end: end.native);
}
