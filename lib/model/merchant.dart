import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import 'model.dart';
import 'transaction.dart';

part 'merchant.g.dart';

@Deprecated('Use Merchant field on transactions')
@JsonSerializable()
class Merchant extends Named<Merchant> with EquatableMixin, SyncIdItem {
  String? get locationUuid => location.uuid.isEmpty ? null : location.uuid;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Location location = Location();

  Merchant({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    final String? locationUuid,
    final String? syncId,
  }) {
    location.uuid = locationUuid ?? '';
    this.syncId = syncId;
  }

  Merchant.from(final Merchant other, final bool newId)
    : super.from(other, newId) {
    location = other.location.copy(newId);
  }

  @override
  UpdateCompanion<Merchant> toUpdateCompanion() => MerchantsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    locationUuid: Value(locationUuid),
    syncId: syncId == null ? const Value.absent() : Value(syncId),
  );
  @override
  List<Object?> get props => super.props..addAll([locationUuid]);

  @override
  Merchant copy([final newId = false]) => Merchant.from(this, newId);

  factory Merchant.fromJson(final Map<String, dynamic> json) =>
      _$MerchantFromJson(json);
  Map<String, dynamic> toJson() => _$MerchantToJson(this);

  @override
  String get key => uuid;
}
