import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import 'model.dart';

part 'device.g.dart';

@JsonSerializable()
class Device extends Named<Device> with EquatableMixin {
  DateTime? syncDate;
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool thisDevice;

  Device({
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    final String uuid = '',
    this.syncDate,
    this.thisDevice = false,
  }) {
    if (name.isEmpty) name = 'New Device';
    this.uuid = uuid;
  }

  Device.from(Device super.other, super.newId)
    : thisDevice = false,
      syncDate = newId ? null : other.syncDate,
      super.from();

  @override
  String get key => uuid;

  @override
  UpdateCompanion<Device> toUpdateCompanion() {
    return DevicesCompanion(
      name: Value(name),
      uuid: Value(uuid),
      deleteDate: Value(deleteDate),
      updateDate: Value(updateDate),
      syncDate: Value(syncDate),
      thisDevice: Value(thisDevice),
    );
  }

  factory Device.fromJson(final Map<String, dynamic> json) =>
      _$DeviceFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceToJson(this);

  @override
  List<Object?> get props => super.props..add(uuid);

  @override
  Device copy([final bool newId = false]) => Device.from(this, newId);
}
