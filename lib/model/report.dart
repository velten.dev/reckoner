import 'dart:convert';

import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import '../view/model/transaction_type.dart';
import 'model.dart';
import 'tz_date_time.dart';
import 'util/json_converters.dart';

part 'report.g.dart';

enum ReportTimeRange { year, month, day, current }

enum ReportType { text, table, bar, line, pie }

enum ReportDataSorting { amount, name }

enum TransactionReportGrouping { account, category, currency, tag, merchant }

typedef ReportGroupMap = Map<String, ReportGroup>;

@JsonSerializable(converters: [TZDateTimeConverter()])
class Report extends Named<Report> with EquatableMixin {
  late ReportType type;
  TZDateTime? startDate;
  TZDateTime? endDate;
  int? relativeStartOffset;
  int? relativeEndOffset;
  ReportTimeRange? relativeRange;
  bool loadOverTime;
  TransactionReportGrouping grouping;
  bool invert;
  ReportDataSorting sorting;
  String? reportText;
  @JsonKey(includeFromJson: false, includeToJson: false)
  ReportTransactionFilter filterObj = ReportTransactionFilter();
  @JsonKey(name: 'filters')
  String get filters => jsonEncode(filterObj);

  IconData get icon => getIcon(type);

  static IconData getIcon(final ReportType type) => switch (type) {
    ReportType.text => Icons.text_fields,
    ReportType.table => Icons.table_chart,
    ReportType.bar => Icons.bar_chart,
    ReportType.line => Icons.line_axis,
    ReportType.pie => Icons.pie_chart,
  };

  int get flex => switch (type) {
    ReportType.line => 3,
    ReportType.bar => 2,
    _ => 1,
  };

  Report({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    this.type = ReportType.line,
    final DateTime? startDate,
    this.relativeStartOffset,
    this.relativeRange,
    final DateTime? endDate,
    this.relativeEndOffset,
    this.loadOverTime = true,
    this.grouping = TransactionReportGrouping.currency,
    this.invert = false,
    this.sorting = ReportDataSorting.amount,
    final String filters = '',
    this.reportText,
  }) : filterObj =
           filters.isEmpty
               ? ReportTransactionFilter()
               : ReportTransactionFilter.fromJson(
                 json.decode(filters) as Map<String, dynamic>,
               ),
       startDate = tzFromDt(startDate),
       endDate = tzFromDt(endDate);

  Report.from(Report super.other, super.newId)
    : type = other.type,
      startDate = other.startDate,
      relativeStartOffset = other.relativeStartOffset,
      relativeRange = other.relativeRange,
      endDate = other.endDate,
      relativeEndOffset = other.relativeEndOffset,
      loadOverTime = other.loadOverTime,
      grouping = other.grouping,
      filterObj = other.filterObj,
      invert = other.invert,
      sorting = other.sorting,
      reportText = other.reportText,
      super.from();

  @override
  UpdateCompanion<Report> toUpdateCompanion() => ReportsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    type: Value(type),
    startDate: Value(startDate),
    endDate: Value(endDate),
    relativeStartOffset: Value(relativeStartOffset),
    relativeRange: Value(relativeRange),
    relativeEndOffset: Value(relativeEndOffset),
    loadOverTime: Value(loadOverTime),
    grouping: Value(grouping),
    invert: Value(invert),
    sorting: Value(sorting),
    reportText: Value(reportText),
    filters: Value(jsonEncode(filterObj)),
  );

  @override
  List<Object?> get props =>
      super.props..addAll([
        type,
        startDate,
        endDate,
        relativeEndOffset,
        relativeStartOffset,
        relativeRange,
      ]);

  @override
  Report copy([final bool newId = false]) => Report.from(this, newId);

  bool get hasCustomRange =>
      (endDate != null && startDate != null) ||
      (relativeRange != null &&
          relativeRange != ReportTimeRange.current &&
          relativeStartOffset != null &&
          relativeEndOffset != null);

  @JsonKey(includeFromJson: false, includeToJson: false)
  TZDateTimeRange? get range {
    final TZDateTime? start =
        startDate ??
        _parseRelativeDate(relativeStartOffset, relativeRange, true);
    final TZDateTime? end =
        endDate ?? _parseRelativeDate(relativeEndOffset, relativeRange, false);
    if (end == null || start == null) return null;

    return TZDateTimeRange(start: start, end: end);
  }

  set range(final TZDateTimeRange? range) {
    startDate = range?.start;
    endDate = range?.end;
  }

  TZDateTime? _parseRelativeDate(
    int? offset,
    final ReportTimeRange? range,
    final bool isStart,
  ) {
    if (offset == null || range == null || range == ReportTimeRange.current) {
      return null;
    }

    final now = tzNow();
    if (!isStart) offset += 1;

    return TZDateTime(
      local,
      range == ReportTimeRange.year ? now.year + offset : now.year,
      switch (range) {
        ReportTimeRange.month => now.month + offset,
        ReportTimeRange.day => now.month,
        _ => 1,
      },
      range == ReportTimeRange.day ? now.day + offset : 1,
    );
  }

  factory Report.fromJson(final Map<String, dynamic> json) =>
      _$ReportFromJson(json);
  Map<String, dynamic> toJson() => _$ReportToJson(this);
}

@JsonSerializable()
class ReportGroup extends Named<ReportGroup> with EquatableMixin {
  late int cellSize;

  List<ReportGroupLayout> layouts = [];

  ReportGroup({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    this.cellSize = 400,
  });

  ReportGroup.from(ReportGroup super.other, super.newId)
    : layouts = other.layouts.map((final e) => e.copy()).toList(),
      cellSize = other.cellSize,
      super.from();

  @override
  UpdateCompanion<ReportGroup> toUpdateCompanion() => ReportGroupsCompanion(
    updateDate: Value(updateDate),
    deleteDate: Value(deleteDate),
    uuid: Value(uuid),
    name: Value(name),
    cellSize: Value(cellSize),
  );

  @override
  List<Object?> get props => super.props..addAll([cellSize, layouts]);

  @override
  ReportGroup copy([final bool newId = false]) => ReportGroup.from(this, newId);

  factory ReportGroup.fromJson(final Map<String, dynamic> json) =>
      _$ReportGroupFromJson(json);
  Map<String, dynamic> toJson() => _$ReportGroupToJson(this);
}

@JsonSerializable()
class ReportGroupLayout extends Common<ReportGroupLayout> with EquatableMixin {
  List<ReportGroupRow> rows = [];

  int order;
  String uuid;

  ReportGroupLayout({this.order = -1, this.uuid = ''});

  ReportGroupLayout.from(
    final ReportGroupLayout other, [
    final bool newId = false,
  ]) : order = other.order,
       uuid = other.uuid,
       rows = other.rows.map((final e) => e.copy()).toList();

  @override
  UpdateCompanion<ReportGroupLayout> toUpdateCompanion() =>
      ReportGroupLayoutsCompanion(uuid: Value(uuid), order: Value(order));

  @override
  List<Object?> get props => [uuid, order];

  @override
  ReportGroupLayout copy([final bool newId = false]) =>
      ReportGroupLayout.from(this, newId);

  factory ReportGroupLayout.fromJson(final Map<String, dynamic> json) =>
      _$ReportGroupLayoutFromJson(json);
  Map<String, dynamic> toJson() => _$ReportGroupLayoutToJson(this);

  @override
  (String, int) get key => (uuid, order);

  @override
  bool get hasData => true;
}

@JsonSerializable()
class ReportGroupRow extends Common<ReportGroupRow> with EquatableMixin {
  int order;
  int layoutOrder;
  String uuid;
  String? primaryReport;
  String? secondaryReport;
  String? tertiaryReport;

  @JsonKey(includeFromJson: false, includeToJson: false)
  List<Report> reports = [];

  ReportGroupRow({
    this.order = -1,
    this.layoutOrder = -1,
    this.uuid = '',
    this.primaryReport,
    this.secondaryReport,
    this.tertiaryReport,
  });

  ReportGroupRow.from(final ReportGroupRow other, [final bool newId = false])
    : order = other.order,
      layoutOrder = other.layoutOrder,
      uuid = other.uuid,
      primaryReport = other.primaryReport,
      secondaryReport = other.secondaryReport,
      tertiaryReport = other.tertiaryReport,
      reports = other.reports.map((final e) => e.copy()).toList();

  @override
  UpdateCompanion<ReportGroupRow> toUpdateCompanion() =>
      ReportGroupRowsCompanion(
        uuid: Value(uuid),
        order: Value(order),
        primaryReport: Value(primaryReport),
        secondaryReport: Value(secondaryReport),
        tertiaryReport: Value(tertiaryReport),
        layoutOrder: Value(layoutOrder),
      );

  @override
  List<Object?> get props => [
    uuid,
    order,
    primaryReport,
    secondaryReport,
    tertiaryReport,
    layoutOrder,
  ];

  @override
  ReportGroupRow copy([final bool newId = false]) =>
      ReportGroupRow.from(this, newId);

  factory ReportGroupRow.fromJson(final Map<String, dynamic> json) =>
      _$ReportGroupRowFromJson(json);
  Map<String, dynamic> toJson() => _$ReportGroupRowToJson(this);

  @override
  (String, int) get key => (uuid, order);

  @override
  bool get hasData =>
      reports.isNotEmpty || (primaryReport?.isNotEmpty ?? false);
}

@JsonSerializable()
class ReportTransactionFilter {
  List<String> accountIds;
  List<String> tags;
  List<String> categoryIds;
  List<String> categoryGroupIds;
  List<String> currencyCodes;
  List<String> merchants;
  TransactionTypeFilter type;
  bool noMerchant;
  bool noTag;

  ReportTransactionFilter({
    this.accountIds = const [],
    this.categoryGroupIds = const [],
    this.categoryIds = const [],
    this.currencyCodes = const [],
    this.merchants = const [],
    this.tags = const [],
    this.type = TransactionTypeFilter.all,
    this.noMerchant = false,
    this.noTag = false,
  });

  bool hasFilter() =>
      accountIds.isNotEmpty ||
      tags.isNotEmpty ||
      categoryGroupIds.isNotEmpty ||
      categoryIds.isNotEmpty ||
      currencyCodes.isNotEmpty ||
      merchants.isNotEmpty;

  factory ReportTransactionFilter.fromJson(final Map<String, dynamic> json) =>
      _$ReportTransactionFilterFromJson(json);
  Map<String, dynamic> toJson() => _$ReportTransactionFilterToJson(this);
}
