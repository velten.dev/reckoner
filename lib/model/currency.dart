import 'package:decimal/decimal.dart';
import 'package:decimal/intl.dart';
import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import 'model.dart';

part 'currency.g.dart';

typedef CurrencyMap = Map<String, Currency>;

@JsonSerializable()
class Currency extends Named<Currency> with EquatableMixin {
  late String code;
  late String symbol;
  late int decimalDigits;
  bool isSystem;

  Currency({
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    super.uuid,
    this.code = '',
    this.symbol = '',
    this.decimalDigits = 2,
    this.isSystem = false,
  });

  Currency.from(final Currency other, final bool newId, {this.isSystem = false})
    : super.from(other, newId) {
    code = newId ? '' : other.code;
    symbol = other.symbol;
    decimalDigits = other.decimalDigits;
  }

  @override
  UpdateCompanion<Currency> toUpdateCompanion() => CurrenciesCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    name: Value(name),
    code: Value(code),
    symbol: Value(symbol),
    decimalDigits: Value(decimalDigits),
    uuid: Value(uuid),
  );

  @override
  List<Object?> get props => super.props..addAll([code, symbol, decimalDigits]);

  @override
  Currency copy([final bool newId = false]) => Currency.from(this, newId);

  factory Currency.fromJson(final Map<String, dynamic> json) =>
      _$CurrencyFromJson(json);
  Map<String, dynamic> toJson() => _$CurrencyToJson(this);

  @override
  String get displayName => name + (code.isEmpty ? '' : ' ($code)');

  String summary() => '$name ($code) ($symbol)';

  DecimalFormatter get decimalFormatter => DecimalFormatter(
    NumberFormat.currency(symbol: symbol, decimalDigits: decimalDigits),
  );

  NumberFormat get formatter =>
      NumberFormat.currency(symbol: symbol, decimalDigits: decimalDigits);

  NumberFormat get compactFormatter => NumberFormat.compactCurrency(
    symbol: symbol,
    decimalDigits: decimalDigits,
  );

  int toDatabase(String value) {
    final String decimalSep = formatter.symbols.DECIMAL_SEP;
    value = value.replaceAll(RegExp('[^-0-9$decimalSep]'), '');

    final decimal = decimalFormatter.tryParse(value) ?? Decimal.zero;

    return (decimal * Decimal.ten.pow(decimalDigits).toDecimal())
        .toBigInt()
        .toInt();
  }

  Decimal amountToDecimal(final int amount) =>
      (Decimal.fromInt(amount) / Decimal.ten.pow(decimalDigits).toDecimal())
          .toDecimal();

  String formatAmount(final int amount) =>
      decimalFormatter.format(amountToDecimal(amount));

  @override
  Object get key => code;
}

mixin CurrencyMixin {
  String get currencyCode => currency.code;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Currency currency = Currency();
}

mixin DefaultCurrencyMixin {
  String? get defaultCurrencyCode =>
      currency?.code.isEmpty ?? true ? null : currency!.code;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Currency? currency;
}

final Map<String, Currency> defaultCurrencies = Map.unmodifiable({
  'USD': Currency(
    name: 'United States Dollar',
    code: 'USD',
    symbol: '\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  'CAD': Currency(
    name: 'Canadian Dollar',
    code: 'CAD',
    symbol: 'C\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  'MXN': Currency(
    name: 'Mexican Dollar',
    code: 'MXN',
    symbol: 'M\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  'BRL': Currency(
    name: 'Real',
    code: 'BRL',
    symbol: 'R\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  'EUR': Currency(
    name: 'Euro',
    code: 'EUR',
    symbol: '€',
    decimalDigits: 2,
    isSystem: true,
  ),
  'GBP': Currency(
    name: 'Great British Pound',
    code: 'GBP',
    symbol: '£',
    decimalDigits: 2,
    isSystem: true,
  ),
  'RUB': Currency(
    name: 'Russian Rubel',
    code: 'RUB',
    symbol: '₽',
    decimalDigits: 2,
    isSystem: true,
  ),
  'JPY': Currency(
    name: 'Japanese Yen',
    code: 'JPY',
    symbol: 'J¥',
    decimalDigits: 0,
    isSystem: true,
  ),
  'CNY': Currency(
    name: 'Chinese Yuan',
    code: 'CNY',
    symbol: 'C¥',
    decimalDigits: 2,
    isSystem: true,
  ),
  'AUC': Currency(
    name: 'Australian Dollar',
    code: 'AUD',
    symbol: 'A\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  'NZD': Currency(
    name: 'New Zeland Dollar',
    code: 'NZD',
    symbol: 'N\$',
    decimalDigits: 2,
    isSystem: true,
  ),
  // TODO: Re-add cryptocurrencies when we can support currencies with a lot of decimal places
  // 'XBT': Currency(
  //   name: 'Bitcoin',
  //   code: 'XBT',
  //   symbol: '₿',
  //   decimalDigits: 8,
  //   isSystem: true,
  // ),
  // 'XMR': Currency(
  //   name: 'Monero',
  //   code: 'XMR',
  //   // Copied from
  //   //https://github.com/yonilevy/crypto-currency-symbols/blob/master/symbols.json
  //   symbol: 'ɱ',
  //   decimalDigits: 12,
  //   isSystem: true,
  // ),
  // 'ETH': Currency(
  //   name: 'Etherium',
  //   code: 'ETH',
  //   symbol: 'Ξ',
  //   decimalDigits: 18,
  //   isSystem: true,
  // ),
});
