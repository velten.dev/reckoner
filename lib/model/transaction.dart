import 'package:drift/drift.dart' show UpdateCompanion, Value;
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../database/reckoner_db.dart';
import 'account.dart';
import 'category.dart';
import 'currency.dart';
import 'model.dart';
import 'tz_date_time.dart';
import 'util/json_converters.dart';

part 'transaction.g.dart';

@JsonSerializable()
class Location extends Common<Location>
    with EquatableMixin
    implements NamedInterface {
  String address;
  String city;
  String region;
  String zip;
  @override
  String name;
  String uuid;
  double? latitude;
  double? longitude;

  Location({
    final String? uuid,
    this.name = '',
    this.address = '',
    this.city = '',
    this.region = '',
    this.zip = '',
    this.latitude,
    this.longitude,
  }) : uuid = uuid ?? generateUuid();

  Location.from(final Location other, final bool newId)
    : address = other.address,
      city = other.city,
      region = other.region,
      zip = other.zip,
      name = other.name,
      uuid = newId ? '' : other.uuid,
      latitude = other.latitude,
      longitude = other.longitude;

  @override
  bool get hasData =>
      name.isNotEmpty &&
      address.isNotEmpty &&
      city.isNotEmpty &&
      region.isNotEmpty &&
      latitude != null &&
      longitude != null;

  @override
  UpdateCompanion<Location> toUpdateCompanion() => LocationsCompanion(
    uuid: Value(uuid),
    name: Value(name),
    address: Value(address),
    city: Value(city),
    region: Value(region),
    zip: Value(zip),
    latitude: Value(latitude),
    longitude: Value(longitude),
  );

  @override
  List<Object?> get props => [
    address,
    city,
    region,
    zip,
    name,
    latitude,
    longitude,
  ];

  @override
  Location copy([final bool newId = false]) => Location.from(this, newId);

  factory Location.fromJson(final Map<String, dynamic> json) =>
      _$LocationFromJson(json);
  Map<String, dynamic> toJson() => _$LocationToJson(this);

  @override
  Object get key => uuid;

  @override
  String get displayName => name;
}

@JsonSerializable()
class TransactionEntity {
  TransactionTransfer? transfer;
  Transaction? transaction;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Transaction get primaryTx => transaction ?? transfer!.source;

  @JsonKey(includeFromJson: false, includeToJson: false)
  bool get isTransfer => transfer != null;

  @JsonKey(includeFromJson: false, includeToJson: false)
  String get uuid => primaryTx.uuid;

  @JsonKey(includeFromJson: false, includeToJson: false)
  bool get isNew => primaryTx.isNew;

  int get amount => isTransfer ? 0 : primaryTx.amount;

  String get sourceName =>
      isTransfer ? transfer!.source.account.name : primaryTx.sourceName;

  String get destinationName =>
      isTransfer
          ? transfer!.destination.account.name
          : primaryTx.destinationName;

  TransactionEntity({this.transaction, this.transfer})
    : assert((transaction == null) != (transfer == null));

  TransactionEntity.from(final TransactionEntity other, final bool newId)
    : transaction = other.transaction?.copy(newId),
      transfer = other.transfer?.copy(newId);

  TransactionEntity copy([final bool newId = false]) =>
      TransactionEntity.from(this, newId);

  factory TransactionEntity.fromJson(final Map<String, dynamic> json) =>
      _$TransactionEntityFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionEntityToJson(this);

  @override
  bool operator ==(final Object other) {
    if (identical(this, other)) return true;
    if (other is! TransactionEntity) return false;
    return other.transaction == transaction && other.transfer == transfer;
  }

  @override
  int get hashCode => (transaction?.hashCode ?? 0) + (transfer?.hashCode ?? 0);
}

@JsonSerializable()
class TransactionTransfer extends Common<TransactionTransfer>
    with EquatableMixin
    implements NamedInterface {
  Transaction source;
  Transaction destination;

  TransactionTransfer({
    final String sourceUuid = '',
    final String destinationUuid = '',
  }) : source = Transaction(uuid: sourceUuid),
       destination = Transaction(uuid: destinationUuid);

  TransactionTransfer.from(final TransactionTransfer other, final bool newId)
    : source = other.source.copy(newId),
      destination = other.destination.copy(newId);

  @override
  TransactionTransfer copy([final bool newId = false]) =>
      TransactionTransfer.from(this, newId);

  @override
  bool get hasData => source.hasData && destination.hasData;

  @override
  UpdateCompanion<TransactionTransfer> toUpdateCompanion() =>
      TransactionTransfersCompanion(
        sourceUuid: Value(source.uuid),
        destinationUuid: Value(destination.uuid),
      );

  @override
  Object get key => (source.uuid, destination.uuid);

  factory TransactionTransfer.fromJson(final Map<String, dynamic> json) =>
      _$TransactionTransferFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionTransferToJson(this);

  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  String get name => source.name;
  @override
  set name(final String value) {
    source.name = name;
    destination.name = name;
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  Map<String, Category> get categories => source.splits.first.categories;
  set categories(final Map<String, Category> value) {
    source.splits.first.categories = value;
    destination.splits.first.categories = value;
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  Set<String> get tags => source.splits.first.tags;
  set tags(final Set<String> value) {
    source.splits.first.tags = value;
    destination.splits.first.tags = value;
  }

  @override
  String get displayName => source.displayName;

  //TODO: Remove when https://github.com/simolus3/drift/issues/3173 is fixed
  String get sourceUuid => source.uuid;
  String get destinationUuid => destination.uuid;

  @override
  List<Object?> get props => [...source.props, ...destination.props];
}

@JsonSerializable(converters: [TZDateTimeConverter()])
class Transaction extends Named<Transaction>
    with EquatableMixin, CurrencyMixin
    implements AmountInterface {
  late TZDateTime date;
  @override
  int amount;
  String merchantName;
  @Deprecated('Use merchant instead')
  String? merchantUuid;

  String? get accountUuid => _account?.uuid;
  Account? _account;
  @JsonKey(includeFromJson: false, includeToJson: false)
  Account get account => _account ?? Account();
  set account(final Account? value) {
    _account = value;
    if (_account != null &&
        !_account!.isMultiCurrency &&
        _account!.currency != null) {
      currency = _account!.currency!;
    }
  }

  String get sourceName => amount > 0 ? merchantName : account.displayName;
  String get destinationName => amount > 0 ? account.displayName : merchantName;

  List<TransactionSplit> splits = [];
  Location? location;

  Transaction({
    super.uuid,
    super.createDate,
    super.updateDate,
    super.deleteDate,
    super.name,
    this.amount = 0,
    this.merchantUuid,
    final String? merchantName,
    final DateTime? date,
    final String? currencyCode,
    final String? accountUuid,
    final String? locationUuid,
  }) : date = tzFromDtNow(date),
       merchantName = merchantName ?? '' {
    if (accountUuid != null) _account = Account(uuid: accountUuid);
    currency.code = currencyCode ?? '';
    if (splits.isEmpty) {
      splits.add(TransactionSplit(uuid: uuid, name: name, amount: amount));
    }
    if (locationUuid != null) location = Location(uuid: locationUuid);
  }

  Transaction.from(final Transaction other, final bool newId)
    : date = other.date,
      amount = other.amount,
      _account = other._account?.copy(),
      splits = copyList(other.splits),
      merchantName = other.merchantName,
      super.from(other, newId) {
    currency = other.currency.copy();
  }

  @override
  UpdateCompanion<Transaction> toUpdateCompanion() => TransactionsCompanion(
    deleteDate: Value(deleteDate),
    updateDate: Value(updateDate),
    uuid: Value(uuid),
    name: Value(name),
    date: Value(date),
    amount: Value(amount),
    accountUuid: Value.absentIfNull(_account?.uuid),
    currencyCode: Value(currencyCode),
    // ignore: deprecated_member_use_from_same_package
    merchantUuid: Value(merchantUuid),
    merchantName:
        merchantName.isEmpty ? const Value(null) : Value(merchantName),
    locationUuid: Value(location?.uuid),
  );

  @override
  List<Object?> get props =>
      super.props..addAll([
        date,
        amount,
        currencyCode,
        _account?.uuid,
        merchantName,
        ...splits
            .map((final s) => s.props)
            .fold([], (final val, final next) => [...val, ...next]),
      ]);

  @override
  Transaction copy([final bool newId = false]) => Transaction.from(this, newId);

  factory Transaction.fromJson(final Map<String, dynamic> json) =>
      _$TransactionFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionToJson(this);

  @override
  String get key => uuid;
}

@JsonSerializable()
class TransactionSplit extends Common<TransactionSplit>
    with EquatableMixin
    implements IdItem<TransactionSplit>, NamedInterface, AmountInterface {
  @override
  String uuid;
  @override
  int id;
  @override
  int amount;
  @override
  String name;

  Map<String, Category> categories = {};
  Set<String> tags = {};

  TransactionSplit({
    required this.uuid,
    this.id = 0,
    this.name = '',
    this.amount = 0,
  });

  TransactionSplit.from(final TransactionSplit other, final bool newId)
    : uuid = other.uuid,
      amount = other.amount,
      name = other.name,
      id = 0,
      categories = Map.from(other.categories),
      tags = Set.from(other.tags) {
    copyIdItemId(other, this, newId);
  }

  @override
  UpdateCompanion<TransactionSplit> toUpdateCompanion() =>
      TransactionSplitsCompanion(
        id: Value(id),
        name: Value(name),
        uuid: Value(uuid),
        amount: Value(amount),
      );

  @override
  List<Object?> get props => [name, amount, ...categories.keys, ...tags];

  factory TransactionSplit.fromJson(final Map<String, dynamic> json) =>
      _$TransactionSplitFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionSplitToJson(this);

  @override
  TransactionSplit copy([final bool newId = false]) =>
      TransactionSplit.from(this, newId);

  @override
  (String, int) get key => (uuid, id);

  @override
  String get parentKey => uuid;

  @override
  bool get hasData => name.isNotEmpty;

  @override
  String get displayName => name;
}

@JsonSerializable()
class TransactionCategory extends Common<TransactionCategory>
    with EquatableMixin {
  String uuid;
  int splitId;

  String get categoryUuid => category.uuid;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Category category = Category(groupUuid: '');

  TransactionCategory({
    final String categoryUuid = '',
    required this.splitId,
    required this.uuid,
  }) {
    category.uuid = categoryUuid;
  }

  TransactionCategory.from(final TransactionCategory other, final bool newId)
    : uuid = other.uuid,
      splitId = other.splitId {
    category = other.category;
  }

  @override
  TransactionCategory copy([final bool newId = false]) =>
      TransactionCategory.from(this, newId);

  @override
  UpdateCompanion<TransactionCategory> toUpdateCompanion() =>
      TransactionCategoriesCompanion(
        uuid: Value(uuid),
        categoryUuid: Value(categoryUuid),
        splitId: Value(splitId),
      );

  @override
  List<Object?> get props => [uuid, splitId, categoryUuid];

  factory TransactionCategory.fromJson(final Map<String, dynamic> json) =>
      _$TransactionCategoryFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionCategoryToJson(this);

  @override
  (String, int, String) get key => (uuid, splitId, categoryUuid);

  @override
  (String, int) get parentKey => (uuid, splitId);

  @override
  bool get hasData => categoryUuid.isNotEmpty;
}

@JsonSerializable()
class TransactionTag extends Common<TransactionTag> with EquatableMixin {
  String uuid;
  String tag;
  int splitId;

  TransactionTag({
    required this.uuid,
    required this.splitId,
    required this.tag,
  });

  TransactionTag.from(final TransactionTag other, final bool newId)
    : uuid = other.uuid,
      splitId = other.splitId,
      tag = other.tag;

  @override
  TransactionTag copy([final bool newId = false]) =>
      TransactionTag.from(this, newId);

  @override
  UpdateCompanion<TransactionTag> toUpdateCompanion() =>
      TransactionTagsCompanion(
        uuid: Value(uuid),
        splitId: Value(splitId),
        tag: Value(tag),
      );

  @override
  List<Object?> get props => [uuid, splitId, tag];

  factory TransactionTag.fromJson(final Map<String, dynamic> json) =>
      _$TransactionTagFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionTagToJson(this);

  @override
  (String, int, String) get key => (uuid, splitId, tag);

  @override
  (String, int) get parentKey => (uuid, splitId);

  @override
  bool get hasData => tag.isNotEmpty;
}
