import 'package:pocketbase/pocketbase.dart';

PocketBase getPBInstance(final String url) => PocketBase(url);
