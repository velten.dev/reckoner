import 'dart:convert';
import 'dart:typed_data';

import 'package:webcrypto/webcrypto.dart' as wc;

import '../../../model/encrypted.dart';

Future<wc.AesCbcSecretKey> _passwordToKey(final String password) async {
  final hash = await wc.Hash.sha256.digestBytes(utf8.encode(password));
  return wc.AesCbcSecretKey.importRawKey(hash);
}

Future<EncryptedData> aesEncrypt(
  final String secret,
  final Object object,
) async {
  final secretKey = await _passwordToKey(secret);
  final clearText = utf8.encode(jsonEncode(object));
  final iv = Uint8List(16);
  wc.fillRandomBytes(iv);
  final cipher = await secretKey.encryptBytes(clearText, iv);
  return EncryptedData(aes: AESEncryptedData.fromBytes(cipher, iv));
}

Future<Map<String, dynamic>?> aesDecrypt(
  final String secret,
  final AESEncryptedData data,
) async {
  final secretKey = await _passwordToKey(secret);
  final cipher = base64Decode(data.cipherText).toList();
  final iv = base64Decode(data.nonce).toList();
  try {
    final bytes = await secretKey.decryptBytes(cipher, iv);
    final jsonString = utf8.decode(bytes);
    return jsonDecode(jsonString) as Map<String, dynamic>;
  } catch (_) {
    return null;
  }
}
