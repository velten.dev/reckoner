import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:drift/drift.dart';
import 'package:drift/remote.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart' show debugPrint, protected;

import '../../database/reckoner_db.dart';
import '../../model/account.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/device.dart';
import '../../model/merchant.dart';
import '../../model/model.dart';
import '../../model/report.dart';
import '../../model/secure_storage.dart';
import '../../model/settings.dart';
import '../../model/transaction.dart';
import '../logger.dart';
import 'encryption.dart';

enum BundleType {
  organization,
  setting,
  category,
  transaction,
  categoryBudget,
  @Deprecated('No longer used')
  merchant,
}

DateTime? chooseDate(final DateTime? current, final DateTime? previous) {
  if (current != null && previous != null) {
    if (current.isBefore(previous)) return previous;
    return current;
  } else if (current != null) {
    return current;
  } else if (previous != null) {
    return previous;
  } else {
    return null;
  }
}

bool _compareList<T extends Tracked<T>>(
  final List<T> current,
  final List<T> other,
) {
  for (final item in current) {
    final otherItem = other.singleWhereOrNull((final e) => e.uuid == item.uuid);
    if (otherItem == null) return true;
    if (item.updateDate.compareTo(otherItem.updateDate) > 0) return true;
  }
  return false;
}

class SyncData {
  List<SyncBundle> bundles;

  SyncData(this.bundles);

  bool canSave() =>
      bundles.fold(true, (final val, final bundle) => val && bundle.canSave());

  static Future<bool> hasData(
    final DateTime? syncDate, [
    final bool? timeInvariant,
  ]) async {
    if (timeInvariant == null || timeInvariant) {
      if (await SettingSyncBundle.hasData(syncDate)) return true;
      if (await CategorySyncBundle.hasData(syncDate)) return true;
      if (await OrganizationSyncBundle.hasData(syncDate)) return true;
      // if (await MerchantSyncBundle.hasData(syncDate)) return true;
    }
    if (timeInvariant == null || !timeInvariant) {
      if (await TransactionSyncBundle.hasData(syncDate)) return true;
      if (await CategoryBudgetSyncBundle.hasData(syncDate)) return true;
    }
    return false;
  }

  static Future<List<SyncBundle>> _tiLoad(final DateTime? syncDate) async => [
    if (await SettingSyncBundle.hasData(syncDate))
      await SettingSyncBundle.load(),
    ...await CategorySyncBundle.load(syncDate),
    ...await OrganizationSyncBundle.load(syncDate),
    // ...await MerchantSyncBundle.load(syncDate),
  ];

  static Future<List<SyncBundle>> _otLoad(final DateTime? syncDate) async => [
    ...await TransactionSyncBundle.load(syncDate),
    ...await CategoryBudgetSyncBundle.load(syncDate),
  ];

  static Future<SyncData> loadTimeInvariant(final DateTime? syncDate) async =>
      SyncData(await _tiLoad(syncDate));

  static Future<SyncData> loadOverTime(final DateTime? syncDate) async =>
      SyncData(await _otLoad(syncDate));

  static Future<SyncData> loadAll(final DateTime? syncDate) async =>
      SyncData([...await _tiLoad(syncDate), ...await _otLoad(syncDate)]);

  void addBundle(final SyncBundle bundle) => bundles.add(bundle);
  void addBundles(final Iterable<SyncBundle> list) => bundles.addAll(list);

  Future<({DateTime? date, List<String> syncIds})> saveLocal(
    final bool firstSync,
  ) async {
    final db = ReckonerDb.I;
    final saveSyncIds = <String>[];
    DateTime? syncDate;

    final settingBundle =
        bundles.singleWhereOrNull((final b) => b is SettingSyncBundle)
            as SettingSyncBundle?;

    if (settingBundle != null) {
      // Don't add if schema versions don't match
      if (db.schemaVersion == settingBundle.schemaVersion) {
        saveSyncIds.add(settingBundle.syncId);
      }
      syncDate = await settingBundle.savePrivate(firstSync);
      bundles.remove(settingBundle);
    }

    await db.transaction(() async {
      for (final bundle in bundles) {
        final bundleSyncDate = await bundle.savePrivate(firstSync);
        if (bundleSyncDate != null) {
          saveSyncIds.add(bundle.syncId);
          syncDate = chooseDate(syncDate, bundleSyncDate);
        }
      }
    });

    return (date: syncDate, syncIds: saveSyncIds);
  }

  static SyncData fromJson(final Map<String, dynamic> json) => SyncData(
    (json['bundles'] as List<dynamic>)
            .map((final e) => SyncBundle.fromJson(e as Map<String, dynamic>))
            .where((final element) => element != null)
            .toList()
        as List<SyncBundle>,
  );

  Map<String, dynamic> toJson() => {
    'bundles': bundles.map((final e) => e.toJson()).toList(),
  };

  Future<DateTime?> send(
    final Future<bool> Function(ReckonerDb, SyncBundle) sendFn, [
    final List<String> savedRemoteSyncIds = const [],
    final int sliceSize = 10,
  ]) async {
    final db = ReckonerDb.I;
    final sendBundles = bundles
        .where((final b) => !savedRemoteSyncIds.contains(b.syncId))
        .slices(sliceSize);

    DateTime? updateDate;

    for (final bundle in sendBundles) {
      final updateDates = await Future.wait(
        bundle.map(
          (final e) async => await sendFn(db, e) ? e.updateDate : null,
        ),
      );
      updateDate = updateDates.fold<DateTime?>(
        updateDate,
        (final prev, final cur) => chooseDate(prev, cur),
      );
    }

    return updateDate;
  }
}

abstract class SyncBundle {
  const SyncBundle();

  static SyncBundle? fromJson(final Map<String, dynamic> json) {
    try {
      final type = EnumToString.fromString(
        BundleType.values,
        json['type'] as String,
      );
      if (type == null) return null;
      return switch (type) {
        BundleType.organization => OrganizationSyncBundle.fromJson(json),
        BundleType.setting => SettingSyncBundle.fromJson(json),
        BundleType.category => CategorySyncBundle.fromJson(json),
        BundleType.transaction => TransactionSyncBundle.fromJson(json),
        BundleType.categoryBudget => CategoryBudgetSyncBundle.fromJson(json),
        // ignore: deprecated_member_use_from_same_package
        BundleType.merchant => MerchantSyncBundle.fromJson(json),
      };
    } catch (err, stack) {
      logError('Reckoner', err, stack);
      debugPrint(err.toString());
      return null;
    }
  }

  bool get isOverTime;
  BundleType get type;
  String get syncId;
  set syncId(final String id) {
    assert(id == syncId);
  }

  Map<String, dynamic> toJson();
  DateTime get updateDate;
  bool canSave() => true;

  Map<String, dynamic> _addType(final Map<String, dynamic> json) => {
    ...json,
    'type': EnumToString.convertToString(type),
  };

  @protected
  Future<DateTime?> savePrivate(final bool firstSync);

  bool? hasNewerData(final SyncBundle other) {
    if (type != other.type) return null;
    return updateDate.compareTo(other.updateDate) > 0;
  }

  Future<String> toServerData(
    final EncryptionSettings encryptionSettings,
  ) async {
    if (encryptionSettings.type == EncryptionType.none) {
      return jsonEncode(this);
    }
    final key = await encryptionSettings.encryptionKey;
    return encryptObject(key, this);
  }

  static Future<SyncBundle?> fromServerData(
    final EncryptionSettings encryptionSettings,
    final String data,
    final String id,
  ) async {
    SyncBundle? bundle;
    if (encryptionSettings.type == EncryptionType.none) {
      final json = jsonDecode(data) as Map<String, dynamic>;
      bundle = SyncBundle.fromJson(json);
    } else {
      final key = await encryptionSettings.encryptionKey;

      bundle = await decryptObject(key, SyncBundle.fromJson, data);
    }
    bundle?.syncId = id;
    return bundle;
  }

  static Future<bool> canRead(final String data, final String key) async {
    try {
      SyncBundle? bundle;
      if (key.isEmpty) {
        final json = jsonDecode(data) as Map<String, dynamic>;
        bundle = SyncBundle.fromJson(json);
      } else {
        bundle = await decryptObject(key, SyncBundle.fromJson, data);
      }
      return bundle != null;
    } catch (_) {
      return false;
    }
  }
}

class OrganizationSyncBundle extends SyncBundle {
  final Organization organization;

  const OrganizationSyncBundle(this.organization);

  @override
  bool get isOverTime => false;
  @override
  BundleType get type => BundleType.organization;

  @override
  @protected
  Future<DateTime?> savePrivate(final bool firstSync) async {
    final db = ReckonerDb.I;
    return chooseDate(
      (await db.commonDao.saveSyncItem(organization, firstSync))?.updateDate,
      await db.commonDao.saveSyncItemList(organization.accounts, firstSync),
    );
  }

  static OrganizationSyncBundle fromJson(final Map<String, dynamic> json) =>
      OrganizationSyncBundle(
        Organization.fromJson(json['organization'] as Map<String, dynamic>),
      );

  @override
  Map<String, dynamic> toJson() =>
      _addType({'organization': organization.toJson()});

  static Future<List<OrganizationSyncBundle>> load(
    final DateTime? syncDate,
  ) async =>
      (await ReckonerDb.I.accountDao.getSyncOrganizations(
        syncDate,
      )).map((final e) => OrganizationSyncBundle(e)).toList();

  @override
  String get syncId => organization.uuid;

  @override
  DateTime get updateDate => organization.updateDate;

  static Future<bool> hasData(final DateTime? syncDate) async =>
      await ReckonerDb.I.commonDao.getDbSyncCount<Organization>(syncDate) > 0;

  @override
  bool? hasNewerData(final SyncBundle other) {
    if (other is! OrganizationSyncBundle) return null;
    if (super.hasNewerData(other) ?? false) return true;
    if (_compareList(organization.accounts, other.organization.accounts)) {
      return true;
    }
    return false;
  }
}

class SettingSyncBundle extends SyncBundle {
  final List<Device> devices;
  final List<Currency> currencies;
  final UserSetting setting;
  final List<ReportGroup> reportGroups;
  final List<Report> reports;
  final int schemaVersion;

  const SettingSyncBundle({
    required this.devices,
    required this.currencies,
    required this.setting,
    required this.reportGroups,
    required this.reports,
    this.schemaVersion = 1,
  });

  @override
  bool get isOverTime => false;

  @override
  @protected
  Future<DateTime?> savePrivate(final bool firstSync) async {
    final db = ReckonerDb.I;
    final date = [
      await db.commonDao.saveSyncItemList(reports, firstSync),
      await db.commonDao.saveSyncItemList(reportGroups, firstSync),
      await db.commonDao.saveSyncItemList(devices, firstSync),
      await db.commonDao.saveSyncItemList(currencies, firstSync),
      (await db.commonDao.saveSyncItem(setting, firstSync))?.updateDate,
    ].fold<DateTime?>(null, chooseDate);
    // Force newer update date if database is newer version
    if (schemaVersion < db.schemaVersion) {
      await db
          .update(db.userSettings)
          .write(UserSettingsCompanion(updateDate: Value(DateTime.now())));
    }
    return date;
  }

  @override
  BundleType get type => BundleType.setting;

  static SettingSyncBundle fromJson(final Map<String, dynamic> json) =>
      SettingSyncBundle(
        devices:
            (json['devices'] as List<dynamic>?)
                ?.map((final e) => Device.fromJson(e as Map<String, dynamic>))
                .toList() ??
            [],
        currencies:
            (json['currencies'] as List<dynamic>?)
                ?.map((final e) => Currency.fromJson(e as Map<String, dynamic>))
                .toList() ??
            [],
        setting: UserSetting.fromJson(json['setting'] as Map<String, dynamic>),
        reportGroups:
            (json['reportGroups'] as List<dynamic>?)
                ?.map(
                  (final e) => ReportGroup.fromJson(e as Map<String, dynamic>),
                )
                .toList() ??
            [],
        reports:
            (json['reports'] as List<dynamic>?)
                ?.map((final e) => Report.fromJson(e as Map<String, dynamic>))
                .toList() ??
            [],
        schemaVersion: json['schemaVersion'] as int? ?? 1,
      );

  @override
  Map<String, dynamic> toJson() => _addType({
    'devices': devices.map((final e) => e.toJson()).toList(),
    'currencies': currencies.map((final e) => e.toJson()).toList(),
    'setting': setting.toJson(),
    'reportGroups': reportGroups.map((final e) => e.toJson()).toList(),
    'reports': reports.map((final e) => e.toJson()).toList(),
    'schemaVersion': schemaVersion,
  });

  static Future<SettingSyncBundle> load() async {
    final db = ReckonerDb.I;
    return SettingSyncBundle(
      devices: await db.commonDao.getDbSyncItems(null),
      setting: (await db.commonDao.getDbItems<UserSetting>()).first,
      currencies: await db.commonDao.getDbSyncItems(null),
      reportGroups: await db.reportDao.getSyncReportGroups(null),
      reports: await db.commonDao.getDbSyncItems(null),
      schemaVersion: db.schemaVersion,
    );
  }

  @override
  String get syncId => setting.uuid;

  @override
  DateTime get updateDate => [
    ...devices.map((final d) => d.updateDate),
    ...currencies.map((final c) => c.updateDate),
    ...reportGroups.map((final g) => g.updateDate),
    ...reports.map((final r) => r.updateDate),
  ].fold(
    setting.updateDate,
    (final prevDate, final newDate) => chooseDate(newDate, prevDate)!,
  );

  static Future<bool> hasData(final DateTime? syncDate) async {
    final db = ReckonerDb.I;
    return (await db.commonDao.getDbSyncCount<Currency>(syncDate) +
            await db.commonDao.getDbSyncCount<Device>(syncDate) +
            await db.commonDao.getDbSyncCount<ReportGroup>(syncDate) +
            await db.commonDao.getDbSyncCount<Report>(syncDate) +
            await db.commonDao.getDbSyncCount<UserSetting>(syncDate)) >
        0;
  }

  @override
  bool? hasNewerData(final SyncBundle other) {
    if (other is! SettingSyncBundle) return null;
    if (super.hasNewerData(other) ?? false) return true;
    if (_compareList(devices, other.devices)) return true;
    if (_compareList(currencies, other.currencies)) return true;
    if (_compareList(reportGroups, other.reportGroups)) return true;
    if (_compareList(reports, other.reports)) return true;
    if (schemaVersion > other.schemaVersion) return true;
    return false;
  }

  @override
  bool canSave() => ReckonerDb.I.schemaVersion >= schemaVersion;
}

class CategorySyncBundle extends SyncBundle {
  final CategoryGroup group;

  const CategorySyncBundle(this.group);

  @override
  bool get isOverTime => false;

  @override
  @protected
  Future<DateTime?> savePrivate(final bool firstSync) async {
    final db = ReckonerDb.I;
    var updateDate =
        (await db.commonDao.saveSyncItem(group, firstSync))?.updateDate;
    final List<Category> categories = List.from(group.categories);
    final List<String> savedCategories = [];
    while (categories.isNotEmpty) {
      for (final cat in categories) {
        if (cat.parentUuid == null ||
            savedCategories.contains(cat.parentUuid)) {
          updateDate = chooseDate(
            (await db.commonDao.saveSyncItem(cat, firstSync))?.updateDate,
            updateDate,
          );
          savedCategories.add(cat.uuid);
        }
      }
      categories.removeWhere((final cat) => savedCategories.contains(cat.uuid));
    }

    return updateDate;
  }

  @override
  BundleType get type => BundleType.category;

  static CategorySyncBundle fromJson(final Map<String, dynamic> json) =>
      CategorySyncBundle(
        CategoryGroup.fromJson(json['categoryGroup'] as Map<String, dynamic>),
      );

  @override
  Map<String, dynamic> toJson() => _addType({'categoryGroup': group.toJson()});

  static Future<List<CategorySyncBundle>> load(
    final DateTime? syncDate,
  ) async =>
      (await ReckonerDb.I.categoryDao.getSyncCategoryGroups(
        syncDate,
      )).map((final e) => CategorySyncBundle(e)).toList();

  @override
  String get syncId => group.uuid;

  @override
  DateTime get updateDate => group.updateDate;

  static Future<bool> hasData(final DateTime? syncDate) async =>
      await ReckonerDb.I.commonDao.getDbSyncCount<CategoryGroup>(syncDate) > 0;

  @override
  bool? hasNewerData(final SyncBundle other) {
    if (other is! CategorySyncBundle) return null;
    if (super.hasNewerData(other) ?? false) return true;
    if (_compareList(group.categories, other.group.categories)) {
      return true;
    }
    return false;
  }
}

class TransactionSyncBundle extends SyncBundle {
  final TransactionEntity txEntity;

  const TransactionSyncBundle(this.txEntity);

  @override
  bool get isOverTime => true;

  @override
  @protected
  Future<DateTime?> savePrivate(
    final bool firstSync, [
    final bool retry = true,
  ]) async {
    final db = ReckonerDb.I;
    try {
      if (txEntity.transaction != null) {
        return (await db.commonDao.saveSyncItem(
          txEntity.transaction!,
          firstSync,
        ))?.updateDate;
      }
      final date = chooseDate(
        (await db.commonDao.saveSyncItem(
          txEntity.transfer!.source,
          firstSync,
        ))?.updateDate,
        (await db.commonDao.saveSyncItem(
          txEntity.transfer!.destination,
          firstSync,
        ))?.updateDate,
      );

      await db.commonDao.saveDbItem(txEntity.transfer!);
      return date;
    } on DriftRemoteException catch (err, stack) {
      final cause = err.remoteCause.toString();
      // Foreign key exception. May be cause by out of order
      // messages where the transaction is received before the
      // dependent piece of data.
      if (cause.contains('SqliteException(787)') && retry) {
        return Future.delayed(
          const Duration(seconds: 10),
          () => savePrivate(firstSync, false),
        );
      } else {
        logError('Reckoner', err, stack);
      }
    } catch (err, stack) {
      logError('Reckoner', err, stack);
    }
    return null;
  }

  @override
  BundleType get type => BundleType.transaction;

  static TransactionSyncBundle fromJson(final Map<String, dynamic> json) =>
      TransactionSyncBundle(
        TransactionEntity.fromJson(json['txEntity'] as Map<String, dynamic>),
      );

  @override
  Map<String, dynamic> toJson() => _addType({'txEntity': txEntity.toJson()});

  static Future<List<TransactionSyncBundle>> load(
    final DateTime? syncDate,
  ) async =>
      (await ReckonerDb.I.transactionDao.getTransactionEntitiesForSync(
        syncDate,
      )).map((final e) => TransactionSyncBundle(e)).toList();

  @override
  String get syncId =>
      txEntity.transaction?.uuid ?? txEntity.transfer!.source.uuid;

  @override
  DateTime get updateDate =>
      txEntity.transaction?.updateDate ??
      chooseDate(
        txEntity.transfer!.source.updateDate,
        txEntity.transfer!.destination.updateDate,
      )!;

  static Future<bool> hasData(final DateTime? syncDate) async =>
      await ReckonerDb.I.commonDao.getDbSyncCount<Transaction>(syncDate) > 0;
}

class CategoryBudgetSyncBundle extends SyncBundle {
  final List<CategoryBudget> limitDates;

  const CategoryBudgetSyncBundle(this.limitDates);

  @override
  bool get isOverTime => true;

  @override
  @protected
  Future<DateTime?> savePrivate(final bool firstSync) =>
      ReckonerDb.I.commonDao.saveSyncItemList(limitDates, firstSync);

  @override
  BundleType get type => BundleType.categoryBudget;

  static CategoryBudgetSyncBundle fromJson(final Map<String, dynamic> json) =>
      CategoryBudgetSyncBundle(
        (json['budgets'] as List<dynamic>?)
                ?.map(
                  (final e) =>
                      CategoryBudget.fromJson(e as Map<String, dynamic>),
                )
                .toList() ??
            [],
      );

  @override
  Map<String, dynamic> toJson() =>
      _addType({'budgets': limitDates.map((final e) => e.toJson()).toList()});

  static Future<List<CategoryBudgetSyncBundle>> load(
    final DateTime? syncDate,
  ) async =>
      (await ReckonerDb.I.categoryDao.getSyncLimitDates(
        syncDate,
      )).values.map((final e) => CategoryBudgetSyncBundle(e)).toList();

  @override
  String get syncId {
    String? syncId = limitDates.fold(
      null,
      (final previousValue, final element) => previousValue ?? element.syncId,
    );
    syncId ??= generateUuid();
    final nullIdList = <String>[];

    for (final limit in limitDates) {
      if (limit.syncId == null) {
        nullIdList.add(limit.uuid);
        limit.syncId = syncId;
      }
    }

    ReckonerDb.I.categoryDao.setBudgetSyncId(nullIdList, syncId);

    return syncId;
  }

  @override
  set syncId(final String id) {
    for (final date in limitDates) {
      date.syncId = id;
    }
  }

  @override
  DateTime get updateDate => limitDates.fold(
    DateTime(0),
    (final previousValue, final element) =>
        previousValue.isBefore(element.updateDate)
            ? element.updateDate
            : previousValue,
  );

  static Future<bool> hasData(final DateTime? syncDate) async =>
      await ReckonerDb.I.commonDao.getDbSyncCount<CategoryBudget>(syncDate) > 0;

  @override
  bool? hasNewerData(final SyncBundle other) {
    if (other is! CategoryBudgetSyncBundle) return null;
    if (_compareList(limitDates, other.limitDates)) return true;
    return false;
  }
}

@Deprecated('Will be removed in future releases')
class MerchantSyncBundle extends SyncBundle {
  final List<Merchant> merchants;

  const MerchantSyncBundle(this.merchants);

  @override
  bool get isOverTime => false;

  @override
  @protected
  Future<DateTime?> savePrivate(final bool firstSync) => Future.value(null);
  // ReckonerDb.I.commonDao.saveSyncItemList(merchants, firstSync);

  @override
  BundleType get type => BundleType.merchant;

  static MerchantSyncBundle fromJson(final Map<String, dynamic> json) =>
      MerchantSyncBundle(
        (json['merchants'] as List<dynamic>?)
                ?.map((final e) => Merchant.fromJson(e as Map<String, dynamic>))
                .toList() ??
            [],
      );

  @override
  Map<String, dynamic> toJson() =>
      _addType({'merchants': merchants.map((final e) => e.toJson()).toList()});

  static Future<List<MerchantSyncBundle>> load(final DateTime? syncDate) async {
    return [];
    // final Map<String, List<Merchant>> sentMap = {};

    // final merchants =
    //     await ReckonerDb.I.commonDao.loadAllDbItems<Merchant>(true).first;

    // List<Merchant> newMerList = [];

    // for (final merchant in merchants) {
    //   if (merchant.syncId == null) {
    //     newMerList.add(merchant);
    //   } else {
    //     sentMap.putIfAbsent(merchant.syncId!, () => []).add(merchant);
    //   }
    // }

    // for (final entry in sentMap.entries) {
    //   if (entry.value.length < 10) {
    //     while (entry.value.length < 10 && newMerList.isNotEmpty) {
    //       entry.value.add(newMerList.removeLast());
    //     }
    //   }
    // }

    // final bundleList =
    //     sentMap.values.map((final e) => MerchantSyncBundle(e)).toList();

    // while (newMerList.isNotEmpty) {
    //   if (newMerList.length < 10) {
    //     bundleList.add(MerchantSyncBundle(newMerList));
    //     newMerList = [];
    //   } else {
    //     bundleList.add(MerchantSyncBundle(newMerList.sublist(0, 10)));
    //     newMerList.removeRange(0, 10);
    //   }
    // }

    // return bundleList;
  }

  @override
  String get syncId {
    String? syncId = merchants.fold(
      null,
      (final previousValue, final element) => previousValue ?? element.syncId,
    );

    syncId ??= generateUuid();
    final nullIdList = <String>[];

    for (final mer in merchants) {
      if (mer.syncId == null) {
        nullIdList.add(mer.uuid);
        mer.syncId = syncId;
      }
    }

    ReckonerDb.I.merchantDao.setSyncId(nullIdList, syncId);

    return syncId;
  }

  @override
  set syncId(final String id) {
    for (final mer in merchants) {
      mer.syncId = id;
    }
  }

  @override
  DateTime get updateDate => merchants.fold(
    DateTime(0),
    (final previousValue, final element) =>
        previousValue.isBefore(element.updateDate)
            ? element.updateDate
            : previousValue,
  );

  static Future<bool> hasData(final DateTime? syncDate) async =>
      (await ReckonerDb.I.commonDao.getDbItems<Merchant>())
          .where((final m) => m.updateDate.isAfter(syncDate ?? DateTime(0)))
          .isNotEmpty;

  @override
  bool? hasNewerData(final SyncBundle other) {
    if (other is! MerchantSyncBundle) return null;
    if (_compareList(merchants, other.merchants)) return true;
    return false;
  }
}
