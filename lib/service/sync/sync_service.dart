import 'dart:async';

import 'package:flutter/foundation.dart';

import '../../database/reckoner_db.dart';
import '../../model/device.dart';
import '../logger.dart';
import '../sync_service_manager.dart';
import 'bundle.dart';

abstract base class SyncService {
  bool get connected;
  String _errorMessage = '';
  String get errorMessage => _errorMessage;
  @protected
  set errorMessage(final String val) => _errorMessage = val;
  List<SyncStatus> _statusList = [SyncStatus.disabled];
  Timer? _connectivityCheck;
  bool _disposed = false;
  @protected
  bool get isDisposed => _disposed;
  final void Function(SyncStatus) setStatus;
  DateTime _lastConCheck = DateTime.now();
  static Device thisDevice = Device();
  bool _initialized = false;
  final bool useIsolate;

  SyncService({required this.setStatus, this.useIsolate = true});

  @mustCallSuper
  Future<void> initialize() async {
    if (_initialized) return;
    _initialized = true;
    thisDevice = await ReckonerDb.I.deviceDao.getCurrentDevice();
    final connected = await connect();
    if (connected && useIsolate) {
      _lastConCheck = DateTime.now();
      _connectivityCheck = Timer.periodic(
        const Duration(seconds: 10),
        (final _) => _checkConnectivity(),
      );
    }
  }

  @mustCallSuper
  Future<bool> connect([final bool force = false]) async {
    try {
      if (connected && !force) return true;
      if (connected) await disconnect();
      resetStatus(SyncStatus.online);
      if (!await login()) {
        resetStatus(SyncStatus.needLogin);
        return false;
      }
      resetStatus(SyncStatus.connected);
      final (success, date) = await bulkSync(useIsolate);
      if (!success) {
        await disconnect();
        return false;
      }
      syncDate = date;
      if (!_disposed && connected) await subscribe();
      return true;
    } catch (err, stack) {
      logError('Reckoner', err, stack);
      resetStatus(SyncStatus.error);
      errorMessage = 'An unexpected error occurred. Check error logs.';
      dispose();
    }

    return false;
  }

  Future<bool> sendUpdates();
  Future<void> disconnect();
  Future<void> reset();
  Future<(bool, DateTime?)> bulkSync([final bool useIsolate = true]);

  @protected
  Future<void> subscribe();
  @protected
  Future<bool> login();
  @protected
  static DateTime? get syncDate => thisDevice.syncDate;
  @protected
  static DateTime? get syncDateWithOffset =>
      thisDevice.syncDate?.add(Duration(hours: -1));
  @protected
  static set syncDate(final DateTime? value) {
    if (value == null) return;
    final chosenDate = chooseDate(value, syncDate)!;
    if (chosenDate.isAtSameMomentAs(value)) {
      thisDevice.syncDate = value;
      ReckonerDb.I.commonDao.saveDbItem(thisDevice).then((final val) {
        thisDevice = val;
      });
    }
  }

  @mustCallSuper
  void pushStatus(final SyncStatus status) {
    if (isDisposed) return;
    _statusList.add(status);
    setStatus.call(status);
  }

  @mustCallSuper
  void popStatus(final SyncStatus status) {
    if (isDisposed) return;
    if (_statusList.length == 1) {
      setStatus.call(_statusList.last);
      return;
    } else if (_statusList.last != status) {
      final index = _statusList.lastIndexOf(status);
      if (index != -1) _statusList.removeAt(index);
      return;
    }
    _statusList.removeLast();
    assert(_statusList.isNotEmpty);
    if (_statusList.isNotEmpty) setStatus.call(_statusList.last);
  }

  @mustCallSuper
  void resetStatus(final SyncStatus status) {
    _statusList = [status];
    setStatus.call(status);
  }

  Future<String> get displayInfo;

  @mustCallSuper
  void dispose() {
    _connectivityCheck?.cancel();
    disconnect();
    _disposed = true;
  }

  Future<int> get connectionStatusCode;

  Future<void> _checkConnectivity() async {
    final code = await connectionStatusCode;
    if (code == 0) {
      resetStatus(SyncStatus.needLogin);
      await disconnect();
      _connectivityCheck?.cancel();
      return;
    }
    final lastCheck = _lastConCheck;
    _lastConCheck = DateTime.now();

    if (code == 429) {
      // Do nothing in this case. The endpoint is responding, but being overloaded with requests
      debugPrint('SyncService: 429 Response');
    } else if (code ~/ 100 != 2) {
      debugPrint('SyncService: $code Response');
      resetStatus(SyncStatus.offline);
      await disconnect();
    } else if (DateTime.now()
            .difference(lastCheck)
            .compareTo(const Duration(seconds: 15)) >
        0) {
      debugPrint('SyncService: Time too long');
      await connect(true);
    } else if (!connected) {
      debugPrint('SyncService: Reconnect');
      await connect();
    } else {
      debugPrint('SyncService: Verified connected');
    }
  }
}
