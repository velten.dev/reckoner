import 'dart:js_interop'
    show ObjectToJSBoxedDartObject, ListToJSArray, StringToJSString;

import 'package:web/web.dart' show console;

import 'util.dart';

final List<WebLoggedError> _errorList = [];

class WebLoggedError extends LoggedError {
  @override
  final String name;
  @override
  final String message;

  WebLoggedError(this.name, this.message);

  @override
  Future<void> delete() {
    _errorList.remove(this);
    return Future.value();
  }
}

Future<void> initLogger() => Future.value();

void logError(
  final String source,
  final Object? exception,
  final StackTrace? stack,
) {
  final message = errorToMessage(source, exception, stack).toList();
  console.error(
    message.map((final e) => e is String ? e.toJS : e?.toJSBox).toList().toJS,
  );
  _errorList.add(WebLoggedError('error ${DateTime.now()}', message.join()));
  errorToast();
}

Future<List<LoggedError>> getErrorLogs() async => Future.value(_errorList);
