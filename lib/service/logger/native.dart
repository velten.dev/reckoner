import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import 'util.dart';

Queue<Iterable<dynamic>> _queue = Queue();
bool _isRunning = false;

class NativeLoggedError extends LoggedError {
  FileSystemEntity file;

  NativeLoggedError(this.file);

  @override
  Future<void> delete() => file.delete();

  @override
  String get message => File(file.path).readAsStringSync();

  @override
  String get name => file.uri.pathSegments.last;
}

Future<void> initLogger() async {
  final appDir = await getApplicationSupportDirectory();
  final errorDir = join(appDir.path, 'errors');
  final directory = Directory(errorDir);
  await directory.create();
  final fileList = directory.listSync();
  if (fileList.length > 10) {
    await Future.wait(fileList.sublist(10).map((final e) => e.delete()));
  }
}

void logError(
  final String source,
  final Object? exception,
  final StackTrace? stack,
) {
  _queue.add(errorToMessage(source, exception, stack));
  if (!_isRunning) _logErrors();
  errorToast();
}

Future<void> _logErrors() async {
  _isRunning = true;
  final dateFilename = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final errorFile = File(
    join(
      (await getApplicationSupportDirectory()).path,
      'errors',
      'error-$dateFilename.log',
    ),
  );
  final sink = errorFile.openWrite(mode: FileMode.append);
  while (_queue.isNotEmpty) {
    sink.writeAll(_queue.removeFirst());
  }
  _isRunning = false;
  await sink.close();
}

Future<List<LoggedError>> getErrorLogs() async {
  final appDir = await getApplicationSupportDirectory();
  final errorDir = join(appDir.path, 'errors');
  final directory = Directory(errorDir);
  return directory.listSync().map((final e) => NativeLoggedError(e)).toList();
}
