import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

abstract class LoggedError {
  String get name;
  String get message;
  Future<void> delete();
}

Iterable<Object?> errorToMessage(
  final String source,
  final Object? exception,
  final StackTrace? stack,
) {
  return [
    '\n\n${source.toUpperCase()} EXCEPTION: ${DateTime.now()}\n\n',
    exception,
    '\n\nSTACKTRACE:\n\n',
    stack,
    '\n\nEND EXCEPTION\n',
  ];
}

void errorToast() {
  try {
    showToast(
      'Error Logged',
      position: const ToastPosition(align: Alignment.bottomCenter),
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 2),
    );
  } catch (_) {}
}
