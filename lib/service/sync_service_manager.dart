import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';

import '../config/environment_const.dart';
import '../config/platform.dart';
import '../database/reckoner_db.dart';
import '../model/secure_storage.dart';
import 'sync/pb_sync_service.dart';
import 'sync/sync_service.dart';

export 'sync/pb_sync_service.dart'
    show validatePocketBaseCredentials, setPocketBaseLoginData;

enum SyncStatus {
  disabled,
  offline,
  online,
  error,
  needLogin,
  connected,
  upload,
  download,
  bulkSync,
}

class SyncServiceManager extends ChangeNotifier {
  SyncManagerSettings _settings = SyncManagerSettings();
  SyncStatus _status = SyncStatus.disabled;
  SyncService? _service;
  static SyncServiceManager? _instance;

  static Future<SyncServiceManager> get instance async {
    _instance ??= SyncServiceManager._(await SecureStorage.syncManagerSettings);
    return _instance!;
  }

  SyncStatus get status => _status;
  @protected
  set status(final SyncStatus value) {
    if (_status == value) return;
    _status = value;
    notifyListeners();
  }

  bool get isConnected => <SyncStatus>[
    SyncStatus.bulkSync,
    SyncStatus.connected,
    SyncStatus.download,
    SyncStatus.online,
    SyncStatus.upload,
  ].contains(_status);

  String get statusMessage => switch (_status) {
    SyncStatus.disabled => 'Synchronization disabled',
    SyncStatus.offline => 'Device offline',
    SyncStatus.online => 'Device online',
    SyncStatus.error => _service?.errorMessage ?? 'Unknown error',
    SyncStatus.needLogin => switch (type) {
      SyncType.pocketBase => 'PocketBase server not configured',
    },
    SyncStatus.connected => switch (type) {
      SyncType.pocketBase => 'Connected to Pocketbase server',
    },
    SyncStatus.upload => 'Uploading data',
    SyncStatus.download => 'Downloading data',
    SyncStatus.bulkSync => 'Syncing data',
  };

  SyncServiceManager._(this._settings) {
    if (isDemo) return;
    if (_settings.enabled) {
      _status = SyncStatus.offline;
      notifyListeners();
    }
  }

  bool get enabled => _settings.enabled;
  set enabled(final bool value) {
    if (_settings.enabled == value) return;
    _settings.enabled = value;
    SecureStorage.syncManagerSettings = Future.value(_settings);
    _restart();
  }

  SyncType get type => _settings.type;
  Future<bool> get isLoggedIn => switch (type) {
    SyncType.pocketBase => PocketBaseSyncService.isLoggedIn,
  };

  Future<void> setSyncData(final SyncManagerSettings settings) async {
    if (settings.type != _settings.type ||
        settings.encryptionSettings != settings.encryptionSettings) {
      await _service?.reset();
      await ReckonerDb.I.deviceDao.resetSyncData();
    }
    SecureStorage.syncManagerSettings = Future.value(settings);
    _settings = settings;
    await _restart();
    if (isMobile) {
      final _ = switch (settings.backgroundEnabled) {
        true => await BackgroundFetch.start(),
        false => await BackgroundFetch.stop(),
      };
    }
  }

  Future<void> reset() async {
    await _service?.reset();
    await ReckonerDb.I.deviceDao.resetSyncData();
    SecureStorage.syncManagerSettings = Future.value(SyncManagerSettings());
    _settings = SyncManagerSettings();
    await _restart();
  }

  Future<void> _restart([final bool useIsolate = true]) async {
    if (isDemo) return;
    _service?.dispose();
    status = enabled ? SyncStatus.offline : SyncStatus.disabled;

    _service = switch ((enabled, type)) {
      (false, _) => null,
      (true, SyncType.pocketBase) => PocketBaseSyncService(
        setStatus: (final status) => this.status = status,
        useIsolate: useIsolate,
      ),
    };

    await _service?.initialize();

    notifyListeners();
  }

  void stop() {
    _service?.dispose();
    _service = null;
    status = SyncStatus.online;
  }

  Future<void> start({
    final bool useIsolate = true,
    final bool force = false,
  }) async {
    if (force || _service == null) await _restart(useIsolate);
  }

  void sendUpdates() => _service?.sendUpdates().ignore();

  Future<void>? bulkSync() => _service?.bulkSync();

  Future<String> get displayInfo async => (await _service?.displayInfo) ?? '';

  @override
  void dispose() {
    _instance = null;
    _service?.dispose();
    super.dispose();
  }
}
