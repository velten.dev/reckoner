import 'dart:async';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart' show ChangeNotifier, DateTimeRange;

import '../config/user_pref_const.dart';
import '../model/category.dart';
import '../model/tz_date_time.dart';
import '../util/compute_date_range.dart';

enum DateRangeDefault { day, week, month, quarter, year, all, custom }

class DateTimeRangeLoader extends ChangeNotifier {
  TZDateTimeRange? _manualRange;
  DateRangeDefault _defaultRange = DateRangeDefault.month;
  int _offset = 0;
  Timer? timer;
  static DateTimeRangeLoader? _instance;

  DateTimeRangeLoader._() {
    final enumStr = getPreferences().getString(transactionRange);
    if (enumStr?.isNotEmpty ?? false) {
      final savedRange = EnumToString.fromString(
        DateRangeDefault.values,
        enumStr!,
      );
      // Don't allow anything longer than the current month as that will
      // impact the load time of the application
      if ([
        DateRangeDefault.day,
        DateRangeDefault.week,
        DateRangeDefault.month,
      ].contains(savedRange)) {
        _defaultRange = savedRange!;
      }
    }
  }

  static DateTimeRangeLoader get instance {
    _instance ??= DateTimeRangeLoader._();
    return _instance!;
  }

  DateRangeDefault get defaultSelection => _defaultRange;
  int get offset => _offset;

  set defaultSelection(final DateRangeDefault range) {
    _defaultRange = range;
    _offset = 0;
    getPreferences().setString(
      transactionRange,
      EnumToString.convertToString(range),
    );
    notifyListeners();
  }

  set offset(final int value) {
    _offset = value;
    timer?.cancel();
    timer = Timer(const Duration(milliseconds: 200), () => notifyListeners());
  }

  TZDateTimeRange? get customRange => _manualRange;
  void setCustomRange(final DateTimeRange? range) {
    _manualRange =
        range == null
            ? null
            : TZDateTimeRange(
              start: TZDateTime(
                local,
                range.start.year,
                range.start.month,
                range.start.day,
              ),
              end: TZDateTime(
                local,
                range.end.year,
                range.end.month,
                range.end.day + 1,
                0,
                0,
                -1,
              ),
            );
    notifyListeners();
  }

  TZDateTimeRange? getRange() {
    final today = tzNow();
    switch (_defaultRange) {
      case DateRangeDefault.custom:
        return _manualRange;
      case DateRangeDefault.all:
        return null;
      case DateRangeDefault.day:
        return computeDateRange(
          period: CategoryLimitPeriod.daily,
          today: today.add(Duration(days: offset)),
        );
      case DateRangeDefault.week:
        return computeDateRange(
          period: CategoryLimitPeriod.weekly,
          today: today.add(Duration(days: offset * 7)),
        );

      case DateRangeDefault.month:
        return computeDateRange(
          period: CategoryLimitPeriod.monthly,
          today: TZDateTime(local, today.year, today.month + offset),
        );
      case DateRangeDefault.quarter:
        return TZDateTimeRange(
          start: TZDateTime(local, today.year, today.month + offset - 2),
          end: TZDateTime(
            local,
            today.year,
            today.month + offset + 1,
            1,
            0,
            0,
            -1,
          ),
        );
      case DateRangeDefault.year:
        return computeDateRange(
          period: CategoryLimitPeriod.yearly,
          today: TZDateTime(local, today.year + offset),
        );
    }
  }
}
