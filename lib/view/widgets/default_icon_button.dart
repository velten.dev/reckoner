import 'package:flutter/material.dart';

class DefaultIconButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Icon icon;
  final String? tooltip;

  const DefaultIconButton({
    super.key,
    required this.icon,
    required this.onPressed,
    this.tooltip,
  });

  @override
  Widget build(final BuildContext context) {
    final button = IconButton(
      onPressed: onPressed,
      icon: icon,
      splashRadius: 25,
    );
    if ((tooltip ?? '').isNotEmpty) {
      return Tooltip(message: tooltip, child: button);
    }
    return button;
  }
}
