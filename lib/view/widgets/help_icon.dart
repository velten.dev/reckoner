import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../config/user_pref_const.dart';
import '../../model/settings.dart';
import 'default_icon_button.dart';

class HelpIcon extends StatelessWidget {
  late final Uri _uri;

  HelpIcon(final NavigationEntry entry, {super.key}) {
    String url = 'https://doc.reckoner.finance/user/screens/';
    url += switch (entry.type) {
      NavigationEntryType.transaction => 'transactions/',
      NavigationEntryType.account => 'accounts/',
      NavigationEntryType.merchant => 'merchants',
      NavigationEntryType.tag => 'tags/',
      NavigationEntryType.options => 'options/',
      NavigationEntryType.report => 'reports/',
      NavigationEntryType.category => 'categories/',
    };

    _uri = Uri.parse(url);
  }

  @override
  Widget build(final BuildContext context) {
    if (getPreferences().getBool(hideHelpIconKey) ?? false) {
      return const SizedBox.shrink();
    }

    return DefaultIconButton(
      icon: const Icon(Icons.help),
      onPressed: () async => launchUrl(_uri),
      tooltip: 'Visit help documentation',
    );
  }
}
