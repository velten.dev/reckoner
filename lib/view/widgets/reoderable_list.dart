import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class ReckonerReoderableListView extends StatelessWidget {
  final List<Widget> children;
  final ReorderCallback onReorder;

  const ReckonerReoderableListView({
    required this.children,
    required this.onReorder,
    super.key,
  });

  @override
  Widget build(final BuildContext context) {
    return ReorderableListView(
      buildDefaultDragHandles: false,
      padding: const EdgeInsets.symmetric(horizontal: 5),
      onReorder: onReorder,
      children:
          children
              .mapIndexed(
                (final index, final element) => _ReorderableListItem(
                  index: index,
                  key: Key('_ReorderableListItem$index'),
                  child: element,
                ),
              )
              .toList(),
    );
  }
}

class _ReorderableListItem extends StatelessWidget {
  final Widget child;
  final int index;

  const _ReorderableListItem({
    required this.child,
    required this.index,
    super.key,
  });

  @override
  Widget build(final BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: child),
        ReorderableDragStartListener(
          index: index,
          child: const Icon(Icons.drag_handle),
        ),
      ],
    );
  }
}
