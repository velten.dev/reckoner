import 'package:flutter/material.dart';

import '../../model/model.dart';

enum ActionChoices { edit, delete, copy, custom }

class ActionPicker<T extends Common<T>> extends StatelessWidget {
  final String title;
  final void Function(BuildContext, T) deleteCallback;
  final void Function(BuildContext, T) editCallback;
  final void Function(BuildContext, T)? copyCallback;
  final T data;
  late final Set<ActionChoices> actions;
  final bool iconsOnly;
  final Decoration? decoration;
  final Widget? child;
  final void Function()? customAction;
  final Icon? customIcon;
  final String? customLabel;

  ActionPicker({
    super.key,
    required this.data,
    required this.deleteCallback,
    required this.editCallback,
    this.copyCallback,
    this.iconsOnly = false,
    this.title = 'Actions',
    final Set<ActionChoices> actions = const {
      ActionChoices.edit,
      ActionChoices.delete,
      ActionChoices.copy,
    },
    this.decoration,
    this.child,
    this.customAction,
    this.customIcon,
    this.customLabel,
  }) {
    if (customAction != null && customIcon != null && customLabel != null) {
      this.actions = Set.from(actions)..add(ActionChoices.custom);
    } else {
      assert(!actions.contains(ActionChoices.custom));
      this.actions = Set.from(actions)..remove(ActionChoices.custom);
    }
  }

  @override
  Widget build(final BuildContext context) {
    final theme = Theme.of(context);

    final textStyle = theme.textTheme.bodyMedium!;

    return DefaultTextStyle(
      style: textStyle,
      child: DecoratedBox(
        decoration:
            decoration ??
            BoxDecoration(
              border: Border.all(color: theme.highlightColor),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
        child: PopupMenuButton<ActionChoices>(
          color: theme.colorScheme.secondaryFixed,
          position: PopupMenuPosition.under,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              child ?? Text(title, overflow: TextOverflow.ellipsis),
              const Icon(Icons.expand_more),
            ],
          ),
          onSelected: (final value) {
            switch (value) {
              case ActionChoices.edit:
                editCallback(context, data);
                break;
              case ActionChoices.delete:
                deleteCallback(context, data);
                break;
              case ActionChoices.copy:
                copyCallback == null
                    ? editCallback(context, data.copy())
                    : copyCallback!(context, data);
                break;
              case ActionChoices.custom:
                customAction!.call();
                break;
            }
          },
          itemBuilder:
              (final context) =>
                  actions.map((final choice) {
                    final List<Widget> children = switch (choice) {
                      ActionChoices.edit => [
                        const Icon(Icons.edit),
                        Text('Edit', style: textStyle),
                      ],
                      ActionChoices.delete => [
                        const Icon(Icons.delete),
                        Text('Delete', style: textStyle),
                      ],
                      ActionChoices.copy => [
                        const Icon(Icons.copy),
                        Text('Clone', style: textStyle),
                      ],
                      ActionChoices.custom => [
                        customIcon!,
                        Text(customLabel!, style: textStyle),
                      ],
                    };

                    return PopupMenuItem<ActionChoices>(
                      value: choice,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: children,
                      ),
                    );
                  }).toList(),
        ),
      ),
    );
  }
}
