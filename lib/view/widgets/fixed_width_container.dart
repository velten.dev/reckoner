import 'dart:math';

import 'package:flutter/widgets.dart';

class FixedWidthContainer extends StatelessWidget {
  final Widget child;
  final double? width;
  final bool topBottomPadding;

  const FixedWidthContainer({
    super.key,
    required this.child,
    this.width,
    this.topBottomPadding = true,
  });

  @override
  Widget build(final BuildContext context) {
    return LayoutBuilder(
      builder: (final context, final constraints) {
        final boxWidth = width ?? min(max(constraints.maxWidth / 3, 500), 700);
        final padding = constraints.maxHeight / 20;

        return Center(
          child: Padding(
            padding:
                topBottomPadding
                    ? EdgeInsets.only(top: padding, bottom: padding)
                    : EdgeInsets.zero,
            child: SizedBox(
              height: double.infinity,
              width: boxWidth,
              child: child,
            ),
          ),
        );
      },
    );
  }
}
