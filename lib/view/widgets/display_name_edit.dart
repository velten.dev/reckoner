import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class DisplayNameEdit extends StatelessWidget {
  final String name;
  final String? path;
  final String? transactionPath;
  final TextStyle? style;
  final double? iconSize;
  final VoidCallback? onEdit;

  const DisplayNameEdit({
    super.key,
    required this.name,
    this.path,
    this.transactionPath,
    this.style,
    this.iconSize,
    this.onEdit,
  });

  Widget _iconBuilder(
    final BuildContext context,
    final String tooltip,
    final VoidCallback onTap,
    final IconData icon,
  ) {
    return Padding(
      padding: EdgeInsets.only(left: 5),
      child: Tooltip(
        message: tooltip,
        child: InkWell(
          hoverColor: Colors.transparent,
          onTap: onTap,
          child: Icon(icon, size: iconSize),
        ),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: Text(name, style: style, overflow: TextOverflow.ellipsis),
        ),
        if (path != null || onEdit != null)
          _iconBuilder(
            context,
            'Edit $name',
            onEdit ?? () => context.push(path!),
            Icons.edit,
          ),
        if (transactionPath != null)
          _iconBuilder(
            context,
            '$name transactions',
            () => context.push(transactionPath!),
            Icons.sync_alt,
          ),
      ],
    );
  }
}
