import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

ToastFuture showError(final String message) => showToast(
  message,
  position: const ToastPosition(align: Alignment.bottomCenter),
  backgroundColor: Colors.red,
  duration: const Duration(seconds: 2),
);

ToastFuture showAlert(final String message) => showToast(
  message,
  position: const ToastPosition(align: Alignment.bottomCenter),
);
