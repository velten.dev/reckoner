import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../model/secure_storage.dart';
import '../../../service/sync_service_manager.dart';

class SyncBackgroundEnableToggle extends WatchingStatefulWidget {
  const SyncBackgroundEnableToggle({super.key});

  @override
  State<StatefulWidget> createState() => _SyncBackgroundEnableToggle();
}

class _SyncBackgroundEnableToggle extends State<SyncBackgroundEnableToggle> {
  bool backgroundEnabled = false;

  @override
  void initState() {
    super.initState();
    SecureStorage.syncManagerSettings.then((final settings) {
      backgroundEnabled = settings.backgroundEnabled;
      if (backgroundEnabled && mounted) setState(() {});
    });
  }

  @override
  Widget build(final BuildContext context) {
    final enabled = watchPropertyValue(
      (final SyncServiceManager m) => m.enabled,
    );
    return FormBuilderSwitch(
      enabled: enabled,
      initialValue: backgroundEnabled,
      decoration: const InputDecoration(border: InputBorder.none),
      name: 'Enable Sync',
      title: Text(
        backgroundEnabled
            ? 'Disable Background Sync'
            : 'Enable Background Sync',
      ),
      onChanged: (final value) async {
        if (value == null) return;
        final settings = await SecureStorage.syncManagerSettings;
        backgroundEnabled = value;
        setState(() => settings.enableBackgroundSync = value);
      },
    );
  }
}
