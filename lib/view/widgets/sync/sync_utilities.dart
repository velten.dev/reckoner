import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../model/device.dart';
import '../../../model/secure_storage.dart';
import 'sync_settings_edit.dart';

const Widget editSyncSettingsWidget = Row(
  mainAxisSize: MainAxisSize.min,
  children: [Icon(Icons.edit), Text('Edit Sync Settings')],
);

Future<void> editSyncSettingsCallback(
  final BuildContext context,
  final bool enabled,
) async {
  final pbSettings = await SecureStorage.pbSyncSettings;
  final settings = await SecureStorage.syncManagerSettings;
  if (context.mounted) {
    await showDialog<void>(
      context: context,
      builder:
          (final _) =>
              SyncSettingsEdit(pbSettings: pbSettings, settings: settings),
    );
  }
}

Widget syncDateDisplay(final Device device) => Text(
  device.syncDate == null
      ? 'Not Synced'
      : 'Synced ${DateFormat.yMEd().format(device.syncDate!)} ${DateFormat.Hm().format(device.syncDate!)}',
);
