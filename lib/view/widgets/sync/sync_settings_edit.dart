import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/platform.dart';
import '../../../model/secure_storage.dart';
import '../../../service/sync_service_manager.dart';
import '../../l10n/localizations.dart';
import '../../util/popup_dialogs.dart';
import '../encryption_settings_edit.dart';
import '../form/edit_fields.dart';
import '../form/input_form.dart';
import '../form/validators.dart';

class SyncSettingsEdit extends StatefulWidget {
  final SyncManagerSettings settings;
  final PocketBaseSyncSettings pbSettings;
  const SyncSettingsEdit({
    super.key,
    required this.settings,
    required this.pbSettings,
  });

  @override
  State<StatefulWidget> createState() => _SyncSettingsEditState();
}

class _SyncSettingsEditState extends State<SyncSettingsEdit> {
  late SyncManagerSettings managerSyncSettings;
  late PocketBaseSyncSettings pbSyncSettings;
  bool isAdmin = false;
  bool changed = false;
  bool validated = false;
  bool isResetChange = false;
  String? validationMessage;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    pbSyncSettings = widget.pbSettings;
    managerSyncSettings = widget.settings;
  }

  List<Widget> get _inputs => switch (managerSyncSettings.type) {
    SyncType.pocketBase => _pocketbaseInputs,
  };

  void _onChange() {
    if (!changed || validated) {
      setState(() {
        changed = true;
        validated = false;
      });
    }
  }

  List<Widget> get _pocketbaseInputs => [
    TextFormField(
      decoration: const InputDecoration(label: Text('URL')),
      initialValue: pbSyncSettings.url.toString(),
      onChanged: (final value) {
        pbSyncSettings.url = Uri.parse(value);
        isResetChange = true;
        _onChange();
      },
      validator:
          (final value) =>
              Uri.parse(value ?? '').isAbsolute ? null : 'Invalid URL',
    ),
    Row(
      children: [
        Expanded(
          flex: 2,
          child: TextFormField(
            decoration: const InputDecoration(label: Text('Username')),
            initialValue: pbSyncSettings.username,
            onChanged: (final value) {
              pbSyncSettings.username = value;
              _onChange();
            },
            validator: (final value) => nonNullValidator(context, value),
          ),
        ),
        Expanded(
          child: FormBuilderCheckbox(
            name: 'isAdmin',
            title: const Text('Is Admin'),
            initialValue: isAdmin,
            onChanged: (final value) {
              isAdmin = value!;
              _onChange();
            },
          ),
        ),
      ],
    ),
    TextFormField(
      decoration: const InputDecoration(label: Text('Password')),
      initialValue: pbSyncSettings.password,
      onChanged: (final value) {
        pbSyncSettings.password = value;
        _onChange();
      },
      validator: (final value) => nonNullValidator(context, value),
      obscureText: true,
    ),
    if (validated)
      const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Icon(Icons.check), Text('Validated credentials')],
      ),
    if (!validated && (validationMessage ?? '').isNotEmpty)
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [const Icon(Icons.warning), Text(validationMessage!)],
      ),
  ];

  List<Widget>? get _actions {
    final localization = ReckonerLocalizations.of(context);
    final cancel = localization.cancel;
    final textStyle = Theme.of(context).textTheme.titleMedium?.copyWith(
      color: Theme.of(context).buttonTheme.colorScheme?.primaryFixed,
    );
    if (!validated) {
      return [
        TextButton(
          onPressed: () async {
            if (!_formKey.currentState!.validate()) return;
            final err = await validatePocketBaseCredentials(
              pbSyncSettings,
              isAdmin,
              managerSyncSettings,
            );
            if ((err ?? '').isEmpty) {
              setState(() => validated = true);
            } else if (mounted) {
              setState(() => validationMessage = err!);
            }
          },
          child: const Text('Validate'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text(cancel, style: textStyle),
        ),
      ];
    }
    return [
      TextButton(
        onPressed: () async {
          if (isResetChange && di<SyncServiceManager>().enabled) {
            final doit = await yesNoDialog(
              context,
              'You have changed setting which will reset the synchronization service. This will reset all synchronization data. Are you sure you want to continue?',
            );
            if (!doit) return;
          }
          final _ = switch (managerSyncSettings.type) {
            SyncType.pocketBase => setPocketBaseLoginData(
              pbSyncSettings,
              isAdmin,
            ),
          }.then(
            (final _) =>
                di<SyncServiceManager>().setSyncData(managerSyncSettings),
          );
          if (mounted) Navigator.pop(context);
        },
        child: Text(localization.save),
      ),
      TextButton(
        onPressed: () => Navigator.pop(context),
        child: Text(cancel, style: textStyle),
      ),
    ];
  }

  @override
  Widget build(final BuildContext context) {
    return InputForm(
      formKey: _formKey,
      title: 'Synchronization Settings Edit',
      inputs: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (isWeb) const Spacer(),
            Expanded(
              flex: 2,
              child: FormBuilderSwitch(
                name: 'syncEnabled',
                decoration: const InputDecoration(border: InputBorder.none),
                title: const Text(
                  'Enable Synchronization',
                  textAlign: TextAlign.right,
                ),
                initialValue: managerSyncSettings.enabled,
                validator: (final val) => nonNullValidator(context, val),
                onChanged:
                    (final value) => managerSyncSettings.enabled = value!,
              ),
            ),
            if (isWeb) const Spacer(),
            if (!isWeb)
              Expanded(
                flex: 2,
                child: FormBuilderSwitch(
                  name: 'backgroundSync',
                  decoration: const InputDecoration(border: InputBorder.none),
                  title: const Text(
                    'Background Synchronization',
                    textAlign: TextAlign.right,
                  ),
                  initialValue: managerSyncSettings.enableBackgroundSync,
                  validator: (final val) => nonNullValidator(context, val),
                  onChanged:
                      (final value) =>
                          managerSyncSettings.enableBackgroundSync = value!,
                ),
              ),
          ],
        ),
        if (SyncType.values.length > 1)
          ChipEdit(
            initialValue: managerSyncSettings.type,
            options:
                SyncType.values
                    .map(
                      (final e) =>
                          FormBuilderChipOption(value: e, child: Text(e.name)),
                    )
                    .toList(),
            label: 'Sync Type',
            validator: (final val) => nonNullValidator(context, val),
            onChanged:
                (final value) => setState(() {
                  isResetChange = true;
                  managerSyncSettings.type = value!;
                }),
          ),
        InputFieldGroup(
          label: 'Encryption Settings',
          child: EncryptionSettingsEdit(
            initialValue: managerSyncSettings.encryptionSettings,
            onChanged:
                (final value) => managerSyncSettings.encryptionSettings = value,
          ),
        ),
        InputFieldGroup(
          label: switch (managerSyncSettings.type) {
            SyncType.pocketBase => 'Pocketbase Server Settings',
          },
          child: Column(children: _inputs),
        ),
      ],
      actions: _actions,
    );
  }
}
