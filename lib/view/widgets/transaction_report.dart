import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:graphic/graphic.dart';
import 'package:intl/intl.dart' show NumberFormat;
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/currency.dart';
import '../../model/report.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../model/report_transactions.dart';
import '../theme.dart';
import '../util/datetime_display.dart';
import 'amount_display.dart';

class TransactionReport extends WatchingWidget {
  final Report report;
  final bool showTitle;

  const TransactionReport(this.report, {super.key, this.showTitle = false});

  @override
  Widget build(final BuildContext context) {
    final range = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange(),
    );
    final type = report.type;
    final theme = Theme.of(context).textTheme;
    final themeHandler = di<ThemeHandler>();

    final widget = Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: StreamBuilder<TransactionReportChartData>(
        stream: ReckonerDb.I.reportDao.loadTxTotals(report, range),
        builder: (final context, final snapshot) {
          if (snapshot.hasData) {
            final chartData = snapshot.data!;
            if (chartData.data.isEmpty && type != ReportType.text) {
              return const SizedBox.shrink();
            }
            return switch (type) {
              ReportType.bar ||
              ReportType.line ||
              ReportType.pie => _TxChart(chartData: chartData, type: type),
              ReportType.table => _TableChart(chartData: chartData),
              ReportType.text => _TextChart(
                chartData: chartData,
                report: report,
              ),
            };
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );

    Color? color;
    if (themeHandler.isDark && !themeHandler.isBlack) {
      color = themeHandler.isBlack ? Colors.grey[900] : Colors.grey[850];
    }

    return Card(
      color: color,
      child:
          showTitle
              ? Stack(
                children: [
                  Positioned.fill(
                    child: Text(
                      report.name,
                      textAlign: TextAlign.center,
                      style: theme.headlineMedium,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: widget,
                  ),
                ],
              )
              : widget,
    );
  }
}

class _TextChart extends StatelessWidget {
  final Report report;
  final TransactionReportChartData chartData;

  const _TextChart({required this.chartData, required this.report});

  String _defaultAmount() {
    final cache = di<DataCache>();

    if (report.filterObj.currencyCodes.isEmpty) {
      return cache
              .currencyMap
              .value[cache.setting.value.defaultCurrencyCode ?? 'USD']
              ?.formatAmount(0) ??
          '\$0';
    }
    return cache.currencyMap.value[report.filterObj.currencyCodes.first]
            ?.formatAmount(0) ??
        '\$0';
  }

  @override
  Widget build(final BuildContext context) {
    final label = report.reportText ?? report.name;
    final String amount =
        chartData.data.isEmpty
            ? _defaultAmount()
            : chartData.data.first.group.amountToDisplay(
              chartData.data.first.amount,
            );
    final style = Theme.of(context).textTheme.headlineMedium;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(label, style: style),
          Text(
            amount,
            style: style?.copyWith(
              color: AmountDisplay.amountColor(
                chartData.data.isEmpty ? 0 : chartData.data.first.amount,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _TableChart extends StatelessWidget {
  final TransactionReportChartData chartData;

  const _TableChart({required this.chartData});

  @override
  Widget build(final BuildContext context) {
    return DataTable(
      columns: [
        const DataColumn(label: Text('Label')),
        if (chartData.report.loadOverTime)
          const DataColumn(label: Text('Date')),
        const DataColumn(label: Text('Amount'), numeric: true),
      ],
      rows:
          chartData.data
              .map(
                (final e) => DataRow(
                  cells: [
                    DataCell(Text(e.group.label)),
                    if (chartData.report.loadOverTime)
                      DataCell(Text(datetimeDisplay(e.date!))),
                    DataCell(Text(e.group.amountToDisplay(e.amount))),
                  ],
                ),
              )
              .toList(),
    );
  }
}

class _TxChart extends StatelessWidget {
  final ReportType type;
  final TransactionReportChartData chartData;
  late final Currency? currency;

  _TxChart({required this.type, required this.chartData})
    : assert(
        type == ReportType.line ||
            type == ReportType.bar ||
            type == ReportType.pie,
      ) {
    if (chartData.report.filterObj.currencyCodes.length == 1) {
      currency =
          di.get<DataCache>().currencyMap.value[chartData
              .report
              .filterObj
              .currencyCodes
              .first];
    }
  }

  Map<String, Variable<TransactionReportData, dynamic>> _variables(
    final BoxConstraints constraints,
  ) {
    final displayWidth = 0.95 * constraints.maxWidth;
    final ticks = max(2, displayWidth ~/ 100);
    final Map<String, Variable<TransactionReportData, dynamic>> varib = {};

    // TODO: Figure out why need to make available to all chart types to avoid error being logged
    varib['date'] = Variable(
      accessor:
          (final TransactionReportData datum) => datum.date ?? DateTime.now(),
      scale: TimeScale(
        formatter: (final dt) => datetimeDisplay(dt, short: ticks < 5),
        marginMin: 0.01,
        marginMax: 0.01,
        tickCount: max(2, displayWidth ~/ 100),
      ),
    );

    if (type == ReportType.pie) {
      varib['amount'] = Variable(
        accessor:
            (final TransactionReportData datum) =>
                datum.group.amountToCur(datum.amount),
        scale: LinearScale(min: 0),
      );
    } else {
      varib['amount'] = Variable(
        accessor:
            (final TransactionReportData datum) =>
                datum.group.amountToCur(datum.amount),
      );
    }
    if (type == ReportType.bar) {
      varib['label'] = Variable(
        accessor: (final TransactionReportData datum) => datum.group.label,
        scale: OrdinalScale(tickCount: max(2, displayWidth ~/ 150)),
      );
    } else {
      varib['label'] = Variable(
        accessor: (final TransactionReportData datum) => datum.group.label,
      );
    }
    varib['currency'] = Variable(
      accessor: (final TransactionReportData datum) => datum.group.currencyCode,
    );

    return varib;
  }

  List<Mark> _marks(final bool isDark) {
    final int lightSwatch = isDark ? 400 : 700;
    final int darkSwatch = isDark ? 200 : 400;

    final colorPallet =
        type == ReportType.pie
            ? [
              Colors.blueAccent[700]!,
              Colors.tealAccent,
              Colors.blueGrey,
              Colors.orangeAccent,
              Colors.purple,
              Colors.yellow,
              Colors.indigo,
              Colors.greenAccent,
              Colors.grey[600]!,
              Colors.pinkAccent,
            ]
            : [
              Colors.blueAccent[lightSwatch]!,
              Colors.tealAccent[lightSwatch]!,
              Colors.blueGrey[darkSwatch]!,
              Colors.orangeAccent[lightSwatch]!,
              Colors.purpleAccent[lightSwatch]!,
              Colors.indigo[lightSwatch]!,
              Colors.yellow[lightSwatch]!,
              Colors.greenAccent[lightSwatch]!,
              Colors.grey[lightSwatch]!,
              Colors.pinkAccent[lightSwatch]!,
            ];
    final labelLength = chartData.data.map((final e) => e.group).toSet().length;
    final List<Color> colorVals = [];

    if (type == ReportType.pie &&
        labelLength % colorPallet.length == 1 &&
        labelLength > 10) {
      colorPallet.add(Colors.deepPurple[300]!);
    }
    for (int i = 0; i < labelLength / colorPallet.length; i++) {
      colorVals.addAll(colorPallet);
    }

    if (type == ReportType.line) {
      return [
        LineMark(
          color: ColorEncode(variable: 'label', values: colorVals),
          position:
              Varset('date') *
              Varset('amount') /
              (Varset('label') * Varset('currency')),
          shape: ShapeEncode(value: BasicLineShape(smooth: true)),
        ),
        PointMark(
          color: ColorEncode(variable: 'label', values: colorVals),
          size: SizeEncode(
            value: 5,
            updaters: {
              'touchMove': {true: (final size) => size * 2},
            },
          ),
        ),
      ];
    } else if (type == ReportType.pie) {
      return [
        IntervalMark(
          color: ColorEncode(variable: 'label', values: colorVals),
          shape: ShapeEncode(value: RectShape()),
          position: Varset('percent') / Varset('label'),
          modifiers: [StackModifier()],
        ),
      ];
    }
    return [
      IntervalMark(
        position: Varset('label') * Varset('amount') / Varset('currency'),
      ),
    ];
  }

  Coord? _coord() {
    if (type == ReportType.pie) {
      return PolarCoord(transposed: true, dimCount: 1);
    }
    return null;
  }

  List<VariableTransform>? _transforms() {
    if (type == ReportType.pie) {
      return [Proportion(variable: 'amount', as: 'percent')];
    }
    return null;
  }

  List<AxisGuide<dynamic>>? _axes(final ThemeData theme) {
    final textStyle = theme.textTheme.bodySmall?.copyWith(
      color: theme.colorScheme.onSurface,
    );
    final formatter = currency?.compactFormatter ?? NumberFormat.compact();

    if (type != ReportType.pie) {
      return [
        AxisGuide(
          line: PaintStyle(
            strokeColor: theme.colorScheme.onSurface,
            strokeWidth: 0.75,
          ),
          label: LabelStyle(textStyle: textStyle, offset: const Offset(0, 7.5)),
        ),
        AxisGuide(
          label: LabelStyle(
            span:
                (final text) => TextSpan(
                  text: formatter.format(double.parse(text)),
                  style: textStyle,
                ),
            offset: const Offset(-7.5, 0),
          ),
          grid: PaintStyle(
            strokeColor: theme.colorScheme.onSurface,
            strokeWidth: 0.25,
          ),
        ),
      ];
    }
    return null;
  }

  Map<String, Selection>? _selections() => {
    'touchMove': PointSelection(
      on: {GestureType.tap, GestureType.hover, GestureType.longPressMoveUpdate},
      dim: type != ReportType.pie ? Dim.x : null,
      variable:
          type != ReportType.pie
              ? (chartData.report.loadOverTime ? 'date' : 'amount')
              : null,
    ),
    'tooltipMouse': PointSelection(
      nearest: type != ReportType.pie ? false : null,
      on: {GestureType.hover},
      devices: {PointerDeviceKind.mouse},
      dim: type == ReportType.bar ? Dim.x : null,
      variable:
          type != ReportType.pie
              ? (chartData.report.loadOverTime ? 'date' : 'amount')
              : null,
    ),
    'tooltipTouch': PointSelection(
      on: {GestureType.tapDown, GestureType.longPressMoveUpdate},
      devices: {PointerDeviceKind.touch},
      dim: type != ReportType.pie ? Dim.x : null,
      variable:
          type != ReportType.pie
              ? (chartData.report.loadOverTime ? 'date' : 'amount')
              : null,
    ),
  };

  TooltipGuide? _tooltip(
    final BoxConstraints constraints,
    final ThemeData theme,
  ) {
    final currencyMap = di<DataCache>().currencyMap.value;

    List<MarkElement> simpleTooltip(
      final Size size,
      final Offset anchor,
      final Map<int, Tuple> selectedTuples,
    ) {
      final displayWidth = 0.95 * constraints.maxWidth;

      List<MarkElement> elements;

      final selectedTupleList = selectedTuples.values;
      String textContent = '';
      if (chartData.report.loadOverTime) {
        final date = selectedTupleList.first['date'] as DateTime;
        textContent = datetimeDisplay(date);
      }

      for (final tuple in selectedTupleList) {
        if (textContent.isNotEmpty) textContent += '\n';
        final amount = tuple['amount'] as double;
        if (tuple['label'] != tuple['currency']) {
          textContent += '${tuple['label']}: ';
        }
        final currency = currencyMap[tuple['currency']]!;
        textContent += currency.formatter.format(amount);
      }

      final textStyle = theme.textTheme.bodyMedium;
      const padding = EdgeInsets.all(5);
      const align = Alignment.topRight;
      double dx = -20, dy = -10;
      const elevation = 1.0;
      final backgroundColor = theme.colorScheme.surface;

      final painter = TextPainter(
        text: TextSpan(text: textContent, style: textStyle),
        textDirection: TextDirection.ltr,
      )..layout();

      final width = padding.left + painter.width + padding.right;
      final height = padding.top + painter.height + padding.bottom;

      if (width + anchor.dx > displayWidth) {
        dx = displayWidth - (width + anchor.dx);
      }
      if (anchor.dy - height - dy < 40) {
        dy = 10 - anchor.dy + height;
      }

      final paintPoint = getBlockPaintPoint(
        anchor + Offset(dx, dy),
        width,
        height,
        align,
      );

      final window = Rect.fromLTWH(paintPoint.dx, paintPoint.dy, width, height);

      final textPaintPoint = paintPoint + padding.topLeft;

      elements = <MarkElement>[
        RectElement(
          rect: window,
          style: PaintStyle(fillColor: backgroundColor, elevation: elevation),
        ),
        LabelElement(
          text: textContent,
          anchor: textPaintPoint,
          style: LabelStyle(textStyle: textStyle, align: Alignment.bottomRight),
        ),
      ];

      return elements;
    }

    return TooltipGuide(
      selections: {'tooltipTouch', 'tooltipMouse'},
      renderer: simpleTooltip,
    );
  }

  @override
  Widget build(final BuildContext context) {
    final data = switch (chartData.report.loadOverTime) {
      true => chartData.data,
      false => switch (chartData.report.sorting) {
        ReportDataSorting.amount =>
          chartData.data..sort(
            (final a, final b) =>
                type != ReportType.pie
                    ? a.amount.compareTo(b.amount)
                    : b.amount.compareTo(a.amount),
          ),
        ReportDataSorting.name =>
          chartData.data..sort(
            (final a, final b) => a.group.label.compareTo(b.group.label),
          ),
      },
    };
    final theme = Theme.of(context);
    final ThemeHandler themeHandler = di.get();

    return LayoutBuilder(
      builder: (final context, final constraints) {
        final maxWidth = constraints.maxWidth;
        final maxHeight =
            type == ReportType.pie
                ? min(maxWidth, constraints.maxHeight)
                : min(maxWidth / 2, constraints.maxHeight);
        return ConstrainedBox(
          constraints: BoxConstraints(maxHeight: maxHeight, maxWidth: maxWidth),
          child: Chart(
            padding:
                (final size) =>
                    (type == ReportType.pie)
                        ? const EdgeInsets.all(10)
                        : EdgeInsets.fromLTRB(
                          50,
                          5,
                          (type == ReportType.line) ? 40 : 10,
                          25,
                        ),
            data: data,
            variables: _variables(constraints),
            marks: _marks(themeHandler.isDark),
            axes: _axes(theme),
            selections: _selections(),
            tooltip: _tooltip(constraints, theme),
            coord: _coord(),
            transforms: _transforms(),
          ),
        );
      },
    );
  }
}
