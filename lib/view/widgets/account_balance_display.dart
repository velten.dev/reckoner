import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/currency.dart';
import '../../service/data_cache.dart';
import 'amount_display.dart';

class BalanceDisplay extends StatelessWidget {
  final Map<String, int> balanceMap;
  final Currency? defaultCurrency;

  const BalanceDisplay({
    super.key,
    required this.balanceMap,
    this.defaultCurrency,
  });

  @override
  Widget build(final BuildContext context) {
    final curMap = di<DataCache>().currencyMap.value;
    final balances =
        balanceMap.entries
            .map(
              (final balance) => AmountDisplay(
                amount: balance.value,
                currency: curMap[balance.key] ?? Currency(),
              ),
            )
            .toList();

    final code = defaultCurrency?.code ?? '';
    if (code.isNotEmpty &&
        balanceMap[code] == null &&
        defaultCurrency != null) {
      balances.insert(0, AmountDisplay(amount: 0, currency: defaultCurrency!));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: balances,
    );
  }
}
