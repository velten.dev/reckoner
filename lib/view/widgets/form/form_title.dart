import 'package:flutter/cupertino.dart';

import '../../l10n/localizations.dart';

Widget titleWidget(
  final BuildContext context,
  final String name,
  final dynamic data,
) {
  return Text(titleString(context, name, data));
}

String titleString(
  final BuildContext context,
  final String name,
  final dynamic data,
) {
  final localization = ReckonerLocalizations.of(context);
  if (data == null) {
    return localization.addItem(name);
  } else {
    return localization.editItem(name);
  }
}
