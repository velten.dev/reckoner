import 'dart:async';

import 'package:flutter/material.dart';

import '../../../service/logger.dart';
import '../../l10n/localizations.dart';

class InputForm extends StatefulWidget {
  final List<Widget> inputs;
  final Widget? child;
  final String title;
  final FutureOr<void> Function()? onSave;
  final FutureOr<void> Function()? onDelete;
  final List<Widget>? actions;
  final GlobalKey<FormState>? formKey;
  final bool alwaysSave;
  final String? deleteLabel;

  const InputForm({
    required this.title,
    this.onSave,
    this.inputs = const [],
    this.child,
    this.actions,
    this.formKey,
    super.key,
    this.alwaysSave = false,
    this.onDelete,
    this.deleteLabel,
  });

  @override
  State<StatefulWidget> createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {
  late bool _doSave;
  late final GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = widget.formKey ?? GlobalKey<FormState>();
    _doSave = widget.alwaysSave;
  }

  Future<void> _save(final BuildContext context) async {
    if (_doSave) {
      if (!_formKey.currentState!.validate()) return;
      _formKey.currentState!.save();
      try {
        await widget.onSave?.call();
      } catch (err, stack) {
        logError('Reckoner', err, stack);
      }
    }
    if (context.mounted) Navigator.pop(context, true);
  }

  @override
  Widget build(final BuildContext context) {
    final localization = ReckonerLocalizations.of(context);
    final save = localization.save;

    return AlertDialog(
      title: Text(widget.title),
      content: SingleChildScrollView(
        child: Form(
          onChanged: () => _doSave = true,
          key: _formKey,
          child:
              widget.child ??
              Column(mainAxisSize: MainAxisSize.min, children: widget.inputs),
        ),
      ),
      actionsPadding: const EdgeInsets.all(8.0),
      actionsAlignment:
          widget.actions == null && widget.onDelete != null
              ? MainAxisAlignment.spaceBetween
              : MainAxisAlignment.end,
      actions:
          widget.actions ??
          [
            if (widget.onDelete != null)
              TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Theme.of(context).colorScheme.error,
                ),
                onPressed: () async {
                  await widget.onDelete?.call();
                  if (context.mounted) Navigator.pop(context, false);
                },
                child: Text(widget.deleteLabel ?? localization.delete),
              ),
            TextButton(onPressed: () => _save(context), child: Text(save)),
          ],
    );
  }
}
