import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';

class ColorInputForm extends FormField<Color> {
  final void Function(Color? value)? onChanged;
  final String? textLabel;
  final bool showQuickColors;

  ColorInputForm({
    super.autovalidateMode = AutovalidateMode.disabled,
    super.key,
    this.onChanged,
    super.onSaved,
    required final Color initialValue,
    super.validator,
    this.textLabel,
    this.showQuickColors = false,
  }) : super(
         initialValue: initialValue,
         builder: (final FormFieldState<Color> field) {
           final String? errorText =
               field.isValid ? null : 'Missing required input';
           final InputDecoration decoration = InputDecoration(
             labelText: textLabel,
             errorText: errorText,
           ).applyDefaults(Theme.of(field.context).inputDecorationTheme);
           return InputDecorator(
             decoration: decoration,
             child: _ColorPicker(
               initialValue: initialValue,
               onChanged: (final value) {
                 onChanged?.call(value);
                 field.didChange(value);
               },
               showQuickColors: showQuickColors,
             ),
           );
         },
       );
}

class _ColorPicker extends StatefulWidget {
  final Color initialValue;
  final void Function(Color) onChanged;
  final bool showQuickColors;

  const _ColorPicker({
    required this.initialValue,
    required this.onChanged,
    required this.showQuickColors,
  });

  @override
  State<StatefulWidget> createState() => _ColorPickerState();
}

class _ColorPickerState extends State<_ColorPicker> {
  late Color selectedColor;

  @override
  void initState() {
    super.initState();
    selectedColor = widget.initialValue;
  }

  Future<void> _colorPicker() async {
    var color = selectedColor;
    await showDialog<void>(
      context: context,
      builder:
          (final context) => AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ColorPicker(
                  color: selectedColor,
                  onColorChanged: (final value) => color = value,
                  pickersEnabled: const <ColorPickerType, bool>{
                    ColorPickerType.both: false,
                    ColorPickerType.primary: true,
                    ColorPickerType.accent: true,
                    ColorPickerType.bw: false,
                    ColorPickerType.custom: true,
                    ColorPickerType.wheel: true,
                  },
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('Done'),
                ),
              ],
            ),
          ),
    );
    if (mounted) {
      setState(() => selectedColor = color);
      widget.onChanged(color);
    }
  }

  @override
  Widget build(final BuildContext context) {
    return Row(
      children: [
        IconButton(
          color: selectedColor,
          splashRadius: 20,
          onPressed: _colorPicker,
          icon: const Icon(Icons.color_lens),
          iconSize: 30,
        ),
        if (widget.showQuickColors)
          Expanded(
            child: ColorPicker(
              color: selectedColor,
              onColorChanged: (final value) {
                setState(() => selectedColor = value);
                widget.onChanged(value);
              },
              pickersEnabled: const <ColorPickerType, bool>{
                ColorPickerType.both: false,
                ColorPickerType.primary: true,
                ColorPickerType.accent: false,
                ColorPickerType.bw: false,
                ColorPickerType.custom: false,
                ColorPickerType.wheel: false,
              },
              enableShadesSelection: false,
            ),
          ),
      ],
    );
  }
}
