import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../util/random_ascii_string.dart';
import '../default_icon_button.dart';
import 'validators.dart';

class PasswordFormField extends StatefulWidget {
  final String initialValue;
  final ValueChanged<String>? onChanged;
  final FormFieldSetter<String>? onSaved;
  final String label;
  final FormFieldValidator<String>? validator;
  final bool hideGenerator;

  const PasswordFormField({
    super.key,
    this.initialValue = '',
    this.onChanged,
    this.onSaved,
    this.label = '',
    this.validator,
    this.hideGenerator = false,
  });

  @override
  State<StatefulWidget> createState() => _PasswordFormFieldState();
}

class _PasswordFormFieldState extends State<PasswordFormField> {
  bool obscureText = true;
  late final TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: widget.initialValue);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextFormField(
            controller: controller,
            decoration:
                widget.label.isNotEmpty
                    ? InputDecoration(label: Text(widget.label))
                    : null,
            obscureText: obscureText,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]')),
            ],
            onChanged: widget.onChanged,
            onSaved: widget.onSaved,
            validator:
                widget.validator ??
                (final value) {
                  final error = nonNullValidator(context, value);
                  if (error != null) return error;
                  if (value!.length < 16) {
                    return 'Password must be at least 16 characters';
                  }
                  return null;
                },
          ),
        ),
        DefaultIconButton(
          icon: Icon(obscureText ? Icons.visibility_off : Icons.visibility),
          onPressed: () => setState(() => obscureText = !obscureText),
        ),
        if (!widget.hideGenerator)
          DefaultIconButton(
            icon: const Icon(Icons.sync),
            onPressed:
                () => setState(() {
                  controller.text = randomAsciiString();
                  widget.onChanged?.call(controller.text);
                }),
          ),
      ],
    );
  }
}
