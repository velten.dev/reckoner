import 'package:flutter/material.dart';

import '../../util/popup_overlay.dart';
import '../default_icon_button.dart';

class MultiselectSearchFormField<T> extends FormField<List<T>> {
  final List<T> searchItems;
  final String Function(T) itemToDisplayString;
  final String Function(T)? itemToSearchString;
  final T? Function(String)? stringToItem;
  final void Function(List<T>?)? onChanged;
  final InputDecoration decoration;
  final bool showDeleteChip;
  final int? maxTextLines;
  final TextOverflow? textOverflow;

  MultiselectSearchFormField({
    required this.searchItems,
    required this.itemToDisplayString,
    this.itemToSearchString,
    this.decoration = const InputDecoration(
      suffixIcon: Icon(Icons.arrow_drop_down_rounded),
    ),
    this.showDeleteChip = false,
    this.onChanged,
    this.stringToItem,
    super.autovalidateMode,
    super.enabled,
    super.initialValue,
    super.key,
    super.onSaved,
    super.restorationId,
    super.validator,
    this.maxTextLines,
    this.textOverflow,
  }) : super(
         builder: (final field) {
           final state = field as _MultiselectSearchFormField<T>;
           final value = field.value;
           Widget? child;
           if (value != null && value.isNotEmpty) {
             if (showDeleteChip) {
               child = Wrap(
                 children:
                     field.value!
                         .map((final e) => state._itemAsChip(e))
                         .toList(),
               );
             } else {
               child = Text(
                 value
                     .map((final e) => itemToDisplayString(e))
                     .reduce(
                       (final value, final element) => '$value, $element',
                     ),
                 maxLines: maxTextLines,
                 overflow: textOverflow,
               );
             }
           }
           return InkWell(
             onTap: () => state.popupSelector(),
             child: InputDecorator(
               decoration: decoration.copyWith(errorText: field.errorText),
               child: child,
             ),
           );
         },
       );

  @override
  FormFieldState<List<T>> createState() => _MultiselectSearchFormField();
}

class _MultiselectSearchFormField<T> extends FormFieldState<List<T>> {
  @override
  MultiselectSearchFormField<T> get widget =>
      super.widget as MultiselectSearchFormField<T>;

  Widget _itemAsChip(final T item) {
    return Chip(
      label: Text(
        widget.itemToDisplayString(item),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.titleSmall,
      ),
      onDeleted: () {
        final list = value;
        if (list?.remove(item) ?? false) didChange(list);
      },
      deleteIcon: const Icon(Icons.close_outlined),
    );
  }

  Future<void> popupSelector() async {
    final newValue = await popupOverlay<List<T>>(
      context,
      _PopupSelector<T>(
        itemToDisplayString: widget.itemToDisplayString,
        itemToSearchString: widget.itemToSearchString,
        items: widget.searchItems,
        initialSelected: value ?? widget.initialValue ?? [],
        stringToItem: widget.stringToItem,
      ),
    );
    if (newValue == value || newValue == null) return;
    widget.onChanged?.call(newValue);
    if (mounted) didChange(newValue);
  }
}

class _PopupSelector<T> extends StatefulWidget {
  final List<T> items;
  final String Function(T) itemToDisplayString;
  final String Function(T)? itemToSearchString;
  final List<T> initialSelected;
  final T? Function(String)? stringToItem;

  const _PopupSelector({
    super.key,
    required this.items,
    required this.itemToDisplayString,
    this.initialSelected = const [],
    this.stringToItem,
    this.itemToSearchString,
  });

  @override
  State<StatefulWidget> createState() => _PopupSelectorState<T>();
}

class _PopupSelectorState<T> extends State<_PopupSelector<T>> {
  late TextEditingController controller;
  late Map<String, T> fullItemMap;
  late Map<String, T> foundItems;
  late List<T> selected;
  List<T> addedItems = [];

  @override
  void initState() {
    super.initState();
    selected = List.from(widget.initialSelected);
    final mapFunction = widget.itemToSearchString ?? widget.itemToDisplayString;
    fullItemMap = widget.items.asMap().map(
      (final key, final value) => MapEntry(mapFunction(value), value),
    );
    if (widget.stringToItem == null) {
      fullItemMap = Map<String, T>.unmodifiable(fullItemMap);
    }
    foundItems = fullItemMap;
    controller = TextEditingController();
    controller.addListener(_textChange);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _textChange() {
    if (controller.text.isEmpty) {
      if (foundItems == fullItemMap) return;
      setState(() => foundItems = fullItemMap);
    }
    final Map<String, T> searchItems = {};
    final searchText = controller.text.toLowerCase();
    fullItemMap.forEach((final key, final value) {
      if (key.toLowerCase().contains(searchText)) {
        searchItems[key] = value;
      }
    });
    if (widget.stringToItem != null &&
        !searchItems.containsKey(controller.text)) {
      final item = widget.stringToItem!(controller.text);
      if (item != null) searchItems[controller.text] = item;
    }

    setState(() => foundItems = searchItems);
  }

  double get height {
    final viewHeight = MediaQuery.of(context).size.height;
    if (viewHeight < 250) return 250;
    return 250;
  }

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: height,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: TextField(
                  controller: controller,
                  onSubmitted:
                      (final val) => Navigator.pop(
                        context,
                        foundItems.isNotEmpty ? foundItems.values.first : null,
                      ),
                ),
              ),
              DefaultIconButton(
                onPressed: () {
                  widget.items.addAll(addedItems);
                  Navigator.pop(context, selected);
                },
                icon: const Icon(Icons.check),
              ),
              DefaultIconButton(
                onPressed: () => Navigator.pop(context, widget.initialSelected),
                icon: const Icon(Icons.cancel),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: [
                for (final map in foundItems.entries)
                  ListTile(
                    leading: Checkbox(
                      value: selected.contains(map.value),
                      onChanged: (final sel) {
                        if (sel ?? false) {
                          if (widget.stringToItem != null &&
                              !addedItems.contains(sel) &&
                              !fullItemMap.containsKey(map.key)) {
                            addedItems.add(map.value);
                            fullItemMap[map.key] = map.value;
                          }
                          setState(() => selected.add(map.value));
                        } else {
                          if (widget.stringToItem != null &&
                              addedItems.contains(map.value)) {
                            addedItems.remove(map.value);
                            fullItemMap.remove(map.key);
                          }
                          setState(() => selected.remove(map.value));
                        }
                      },
                    ),
                    selected: selected.contains(map.value),
                    title: Text(widget.itemToDisplayString(map.value)),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
