import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../../model/tz_date_time.dart';
import '../default_icon_button.dart';

class DateInputForm extends FormField<TZDateTime> {
  final void Function(TZDateTime? value)? onChanged;
  final String? textLabel;
  static final format = DateFormat.yMd();

  DateInputForm({
    super.key,
    super.autovalidateMode = AutovalidateMode.disabled,
    this.onChanged,
    super.onSaved,
    super.initialValue,
    super.validator,
    this.textLabel,
  }) : super(
         builder: (final FormFieldState<TZDateTime> field) {
           final state = field as _DateInput;
           final String? errorText =
               state.isValid ? null : 'Missing required input';
           final InputDecoration decoration = InputDecoration(
             labelText: textLabel,
             errorText: errorText,
             suffixIcon: DefaultIconButton(
               icon: const Icon(Icons.calendar_today),
               onPressed: () => state.requestUpdate(),
             ),
           ).applyDefaults(Theme.of(field.context).inputDecorationTheme);
           return TextField(
             keyboardType: TextInputType.datetime,
             controller: state.controller,
             onChanged: (final val) {
               final value = tryParse(val);
               if (value != null) state.date = value;
               onChanged?.call(value);
               field.didChange(value);
             },
             inputFormatters: const [
               TextInputFormatter.withFunction(textFormatter),
             ],
             decoration: decoration,
           );
         },
       );

  @override
  FormFieldState<TZDateTime> createState() => _DateInput();

  static TextEditingValue textFormatter(
    final TextEditingValue oldValue,
    final TextEditingValue newValue,
  ) {
    if (newValue.text.length >= 10) {
      final tempVal = tryParse(newValue.text);
      if (tempVal != null) {
        return TextEditingValue(text: tryFormat(tempVal));
      } else {
        return oldValue;
      }
    }
    return newValue;
  }

  /// Returns an empty string if [DateFormat.format()] throws or [date] is null.
  static String tryFormat(final TZDateTime? date) {
    if (date != null) {
      try {
        return format.format(date);
      } catch (_) {}
    }
    return '';
  }

  /// Returns null if [format.parse()] throws or string is empty.
  static TZDateTime? tryParse(final String string) {
    if (string.isNotEmpty) {
      try {
        return tzParseDate(format.parse(string));
      } catch (_) {}
    }
    return null;
  }
}

class _DateInput extends FormFieldState<TZDateTime> {
  late TextEditingController controller;
  bool isShowingDialog = false;
  late TZDateTime date;

  @override
  DateInputForm get widget => super.widget as DateInputForm;

  @override
  void initState() {
    super.initState();
    date = widget.initialValue ?? tzNow();
    controller = TextEditingController(text: DateInputForm.tryFormat(date));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<void> requestUpdate() async {
    if (!isShowingDialog) {
      isShowingDialog = true;
      final pickedDate = await showDatePicker(
        context: context,
        firstDate: DateTime(1900),
        initialDate: date,
        lastDate: DateTime(2100),
      );
      final newValue = pickedDate == null ? null : tzParseDate(pickedDate);
      isShowingDialog = false;
      if (newValue != null) {
        date = newValue;
        controller.text = DateInputForm.tryFormat(newValue);
        if (widget.onChanged != null) widget.onChanged!(newValue);
      }
    }
  }
}
