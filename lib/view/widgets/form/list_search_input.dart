import 'package:flutter/material.dart';

import '../../util/popup_overlay.dart';
import '../default_icon_button.dart';

class ListSearchFormField<T> extends FormField<T> {
  final List<T> searchItems;
  final String Function(T) itemToDisplayString;
  final Widget Function(T)? itemToDisplayWidget;
  final String Function(T)? itemToSearchString;
  final void Function(T?)? onChanged;
  final InputDecoration decoration;
  final T? Function(String)? stringToItem;

  ListSearchFormField({
    required this.searchItems,
    required this.itemToDisplayString,
    this.itemToDisplayWidget,
    this.itemToSearchString,
    this.decoration = const InputDecoration(
      suffixIcon: Icon(Icons.arrow_drop_down_rounded),
    ),
    this.onChanged,
    this.stringToItem,
    super.autovalidateMode,
    super.enabled,
    super.initialValue,
    super.key,
    super.onSaved,
    super.restorationId,
    super.validator,
  }) : super(
         builder:
             (final field) => InkWell(
               onTap:
                   () =>
                       (field as _ListSearchFormFieldState<T>).popupSelector(),
               child: InputDecorator(
                 decoration: decoration.copyWith(errorText: field.errorText),
                 child:
                     field.value != null
                         ? itemToDisplayWidget?.call(field.value as T) ??
                             Text(
                               itemToDisplayString(field.value as T),
                               overflow: TextOverflow.ellipsis,
                             )
                         : null,
               ),
             ),
       );

  @override
  FormFieldState<T> createState() => _ListSearchFormFieldState();
}

class _ListSearchFormFieldState<T> extends FormFieldState<T> {
  @override
  ListSearchFormField<T> get widget => super.widget as ListSearchFormField<T>;

  Future<void> popupSelector() async {
    final result = await popupOverlay<(T?, bool)>(
      context,
      _PopupSelector(
        itemToDisplayString: widget.itemToDisplayString,
        itemToSearchString: widget.itemToSearchString,
        itemToDisplayWidget: widget.itemToDisplayWidget,
        items: widget.searchItems,
        stringToItem: widget.stringToItem,
      ),
    );
    if (result == null) return;
    final newValue = result.$1;
    if (newValue == value) return;
    widget.onChanged?.call(newValue);
    if (mounted) didChange(newValue);
  }
}

class _PopupSelector<T> extends StatefulWidget {
  final List<T> items;
  final String Function(T) itemToDisplayString;
  final String Function(T)? itemToSearchString;
  final T? Function(String)? stringToItem;
  final Widget Function(T)? itemToDisplayWidget;

  const _PopupSelector({
    super.key,
    required this.items,
    required this.itemToDisplayString,
    this.itemToDisplayWidget,
    this.itemToSearchString,
    this.stringToItem,
  });

  @override
  State<StatefulWidget> createState() => _PopupSelectorState<T>();
}

class _PopupSelectorState<T> extends State<_PopupSelector<T>> {
  late TextEditingController controller;
  late Map<String, T> fullItemMap;
  late Map<String, T> foundItems;

  @override
  void initState() {
    super.initState();
    final mapFunction = widget.itemToSearchString ?? widget.itemToDisplayString;
    fullItemMap = widget.items.asMap().map(
      (final key, final value) => MapEntry(mapFunction(value), value),
    );
    if (widget.stringToItem == null) {
      fullItemMap = Map<String, T>.unmodifiable(fullItemMap);
    }
    foundItems = fullItemMap;
    controller = TextEditingController();
    controller.addListener(_textChange);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _textChange() {
    if (controller.text.isEmpty) {
      if (foundItems == fullItemMap) return;
      setState(() => foundItems = fullItemMap);
    }
    final Map<String, T> searchItems = {};
    final searchText = controller.text.toLowerCase();
    fullItemMap.forEach((final key, final value) {
      if (key.toLowerCase().contains(searchText)) {
        searchItems[key] = value;
      }
    });
    if (widget.stringToItem != null &&
        !searchItems.containsKey(controller.text)) {
      final item = widget.stringToItem!(controller.text);
      if (item != null) searchItems[controller.text] = item;
    }

    setState(() => foundItems = searchItems);
  }

  double get height {
    final viewHeight = MediaQuery.of(context).size.height;
    if (viewHeight < 250) return 250;
    return 250;
  }

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: height,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Expanded(
                child: TextField(
                  controller: controller,
                  onSubmitted:
                      (final val) => Navigator.pop(context, (
                        foundItems.isNotEmpty ? foundItems.values.first : null,
                        true,
                      )),
                ),
              ),
              DefaultIconButton(
                icon: const Icon(Icons.clear),
                onPressed: () => Navigator.pop(context, (null, true)),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: [
                for (final map in foundItems.entries)
                  ListTile(
                    onTap: () => Navigator.pop(context, (map.value, true)),
                    title:
                        widget.itemToDisplayWidget?.call(map.value) ??
                        Text(widget.itemToDisplayString(map.value)),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
