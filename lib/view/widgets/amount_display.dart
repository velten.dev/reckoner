import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/currency.dart';
import '../theme.dart';

class AmountDisplay extends StatelessWidget {
  final int amount;
  final Currency currency;
  final bool transfer;
  final TextStyle? style;

  const AmountDisplay({
    required this.amount,
    required this.currency,
    this.transfer = false,
    this.style,
    super.key,
  });

  static Color? amountColor(final int amount, [final bool transfer = false]) {
    final ThemeHandler themeHandler = di();
    return transfer
        ? themeHandler.isDark
            ? Colors.blueAccent[100]
            : Colors.blue[900]
        : amount >= 0
        ? themeHandler.isDark
            ? Colors.greenAccent[100]
            : Colors.green[900]
        : themeHandler.isDark
        ? Colors.redAccent[100]
        : Colors.red[900];
  }

  @override
  Widget build(final BuildContext context) {
    final String text = currency.formatAmount(transfer ? amount.abs() : amount);
    final color = amountColor(amount, transfer);

    final TextStyle displayStyle =
        style?.copyWith(
          color: color,
          fontWeight: transfer ? FontWeight.normal : null,
        ) ??
        TextStyle(color: color);

    return Text(
      text,
      overflow: TextOverflow.ellipsis,
      style: displayStyle,
      textAlign: TextAlign.right,
    );
  }
}
