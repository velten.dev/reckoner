import 'package:flutter/material.dart';

import '../../../model/model.dart';
import '../../../model/transaction.dart';
import '../form/validators.dart';

String? Function(NamedInterface?) transferAccountValidator(
  final BuildContext context,
  final TransactionTransfer transfer,
) {
  return (final value) {
    final message = nonNullValidator(context, value);
    if (message != null) return message;
    if (transfer.source.account == transfer.destination.account) {
      return 'Transfers must use different accounts';
    }
    return null;
  };
}
