import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/transaction_type.dart';

class TransactionTypeFilterSelect extends StatelessWidget {
  const TransactionTypeFilterSelect({super.key});

  @override
  Widget build(final BuildContext context) {
    return PopupMenuButton<TransactionTypeFilter>(
      color: Theme.of(context).colorScheme.secondaryFixed,
      position: PopupMenuPosition.under,
      tooltip: 'Filter Transactions Type',
      icon: const Icon(Icons.filter_alt),
      splashRadius: 25,
      onSelected: (final value) {
        di<ValueNotifier<TransactionTypeFilter>>().value = value;
      },
      itemBuilder:
          (final context) => <PopupMenuEntry<TransactionTypeFilter>>[
            const PopupMenuItem<TransactionTypeFilter>(
              value: TransactionTypeFilter.all,
              child: Center(child: Text('All')),
            ),
            const PopupMenuItem<TransactionTypeFilter>(
              value: TransactionTypeFilter.expense,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.arrow_downward, color: Colors.red),
                  Text('Expense', style: TextStyle(color: Colors.red)),
                ],
              ),
            ),
            const PopupMenuItem<TransactionTypeFilter>(
              value: TransactionTypeFilter.income,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.arrow_upward, color: Colors.green),
                  Text('Income', style: TextStyle(color: Colors.green)),
                ],
              ),
            ),
          ],
    );
  }
}
