import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../model/report.dart';
import '../default_icon_button.dart';
import '../form/form_title.dart';
import '../form/input_form.dart';
import '../report_filter_inputs.dart';

class TransactionFilterButton extends StatelessWidget {
  const TransactionFilterButton({super.key});

  @override
  Widget build(final BuildContext context) {
    return DefaultIconButton(
      icon: Icon(Icons.filter_alt),
      onPressed:
          () => showDialog<void>(
            context: context,
            builder: (final context) {
              final filter = ReportTransactionFilter.fromJson(
                di<ValueNotifier<ReportTransactionFilter>>().value.toJson(),
              );

              return InputForm(
                title: titleString(context, 'Transaction Filter', filter),
                onSave:
                    () =>
                        di<ValueNotifier<ReportTransactionFilter>>().value =
                            filter,
                onDelete:
                    () =>
                        di<ValueNotifier<ReportTransactionFilter>>().value =
                            ReportTransactionFilter(),
                deleteLabel: 'Reset',
                inputs: [
                  ReportFilterTxTypeEdit(filter),
                  ReportFilterEdit(filter),
                ],
              );
            },
          ),
    );
  }
}
