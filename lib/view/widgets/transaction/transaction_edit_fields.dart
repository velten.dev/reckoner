import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_simple_calculator/flutter_simple_calculator.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:watch_it/watch_it.dart';

import '../../../model/account.dart';
import '../../../model/category.dart';
import '../../../model/currency.dart';
import '../../../model/model.dart';
import '../../../model/transaction.dart';
import '../../../model/tz_date_time.dart';
import '../../../service/data_cache.dart';
import '../../model/currency_input_formatter.dart';
import '../default_icon_button.dart';
import '../form/date_input.dart';
import '../form/input_form.dart';
import '../form/list_search_input.dart';
import '../form/popup_menu_button_input.dart';
import '../form/validators.dart';
import '../location_edit.dart';
import 'transaction_edit_data.dart';

CurrencyInputFormatter txCurFormatter(
  final BuildContext context,
  final Transaction model, [
  final bool showSymbol = true,
]) {
  final currencyMap = di<DataCache>().currencyMap.value;
  final currency = currencyMap[model.currencyCode] ?? model.currency;
  return CurrencyInputFormatter(
    currency: currency,
    enableNegative: false,
    showSymbol: showSymbol,
  );
}

class TxNameEdit extends StatelessWidget {
  final NamedInterface model;
  final AutovalidateMode autovalidateMode;
  final ValueChanged<String>? onChanged;
  final bool showLabel;

  const TxNameEdit({
    super.key,
    required this.model,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.onChanged,
    this.showLabel = false,
  });

  @override
  Widget build(final BuildContext context) {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      autovalidateMode: autovalidateMode,
      decoration:
          showLabel ? const InputDecoration(labelText: 'Description') : null,
      initialValue: model.name,
      onChanged: onChanged ?? (final value) => model.name = value,
      validator: (final value) => nonNullValidator(context, value),
    );
  }
}

class TxMerchantEdit extends StatelessWidget {
  final Transaction transaction;
  final void Function(VoidCallback fn)? setState;

  const TxMerchantEdit(this.transaction, {this.setState, super.key});

  @override
  Widget build(final BuildContext context) {
    final cache = di.get<DataCache>();

    final initialValue = transaction.merchantName;

    return Row(
      children: [
        Expanded(
          child: ListSearchFormField<String>(
            decoration: InputDecoration(
              suffixIcon: const Icon(Icons.arrow_drop_down_rounded),
              label: Text('Merchant'),
            ),
            onChanged:
                (final v) =>
                    setState == null
                        ? transaction.merchantName = v ?? ''
                        : setState!(() => transaction.merchantName = v ?? ''),
            onSaved: (final v) => transaction.merchantName = v ?? '',
            initialValue: initialValue,
            itemToDisplayString: (final item) => item,
            itemToDisplayWidget: (final m) {
              if (cache.merchants.value.contains(m)) return Text(m);
              if (m.trim().isEmpty) return SizedBox.shrink();
              return Tooltip(
                message: 'Unsaved new merchant',
                child: Row(
                  children: [
                    Text(m, overflow: TextOverflow.ellipsis),
                    Spacer(),
                    Icon(Icons.add_business),
                  ],
                ),
              );
            },
            searchItems: cache.merchants.value.toList(),
            stringToItem: (final v) => v,
          ),
        ),
        DefaultIconButton(
          icon: const Icon(Icons.location_on),
          onPressed: () async {
            final location = transaction.location ?? Location();
            await showDialog<void>(
              context: context,
              builder:
                  (final context) => InputForm(
                    title: 'Location Edit',
                    inputs: [LocationEdit(location: location)],
                  ),
            );

            if (location.hasData) transaction.location = location;
          },
        ),
      ],
    );
  }
}

class TxAccountEdit extends StatelessWidget {
  final Transaction transaction;
  final FormFieldValidator<TransactionAccount?>? validator;
  final Account? filterAccount;
  final void Function(VoidCallback fn)? setState;
  final String label;

  const TxAccountEdit({
    super.key,
    required this.transaction,
    this.validator,
    this.filterAccount,
    this.label = 'Account',
    this.setState,
  });

  @override
  Widget build(final BuildContext context) {
    final cache = di.get<DataCache>();

    return ListSearchFormField<Account>(
      decoration: InputDecoration(
        suffixIcon: const Icon(Icons.arrow_drop_down_rounded),
        label: Text(label),
      ),
      onChanged:
          (final a) =>
              setState == null
                  ? transaction.account = a
                  : setState!(() => transaction.account = a),
      initialValue: transaction.account,
      itemToDisplayString: (final item) => item.displayName,
      searchItems: cache.accounts.value.toList()..remove(filterAccount),
      validator: (final value) => nonNullValidator(context, value),
    );
  }
}

enum CurrencyDisplay { name, displayName, symbol, code, short }

class TxCurrencyEdit extends StatelessWidget {
  final Transaction model;
  final AutovalidateMode autovalidateMode;
  final CurrencyDisplay displayType;
  final ValueChanged<Currency?>? onChanged;
  final bool showLabel;

  const TxCurrencyEdit({
    super.key,
    required this.model,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.displayType = CurrencyDisplay.name,
    this.onChanged,
    this.showLabel = false,
  });

  @override
  Widget build(final BuildContext context) {
    if (model.account.isMultiCurrency ||
        model.account.defaultCurrencyCode == null) {
      String currencyToString(final Currency? item) {
        if (item == null || item.name.isEmpty) return '';
        return switch (displayType) {
          CurrencyDisplay.displayName => item.displayName,
          CurrencyDisplay.name => item.name,
          CurrencyDisplay.symbol => item.symbol,
          CurrencyDisplay.code => item.code,
          CurrencyDisplay.short => '${item.code} (${item.symbol})',
        };
      }

      final currencies = di.get<DataCache>().currencyMap.value.values;
      if (displayType == CurrencyDisplay.symbol ||
          displayType == CurrencyDisplay.code ||
          displayType == CurrencyDisplay.short) {
        return PopupMenuButtonFormField<Currency>(
          color: Theme.of(context).colorScheme.secondaryFixed,
          onChanged: onChanged ?? (final currency) => model.currency = currency,
          initialValue: model.currency,
          itemBuilder:
              (final context) =>
                  currencies
                      .map(
                        (final e) => PopupMenuItem<Currency>(
                          value: e,
                          child: Center(
                            child: Text(
                              currencyToString(e),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      )
                      .toList(),
          widgetBuilder:
              (final currency) => Row(
                children: [
                  Expanded(
                    child: Text(
                      currencyToString(currency),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const Icon(Icons.arrow_drop_down_rounded),
                ],
              ),
          validator: (final value) {
            final ret = nonNullValidator(context, value?.code);
            return ret;
          },
        );
      }
      return ListSearchFormField<Currency>(
        itemToDisplayString: currencyToString,
        decoration: const InputDecoration(
          suffixIcon: Icon(Icons.arrow_drop_down_rounded),
          label: Text('Currency'),
        ),
        itemToSearchString:
            (final item) => '${item.name} ${item.code} ${item.symbol}',
        searchItems: di.get<DataCache>().currencyMap.value.values.toList(),
        initialValue: model.currency,
        onChanged:
            onChanged ??
            (final currency) => model.currency = currency ?? model.currency,
        validator: (final value) => nonNullValidator(context, value),
      );
    }
    return const SizedBox.shrink();
  }
}

class TxAmountAndCurrencyEdit extends StatefulWidget {
  final Transaction model;
  final InputDecoration? decoration;
  final TransactionAmountNotifier updateAmounts;

  const TxAmountAndCurrencyEdit({
    super.key,
    required this.model,
    required this.updateAmounts,
    this.decoration,
  });

  @override
  State<StatefulWidget> createState() => _TxAmountAndCurrencyEditState();
}

class _TxAmountAndCurrencyEditState extends State<TxAmountAndCurrencyEdit> {
  late final Transaction model;

  @override
  void initState() {
    super.initState();
    model = widget.model;
  }

  @override
  Widget build(final BuildContext context) {
    final currencyEdit = txCurrencyEdit(
      model,
      onChanged: (final currency) {
        if (currency == null) return;
        setState(() => model.currency = currency);
        widget.updateAmounts.notify();
      },
      displayType: CurrencyDisplay.short,
    );
    return Row(
      children: [
        if (currencyEdit != null) SizedBox(width: 90, child: currencyEdit),
        if (currencyEdit != null) SizedBox(width: 10),
        Expanded(
          child: TxAmountEdit(
            transaction: model,
            updateAmounts: widget.updateAmounts,
            decoration: widget.decoration,
          ),
        ),
      ],
    );
  }
}

Widget? txCurrencyEdit(
  final Transaction model, {
  final AutovalidateMode autovalidateMode = AutovalidateMode.onUserInteraction,
  final CurrencyDisplay displayType = CurrencyDisplay.name,
  final ValueChanged<Currency?>? onChanged,
  final bool showLabel = false,
}) {
  if (model.account.isMultiCurrency ||
      model.account.defaultCurrencyCode == null) {
    return TxCurrencyEdit(
      model: model,
      autovalidateMode: autovalidateMode,
      displayType: displayType,
      onChanged: onChanged,
      showLabel: showLabel,
    );
  }
  return null;
}

class TxAmountEdit extends StatefulWidget {
  final Transaction transaction;
  final TransactionSplit? split;
  final AutovalidateMode autovalidateMode;
  final ValueChanged<int>? onSaved;
  final InputDecoration? decoration;
  final bool showLabel;
  final bool showSymbol;
  final TransactionAmountNotifier updateAmounts;

  const TxAmountEdit({
    super.key,
    required this.transaction,
    required this.updateAmounts,
    this.split,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.onSaved,
    this.showLabel = false,
    this.showSymbol = true,
    this.decoration,
  });

  @override
  State<StatefulWidget> createState() => _TxAmountEditState();
}

class _TxAmountEditState extends State<TxAmountEdit> {
  late AmountInterface model;
  late FocusNode focusNode;
  late TextEditingController textController;
  late CurrencyInputFormatter formatter;
  late final int sign;

  @override
  void initState() {
    super.initState();
    sign = widget.transaction.amount >= 0 ? 1 : -1;
    model = (widget.split ?? widget.transaction) as AmountInterface;
    formatter = txCurFormatter(context, widget.transaction, widget.showSymbol);
    textController = TextEditingController(
      text: formatter.absFormat(formatter.fromDB(model.amount)),
    );
    widget.updateAmounts.addListener(
      () =>
          textController.text = formatter.absFormat(
            formatter.fromDB(model.amount),
          ),
    );
    focusNode =
        FocusNode()..addListener(() {
          if (focusNode.hasFocus) {
            textController.selection = TextSelection(
              baseOffset: 0,
              extentOffset: textController.text.length,
            );
          } else {
            _onEditingComplete();
          }
        });
  }

  void _onEditingComplete() {
    model.amount =
        sign * formatter.toDB(formatter.absFormat(textController.text));
    balanceTxSplits(widget.transaction, widget.split);
    widget.updateAmounts.notify();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    formatter = txCurFormatter(context, widget.transaction, widget.showSymbol);
    Future.delayed(
      const Duration(seconds: 1),
      () =>
          textController.text = formatter.absFormat(
            formatter.fromDB(model.amount),
          ),
    );

    return Row(
      children: [
        Expanded(
          child: TextFormField(
            keyboardType: TextInputType.number,
            controller: textController,
            autovalidateMode: widget.autovalidateMode,
            decoration:
                widget.decoration ??
                (widget.showLabel
                    ? const InputDecoration(labelText: 'Amount')
                    : null),
            onSaved: (final value) {
              _onEditingComplete();
              widget.onSaved?.call(sign * formatter.toDB(value ?? '0').abs());
            },
            textAlign: TextAlign.right,
            onEditingComplete: _onEditingComplete,
            focusNode: focusNode,
          ),
        ),
        IconButton(
          onPressed: () async {
            final val = await showModalBottomSheet<double>(
              isScrollControlled: true,
              context: context,
              builder: (final BuildContext context) {
                final calcController = CalcController();
                return SizedBox(
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: SimpleCalculator(
                    controller: calcController,
                    value:
                        formatter.currency
                            .amountToDecimal(model.amount)
                            .toDouble(),
                    onChanged: (final key, final value, final expression) {
                      if (calcController.acLabel == 'AC' &&
                          (calcController.expression?.contains(
                                RegExp('[×÷\\-\\+%]'),
                              ) ??
                              false)) {
                        Navigator.pop(context, calcController.value);
                        calcController.dispose();
                      }
                    },
                  ),
                );
              },
            );
            if (val != null) {
              textController.text = val.abs().toString();
              _onEditingComplete();
            }
          },
          icon: FaIcon(FontAwesomeIcons.calculator),
        ),
      ],
    );
  }
}

class TxDateEdit extends StatelessWidget {
  final Transaction model;
  final AutovalidateMode autovalidateMode;
  final ValueChanged<TZDateTime?>? onChanged;
  final bool showLabel;

  const TxDateEdit({
    super.key,
    required this.model,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.onChanged,
    this.showLabel = false,
  });

  @override
  Widget build(final BuildContext context) {
    return DateInputForm(
      autovalidateMode: autovalidateMode,
      textLabel: showLabel ? 'Date' : null,
      initialValue: model.date,
      onChanged: onChanged ?? (final value) => model.date = value ?? model.date,
      validator: (final value) => nonNullValidator(context, value),
    );
  }
}

class TxTagEdit extends StatelessWidget {
  final TransactionSplit model;
  final AutovalidateMode autovalidateMode;
  final FormFieldSetter<String>? onSaved;
  final bool showLabel;
  final bool showDeleteChip;
  final int? maxTextLines;
  final TextOverflow? textOverflow;

  const TxTagEdit({
    super.key,
    required this.model,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.onSaved,
    this.showLabel = false,
    this.showDeleteChip = false,
    this.maxTextLines,
    this.textOverflow,
  });

  @override
  Widget build(final BuildContext context) {
    return TextFormField(
      textCapitalization: TextCapitalization.words,
      autovalidateMode: autovalidateMode,
      decoration: InputDecoration(label: showLabel ? const Text('Tags') : null),
      initialValue: model.tags.join(', '),
      onSaved: onSaved ?? (final tags) => model.tags = tagStrToSet(tags),
    );
  }
}

Set<String> tagStrToSet(final String? tags) =>
    (tags ?? '')
        .split(',')
        .map((final e) => e.trim())
        .where((final e) => e.isNotEmpty)
        .toSet();

class TxCategoryEdit extends StatelessWidget {
  final CategoryGroup group;
  final TransactionSplit model;
  final AutovalidateMode autovalidateMode;
  final ValueChanged<Category?>? onChanged;
  final bool showLabel;
  final bool validate;

  const TxCategoryEdit({
    super.key,
    required this.group,
    required this.model,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.onChanged,
    this.showLabel = false,
    this.validate = true,
  });

  @override
  Widget build(final BuildContext context) {
    return ListSearchFormField<Category>(
      autovalidateMode: autovalidateMode,
      decoration: InputDecoration(
        suffixIcon: const Icon(Icons.arrow_drop_down_rounded),
        label: showLabel ? Text(group.name) : null,
      ),
      onChanged:
          onChanged ??
          (final data) =>
              data != null
                  ? model.categories[group.uuid] = data
                  : model.categories.remove(group.uuid),
      initialValue: group.allCategories.singleWhereOrNull(
        (final c) => c.uuid == model.categories[group.uuid]?.uuid,
      ),
      itemToDisplayString: (final item) => item.displayName,
      searchItems: group.allCategories,
      validator:
          (final value) =>
              group.isRequired && validate
                  ? nonNullValidator(context, value)
                  : null,
    );
  }
}

bool balanceTxSplits(
  final Transaction transaction, [
  final TransactionSplit? editingSplit,
]) {
  int amount = transaction.amount;
  final int splitAmount = transaction.splits.fold(
    0,
    (final amnt, final split) => amnt + split.amount,
  );
  if (splitAmount == amount) return false;

  final sign = amount < 0 ? -1 : 1;
  int splitTotal = 0;

  if (editingSplit != null && transaction.splits.contains(editingSplit)) {
    if (editingSplit.amount.abs() >= amount.abs()) {
      for (final split in transaction.splits) {
        split.amount = 0;
      }
      editingSplit.amount = amount;
      return true;
    }
    editingSplit.amount = sign * editingSplit.amount.abs();
    splitTotal = editingSplit.amount.abs();
  }

  amount = amount.abs();
  final List<TransactionSplit> splitList = List.from(transaction.splits)
    ..remove(editingSplit);
  for (final split in splitList) {
    if (split == transaction.splits.last) {
      split.amount = sign * (amount - splitTotal);
      break;
    }
    if (amount - splitTotal >= split.amount.abs()) {
      split.amount = sign * split.amount.abs();
      splitTotal += split.amount.abs();
    } else if (amount - splitTotal > 0) {
      split.amount = sign * (amount - splitTotal);
      splitTotal = amount;
    } else {
      split.amount = 0;
    }
  }

  return true;
}
