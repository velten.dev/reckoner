import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../database/reckoner_db.dart';
import '../../../model/account.dart';
import '../../../model/transaction.dart';
import '../../../service/sync_service_manager.dart';
import 'transaction_edit_fields.dart';

enum TransactionType { debit, credit, transfer }

class TransactionAmountNotifier with ChangeNotifier {
  void notify() => notifyListeners();
}

class TransactionAccount {
  final Account? account;
  final String? merchantName;

  TransactionAccount({this.account, this.merchantName});

  void save(final Transaction tx) {
    if (account != null) tx.account = account;
    if (merchantName != null) tx.merchantName = merchantName!;
  }

  String get displayName => account?.displayName ?? merchantName ?? '';
}

class TransactionEditData {
  final TransactionEntity? loadedTxEntity;
  final updateAmounts = TransactionAmountNotifier();
  late Transaction sourceTx;
  late Transaction destTx;

  void Function(VoidCallback fn)? setState;
  late TransactionType _type;

  TransactionEditData({this.loadedTxEntity, this.setState}) {
    _type = TransactionType.debit;
    sourceTx = loadedTxEntity?.primaryTx.copy() ?? Transaction();
    destTx = loadedTxEntity?.transfer?.destination.copy() ?? Transaction();
    if (loadedTxEntity?.isTransfer ?? false) {
      _type = TransactionType.transfer;
    } else if (loadedTxEntity != null) {
      _type =
          loadedTxEntity!.primaryTx.amount < 0
              ? TransactionType.debit
              : TransactionType.credit;
    }
  }

  TransactionType get type => _type;

  set type(final TransactionType value) {
    if (!isNew && _type == TransactionType.transfer) {
      throw Exception('Cannot change transfer once created');
    }
    _type = value;
  }

  bool get isTransfer => _type == TransactionType.transfer;

  bool get differentCurrencies {
    if (!isTransfer) return false;
    final src = sourceTx.account;
    final dest = destTx.account;
    return src.isMultiCurrency ||
        dest.isMultiCurrency ||
        src.defaultCurrencyCode != dest.defaultCurrencyCode;
  }

  bool get isNew => loadedTxEntity == null;

  Iterable<Widget> accountWidgets(
    final void Function(VoidCallback fn)? setState,
  ) => [
    if (type == TransactionType.transfer)
      TxAccountEdit(
        transaction: sourceTx,
        filterAccount: destTx.account,
        label: 'Source Account',
        setState: setState,
      ),
    if (type == TransactionType.transfer)
      TxAccountEdit(
        transaction: destTx,
        filterAccount: sourceTx.account,
        label: 'Destination Account',
        setState: setState,
      ),
    if (type != TransactionType.transfer)
      TxAccountEdit(transaction: sourceTx, setState: setState),
    if (type != TransactionType.transfer)
      TxMerchantEdit(sourceTx, setState: setState),
  ];

  List<Widget> amountEditWidgets([final bool showLabel = false]) => [
    _sourceTxAmountEdit(showLabel),
    if (isTransfer && differentCurrencies) _destTxAmountEdit(showLabel),
  ];

  Widget _sourceTxAmountEdit(final bool showLabel) {
    if (sourceTx.account.isMultiCurrency) {
      return TxAmountAndCurrencyEdit(
        model: sourceTx,
        updateAmounts: updateAmounts,
        decoration:
            showLabel
                ? InputDecoration(
                  label: Text(isTransfer ? 'Source Amount' : 'Amount'),
                )
                : null,
      );
    }
    return TxAmountEdit(
      transaction: sourceTx,
      updateAmounts: updateAmounts,
      showLabel: showLabel,
    );
  }

  Widget _destTxAmountEdit(final bool showLabel) {
    if (destTx.account.isMultiCurrency) {
      return TxAmountAndCurrencyEdit(
        model: destTx,
        updateAmounts: updateAmounts,
        decoration:
            showLabel
                ? InputDecoration(
                  label: Text(isTransfer ? 'Destination Amount' : 'Amount'),
                )
                : null,
      );
    }
    return TxAmountEdit(
      transaction: destTx,
      updateAmounts: updateAmounts,
      showLabel: showLabel,
    );
  }

  TransactionEntity? get entity {
    if (isTransfer) {
      destTx.date = sourceTx.date;
      if (!differentCurrencies) {
        destTx
          ..amount = sourceTx.amount
          ..currency = sourceTx.currency;
      }
      sourceTx.amount = sourceTx.amount.abs() * -1;
      destTx.amount = destTx.amount.abs();
      return TransactionEntity(
        transfer:
            TransactionTransfer()
              ..source = sourceTx
              ..destination = destTx,
      );
    } else {
      final sign = type == TransactionType.debit ? -1 : 1;
      sourceTx.amount = sourceTx.amount.abs() * sign;
      for (final split in sourceTx.splits) {
        split.amount = split.amount.abs() * sign;
      }
      return TransactionEntity(transaction: sourceTx);
    }
  }

  Future<TransactionEntity?> save() async {
    final saveEntity = entity;
    if (saveEntity == null) return null;
    if (saveEntity == loadedTxEntity) return saveEntity;
    final newTx = await ReckonerDb.I.transactionDao.saveTransactionEntity(
      saveEntity,
    );
    di<SyncServiceManager>().sendUpdates();
    return newTx;
  }
}
