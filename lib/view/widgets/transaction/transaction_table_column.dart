import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:material_table_view/material_table_view.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/user_pref_const.dart';
import '../../../service/data_cache.dart';

class TransactionTableColumn extends TableColumn {
  TransactionTableColumn({
    required this.index,
    required super.width,
    super.freezePriority = 0,
    super.sticky = false,
    super.flex = 0,
    super.translation = 0,
    super.minResizeWidth,
    super.maxResizeWidth,
  }) : key = ValueKey<int>(index);

  final int index;

  @override
  final ValueKey<int> key;

  @override
  TransactionTableColumn copyWith({
    final double? width,
    final int? freezePriority,
    final bool? sticky,
    final int? flex,
    final double? translation,
    final double? minResizeWidth,
    final double? maxResizeWidth,
  }) => TransactionTableColumn(
    index: index,
    width: width ?? this.width,
    freezePriority: freezePriority ?? this.freezePriority,
    sticky: sticky ?? this.sticky,
    flex: flex ?? this.flex,
    translation: translation ?? this.translation,
    minResizeWidth: minResizeWidth ?? this.minResizeWidth,
    maxResizeWidth: maxResizeWidth ?? this.maxResizeWidth,
  );

  factory TransactionTableColumn.fromJson(final Map<String, dynamic> json) {
    return TransactionTableColumn(
      index: int.tryParse(json['index'] as String? ?? '') ?? 0,
      width: double.tryParse(json['width'] as String? ?? '') ?? 100,
      freezePriority:
          int.tryParse(json['freezePriority'] as String? ?? '') ?? 0,
      sticky: bool.tryParse(json['sticky'] as String? ?? '') ?? false,
      flex: int.tryParse(json['flex'] as String? ?? '') ?? 0,
      translation: double.tryParse(json['translation'] as String? ?? '') ?? 0,
      minResizeWidth: double.tryParse(json['minResizeWidth'] as String? ?? ''),
      maxResizeWidth: double.tryParse(json['maxResizeWidth'] as String? ?? ''),
    );
  }

  Map<String, String> toJson() {
    return {
      'index': index.toString(),
      'width': width.toString(),
      'freezePriority': freezePriority.toString(),
      'sticky': sticky.toString(),
      'flex': flex.toString(),
      if (minResizeWidth != null) 'minResizeWidth': minResizeWidth.toString(),
      if (maxResizeWidth != null) 'maxResizeWidth': maxResizeWidth.toString(),
    };
  }
}

List<TransactionTableColumn> getColumns() {
  final groups = di<DataCache>().categoryGroups.value;
  final columns =
      getPreferences()
          .getStringList(tableColumnOrder)
          ?.map(
            (final j) => TransactionTableColumn.fromJson(
              jsonDecode(j) as Map<String, dynamic>,
            ),
          )
          .toList();
  if (columns != null && columns.length == 7 + groups.length) {
    bool good = true;
    for (int i = 0; i < columns.length; i++) {
      if (columns.firstWhereOrNull((final c) => c.index == i) == null) {
        good = false;
      }
    }
    if (good) return columns;
  }
  return [
    TransactionTableColumn(index: 0, width: 56.0, freezePriority: 100),
    TransactionTableColumn(
      index: 1,
      width: 200,
      minResizeWidth: 100,
      freezePriority: 100,
      sticky: true,
    ), // Name
    TransactionTableColumn(index: 2, width: 125, minResizeWidth: 75), //Amount
    TransactionTableColumn(index: 3, width: 150, minResizeWidth: 75), //Date
    TransactionTableColumn(index: 4, width: 200, minResizeWidth: 100), //Source
    TransactionTableColumn(
      index: 5,
      width: 200,
      minResizeWidth: 100,
    ), //Destination
    ...groups.mapIndexed(
      (final i, final g) =>
          TransactionTableColumn(index: 6 + i, width: 200, minResizeWidth: 100),
    ),
    TransactionTableColumn(
      index: 6 + groups.length,
      width: 200,
      minResizeWidth: 100,
      flex: 1,
    ),
  ];
}

void saveColumns(final List<TransactionTableColumn> columns) {
  getPreferences().setStringList(
    tableColumnOrder,
    columns.map((final c) => jsonEncode(c.toJson())).toList(),
  );
}
