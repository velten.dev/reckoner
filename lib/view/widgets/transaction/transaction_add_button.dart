import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../config/routes_const.dart';
import '../default_icon_button.dart';

class AddTransactionButton extends StatelessWidget {
  const AddTransactionButton({super.key});

  @override
  Widget build(final BuildContext context) {
    return DefaultIconButton(
      icon: const Icon(Icons.add_circle_outline),
      onPressed: () => context.push(transactionEditPath(newId)),
      tooltip: 'Add Transaction',
    );
  }
}
