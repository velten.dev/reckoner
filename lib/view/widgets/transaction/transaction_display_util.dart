import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../model/transaction.dart';
import '../../theme.dart';
import '../amount_display.dart';

class TransactionText extends StatelessWidget {
  final String text;

  const TransactionText(this.text, {super.key});

  @override
  Widget build(final BuildContext context) =>
      Text(text, overflow: TextOverflow.ellipsis);
}

class TransactionAmountDisplay extends StatelessWidget {
  final TransactionEntity entity;
  final TransactionSplit? split;
  final TextStyle? style;
  const TransactionAmountDisplay(
    this.entity, {
    this.split,
    this.style,
    super.key,
  });

  @override
  Widget build(final BuildContext context) {
    final ThemeHandler themeHandler = di();

    if (entity.isTransfer) {
      final tr = entity.transfer!;
      final text =
          tr.source.currencyCode == tr.destination.currencyCode
              ? tr.source.currency.formatAmount(tr.source.amount.abs())
              : '${tr.source.currency.formatAmount(tr.source.amount)} → ${tr.destination.currency.formatAmount(tr.destination.amount)}';
      final color =
          themeHandler.isDark ? Colors.blueAccent[100] : Colors.blue[900];
      final TextStyle displayStyle =
          style?.copyWith(color: color, fontWeight: FontWeight.normal) ??
          TextStyle(color: color);

      return Text(
        text,
        overflow: TextOverflow.ellipsis,
        style: displayStyle,
        textAlign: TextAlign.right,
      );
    }

    return AmountDisplay(
      amount: split?.amount ?? entity.primaryTx.amount,
      currency: entity.primaryTx.currency,
      style: style,
    );
  }
}
