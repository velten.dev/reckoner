import 'package:flutter/material.dart';

class DashedLine extends StatelessWidget {
  const DashedLine({super.key, this.height = 1, this.width = 10, this.color});
  final double height;
  final double width;
  final Color? color;

  @override
  Widget build(final BuildContext context) {
    return LayoutBuilder(
      builder: (final BuildContext context, final BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * width)).floor();
        return Flex(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
          children: List.generate(dashCount, (final _) {
            return SizedBox(
              width: width,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color:
                      color ??
                      Theme.of(
                        context,
                      ).colorScheme.onPrimaryContainer.withAlpha(64),
                ),
              ),
            );
          }),
        );
      },
    );
  }
}
