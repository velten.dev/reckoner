import 'package:flutter/material.dart';

import '../../config/platform.dart';

class PasskeyInputField extends StatelessWidget {
  final FocusNode? focusNode;
  final ValueChanged<String>? onSubmit;
  final ValueChanged<String>? onChanged;
  final InputDecoration? decoration;
  final FormFieldValidator<String>? validator;
  final TextStyle? style;
  final TextEditingController? controller;
  final TextInputAction? textInputAction;
  final bool autofocus;

  const PasskeyInputField({
    this.controller,
    this.decoration,
    this.focusNode,
    super.key,
    this.onChanged,
    this.onSubmit,
    this.style,
    this.textInputAction,
    this.validator,
    this.autofocus = false,
  });

  @override
  Widget build(final BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: isMobile ? TextInputType.number : null,
      style: style,
      focusNode: focusNode,
      autofocus: autofocus,
      obscureText: true,
      decoration: decoration,
      textAlign: TextAlign.center,
      initialValue: controller == null ? '' : null,
      validator: validator,
      onChanged: onChanged,
      onFieldSubmitted: onSubmit,
      textInputAction: textInputAction,
    );
  }
}
