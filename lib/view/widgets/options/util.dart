import 'package:flutter/material.dart';

import '../../../config/user_pref_const.dart';

Future<void> setIndex(final BuildContext context) async {
  try {
    final index = DefaultTabController.of(context).index;
    await getPreferences().setInt(optionsTab, index);
  } catch (_) {}
}
