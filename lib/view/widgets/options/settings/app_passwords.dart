import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../config/environment_const.dart';
import '../../../../config/platform.dart';
import '../../../../database/reckoner_db.dart';
import '../../../../model/secure_storage.dart';
import '../../../../service/app_lock.dart';
import '../../../../service/sync_service_manager.dart';
import '../../../util/popup_dialogs.dart';
import '../../../util/reset_reckoner.dart';
import '../../form/edit_fields.dart';
import '../../form/input_form.dart';
import '../../form/password_input.dart';
import '../../form/validators.dart';
import '../../passkey_input_field.dart';
import '../../toast_message.dart';

class AppPasswords extends WatchingWidget {
  const AppPasswords({super.key});

  @override
  Widget build(final BuildContext context) {
    final hasPassword = watchPropertyValue(
      (final AppLock a) => AppLock.hasPassword,
    );
    final AppLock appLock = di();

    return FutureBuilder(
      future: Future(() async => (await SecureStorage.dbPassword).isNotEmpty),
      builder: (final context, final snapshot) {
        final hasDbEncryption = snapshot.data ?? false;
        return Wrap(
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            TextButton(
              onPressed: () async {
                if (await yesNoDialog(
                  context,
                  'This action will delete all of your data. This includes the database and any passwords stored. This action will not delete backups. Do you want to continue',
                  continueText: 'Delete Everything',
                )) {
                  if (context.mounted) await resetReckoner(context);
                }
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.warning,
                    color: Theme.of(context).colorScheme.error,
                  ),
                  Text(
                    'Reset Reckoner',
                    style: TextStyle(
                      inherit: true,
                      color: Theme.of(context).colorScheme.error,
                    ),
                  ),
                ],
              ),
            ),
            if (hasDbEncryption && !isWeb)
              TextButton(
                onPressed: () async {
                  if (await appLock.prompt(context) ?? false) {
                    await Clipboard.setData(
                      ClipboardData(text: await SecureStorage.dbPassword),
                    );
                    showAlert('Copied to clipboard');
                  }
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [Icon(Icons.copy), Text('Copy Database Password')],
                ),
              ),
            if (!hasDbEncryption && !isWeb)
              const Center(child: Text('No ReckonerDb password')),
            if (!isWeb && !isDemo)
              TextButton(
                onPressed: () async {
                  if (await appLock.prompt(context) ?? false) {
                    final syncManagerSettings =
                        await SecureStorage.syncManagerSettings;
                    final backupSettings = await SecureStorage.backupSettings;
                    bool approved = true;
                    if (!context.mounted) return;
                    final syncUsesEncryptionKey =
                        (syncManagerSettings.encryptionSettings.type ==
                            EncryptionType.db);
                    if (syncUsesEncryptionKey) {
                      approved = await yesNoDialog(
                        context,
                        'The synchronization service uses the database encryption key. Changing the database encryption key with reset the synchronization service.',
                      );
                    }
                    if (!context.mounted) return;
                    if (backupSettings.encryptionSettings.type ==
                        EncryptionType.db) {
                      approved = await yesNoDialog(
                        context,
                        'The backup service uses the database encryption key. Changing the database encryption key will cause new backups to use this key. Old backups will not be modified.',
                      );
                    }
                    if (approved && context.mounted) {
                      await showDialog<void>(
                        context: context,
                        builder:
                            (final context) => InputForm(
                              title: 'ReckonerDb Password',
                              inputs: [
                                PasswordFormField(
                                  onSaved: (final newValue) async {
                                    if (syncUsesEncryptionKey) {
                                      await di
                                          .get<SyncServiceManager>()
                                          .setSyncData(SyncManagerSettings());
                                    }
                                    await ReckonerDb.changeEncryption(
                                      newValue ?? '',
                                    );
                                  },
                                ),
                              ],
                            ),
                      );
                    }
                  }
                },
                child: const Text('Set Database Password'),
              ),
            if (!hasPassword)
              TextButton(
                onPressed: () async {
                  await showDialog<void>(
                    context: context,
                    builder:
                        (final context) =>
                            const AlertDialog(content: _PassCodeEdit()),
                  );
                },
                child: const Text('Set App Passcode'),
              ),
            if (hasPassword)
              TextButton(
                onPressed: () async {
                  await appLock.clearPassword(context);
                },
                child: const Text('Disable App Passcode'),
              ),
            if (AppLock.canUseBiometrics & !hasPassword)
              TextButton(
                onPressed: () async {
                  await appLock.enableBiometrics(context);
                },
                child: const Text('Use device biometrics'),
              ),
          ],
        );
      },
    );
  }
}

class _PassCodeEdit extends StatefulWidget {
  const _PassCodeEdit();
  @override
  State<StatefulWidget> createState() => _PassCodeEditState();
}

class _PassCodeEditState extends State<_PassCodeEdit> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String oldPasscode = '';
  String newPasscode = '';
  List<TextEditingController> controllers = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  @override
  void dispose() {
    for (final cont in controllers) {
      cont.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return FutureBuilder(
      future: Future(
        () async => (await SecureStorage.appLockSettings).hasPassword,
      ),
      builder: (final context, final snapshot) {
        final hasPassword = snapshot.data ?? false;
        return InputFieldGroup(
          label: 'App Lock Passcode',
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if (hasPassword)
                  PasskeyInputField(
                    controller: controllers[0],
                    decoration: const InputDecoration(
                      label: Text('Current Passkey'),
                    ),
                    validator:
                        (final curPasscode) =>
                            nonNullValidator(context, curPasscode),
                    onChanged: (final value) => oldPasscode = value,
                    onSubmit: (final curPasscode) async {
                      if (!await di<AppLock>().setPassword(
                        curPasscode,
                        newPasscode,
                      )) {
                        showError('Failed to set new passcode');
                      }
                    },
                  ),
                PasskeyInputField(
                  controller: controllers[1],
                  decoration: const InputDecoration(label: Text('New Passkey')),
                  validator:
                      (final newPasskey) =>
                          nonNullValidator(context, newPasskey),
                  onChanged: (final value) => newPasscode = value,
                ),
                PasskeyInputField(
                  controller: controllers[2],
                  decoration: const InputDecoration(
                    label: Text('Reenter Passkey'),
                  ),
                  validator: (final passcode) {
                    var val = nonNullValidator(context, passcode);
                    if (val == null && newPasscode != passcode) {
                      val = 'Passkeys do not match';
                    }
                    return val;
                  },
                ),
                TextButton(
                  onPressed: () async {
                    if (_formKey.currentState?.validate() ?? false) {
                      if (!await di<AppLock>().setPassword(
                        oldPasscode,
                        newPasscode,
                      )) {
                        showError('Failed to set new passcode');
                      } else {
                        showAlert('Changed passcode');
                        if (context.mounted) Navigator.pop(context);
                      }
                      for (final controller in controllers) {
                        controller.clear();
                      }
                      setState(() {
                        newPasscode = '';
                        oldPasscode = '';
                      });
                    }
                  },
                  child: const Text('Change'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
