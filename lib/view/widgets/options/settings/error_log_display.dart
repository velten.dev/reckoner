import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../service/logger.dart';
import '../../../util/popup_dialogs.dart';
import '../../default_icon_button.dart';

class ErrorLogDisplay extends StatefulWidget {
  const ErrorLogDisplay({super.key});

  @override
  State<StatefulWidget> createState() => _ErrorLogDisplayState();
}

class _ErrorLogDisplayState extends State<ErrorLogDisplay> {
  @override
  Widget build(final BuildContext context) {
    return FutureBuilder(
      future: getErrorLogs(),
      builder: (final _, final snapshot) {
        final errorFiles =
            (snapshot.data ?? [])
              ..sort((final a, final b) => a.name.compareTo(b.name));
        return ListView(
          shrinkWrap: true,
          children:
              errorFiles
                  .map(
                    (final error) => ListTile(
                      title: Text(error.name),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          DefaultIconButton(
                            icon: const Icon(Icons.copy),
                            onPressed:
                                () => Clipboard.setData(
                                  ClipboardData(text: error.message),
                                ),
                          ),
                          DefaultIconButton(
                            icon: const Icon(Icons.delete),
                            onPressed:
                                () => yesNoDialog(
                                  context,
                                  'Delete Error log ${error.name}',
                                ).then((final doDelete) {
                                  if (doDelete) {
                                    error.delete().ignore();
                                    setState(() {});
                                  }
                                }),
                          ),
                        ],
                      ),
                    ),
                  )
                  .toList(),
        );
      },
    );
  }
}
