import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:nanoid2/nanoid2.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../../config/platform.dart';
import '../../../../database/reckoner_db.dart';
import '../../../../model/secure_storage.dart';
import '../../../reckoner.dart';
import '../../../util/popup_dialogs.dart';
import '../../encryption_settings_edit.dart';
import '../../form/edit_fields.dart';
import '../../form/input_form.dart';
import '../../form/validators.dart';
import '../../toast_message.dart';

String _cadenceToString(final BackupCadence cadence) => switch (cadence) {
  BackupCadence.daily => 'daily',
  BackupCadence.weekly => 'weekly',
  BackupCadence.monthly => 'monthly',
};

Future<bool> _testFileAccess(final String path) async {
  if (isMobile) {
    try {
      final testFile = File(path);
      final sink = testFile.openWrite()..write('test');
      await sink.close();
      await testFile.delete();
      return true;
    } on PathAccessException {
      if (Platform.isAndroid) {
        var permissionStatus = await Permission.manageExternalStorage.status;
        if (permissionStatus.isDenied) {
          permissionStatus = await Permission.manageExternalStorage.request();
          if (permissionStatus.isDenied) {
            await openAppSettings();
            return false;
          } else {
            return true;
          }
        } else if (permissionStatus.isPermanentlyDenied) {
          await openAppSettings();
          return false;
        }
      } else {
        showError('Cannot access directory');
      }
      return false;
    }
  }
  return true;
}

class BackupSettingsDisplay extends StatefulWidget {
  const BackupSettingsDisplay({super.key});

  @override
  State<StatefulWidget> createState() => _BackupSettingsDisplayState();
}

class _BackupSettingsDisplayState extends State<BackupSettingsDisplay> {
  var backupSettings = BackupSettings();

  @override
  void initState() {
    super.initState();
    SecureStorage.backupSettings.then((final value) {
      backupSettings = value;
      if (mounted) setState(() {});
    });
  }

  Future<String?> _getEncryptionKey(final BuildContext context) async {
    if (!context.mounted) return null;
    String? key;
    await showDialog<String>(
      context: context,
      builder: (final ctx) {
        var settings = EncryptionSettings();

        return InputForm(
          alwaysSave: true,
          title: 'Encryption Key',
          inputs: [
            EncryptionSettingsEdit(
              hideGenerator: true,
              onChanged: (final value) => settings = value,
            ),
          ],
          onSave: () async => key = await settings.encryptionKey,
        );
      },
    );
    return key;
  }

  @override
  Widget build(final BuildContext context) {
    final String lastBackupStr =
        backupSettings.lastBackupDate.year == 0
            ? 'Never'
            : DateFormat().format(backupSettings.lastBackupDate);
    final String backupCadenceStr =
        backupSettings.cronJob
            ? 'Backing up ${_cadenceToString(backupSettings.cadence)}'
            : 'Not automatically backing up';

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: TextButton(
            onPressed:
                () => showDialog<bool>(
                  context: context,
                  builder:
                      (final _) => _BackupSettingsEdit(backupSettings, (
                        final settings,
                      ) {
                        SecureStorage.backupSettings = Future.value(settings);
                        setState(() => backupSettings = settings);
                      }),
                ),
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Icon(Icons.edit), Text('Edit Automatic Backup')],
            ),
          ),
        ),
        Text(backupSettings.filePath),
        Text('Last Backup $lastBackupStr'),
        Text(backupCadenceStr),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
              onPressed: () async {
                if (!await yesNoDialog(
                  context,
                  'Importing deletes the current database and uses the imported data only. Any data not backed up will be lost',
                )) {
                  return;
                }

                if (!context.mounted) return;
                final key = await _getEncryptionKey(context);
                if (key == null) return;
                final success = await ReckonerDb.restore(key);

                if (context.mounted) {
                  Reckoner.restartApp(context);
                }

                success
                    ? showAlert('Database imported')
                    : showError('Failed to import database');
              },
              child: const Text('Import'),
            ),
            TextButton(
              onPressed: () async {
                final key = await _getEncryptionKey(context);
                if (key == null) return;
                final tempDir = await getTemporaryDirectory();
                final tempFileName = join(tempDir.path, '${nanoid()}.db');
                if (!await ReckonerDb.backup(tempFileName, key)) {
                  showError('Database not exported');
                  return;
                }

                final filepath = await FilePicker.platform.saveFile(
                  initialDirectory: backupSettings.path,
                  fileName: backupSettings.fullFileName,
                  allowedExtensions: ['db', 'db3', 'sqlite', 'sqlite3'],
                  type: FileType.custom,
                  bytes:
                      isMobile ? await File(tempFileName).readAsBytes() : null,
                );
                if (filepath == null) {
                  showError('Database not exported');
                  return;
                }
                final file = File(tempFileName);
                if (!isMobile) await File(tempFileName).copy(filepath);
                file.delete().ignore();
                showAlert('Database exported');
                final newSettings = await SecureStorage.backupSettings;
                setState(() => backupSettings = newSettings);
              },
              child: const Text('Export'),
            ),
          ],
        ),
      ],
    );
  }
}

class _BackupSettingsEdit extends StatefulWidget {
  final BackupSettings settings;
  final void Function(BackupSettings) save;

  const _BackupSettingsEdit(this.settings, this.save);

  @override
  State<StatefulWidget> createState() => _BackupSettingsEditState();
}

class _BackupSettingsEditState extends State<_BackupSettingsEdit> {
  late String path, filename;
  late bool appendDate, cronJob;
  late int backupCount;
  late BackupCadence cadence;
  late EncryptionSettings encryptionSettings;

  @override
  void initState() {
    super.initState();
    path = widget.settings.path;
    filename = widget.settings.filename;
    appendDate = widget.settings.appendDate;
    cronJob = widget.settings.cronJob;
    cadence = widget.settings.cadence;
    backupCount = widget.settings.backupCount;
    encryptionSettings = widget.settings.encryptionSettings;
  }

  @override
  Widget build(final BuildContext context) {
    return InputForm(
      alwaysSave: true,
      title: 'Backup Settings',
      child: Column(
        children: [
          InputDecorator(
            decoration: const InputDecoration(
              label: Text('Path'),
              border: InputBorder.none,
            ),
            child: TextButton(
              onPressed: () async {
                final directory = await FilePicker.platform.getDirectoryPath(
                  initialDirectory: path,
                );
                if (directory == null) return;
                if (!await _testFileAccess(join(directory, nanoid()))) return;
                setState(() => path = directory);
              },
              child: Text(path),
            ),
          ),
          TextFormField(
            initialValue: filename,
            decoration: const InputDecoration(label: Text('Filename')),
            onChanged: (final value) => filename = value,
            validator: (final value) {
              var validator = nonNullValidator(context, value);
              if (validator == null && value?.toLowerCase() == 'reckoner') {
                validator = 'Use a different name';
              }
              return validator;
            },
          ),
          FormBuilderCheckbox(
            name: 'appendDate',
            title: const Text('Append Date to Filename'),
            initialValue: appendDate,
            onChanged:
                (final value) =>
                    setState(() => appendDate = value ?? appendDate),
          ),
          if (appendDate)
            FormBuilderSlider(
              name: 'backupsToKeep',
              decoration: const InputDecoration(
                label: Text('Number of Backups to Keep'),
              ),
              initialValue: backupCount.toDouble(),
              min: 1,
              max: 20,
              divisions: 19,
              onChanged:
                  (final value) => backupCount = value?.toInt() ?? backupCount,
            ),
          EncryptionSettingsEdit(
            initialValue: encryptionSettings,
            onChanged: (final value) => encryptionSettings = value,
          ),
          FormBuilderCheckbox(
            name: 'cron',
            title: const Text('Preform Regular Backups'),
            initialValue: cronJob,
            onChanged:
                (final value) => setState(() => cronJob = value ?? cronJob),
          ),
          if (cronJob)
            ChipEdit(
              label: 'Backup Schedule',
              initialValue: cadence,
              options:
                  BackupCadence.values
                      .map(
                        (final e) => FormBuilderChipOption(
                          value: e,
                          child: Text(_cadenceToString(e)),
                        ),
                      )
                      .toList(),
              onChanged: (final value) => cadence = value ?? cadence,
            ),
        ],
      ),
      onSave: () async {
        widget.save(
          BackupSettings(
            appendDate: appendDate,
            backupCount: backupCount,
            cadence: cadence,
            cronJob: cronJob,
            filename: filename,
            path: path,
            lastBackupDate: widget.settings.lastBackupDate,
            encryptionSettings: encryptionSettings,
          ),
        );
      },
    );
  }
}
