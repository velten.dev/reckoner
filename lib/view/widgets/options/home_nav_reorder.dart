import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../service/data_cache.dart';
import '../../util/save_item.dart';
import '../fixed_width_container.dart';
import '../layout_widgets.dart';
import '../reoderable_list.dart';
import 'nav_edit/edit_category_group.dart';
import 'nav_edit/edit_report_group.dart';
import 'nav_edit/home_nav_tile.dart';
import 'util.dart';

enum _EditChoices { category, report }

class HomeNavReorder extends WatchingWidget {
  const HomeNavReorder({super.key});

  @override
  Widget build(final BuildContext context) {
    setIndex(context);
    final setting = watchValue((final DataCache c) => c.setting).copy();
    return FloatingButtonOverlay(
      FixedWidthContainer(
        child: ReckonerReoderableListView(
          children: setting.navList.map((final e) => HomeNavTile(e)).toList(),
          onReorder: (final oldIndex, newIndex) async {
            if (oldIndex < newIndex) newIndex -= 1;
            setting.moveEntry(oldIndex, newIndex);
            await saveItem(setting);
          },
        ),
      ),
      floatingButton: PopupMenuButton<_EditChoices>(
        elevation: 6,
        splashRadius: 1,
        enableFeedback: false,
        color: Theme.of(context).colorScheme.secondaryFixed,
        onSelected:
            (final value) =>
                value == _EditChoices.category
                    ? editCategoryGroup(context, null)
                    : editReportGroup(context, null),
        itemBuilder:
            (final context) => [
              const PopupMenuItem(
                value: _EditChoices.category,
                child: Text('Category'),
              ),
              const PopupMenuItem(
                value: _EditChoices.report,
                child: Text('Report'),
              ),
            ],
        child: const ReckonerActionButton(child: Icon(Icons.playlist_add)),
      ),
    );
  }
}
