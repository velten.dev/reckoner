import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../../../model/category.dart';
import '../../../l10n/localizations.dart';
import '../../../util/popup_dialogs.dart';
import '../../../util/save_item.dart';
import '../../form/color_input.dart';
import '../../form/edit_fields.dart';
import '../../form/input_form.dart';
import '../../form/validators.dart';
import '../../layout_widgets.dart';

void editCategoryGroup(final BuildContext context, final CategoryGroup? group) {
  final CategoryGroup model = group ?? CategoryGroup();
  final localization = ReckonerLocalizations.of(context);
  String title = localization.addItem('Category');
  if (group != null) title = localization.editItem('Category');

  showDialog<void>(
    context: context,
    builder:
        (final _) => InputForm(
          title: title,
          onSave: () => saveItem<CategoryGroup>(model),
          onDelete:
              group == null
                  ? null
                  : () => deletePopup(context, model, 'Category Group'),
          child: _CategoryGroupEdit(model),
        ),
  );
}

class _CategoryGroupEdit extends StatefulWidget {
  final CategoryGroup? group;

  const _CategoryGroupEdit(this.group);

  @override
  State<StatefulWidget> createState() => _CategoryGroupEditState();
}

class _CategoryGroupEditState extends State<_CategoryGroupEdit> {
  late CategoryGroup _model;

  @override
  void initState() {
    super.initState();
    _model = widget.group ?? CategoryGroup();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        NameEdit(model: _model),
        FormBuilderSwitch(
          name: 'required',
          title: const Text('Is required'),
          initialValue: _model.isRequired,
          onChanged:
              (final value) => _model.isRequired = value ?? _model.isRequired,
        ),
        FormBuilderSwitch(
          name: 'limits',
          title: const Text('Use Budgets/Limits'),
          initialValue: _model.requireLimits,
          onChanged:
              (final value) =>
                  value == null
                      ? null
                      : setState(() => _model.requireLimits = value),
        ),
        if (_model.requireLimits && kDebugMode)
          FormBuilderSwitch(
            name: 'autobalance',
            title: const Text('Enable Limit AutoBalance'),
            initialValue: _model.enableAutoBalance,
            onChanged:
                (final value) =>
                    value == null ? null : _model.enableAutoBalance = value,
          ),
        if (_model.requireLimits)
          ColorInputForm(
            textLabel: 'Default Limit Color',
            initialValue: Color(_model.defaultColor),
            onChanged:
                (final color) =>
                    // ignore: deprecated_member_use
                    color == null ? null : _model.defaultColor = color.value,
            showQuickColors: true,
          ),
        if (_model.requireLimits)
          ColorInputForm(
            textLabel: 'Limit Exceeded Color',
            initialValue: Color(_model.overColor),
            onChanged:
                (final color) =>
                    // ignore: deprecated_member_use
                    color == null ? null : _model.overColor = color.value,
            showQuickColors: true,
          ),
        if (_model.requireLimits) _CategoryGroupBudgetEdit(_model),
      ],
    );
  }
}

class _CategoryGroupBudgetEdit extends StatefulWidget {
  final CategoryGroup model;
  const _CategoryGroupBudgetEdit(this.model);
  @override
  State<StatefulWidget> createState() => _CategoryGroupBudgetEditState();
}

class _CategoryGroupBudgetEditState extends State<_CategoryGroupBudgetEdit> {
  late CategoryGroup model;

  @override
  void initState() {
    super.initState();
    model = widget.model;
    model.datePeriod ??= CategoryLimitPeriod.monthly;
    model.datePeriodInterval ??= 1;
  }

  @override
  Widget build(final BuildContext context) {
    final int sliderMax = switch (model.datePeriod!) {
      CategoryLimitPeriod.daily => 30,
      CategoryLimitPeriod.weekly => 26,
      CategoryLimitPeriod.monthly => 18,
      CategoryLimitPeriod.yearly => 5,
    };

    return Row(
      children: [
        Expanded(
          flex: 2,
          child: ChipEdit<CategoryLimitPeriod>(
            alignment: WrapAlignment.spaceEvenly,
            label: 'Limit Generation Period',
            options: [
              for (final e in CategoryLimitPeriod.values)
                FormBuilderChipOption(
                  value: e,
                  child: Text(EnumToString.convertToString(e)),
                ),
            ],
            initialValue: model.datePeriod,
            validator: (final value) => nonNullValidator(context, value),
            onChanged:
                (final value) => setState(() => model.datePeriod = value),
          ),
        ),
        fixedWidthSpacer(),
        Expanded(
          flex: 1,
          child: FormBuilderSlider(
            name: 'periodPerLimit',
            decoration: const InputDecoration(labelText: 'Periods per Budget'),
            displayValues: DisplayValues.current,
            min: 1,
            max: sliderMax.toDouble(),
            divisions: sliderMax - 1,
            initialValue: model.datePeriodInterval!.toDouble(),
            onChangeEnd:
                (final value) =>
                    setState(() => model.datePeriodInterval = value.round()),
          ),
        ),
      ],
    );
  }
}
