import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../../../model/report.dart';
import '../../../util/popup_dialogs.dart';
import '../../../util/save_item.dart';
import '../../form/edit_fields.dart';
import '../../form/form_title.dart';
import '../../form/input_form.dart';

enum _CellSizes { small, medium, large }

int _enumToSize(final _CellSizes size) => switch (size) {
  _CellSizes.small => 200,
  _CellSizes.medium => 400,
  _CellSizes.large => 600,
};

_CellSizes _sizeToEnum(final int size) {
  if (size >= 600) {
    return _CellSizes.large;
  } else if (size >= 400) {
    return _CellSizes.medium;
  }
  return _CellSizes.small;
}

void editReportGroup(final BuildContext context, final ReportGroup? group) {
  final ReportGroup model = group ?? ReportGroup();

  showDialog<void>(
    context: context,
    builder:
        (final _) => InputForm(
          title: titleString(context, 'Report', group),
          onSave: () => saveItem<ReportGroup>(model),
          onDelete:
              group == null
                  ? null
                  : () => deletePopup(context, model, 'Report Group'),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              NameEdit(model: model),
              ChipEdit<_CellSizes>(
                label: 'ChipSize',
                initialValue: _sizeToEnum(model.cellSize),
                options: const [
                  FormBuilderChipOption(
                    value: _CellSizes.small,
                    child: Text('Small'),
                  ),
                  FormBuilderChipOption(
                    value: _CellSizes.medium,
                    child: Text('Medium'),
                  ),
                  FormBuilderChipOption(
                    value: _CellSizes.large,
                    child: Text('Large'),
                  ),
                ],
                onChanged:
                    (final value) =>
                        model.cellSize = _enumToSize(value ?? _CellSizes.small),
              ),
            ],
          ),
        ),
  );
}
