import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/routes_const.dart';
import '../../../model/report.dart';
import '../../../service/data_cache.dart';
import '../../l10n/localizations.dart';
import '../fixed_width_container.dart';
import '../layout_widgets.dart';
import 'util.dart';

class ReportDefinitions extends WatchingWidget {
  const ReportDefinitions({super.key});

  Widget _tile(
    final BuildContext context,
    final String title,
    final List<Report> reports,
  ) => ExpansionTile(
    leading: Icon(reports.first.icon),
    title: Text(title),
    initiallyExpanded: true,
    children:
        reports
            .map(
              (final rpt) => Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(rpt.name, overflow: TextOverflow.ellipsis),
                    SizedBox(width: 5),
                    InkWell(
                      onTap: () => context.push(reportEditPath(rpt.uuid)),
                      child: Icon(Icons.edit, size: 20),
                    ),
                  ],
                ),
              ),
            )
            .toList(),
  );

  @override
  Widget build(final BuildContext context) {
    setIndex(context);
    final reports = watchValue((final DataCache c) => c.reportMap).values;
    final Map<ReportType, List<Report>> reportMap = {};
    for (final report in reports) {
      reportMap.putIfAbsent(report.type, () => []).add(report);
    }

    return FloatingButtonOverlay(
      FixedWidthContainer(
        child: ListView(
          children: [
            if (reportMap.containsKey(ReportType.line))
              _tile(context, 'Line Charts', reportMap[ReportType.line]!),
            if (reportMap.containsKey(ReportType.bar))
              _tile(context, 'Bar Charts', reportMap[ReportType.bar]!),
            if (reportMap.containsKey(ReportType.pie))
              _tile(context, 'Pie Charts', reportMap[ReportType.pie]!),
            if (reportMap.containsKey(ReportType.table))
              _tile(context, 'Table Report', reportMap[ReportType.table]!),
            if (reportMap.containsKey(ReportType.text))
              _tile(context, 'Text Report', reportMap[ReportType.text]!),
          ],
        ),
      ),
      heroTag: 'add',
      onPressed: () => context.push(reportEditPath(newId)),
      tooltip: ReckonerLocalizations.of(context).addItem('Report'),
      icon: Icons.add_chart,
    );
  }
}
