import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../config/environment_const.dart';
import '../../../config/platform.dart';
import '../../../service/logger.dart';
import '../form/edit_fields.dart';
import '../sync/sync_display.dart';
import 'settings/about_view.dart';
import 'settings/app_passwords.dart';
import 'settings/backup_settings.dart';
import 'settings/date_settings.dart';
import 'settings/debug_settings.dart';
import 'settings/error_log_display.dart';
import 'settings/ui_settings.dart';
import 'util.dart';

class UserSettingsEdit extends StatelessWidget {
  final bool fromError;
  const UserSettingsEdit({super.key, this.fromError = false});

  @override
  Widget build(final BuildContext context) {
    setIndex(context);
    return FutureBuilder(
      future: getErrorLogs(),
      builder: (final _, final snapshot) {
        final hasErrorFiles = snapshot.data?.isNotEmpty ?? false;
        return SingleChildScrollView(
          child: Wrap(
            alignment: WrapAlignment.center,
            runAlignment: WrapAlignment.center,
            children: [
              if (kDebugMode && !fromError)
                const _SettingGroup(
                  label: 'Debug Settings',
                  child: DebugSettings(),
                ),
              if (hasErrorFiles)
                const _SettingGroup(
                  label: 'Error Log',
                  child: ErrorLogDisplay(),
                ),
              if (!isDemo && !fromError)
                const _SettingGroup(
                  label: 'Synchronization Settings',
                  child: SyncDisplay(),
                ),
              const _SettingGroup(
                label: 'User Interface Settings',
                child: UISettings(),
              ),
              if (!isWeb)
                const _SettingGroup(
                  label: 'Database Import/Export',
                  child: BackupSettingsDisplay(),
                ),
              const _SettingGroup(
                label: 'Database and Security',
                child: AppPasswords(),
              ),
              if (!fromError)
                const _SettingGroup(
                  label: 'Date Settings',
                  child: DateSettings(),
                ),
              const _SettingGroup(label: 'About', child: AboutView()),
            ],
          ),
        );
      },
    );
  }
}

class _SettingGroup extends StatelessWidget {
  final Widget child;
  final String label;
  const _SettingGroup({required this.label, required this.child});

  @override
  Widget build(final BuildContext context) =>
      SizedBox(width: 700, child: InputFieldGroup(label: label, child: child));
}
