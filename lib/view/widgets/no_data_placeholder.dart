import 'package:flutter/material.dart';

class NoDataPlaceholder extends StatelessWidget {
  final String text;
  const NoDataPlaceholder(this.text, {super.key});
  @override
  Widget build(final BuildContext context) {
    return Center(
      child: Text(
        text,
        style: Theme.of(context).textTheme.titleLarge,
        textAlign: TextAlign.center,
      ),
    );
  }
}
