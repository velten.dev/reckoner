import 'package:flutter/material.dart';

import '../model/layout_config.dart';
import 'app_bar.dart';
import 'default_icon_button.dart';
import 'default_text_button.dart';

typedef SaveFunction<T> = Future<T> Function();
typedef CancelFunction<T> = T Function();
typedef DeleteFunction = Future<bool> Function();
typedef BackFunction = Future<bool> Function();

class InputScreen<T> extends StatelessWidget {
  final String? heroTag;
  final Widget title;
  final SaveFunction<T?> onSave;
  final SaveFunction<T?>? onCopy;
  final DeleteFunction? onDelete;
  final BackFunction? onReturn;
  final Widget body;

  const InputScreen({
    super.key,
    required this.body,
    this.heroTag,
    required this.onSave,
    required this.title,
    this.onCopy,
    this.onDelete,
    this.onReturn,
  });

  Widget _delete(final BuildContext context, final LayoutConfig layoutCfg) {
    final theme = Theme.of(context);

    Future<void> deleteCallback() async {
      final result = await onDelete?.call();
      if (result != null && context.mounted) {
        Navigator.pop(context, result);
      }
    }

    if (layoutCfg.isMobile) {
      return IconButton(
        onPressed: deleteCallback,
        icon: Icon(Icons.delete),
        style: IconButton.styleFrom(backgroundColor: theme.colorScheme.error),
        tooltip: 'Delete',
      );
    }

    return TextButton.icon(
      style: TextButton.styleFrom(
        backgroundColor: theme.colorScheme.error,
        foregroundColor: theme.colorScheme.onError,
      ),
      onPressed: deleteCallback,
      icon: Icon(Icons.delete, color: theme.colorScheme.onPrimaryFixed),
      label: Text(
        'Delete',
        style: theme.textTheme.titleLarge?.copyWith(
          color: theme.colorScheme.onError,
        ),
      ),
    );
  }

  Widget _copy(final BuildContext context, final LayoutConfig layoutCfg) {
    final theme = Theme.of(context);

    Future<void> copyCallback() async {
      final result = await onCopy?.call();
      if (result != null && context.mounted) {
        Navigator.pop(context, result);
      }
    }

    if (layoutCfg.isMobile) {
      return IconButton(
        onPressed: copyCallback,
        icon: Icon(Icons.save_as),
        style: IconButton.styleFrom(
          foregroundColor: theme.colorScheme.onPrimaryFixed,
          backgroundColor: theme.colorScheme.secondaryFixed,
        ),
        tooltip: 'Save As New',
      );
    }

    return TextButton.icon(
      style: TextButton.styleFrom(
        foregroundColor: theme.colorScheme.onPrimaryFixed,
        backgroundColor: theme.colorScheme.secondaryFixed,
      ),
      onPressed: copyCallback,
      icon: Icon(Icons.save_as, color: theme.colorScheme.onPrimaryFixed),
      label: Text(
        'Save As New',
        style: theme.textTheme.titleLarge?.copyWith(
          color: theme.colorScheme.onPrimaryFixed,
        ),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    final theme = Theme.of(context);
    final layout = LayoutConfig(context);

    return Scaffold(
      appBar: ReckonerAppBar(
        leading:
            onReturn == null
                ? null
                : DefaultIconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () async {
                    if (await onReturn!() && context.mounted) {
                      Navigator.pop(context);
                    }
                  },
                  tooltip: 'Go Back',
                ),
        actions: [
          if (onDelete != null)
            Padding(
              padding: EdgeInsets.all(8),
              child: _delete(context, layout),
            ),
          if (onCopy != null)
            Padding(padding: EdgeInsets.all(8), child: _copy(context, layout)),
        ],
        title: heroTag != null ? Hero(tag: heroTag!, child: title) : title,
      ),
      body: Column(
        children: [
          Flexible(child: body),
          Padding(
            padding: EdgeInsets.all(8),
            child: DefaultTextButton(
              onPressed: () async {
                final result = await onSave();
                if (result != null && context.mounted) {
                  Navigator.pop(context, result);
                }
              },
              icon: Icon(
                Icons.save,
                size: 30,
                color: theme.colorScheme.onPrimaryFixed,
              ),
              child: Text(
                'Save',
                style: theme.textTheme.displaySmall?.copyWith(
                  color: theme.colorScheme.onSecondaryFixed,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
