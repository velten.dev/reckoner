import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/account.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/report.dart';
import '../../service/data_cache.dart';
import '../model/transaction_type.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/multiselect_search_input.dart';

class ReportFilterTxTypeEdit extends StatelessWidget {
  final ReportTransactionFilter filter;
  final ValueChanged<TransactionTypeFilter?>? onChanged;

  const ReportFilterTxTypeEdit(this.filter, {this.onChanged, super.key});
  @override
  Widget build(final BuildContext context) {
    final color = Theme.of(context).colorScheme.onSurface;
    return InputWrapper(
      child: ChipEdit<TransactionTypeFilter>(
        label: 'Transaction Type Filter',
        initialValue: filter.type,
        onChanged: (final value) {
          onChanged?.call(value);
          filter.type = value!;
        },
        options: [
          FormBuilderChipOption(
            value: TransactionTypeFilter.all,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [Icon(Icons.height, color: color), Text('All')],
            ),
          ),
          FormBuilderChipOption(
            value: TransactionTypeFilter.income,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [Icon(Icons.north, color: color), Text('Income')],
            ),
          ),
          FormBuilderChipOption(
            value: TransactionTypeFilter.expense,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [Icon(Icons.south, color: color), Text('Expenses')],
            ),
          ),
        ],
      ),
    );
  }
}

class ReportFilterEdit extends WatchingStatefulWidget {
  final ReportTransactionFilter filter;
  const ReportFilterEdit(this.filter, {super.key});

  @override
  State<StatefulWidget> createState() => _ReportFilterEditState();
}

class _ReportFilterEditState extends State<ReportFilterEdit> {
  Map<String, Category> catMap = {};
  ReportTransactionFilter get filter => widget.filter;

  @override
  Widget build(final BuildContext context) {
    final color = Theme.of(context).colorScheme.onSurface;
    final iconThemeData = IconTheme.of(context).copyWith(color: color);

    final categoryGroups = watchValue((final DataCache c) => c.categoryGroups);
    final accounts = watchValue((final DataCache c) => c.accounts);
    final currencyMap = watchValue((final DataCache c) => c.currencyMap);
    final merchants = watchValue((final DataCache c) => c.merchants);
    final tags = watchValue((final DataCache c) => c.tags);

    final filterGroups =
        categoryGroups
            .where((final g) => filter.categoryGroupIds.contains(g.uuid))
            .toList();
    final Map<String, Category> catMap = (filterGroups.isEmpty
            ? categoryGroups
            : filterGroups)
        .map((final g) => g.categories)
        .fold(<Category>[], (final prev, final element) => prev + element)
        .asMap()
        .map((final _, final c) => MapEntry(c.uuid, c));

    return IconTheme(
      data: iconThemeData,
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          InputWrapper(
            child: MultiselectSearchFormField<Account>(
              decoration: const InputDecoration(
                suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                label: Text('Account Filters'),
              ),
              itemToDisplayString: (final item) => item.displayName,
              searchItems: accounts,
              initialValue:
                  accounts
                      .where((final a) => filter.accountIds.contains(a.uuid))
                      .toList(),
              onChanged:
                  (final value) =>
                      filter.accountIds =
                          value?.map((final e) => e.uuid).toList() ?? [],
            ),
          ),
          if (categoryGroups.isNotEmpty)
            InputWrapper(
              child: MultiselectSearchFormField<CategoryGroup>(
                decoration: const InputDecoration(
                  suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                  label: Text('Category Group Filters'),
                ),
                itemToDisplayString: (final item) => item.displayName,
                searchItems: categoryGroups,
                initialValue: filterGroups,
                onChanged:
                    (final value) => setState(
                      () =>
                          filter.categoryGroupIds =
                              value?.map((final e) => e.uuid).toList() ?? [],
                    ),
              ),
            ),
          if (categoryGroups.isNotEmpty)
            InputWrapper(
              child: MultiselectSearchFormField<Category>(
                decoration: const InputDecoration(
                  suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                  label: Text('Category Filters'),
                ),
                itemToDisplayString: (final item) => item.displayName,
                searchItems: catMap.values.toList(),
                initialValue:
                    filter.categoryIds
                        .where((final id) => catMap.containsKey(id))
                        .map((final e) => catMap[e]!)
                        .toList(),
                onChanged:
                    (final value) =>
                        filter.categoryIds =
                            value?.map((final e) => e.uuid).toList() ?? [],
              ),
            ),
          InputWrapper(
            child: MultiselectSearchFormField<Currency>(
              decoration: const InputDecoration(
                suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                label: Text('Currency Filters'),
              ),
              itemToDisplayString: (final item) => item.displayName,
              searchItems: currencyMap.values.toList(),
              initialValue:
                  filter.currencyCodes
                      .where((final code) => currencyMap.containsKey(code))
                      .map((final e) => currencyMap[e]!)
                      .toList(),
              onChanged:
                  (final value) =>
                      filter.currencyCodes =
                          value?.map((final e) => e.code).toList() ?? [],
            ),
          ),
          InputWrapper(
            child: MultiselectSearchFormField<String>(
              decoration: const InputDecoration(
                suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                label: Text('Merchant Filters'),
              ),
              itemToDisplayString: (final item) => item,
              searchItems: merchants,
              initialValue:
                  merchants
                      .where((final m) => filter.merchants.contains(m))
                      .toList(),
              onChanged: (final value) => filter.merchants = value ?? [],
            ),
          ),
          if (tags.isNotEmpty)
            InputWrapper(
              child: MultiselectSearchFormField<String>(
                decoration: const InputDecoration(
                  suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                  label: Text('Tag Filters'),
                ),
                itemToDisplayString: (final item) => item,
                searchItems: tags,
                initialValue: filter.tags,
                onChanged: (final value) => filter.tags = value ?? [],
              ),
            ),
        ],
      ),
    );
  }
}
