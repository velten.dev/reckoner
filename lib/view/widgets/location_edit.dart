import 'package:flutter/material.dart';

import '../../model/transaction.dart';
import 'form/edit_fields.dart';
import 'form/validators.dart';

class LocationEditTile extends StatefulWidget {
  final Location location;
  final bool compact;
  const LocationEditTile({
    required this.location,
    super.key,
    this.compact = false,
  });

  @override
  State<StatefulWidget> createState() => _LocationEditTileState();
}

class _LocationEditTileState extends State<LocationEditTile> {
  late bool validate;

  @override
  void initState() {
    super.initState();
    validate = widget.location.hasData;
  }

  @override
  Widget build(final BuildContext context) {
    return ExpansionTile(
      title: const Text('Location'),
      childrenPadding: const EdgeInsets.only(left: 20, right: 20),
      initiallyExpanded: validate,
      maintainState: true,
      trailing: Icon(validate ? Icons.remove : Icons.add),
      iconColor: Colors.red,
      collapsedIconColor: Colors.green,
      onExpansionChanged: (final value) => setState(() => validate = value),
      children: [
        LocationEdit(
          compact: widget.compact,
          location: widget.location,
          validate: validate,
        ),
      ],
    );
  }
}

class LocationEdit extends StatelessWidget {
  final Location location;
  final bool validate;
  final bool compact;

  const LocationEdit({
    required this.location,
    super.key,
    this.validate = true,
    this.compact = true,
  });

  @override
  Widget build(final BuildContext context) {
    String? validator(final String? value) {
      if (!validate) return null;
      return nonNullValidator(context, value);
    }

    final cityEdit = TextFormField(
      textCapitalization: TextCapitalization.words,
      decoration: const InputDecoration(labelText: 'City'),
      initialValue: location.city,
      onChanged: (final value) => location.city = value,
      validator: validator,
    );

    final regionEdit = TextFormField(
      textCapitalization: TextCapitalization.characters,
      decoration: const InputDecoration(labelText: 'State'),
      initialValue: location.region,
      onChanged: (final value) => location.region = value,
      validator: validator,
    );

    final zipEdit = TextFormField(
      decoration: const InputDecoration(labelText: 'Zip'),
      initialValue: location.zip,
      onChanged: (final value) => location.zip = value,
      validator: validator,
    );

    final inputs = [
      NameEdit(model: location, validate: validate),
      TextFormField(
        textCapitalization: TextCapitalization.words,
        decoration: const InputDecoration(labelText: 'Address'),
        initialValue: location.address,
        maxLines: 2,
        onChanged: (final value) => location.address = value,
        validator: validator,
      ),
    ];

    if (compact) {
      const padding = EdgeInsets.only(left: 10, right: 10);
      inputs.add(
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Padding(padding: padding, child: cityEdit),
            ),
            Expanded(
              flex: 1,
              child: Padding(padding: padding, child: regionEdit),
            ),
            Expanded(flex: 1, child: Padding(padding: padding, child: zipEdit)),
          ],
        ),
      );
    } else {
      inputs.addAll([cityEdit, regionEdit, zipEdit]);
    }

    return Column(children: inputs);
  }
}
