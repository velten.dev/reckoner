import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/category.dart';
import '../../model/report.dart';
import '../../service/data_cache.dart';
import '../model/layout_config.dart';
import '../widgets/app_bar.dart';
import '../widgets/date_range_picker.dart';
import 'transactions.dart';

class TransactionScreen extends WatchingWidget {
  final ReportTransactionFilter filter;

  const TransactionScreen(this.filter, {super.key});

  @override
  Widget build(final BuildContext context) {
    String title;
    String heroTag;

    if (filter.accountIds.length == 1) {
      title =
          watchValue((final DataCache c) => c.accounts)
              .singleWhere((final a) => a.uuid == filter.accountIds.first)
              .displayName;
      heroTag = 'account${filter.accountIds.first}';
    } else if (filter.merchants.length == 1) {
      title = watchValue(
        (final DataCache c) => c.merchants,
      ).singleWhere((final a) => a == filter.merchants.first);
      heroTag = 'merchant${filter.merchants.first}';
    } else if (filter.noMerchant) {
      title = 'No Merchant';
      heroTag = 'noMerchant';
    } else if (filter.noTag) {
      title = 'No Tag';
      heroTag = 'noTag';
    } else if (filter.categoryIds.length == 1) {
      final catGroups = watchValue((final DataCache c) => c.categoryGroups);

      final Category? category = catGroups.fold(
        null,
        (final previousValue, final element) =>
            previousValue ?? element.findCategory(filter.categoryIds.first),
      );
      filter.categoryIds = category!.familyUuids;
      title = category.name;
      heroTag = 'category${filter.categoryIds.first}';
    } else if (filter.categoryGroupIds.length == 1) {
      final categoryGroup = watchValue(
        (final DataCache c) => c.categoryGroups,
      ).singleWhere(
        (final group) => group.uuid == filter.categoryGroupIds.first,
      );
      title = 'Uncategorized: ${categoryGroup.displayName}';
      heroTag = 'categoryGroup${filter.categoryGroupIds.first}';
    } else if (filter.tags.length == 1) {
      title = filter.tags.first;
      heroTag = 'tag${filter.tags.first}';
    } else {
      final currency =
          watchValue((final DataCache c) => c.currencyMap)[filter
              .currencyCodes
              .first]!;
      title = currency.displayName;
      heroTag = 'currency${filter.currencyCodes.first}';
    }

    final layoutCfg = LayoutConfig(context);
    final mobileView =
        [LayoutTypes.tall, LayoutTypes.square].contains(layoutCfg.layout) &&
        layoutCfg.isMobile &&
        layoutCfg.screenWidth < layoutCfg.screenHeight;

    return Scaffold(
      appBar: ReckonerAppBar(
        actions: [if (!mobileView) DateRangePicker()],
        title: Hero(tag: heroTag, child: Text(title)),
      ),
      body: Column(
        children: [
          if (mobileView)
            Container(
              color: Theme.of(context).colorScheme.secondaryFixed,
              width: double.infinity,
              child: const Center(child: DateRangePicker()),
            ),
          Expanded(child: TransactionsDisplay(filter: filter)),
        ],
      ),
    );
  }
}
