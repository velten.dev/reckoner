import 'package:flutter/material.dart';

import '../../model/secure_storage.dart';
import '../../service/app_lock.dart';
import '../router.dart';
import '../widgets/passkey_input_field.dart';
import '../widgets/shake_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String passkey = '';
  bool shake = false;

  @override
  Widget build(final BuildContext context) {
    return FutureBuilder(
      future: Future(() async {
        final settings = await SecureStorage.appLockSettings;
        return (
          canUnlockTime: settings.canUnlockTime,
          useBiometrics: settings.useBiometrics,
          length: settings.password.length,
        );
      }),
      builder: (final context, final snapshot) {
        final unlockTime = snapshot.data?.canUnlockTime;
        final useBiometrics = snapshot.data?.useBiometrics ?? false;
        final length = snapshot.data?.length ?? 0;
        Widget? child;

        // Should be impossible with the router, but doing defensive coding
        // ignore: invalid_use_of_protected_member
        if (!AppLock.isLocked) AppRouter.appLockRoute();

        if (unlockTime != null) {
          final tryAgainDuration = unlockTime.difference(DateTime.now());
          final tryAgainMin = (tryAgainDuration.inSeconds / 60).round();
          if (tryAgainMin > 0) {
            Future.delayed(
              tryAgainDuration,
              () => setState(() => shake = true),
            );
            child = Center(
              child: Text(
                'Too many unlock attempts tried.\nTry again in $tryAgainMin minutes.',
              ),
            );
          }
        }

        if (useBiometrics) {
          if (child == null) AppLock.unlock();

          child ??= const TextButton(
            onPressed: AppLock.unlock,
            child: Text('Reckoner is locked. Please login.'),
          );
        } else {
          child ??= PasskeyInputField(
            decoration: InputDecoration.collapsed(
              hintText: List.filled(length, '∘', growable: false).fold<String>(
                '',
                (final value, final element) => value + element,
              ),
            ),
            autofocus: true,
            style: Theme.of(context).textTheme.displayLarge,
            onChanged: (final value) async {
              if (value.length != length) return;
              passkey = value;
              if (!await AppLock.unlock(passkey)) {
                shake = true;
                setState(() => passkey = '');
              }
            },
          );

          if (shake) {
            shake = false;
            child = ShakeWidget(shakeOffset: 40, child: child);
          }
        }

        return Scaffold(body: Center(child: child));
      },
    );
  }
}
