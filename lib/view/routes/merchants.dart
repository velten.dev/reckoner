import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../database/dao/balance_dao.dart';
import '../../database/reckoner_db.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../l10n/localizations.dart';
import '../util/popup_dialogs.dart';
import '../widgets/account_balance_display.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

class MerchantDisplay extends WatchingWidget {
  static const style = TextStyle(fontWeight: FontWeight.bold);

  const MerchantDisplay({super.key});

  Future<void> renameMerchant(
    final BuildContext context,
    final String existingMerchant,
  ) => showDialog(
    context: context,
    builder: (final ctx) {
      String newName = existingMerchant;
      return InputForm(
        title: titleString(context, 'Merchant', existingMerchant),
        onSave:
            () => ReckonerDb.I.transactionMerchantDao.updateMerchant(
              existingMerchant,
              newName,
            ),
        onDelete:
            existingMerchant.isEmpty
                ? null
                : () async {
                  if (await yesNoDialog(
                    context,
                    ReckonerLocalizations.of(context).deleteMsg('merchant'),
                  )) {
                    await ReckonerDb.I.transactionMerchantDao.deleteMerchant(
                      existingMerchant,
                    );
                  }
                },
        inputs: [
          TextFormField(
            decoration: const InputDecoration(label: Text('Name')),
            initialValue: newName,
            onChanged: (final value) => newName = value,
          ),
        ],
      );
    },
  );

  Widget _merchantDisplay(final BuildContext context, final String item) {
    return DisplayNameEdit(
      name: item,
      onEdit: () => renameMerchant(context, item),
      transactionPath: merchantTransactionsPath(item),
      iconSize: 20,
    );
  }

  @override
  Widget build(final BuildContext context) {
    final merchants = watchValue((final DataCache c) => c.merchants)
      ..sort((final a, final b) => a.compareTo(b));
    final endDate = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange()?.end,
    );

    return StreamBuilder(
      stream: ReckonerDb.I.balanceDao.loadBalances(
        type: BalanceType.merchant,
        endDate: endDate,
      ),
      builder: (final context, final snapshot) {
        final balanceMap = snapshot.data ?? {};
        final tiles =
            merchants
                .map(
                  (final e) => ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(child: _merchantDisplay(context, e)),
                        fixedWidthSpacer(),
                        BalanceDisplay(balanceMap: balanceMap[e] ?? {}),
                      ],
                    ),
                    visualDensity: const VisualDensity(
                      vertical: VisualDensity.minimumDensity,
                    ),
                  ),
                )
                .toList();

        if (balanceMap.containsKey(null)) {
          tiles.add(
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () => context.push(noMerchantTransactionsPath),
                    child: const Text('(No Merchant)'),
                  ),
                  BalanceDisplay(balanceMap: balanceMap[null]!),
                ],
              ),
              visualDensity: const VisualDensity(
                vertical: VisualDensity.minimumDensity,
              ),
            ),
          );
        }

        return ReportHeader(
          report: di<DataCache>().merchantReport,
          child:
              tiles.isEmpty
                  ? const NoDataPlaceholder(
                    'No merchants created. Add a merchant by editing or creating a transaction.',
                  )
                  : FixedWidthContainer(
                    child: Column(
                      children: [
                        const ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Name', style: style),
                              Text('Total to Date', style: style),
                            ],
                          ),
                        ),
                        Expanded(child: ListView(children: tiles)),
                      ],
                    ),
                  ),
        );
      },
    );
  }
}
