import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/report.dart';
import '../../service/data_cache.dart';
import '../util/save_item.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/form/list_search_input.dart';
import '../widgets/input_screen.dart';

class ReportGroupEdit extends StatefulWidget {
  final String groupId;

  const ReportGroupEdit({super.key, required this.groupId});

  @override
  State<ReportGroupEdit> createState() => _ReportGroupEditState();
}

class _ReportGroupEditState extends State<ReportGroupEdit> {
  late final ReportGroup group;
  late final Map<String, Report> reportMap;

  @override
  void initState() {
    super.initState();
    final origGroup = di<DataCache>().reportGroupMap.value[widget.groupId];
    group = origGroup!.copy();
    if (group.layouts.isEmpty) group.layouts.add(ReportGroupLayout());
    reportMap = di<DataCache>().reportMap.value;
  }

  Widget mapReportRow(
    final double maxWidth,
    final int index,
    final ReportGroupRow row,
  ) => Padding(
    padding: const EdgeInsets.all(5),
    child: InputDecorator(
      decoration: InputDecoration(
        label: Text('Row ${index + 1}'),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      child: OverflowBar(
        overflowAlignment: OverflowBarAlignment.center,
        alignment: MainAxisAlignment.spaceAround,
        children: [
          TextButton(
            onPressed:
                () => setState(() => group.layouts.first.rows.removeAt(index)),
            child: const Row(
              mainAxisSize: MainAxisSize.min,
              children: [Icon(Icons.cancel), Text('Remove Row')],
            ),
          ),
          ...row.reports.mapIndexed(
            (final index, final rpt) => SizedBox(
              width: max(300, maxWidth / 5),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: ListSearchFormField(
                        searchItems: reportMap.values.toList(),
                        itemToDisplayString: (final rpt) => rpt.displayName,
                        itemToDisplayWidget:
                            (final rpt) => Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(rpt.icon),
                                Flexible(
                                  child: Text(
                                    rpt.displayName,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                        initialValue: rpt,
                        onChanged:
                            (final rpt) =>
                                rpt != null
                                    ? setState(() => row.reports[index] = rpt)
                                    : null,
                      ),
                    ),
                    DefaultIconButton(
                      onPressed:
                          () => setState(() => row.reports.removeAt(index)),
                      icon: const Icon(Icons.delete),
                    ),
                  ],
                ),
              ),
            ),
          ),
          if (row.reports.length < 3)
            TextButton(
              onPressed: () => setState(() => row.reports.add(Report())),
              child: const Text('Add Report to Row'),
            ),
        ],
      ),
    ),
  );

  @override
  Widget build(final BuildContext context) {
    return InputScreen(
      body: SingleChildScrollView(
        child: LayoutBuilder(
          builder:
              (final context, final constraints) => Column(
                children: [
                  ...group.layouts.first.rows.mapIndexed(
                    (final ind, final e) =>
                        mapReportRow(constraints.maxWidth, ind, e),
                  ),
                  Center(
                    child: TextButton(
                      onPressed:
                          () => setState(
                            () =>
                                group.layouts.first.rows.add(ReportGroupRow()),
                          ),
                      child: const Text('Add Row'),
                    ),
                  ),
                ],
              ),
        ),
      ),
      onSave: () async {
        group.layouts.first.rows.mapIndexed(
          (final index, final element) => element.order = index,
        );
        await saveItem(group);
        return true;
      },
      title: Text(group.name),
    );
  }
}
