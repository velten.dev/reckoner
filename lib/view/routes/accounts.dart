import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../database/dao/balance_dao.dart';
import '../../database/reckoner_db.dart';
import '../../model/account.dart';
import '../../model/currency.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../l10n/localizations.dart';
import '../util/popup_dialogs.dart';
import '../util/save_item.dart';
import '../widgets/account_balance_display.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

enum DataChoices { account, organization }

class AccountsDisplay extends WatchingWidget {
  const AccountsDisplay({super.key});

  @override
  Widget build(final BuildContext context) {
    final currencyMap = di<DataCache>().currencyMap.value;
    final dataList = watchValue((final DataCache c) => c.organizations);
    final endDate = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange()?.end,
    );

    return StreamBuilder(
      stream: ReckonerDb.I.balanceDao.loadBalances(
        type: BalanceType.account,
        endDate: endDate,
      ),
      builder: (final context, final snapshot) {
        final Widget body = ReportHeader(
          report: di<DataCache>().accountReport,
          child: _body(context, dataList, currencyMap, snapshot.data ?? {}),
        );
        return FloatingButtonOverlay(
          body,
          heroTag: 'add',
          onPressed: () => editHandler(context, null),
          tooltip: ReckonerLocalizations.of(context).addItem(itemName),
          icon: Icons.add_business,
        );
      },
    );
  }

  Widget _body(
    final BuildContext context,
    final List<Organization> dataList,
    final CurrencyMap currencyMap,
    final BalanceMap balances,
  ) {
    if (dataList.isEmpty) {
      return const NoDataPlaceholder(
        'No organizations created. Please create an organization first.',
      );
    }
    return FixedWidthContainer(
      child: ListView(
        children:
            dataList
                .map(
                  (final orgInfo) => ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        DisplayNameEdit(
                          name: orgInfo.name,
                          onEdit: () => editHandler(context, orgInfo),
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        TextButton(
                          child: Text(
                            '+ Add Account',
                            style: Theme.of(context).textTheme.bodySmall,
                          ),
                          onPressed:
                              () => context.push(
                                accountEditPath(newId),
                                extra: {'orgId': orgInfo.uuid},
                              ),
                        ),
                      ],
                    ),
                    subtitle: Column(
                      children:
                          orgInfo.accounts
                              .map(
                                (final e) => _accountSummary(
                                  context,
                                  e,
                                  currencyMap,
                                  balances,
                                ),
                              )
                              .toList(),
                    ),
                  ),
                )
                .toList(),
      ),
    );
  }

  List<({int color, String msg})> _checkAlerts(
    final Account account,
    final BalanceMap balanceMap,
  ) {
    final List<({int color, String msg})> warnList = [];
    final balances = balanceMap[account.uuid] ?? {};
    for (final alert in account.alerts) {
      final curCode = alert.currencyCode;
      final balance = balances[curCode] ?? 0;
      if (alert.isMax ? balance >= alert.amount : balance <= alert.amount) {
        warnList.add((color: alert.color, msg: alert.message));
      }
    }
    return warnList;
  }

  Widget _accountSummary(
    final BuildContext context,
    final Account item,
    final Map<String, Currency> currencyMap,
    final BalanceMap balanceMap,
  ) {
    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: DisplayNameEdit(
              name: item.name,
              path: accountEditPath(item.uuid),
              transactionPath: accountTransactionsPath(item.uuid),
              style: Theme.of(context).textTheme.titleLarge,
            ),
          ),

          //Needs to be in the title as in the trailing could cause a pixel
          //overflow. The title is the main size of the ListTile
          BalanceDisplay(
            balanceMap: balanceMap[item.uuid] ?? {},
            defaultCurrency: currencyMap[item.defaultCurrencyCode ?? ''],
          ),
        ],
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:
            _checkAlerts(item, balanceMap).map((final alert) {
              final color = Color(alert.color);
              final TextStyle style = Theme.of(
                context,
              ).textTheme.bodyMedium!.copyWith(color: color);
              return Text(alert.msg, style: style);
            }).toList(),
      ),
    );
  }

  Future<void> editHandler(
    final BuildContext context,
    final Organization? editItem,
  ) => showDialog(
    context: context,
    builder: (final context) {
      final model = editItem?.copy() ?? Organization();

      return InputForm(
        title: titleString(context, 'Organization', editItem),
        onSave: () => saveItem<Organization>(model),
        onDelete:
            editItem == null
                ? null
                : () => deletePopup(context, model, itemName),
        inputs: [NameEdit(model: model)],
      );
    },
  );

  String get itemName => 'Organization';
}
