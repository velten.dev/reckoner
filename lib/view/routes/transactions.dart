import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../model/report.dart';
import '../../model/transaction.dart';
import '../../service/data_cache.dart';
import '../l10n/localizations.dart';
import '../model/layout_config.dart';
import '../model/transaction_type.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';
import '../widgets/transaction/transaction_list.dart';
import '../widgets/transaction/transaction_table.dart';

class TransactionsDisplay extends WatchingWidget {
  final ReportTransactionFilter? filter;

  const TransactionsDisplay({super.key, this.filter});

  @override
  Widget build(final BuildContext context) {
    final report = di<DataCache>().transactionReport;
    if (watchValue((final DataCache c) => c.accounts).isEmpty) {
      return const NoDataPlaceholder(
        'No accounts added, please add an account before adding transactions.',
      );
    }

    final txFilter =
        filter ?? watchIt<ValueNotifier<ReportTransactionFilter>>().value;

    bool filterFunction(final Transaction tx) {
      if (txFilter.type == TransactionTypeFilter.expense && tx.amount > 0) {
        return false;
      }
      if (txFilter.type == TransactionTypeFilter.income && tx.amount < 0) {
        return false;
      }
      if (txFilter.accountIds.isNotEmpty &&
          !txFilter.accountIds.contains(tx.accountUuid)) {
        return false;
      }
      if (txFilter.currencyCodes.isNotEmpty &&
          !txFilter.currencyCodes.contains(tx.currencyCode)) {
        return false;
      }
      if (txFilter.merchants.isNotEmpty &&
          !txFilter.merchants.contains(tx.merchantName)) {
        return false;
      }
      if (txFilter.noMerchant && tx.merchantName.isNotEmpty) return false;
      if (txFilter.tags.isNotEmpty &&
          !tx.splits.fold(
            false,
            (final previousValue, final element) =>
                previousValue ||
                element.tags.intersection(txFilter.tags.toSet()).isEmpty,
          )) {
        return false;
      }
      if (txFilter.noTag &&
          !tx.splits.fold(
            false,
            (final previousValue, final element) =>
                previousValue || element.tags.isEmpty,
          )) {
        return false;
      }
      if (txFilter.categoryIds.isNotEmpty &&
          !tx.splits.fold(
            false,
            (final previousValue, final element) =>
                previousValue ||
                txFilter.categoryIds
                    .toSet()
                    .intersection(
                      element.categories.values
                          .map((final c) => c.uuid)
                          .toSet(),
                    )
                    .isNotEmpty,
          )) {
        return false;
      }
      if (txFilter.categoryGroupIds.isNotEmpty &&
          !tx.splits.fold(
            false,
            (final previousValue, final element) =>
                previousValue ||
                element.categories.keys
                    .toSet()
                    .intersection(txFilter.categoryGroupIds.toSet())
                    .isEmpty,
          )) {
        return false;
      }
      return true;
    }

    var transactions = watchValue((final DataCache c) => c.transactions);

    if (txFilter.merchants.isNotEmpty || txFilter.noMerchant) {
      transactions =
          transactions.where((final e) => e.isTransfer == false).toList();
    }
    transactions =
        transactions
            .where(
              (final tx) =>
                  tx.transaction != null
                      ? filterFunction(tx.transaction!)
                      : filterFunction(tx.transfer!.source) ||
                          filterFunction(tx.transfer!.destination),
            )
            .toList();

    final layoutConfig = LayoutConfig(context);
    final isUltraWide = layoutConfig.layout == LayoutTypes.ultawide;
    final child =
        layoutConfig.isMobile && !isUltraWide
            ? TransactionList(txEntities: transactions)
            : TransactionTable(txEntities: transactions);

    if (transactions.isEmpty && !isUltraWide) {
      return FloatingButtonOverlay(
        const NoDataPlaceholder(
          'No transactions loaded. Add a transaction or change the date range filer.',
        ),
        heroTag: 'add',
        onPressed: () => context.push(transactionEditPath(newId)),
        tooltip: ReckonerLocalizations.of(context).addItem('transactions'),
        icon: Icons.add,
      );
    }

    if (transactions.isNotEmpty && report != null) {
      return ReportHeader(report: report, child: child);
    }
    return child;
  }
}
