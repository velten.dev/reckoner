import 'dart:math';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/environment_const.dart';
import '../../config/user_pref_const.dart';
import '../../model/settings.dart';
import '../../service/app_lock.dart';
import '../../service/data_cache.dart';
import '../model/layout_config.dart';
import '../widgets/app_bar.dart';
import '../widgets/date_range_picker.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/help_icon.dart';
import '../widgets/sync/sync_icon_button.dart';
import '../widgets/transaction/transaction_add_button.dart';
import '../widgets/transaction/transaction_filter_button.dart';

class RootShell extends WatchingStatefulWidget {
  final Widget child;
  final String route;
  final String? id;
  const RootShell({
    super.key,
    required this.child,
    required this.route,
    this.id,
  });

  @override
  State<RootShell> createState() => _RootShellState();
}

class _RootShellState extends State<RootShell> {
  late bool _collapse = false;
  bool isTall = false;
  bool isMobile = false;
  int _selectedIndex = 0;
  List<NavigationEntry> entries = [];

  Widget get _navRail {
    final theme = Theme.of(context);
    final iconTheme = theme.iconTheme;
    final textTheme = theme.textTheme.titleSmall;
    final onPrimaryFixed = theme.colorScheme.onPrimaryFixed;
    return NavigationRail(
      elevation: 4,
      backgroundColor: theme.colorScheme.primaryFixed,
      indicatorColor: theme.colorScheme.secondaryFixed,
      selectedIconTheme: iconTheme.copyWith(color: onPrimaryFixed),
      selectedLabelTextStyle: textTheme?.copyWith(color: onPrimaryFixed),
      unselectedIconTheme: iconTheme.copyWith(
        color: onPrimaryFixed.withAlpha(150),
      ),
      unselectedLabelTextStyle: textTheme?.copyWith(
        color: onPrimaryFixed.withAlpha(150),
      ),
      destinations:
          entries
              .map(
                (final e) => NavigationRailDestination(
                  icon: Icon(e.icon),
                  label: Text(e.name),
                ),
              )
              .toList(),
      selectedIndex: _selectedIndex,
      onDestinationSelected: (final index) {
        if (isTall && !isMobile) Navigator.of(context).pop();
        context.go(entries[index].route);
      },
      useIndicator: true,
      extended: !_collapse || isTall,
    );
  }

  Widget? get _bottomBar =>
      isMobile
          ? BottomNavigationBar(
            backgroundColor: Theme.of(context).colorScheme.primaryFixed,
            selectedItemColor: Theme.of(context).colorScheme.onPrimaryFixed,
            unselectedItemColor: Theme.of(
              context,
            ).colorScheme.onPrimaryFixed.withAlpha(150),
            items:
                entries
                    .map(
                      (final e) => BottomNavigationBarItem(
                        icon: Icon(e.icon),
                        label: e.name,
                        backgroundColor:
                            Theme.of(context).colorScheme.primaryFixed,
                      ),
                    )
                    .toList(),
            currentIndex: _selectedIndex,
            onTap:
                (final index) => context.go(
                  entries[index].route,
                  extra: {'name': entries[index].name},
                ),
          )
          : null;

  Widget? get _collapseButton =>
      isTall
          ? null
          : IconButton(
            onPressed: () {
              final val = !_collapse;
              getPreferences().setBool(collapseKey, val);
              setState(() => _collapse = !_collapse);
            },
            icon:
                _collapse
                    ? const Icon(Icons.double_arrow)
                    : Transform.rotate(
                      angle: 180 * pi / 180,
                      child: const Icon(Icons.double_arrow),
                    ),
            splashRadius: 30,
          );

  NavigationEntry get _entry => entries[_selectedIndex];

  Widget get _body => Row(
    children: [
      if (!isTall) _navRail,
      Expanded(
        child: Column(
          children: [
            if (isTall && isMobile)
              Container(
                color: Theme.of(context).colorScheme.secondaryFixed,
                width: double.infinity,
                child: const Center(child: DateRangePicker()),
              ),
            Expanded(child: widget.child),
          ],
        ),
      ),
    ],
  );

  ReckonerAppBar _appBar(final bool hasPassword) {
    final List<Widget> appBarActions = [
      const AddTransactionButton(),
      if (widget.route.contains('transactions'))
        const TransactionFilterButton(),
      if (!isDemo) const SyncIconButton(),
      if (!isMobile || !isTall) const DateRangePicker(),
      if (hasPassword)
        const DefaultIconButton(
          onPressed: AppLock.lock,
          icon: Icon(Icons.lock),
        ),
    ];

    return ReckonerAppBar(
      actions: appBarActions,
      leading: _collapseButton,
      title: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [Text(_entry.name), HelpIcon(_entry)],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _collapse = getPreferences().getBool(collapseKey) ?? false;
  }

  @override
  Widget build(final BuildContext context) {
    final layoutCfg = LayoutConfig(context);
    isTall = [LayoutTypes.tall, LayoutTypes.square].contains(layoutCfg.layout);
    isMobile =
        layoutCfg.isMobile && layoutCfg.screenWidth < layoutCfg.screenHeight;
    final hasPassword = watchPropertyValue(
      (final AppLock a) => AppLock.hasPassword,
    );

    entries =
        watchValue(
          (final DataCache c) => c.setting,
        ).navList.where((final e) => !e.hide).toList();
    _selectedIndex = entries.indexWhere(
      (final element) =>
          element.route.split('/')[1] == widget.route.split('/')[1] &&
          element.uuid == widget.id,
    );

    if (_selectedIndex < 0) _selectedIndex = 0;
    return Scaffold(
      appBar: _appBar(hasPassword),
      drawer: isTall && !isMobile ? Drawer(child: _navRail) : null,
      body: _body,
      bottomNavigationBar: _bottomBar,
    );
  }
}
