import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/account.dart';
import '../../model/currency.dart';
import '../../model/settings.dart';
import '../../service/data_cache.dart';
import '../model/currency_input_formatter.dart';
import '../util/popup_dialogs.dart';
import '../util/save_item.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/form/color_input.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form_screen.dart';
import '../widgets/form/list_search_input.dart';
import '../widgets/form/validators.dart';

class AccountEdit extends WatchingStatefulWidget {
  final String? accountId;
  final String? orgId;
  final bool copy;

  const AccountEdit({super.key, this.accountId, this.copy = false, this.orgId});

  @override
  State<AccountEdit> createState() => _AccountEdit();
}

class _AccountEdit extends State<AccountEdit> {
  List<Organization> _organizationList = [];
  CurrencyMap _currencyMap = {};
  late Account _model;
  late Account? _prevAccount;
  late UserSetting _settings;

  @override
  void initState() {
    super.initState();
    _settings = di<DataCache>().setting.value;
    final uuid = widget.accountId;
    _prevAccount = di<DataCache>().accounts.value.singleWhereOrNull(
      (final a) => a.uuid == uuid,
    );
    _model = _prevAccount?.copy() ?? Account();
    final org = di<DataCache>().organizations.value.singleWhereOrNull(
      (final element) => element.uuid == widget.orgId,
    );
    if (org != null) _model.organization = org;
  }

  Widget _idEdit() {
    final List<Widget> list = [
      ..._model.identifiers.map(
        (final e) => _IdEdit(e, () => _model.identifiers.remove(e)),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextButton(
          onPressed:
              () => setState(
                () => _model.identifiers.add(
                  AccountIdentifier(uuid: _model.uuid),
                ),
              ),
          child: const Text('Add Identifier'),
        ),
      ),
    ];
    return Column(children: list);
  }

  Widget _alertEdit() {
    return Column(
      children: [
        TextButton(
          onPressed: () async {
            var currency = _currencyMap[_model.defaultCurrencyCode];
            if (_model.isMultiCurrency || currency == null) {
              currency = await selectCurrency(context, null);
            }
            if (currency == null) return;
            setState(
              () => _model.alerts.add(
                AccountAlert(
                  uuid: _model.uuid,
                  currencyCode: currency!.code,
                  // ignore: deprecated_member_use
                  color: Theme.of(context).colorScheme.error.value,
                ),
              ),
            );
          },
          child: const Text('Add Account Alert'),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children:
                _model.alerts.map((final warn) {
                  return SizedBox(
                    width: 220,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: _AlertEdit(
                        alert: warn,
                        currency: _currencyMap[warn.currencyCode],
                        deleteCallback:
                            () => setState(() => _model.alerts.remove(warn)),
                      ),
                    ),
                  );
                }).toList(),
          ),
        ),
      ],
    );
  }

  List<Widget> _inputs() => [
    Wrap(
      alignment: WrapAlignment.center,
      children: [
        InputWrapper(
          child: ListSearchFormField<Organization>(
            decoration: const InputDecoration(
              suffixIcon: Icon(Icons.arrow_drop_down_rounded),
              label: Text('Organization'),
            ),
            itemToDisplayString: (final item) => item.name,
            onSaved:
                (final data) =>
                    _model.organization = data ?? _model.organization,
            initialValue: _model.organization,
            searchItems: _organizationList,
          ),
        ),
        InputWrapper(child: NameEdit(model: _model)),
        InputWrapper(
          child: FormBuilderCheckbox(
            name: 'isMultiCurrency',
            title: const Text('Allow multiple currencies'),
            initialValue: _model.isMultiCurrency,
            onChanged: (final value) {
              if (value == null) return;
              if (_model.isMultiCurrency != value) {
                setState(() => _model.isMultiCurrency = value);
              }
            },
          ),
        ),
        InputWrapper(
          child: ListSearchFormField<Currency>(
            decoration: InputDecoration(
              suffixIcon: const Icon(Icons.arrow_drop_down_rounded),
              label: Text(
                _model.isMultiCurrency ? 'Default Currency' : 'Currency',
              ),
            ),
            itemToDisplayString: (final item) => item.name,
            onChanged: (final data) => _model.currency = data,
            initialValue:
                _currencyMap[_model.defaultCurrencyCode] ??
                _currencyMap[_settings.defaultCurrencyCode],
            searchItems: _currencyMap.values.toList(),
            validator:
                (final value) =>
                    _model.isMultiCurrency
                        ? null
                        : nonNullValidator(context, value),
          ),
        ),
      ],
    ),
    ExpansionTile(
      title: const Text('Identifiers'),
      childrenPadding: const EdgeInsets.only(left: 20, right: 20),
      initiallyExpanded: true,
      maintainState: true,
      children: [_idEdit()],
    ),
    ExpansionTile(
      title: const Text('Account Alerts'),
      initiallyExpanded: true,
      maintainState: true,
      children: [_alertEdit()],
    ),
  ];

  @override
  Widget build(final BuildContext context) {
    _currencyMap = watchValue((final DataCache c) => c.currencyMap);
    _settings = watchValue((final DataCache c) => c.setting);
    _organizationList = watchValue((final DataCache c) => c.organizations);

    return InputFormScreen(
      title: titleWidget(context, 'Account', _model.createDate),
      inputs: _inputs(),
      afterFormSave: () => saveItem(_model),
      hasChanges: () {
        if (widget.accountId == null) return false;
        final prevAccount = di<DataCache>().accounts.value.singleWhere(
          (final a) => a.uuid == widget.accountId,
        );
        if (prevAccount == _model) return false;
        return true;
      },
      deleteCallback:
          _prevAccount == null
              ? null
              : () async =>
                  await ReckonerDb.I.commonDao.softDeleteDbItem(_prevAccount!) >
                  0,
      beforeCopy: _model.isNew ? null : () => _model = _model.copy(true),
    );
  }
}

class _IdEdit extends StatelessWidget {
  final AccountIdentifier identifier;
  final void Function() deleteCallback;

  const _IdEdit(this.identifier, this.deleteCallback);

  @override
  Widget build(final BuildContext context) {
    return InputFieldGroup(
      child: Wrap(
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          InputWrapper(child: NameEdit(model: identifier)),
          InputWrapper(
            child: TextFormField(
              decoration: const InputDecoration(labelText: 'Identifier'),
              initialValue: identifier.identifier,
              onChanged: (final value) => identifier.identifier = value,
              validator: (final value) => nonNullValidator(context, value),
            ),
          ),
          DefaultIconButton(
            onPressed: () => deleteCallback,
            icon: const Icon(Icons.delete),
          ),
        ],
      ),
    );
  }
}

class _AlertEdit extends StatelessWidget {
  final AccountAlert alert;
  final Currency? currency;
  final void Function() deleteCallback;
  const _AlertEdit({
    required this.alert,
    this.currency,
    required this.deleteCallback,
  });

  @override
  Widget build(final BuildContext context) {
    final currencyFormatter = CurrencyInputFormatter(currency: currency);

    return InputFieldGroup(
      child: Column(
        children: [
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(labelText: 'Amount'),
            onChanged:
                (final value) => alert.amount = currencyFormatter.toDB(value),
            initialValue:
                alert.amount == 0
                    ? null
                    : currencyFormatter.fromDB(alert.amount),
            inputFormatters: [currencyFormatter],
            validator: (final value) => nonNullValidator(context, value),
          ),
          TextFormField(
            textCapitalization: TextCapitalization.sentences,
            decoration: const InputDecoration(labelText: 'Message'),
            initialValue: alert.message,
            onSaved: (final value) => alert.message = value ?? '',
            validator: (final value) => nonNullValidator(context, value),
          ),
          ChipEdit<bool>(
            label: 'Alert Type',
            initialValue: alert.isMax,
            onChanged: (final value) => alert.isMax = value ?? false,
            options: const [
              FormBuilderChipOption(value: true, child: Text('Maximum')),
              FormBuilderChipOption(value: false, child: Text('Minimum')),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: ColorInputForm(
                  initialValue: Color(alert.color),
                  // ignore: deprecated_member_use
                  onSaved: (final color) => alert.color = color?.value ?? 0,
                ),
              ),
              DefaultIconButton(
                icon: const Icon(Icons.delete),
                onPressed: deleteCallback,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
