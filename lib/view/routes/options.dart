import 'package:flutter/material.dart';

import '../../config/platform.dart';
import '../../config/user_pref_const.dart';
import '../widgets/options/currency_edit.dart';
import '../widgets/options/home_nav_reorder.dart';
import '../widgets/options/reports.dart';
import '../widgets/options/user_settings.dart';

class OptionsDisplay extends StatelessWidget {
  final int initialIndex;

  const OptionsDisplay({super.key, this.initialIndex = 0});

  @override
  Widget build(final BuildContext context) {
    const tabs = [
      Tab(icon: Icon(Icons.settings_applications), text: 'Settings'),
      Tab(icon: Icon(Icons.money_rounded), text: 'Currencies'),
      Tab(icon: Icon(Icons.move_down), text: 'Reorder'),
      Tab(icon: Icon(Icons.add_chart), text: 'Add/Edit Report'),
    ];

    var initialIndex = getPreferences().getInt(optionsTab) ?? 0;
    if (initialIndex < 0 || initialIndex >= tabs.length) initialIndex = 0;

    return LayoutBuilder(
      builder: (final context, final constraints) {
        return DefaultTabController(
          length: tabs.length,
          initialIndex: initialIndex,
          child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Theme.of(context).colorScheme.secondaryFixed,
              foregroundColor: Theme.of(context).colorScheme.onSecondaryFixed,
              toolbarHeight: appBarToolbarHeight,
              flexibleSpace: SafeArea(
                child: TabBar(
                  isScrollable: constraints.maxWidth < 600,
                  tabs: tabs,
                ),
              ),
            ),
            body: const TabBarView(
              children: [
                UserSettingsEdit(),
                CurrencyEdit(),
                HomeNavReorder(),
                ReportDefinitions(),
              ],
            ),
          ),
        );
      },
    );
  }
}
