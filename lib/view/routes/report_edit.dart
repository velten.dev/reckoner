import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/report.dart';
import '../../model/tz_date_time.dart';
import '../../service/data_cache.dart';
import '../model/layout_config.dart';
import '../model/transaction_type.dart';
import '../util/datetime_display.dart';
import '../util/relative_range_label.dart';
import '../util/save_item.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/input_form_screen.dart';
import '../widgets/report_filter_inputs.dart';

enum _ReportRange { current, all, date, relative }

class ReportEdit extends StatefulWidget {
  final String? reportId;
  final bool copy;

  const ReportEdit({super.key, this.reportId, this.copy = false});

  @override
  State<StatefulWidget> createState() => _ReportEditState();
}

class _ReportEditState extends State<ReportEdit> {
  late Report _model;
  late Report? _prevReport;
  _ReportRange range = _ReportRange.current;

  @override
  void initState() {
    super.initState();
    _prevReport = di.get<DataCache>().reportMap.value[widget.reportId];
    _model = _prevReport?.copy() ?? Report();
    if (_model.hasCustomRange) {
      range =
          _model.relativeEndOffset != null
              ? _ReportRange.relative
              : _ReportRange.date;
    }
  }

  List<Widget> inputs() => [
    InputWrapper(child: NameEdit(model: _model)),
    Wrap(
      alignment: WrapAlignment.center,
      children: [
        //Report Type
        InputWrapper(
          child: ChipEdit<ReportType>(
            label: 'Report Type',
            initialValue: _model.type,
            onChanged: (final value) {
              _model.loadOverTime = value == ReportType.line;
              if (value == ReportType.text) {
                _model.grouping = TransactionReportGrouping.currency;
              }
              setState(() => _model.type = value ?? _model.type);
            },
            options: [
              FormBuilderChipOption(
                value: ReportType.line,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Report.getIcon(ReportType.line)),
                    const Text('Line Graph'),
                  ],
                ),
              ),
              FormBuilderChipOption(
                value: ReportType.bar,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Report.getIcon(ReportType.bar)),
                    const Text('Bar Graph'),
                  ],
                ),
              ),
              FormBuilderChipOption(
                value: ReportType.pie,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Report.getIcon(ReportType.pie)),
                    const Text('Pie Graph'),
                  ],
                ),
              ),
              if (kDebugMode)
                FormBuilderChipOption(
                  value: ReportType.table,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Report.getIcon(ReportType.table)),
                      const Text('Table Report'),
                    ],
                  ),
                ),
              FormBuilderChipOption(
                value: ReportType.text,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Report.getIcon(ReportType.text)),
                    const Text('Text Report'),
                  ],
                ),
              ),
            ],
          ),
        ),
        if (_model.type != ReportType.text)
          InputWrapper(
            child: ChipEdit<TransactionReportGrouping>(
              label: 'Report Data Grouping',
              initialValue: _model.grouping,
              onChanged:
                  (final value) => setState(
                    () => _model.grouping = value ?? _model.grouping,
                  ),
              options: const [
                FormBuilderChipOption(
                  value: TransactionReportGrouping.account,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.account_balance), Text('Accounts')],
                  ),
                ),
                FormBuilderChipOption(
                  value: TransactionReportGrouping.category,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.category), Text('Category')],
                  ),
                ),
                FormBuilderChipOption(
                  value: TransactionReportGrouping.currency,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.money), Text('Currency')],
                  ),
                ),
                FormBuilderChipOption(
                  value: TransactionReportGrouping.merchant,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.store), Text('Merchant')],
                  ),
                ),
                FormBuilderChipOption(
                  value: TransactionReportGrouping.tag,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.label), Text('Tag')],
                  ),
                ),
              ],
            ),
          ),
        ReportFilterTxTypeEdit(
          _model.filterObj,
          onChanged:
              (final value) =>
                  _model.invert = value == TransactionTypeFilter.expense,
        ),
        if (!_model.loadOverTime && _model.type != ReportType.text)
          InputWrapper(
            child: ChipEdit<ReportDataSorting>(
              label: 'Report Data Sorting',
              initialValue: _model.sorting,
              onChanged: (final value) => _model.sorting = value!,
              options: const [
                FormBuilderChipOption(
                  value: ReportDataSorting.amount,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.sort), Text('Amount')],
                  ),
                ),
                FormBuilderChipOption(
                  value: ReportDataSorting.name,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [Icon(Icons.text_rotate_vertical), Text('Name')],
                  ),
                ),
              ],
            ),
          ),
        InputWrapper(
          child: ChipEdit<_ReportRange>(
            label: 'Date Filter',
            initialValue: range,
            onChanged: (final value) {
              if (value != _ReportRange.date) {
                _model
                  ..startDate = null
                  ..endDate = null;
              }
              if (value != _ReportRange.relative) {
                _model
                  ..relativeRange = null
                  ..relativeStartOffset = null
                  ..relativeEndOffset = null;
              }
              if (value == _ReportRange.current) {
                _model.relativeRange = ReportTimeRange.current;
              }
              setState(() => range = value ?? range);
            },
            options: [
              FormBuilderChipOption(
                value: _ReportRange.current,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(Icons.date_range),
                    const Text('Current Range'),
                  ],
                ),
              ),
              if (!_model.loadOverTime)
                FormBuilderChipOption(
                  value: _ReportRange.all,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(Icons.calendar_month),
                      const Text('All'),
                    ],
                  ),
                ),
              const FormBuilderChipOption(
                value: _ReportRange.date,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Stack(children: [Icon(Icons.today), Icon(Icons.event)]),
                    Text('Specific Dates'),
                  ],
                ),
              ),
              const FormBuilderChipOption(
                value: _ReportRange.relative,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [Icon(Icons.event_note), Text('Relative Dates')],
                ),
              ),
            ],
          ),
        ),
        if (range == _ReportRange.date)
          InputWrapper(child: _DateRangeInput(_model)),
        if (range == _ReportRange.relative)
          InputWrapper(child: _RelativeDateInput(_model)),
      ],
    ),
    ReportFilterEdit(_model.filterObj),
  ];

  @override
  Widget build(final BuildContext context) {
    return InputFormScreen(
      inputs: inputs(),
      hasChanges: () => widget.reportId != null && _prevReport != _model,
      deleteCallback:
          _prevReport == null
              ? null
              : () async =>
                  await ReckonerDb.I.commonDao.softDeleteDbItem(_prevReport!) >
                  0,
      beforeCopy: _model.isNew ? null : () => _model = _model.copy(true),
      afterFormSave: () async {
        for (final report in di<DataCache>().reportMap.value.values) {
          if (report == _model && report.uuid != _model.uuid) {
            await showDialog<bool>(
              context: context,
              builder:
                  (final context) => AlertDialog(
                    content: Row(
                      children: [
                        const Icon(Icons.warning, color: Colors.orange),
                        Expanded(
                          child: Text(
                            'This report is identical to the existing report: "${report.name}". Please modify report parameters or rename the existing report.',
                          ),
                        ),
                        const Icon(Icons.warning, color: Colors.orange),
                      ],
                    ),
                  ),
            );
            return null;
          }
        }
        return saveItem(_model);
      },
      title: const Text('Edit Report'),
    );
  }
}

class _DateRangeInput extends StatefulWidget {
  final Report report;
  const _DateRangeInput(this.report);
  @override
  State<StatefulWidget> createState() => _DateRangeInputState();
}

class _DateRangeInputState extends State<_DateRangeInput> {
  late final Report report;

  @override
  void initState() {
    super.initState();
    report = widget.report;
  }

  @override
  Widget build(final BuildContext context) {
    final range = report.range;
    final displayWidget =
        range == null
            ? const Text('No dates selected')
            : Text(
              '${datetimeDisplay(range.start)} - ${datetimeDisplay(range.end)}',
            );

    return SizedBox(
      width: 400,
      child: InputFieldGroup(
        label: 'Date Range',
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            displayWidget,
            DefaultIconButton(
              icon: const Icon(Icons.calendar_today),
              onPressed: () async {
                final manualRange = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime(1990),
                  lastDate: DateTime(DateTime.now().year + 1),
                  builder:
                      (final context, final child) => Column(
                        children: [
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 500.0),
                            child: child,
                          ),
                        ],
                      ),
                );
                if (manualRange != null) {
                  final range = TZDateTimeRange.from(manualRange);
                  setState(
                    () =>
                        report
                          ..startDate = range.start
                          ..endDate = range.end,
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _RelativeDateInput extends StatefulWidget {
  final Report report;
  const _RelativeDateInput(this.report);
  @override
  State<StatefulWidget> createState() => _RelativeDateInputState();
}

class _RelativeDateInputState extends State<_RelativeDateInput> {
  late final Report report;
  int rebuild = 0;

  @override
  void initState() {
    super.initState();
    report = widget.report;
  }

  String relativeRange(final int offset) => relativeRangeLabel(
    switch (report.relativeRange ?? ReportTimeRange.month) {
      ReportTimeRange.day => TimeRange.day,
      ReportTimeRange.month => TimeRange.month,
      ReportTimeRange.year => TimeRange.year,
      _ => TimeRange.year,
    },
    offset,
  );

  @override
  Widget build(final BuildContext context) {
    report.relativeRange ??= ReportTimeRange.month;
    report.relativeStartOffset ??= 0;
    report.relativeEndOffset ??= 0;

    double min = -10, max = 10;

    Row(
      children: [
        FormBuilderRangeSlider(
          name: 'slider',
          min: min,
          max: max,
          initialValue: RangeValues(min, max),
        ),
        TextButton(
          onPressed:
              () => setState(() {
                min += 1;
                max -= 1;
              }),
          child: const Text('Change Range'),
        ),
      ],
    );

    return Row(
      children: [
        Expanded(
          child: FormBuilderRangeSlider(
            name: 'sliderRange${rebuild++}',
            decoration: const InputDecoration(border: InputBorder.none),
            displayValues:
                LayoutConfig(context).isMobile
                    ? DisplayValues.current
                    : DisplayValues.all,
            min: switch (report.relativeRange!) {
              ReportTimeRange.year => -1,
              ReportTimeRange.month => -6,
              ReportTimeRange.day => -90,
              _ => 0,
            },
            max: switch (report.relativeRange!) {
              ReportTimeRange.year => 1,
              ReportTimeRange.month => 6,
              ReportTimeRange.day => 90,
              _ => 0,
            },
            divisions: switch (report.relativeRange!) {
              ReportTimeRange.year => 2,
              ReportTimeRange.month => 12,
              ReportTimeRange.day => 180,
              _ => 0,
            },
            initialValue: RangeValues(
              report.relativeStartOffset!.toDouble(),
              report.relativeEndOffset!.toDouble(),
            ),
            onChanged:
                (final value) =>
                    report
                      ..relativeStartOffset = value?.start.round()
                      ..relativeEndOffset = value?.end.round(),
            valueWidget: (final value) {
              if (report.relativeStartOffset == report.relativeEndOffset) {
                return Text(relativeRange(report.relativeStartOffset!));
              }
              return Text(
                '${relativeRange(report.relativeStartOffset!)} to ${relativeRange(report.relativeEndOffset!)}',
              );
            },
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: ChipEdit<ReportTimeRange>(
            initialValue: report.relativeRange,
            onChanged: (final value) {
              report
                ..relativeStartOffset = null
                ..relativeEndOffset = null;
              setState(() => report.relativeRange = value);
            },
            options: const [
              FormBuilderChipOption(
                value: ReportTimeRange.year,
                child: Text('year'),
              ),
              FormBuilderChipOption(
                value: ReportTimeRange.month,
                child: Text('month'),
              ),
              FormBuilderChipOption(
                value: ReportTimeRange.day,
                child: Text('day'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
