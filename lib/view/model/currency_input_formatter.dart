import 'package:decimal/decimal.dart';
import 'package:flutter/services.dart';

import '../../model/currency.dart';

class CurrencyInputFormatter extends TextInputFormatter {
  final Currency currency;
  final String? locale;
  final bool enableNegative;
  late final Decimal multiplier;

  CurrencyInputFormatter({
    final Currency? currency,
    this.locale,
    this.enableNegative = true,
    final bool showSymbol = true,
  }) : currency = currency ?? Currency();

  CurrencyInputFormatter copyWith({
    final Currency? currency,
    final String? locale,
    final bool? enableNegative,
  }) {
    return CurrencyInputFormatter(
      currency: currency ?? this.currency,
      locale: locale ?? this.locale,
      enableNegative: enableNegative ?? this.enableNegative,
    );
  }

  String fromDB(final int number) => currency.formatAmount(number);

  int toDB(final String value) => currency.toDatabase(value);

  String format(final String text) => fromDB(toDB(text));

  String absFormat(final String text) => fromDB(toDB(text).abs());

  @override
  TextEditingValue formatEditUpdate(
    final TextEditingValue oldValue,
    final TextEditingValue newValue,
  ) {
    if (currency.decimalFormatter.tryParse(newValue.text) != null) {
      return newValue;
    }
    return TextEditingValue(text: fromDB(toDB(newValue.text)));
  }
}
