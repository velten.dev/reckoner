const Map<String, String> localizedValuesEn = {
  'add': 'Add',
  'edit': 'Edit',
  'delete': 'Delete',
  'save': 'Save',
  'cancel': 'Cancel',
  'continue': 'Continue',
  'name': 'Name',
  'symbol': 'Symbol',
  //Popup message to verify a user wants to delete a data item
  'deleteMsg': 'Are you sure you want to delete this {{item}}?',
  //Tooltip on button when adding an item like a Currency
  'addItem': 'Add {{item}}',
  //Tooltip on button when editing an item like a Currency
  'editItem': 'Edit {{item}}',
  //Text placeholder when trying to display a list of data like Accounts and there are none
  'addPlaceholder': 'Click the "+" icon to add a {{item}}!',
  //Error message when user tries to save data when a value is required
  'missingValue': 'Please enter a value',
  'currency': 'Currency',
  'currencyTitle': 'Currencies',
  'currencyCode': 'Code',
  'currencyDigits': 'Decimal Digits',
  'category': 'Category',
};
