import 'package:flutter/material.dart';

import 'localization_en.dart';

String _interpolate(final String string, final Map<String, String> params) {
  final keys = params.keys;
  String result = string;
  for (final key in keys) {
    result = result.replaceAll('{{$key}}', params[key]!);
  }

  return result;
}

class ReckonerLocalizations {
  late final Map<String, String> localizedValues;

  ReckonerLocalizations(final Map<String, String>? localizedValues) {
    this.localizedValues = localizedValues ?? localizedValuesEn;
  }

  static ReckonerLocalizations of(final BuildContext context) {
    return Localizations.of<ReckonerLocalizations>(
      context,
      ReckonerLocalizations,
    )!;
  }

  String _getValue(final String key) =>
      localizedValues[key] ?? localizedValuesEn[key]!;

  String get add => _getValue('add');
  String get edit => _getValue('edit');
  String get delete => _getValue('delete');
  String get save => _getValue('save');
  String get cancel => _getValue('cancel');
  String get continueButton => _getValue('continue');
  String get name => _getValue('name');
  String get symbol => _getValue('symbol');
  String deleteMsg(final String item) =>
      _interpolate(_getValue('deleteMsg'), {'item': item});
  String addItem(final String item) =>
      _interpolate(_getValue('addItem'), {'item': item});
  String editItem(final String item) =>
      _interpolate(_getValue('editItem'), {'item': item});
  String addPlaceholder(final String item) =>
      _interpolate(_getValue('addPlaceholder'), {'item': item});
  String get missingValue => _getValue('missingValue');

  String get currency => _getValue('currency');
  String get currencyTitle => _getValue('currencyTitle');
  String get currencyCode => _getValue('currencyCode');
  String get currencyDigits => _getValue('currencyDigits');

  String get category => _getValue('category');

  static const LocalizationsDelegate<ReckonerLocalizations> delegate =
      _ReckonerLocalizationDelegate();
}

class _ReckonerLocalizationDelegate
    extends LocalizationsDelegate<ReckonerLocalizations> {
  const _ReckonerLocalizationDelegate();

  @override
  bool isSupported(final Locale locale) => locale.languageCode == 'en';

  //Should do this by adding an assets directory and loading json locale through the root bundle
  //https://api.flutter.dev/flutter/services/rootBundle.html
  @override
  Future<ReckonerLocalizations> load(final Locale locale) =>
      Future.value(ReckonerLocalizations(null));

  @override
  bool shouldReload(
    covariant final LocalizationsDelegate<ReckonerLocalizations> old,
  ) => false;
}
