import 'dart:async';

import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/currency.dart';
import '../../model/model.dart';
import '../../model/transaction.dart';
import '../../service/data_cache.dart';
import '../../service/logger.dart';
import '../../service/sync_service_manager.dart';
import '../l10n/localizations.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/default_text_button.dart';
import '../widgets/form/list_search_input.dart';

Future<int> _deleteItem<D extends Tracked<D>>(final D item) async {
  final db = ReckonerDb.I;
  final cnt = await db.commonDao.softDeleteDbItem(item);
  di<SyncServiceManager>().sendUpdates();
  return cnt;
}

Future<bool> deletePopup<D extends Tracked<D>>(
  final BuildContext context,
  final D item,
  final String itemName,
) {
  return yesNoDialog(
    context,
    ReckonerLocalizations.of(context).deleteMsg(itemName),
    callback: () async {
      try {
        await _deleteItem(item);
      } catch (err, stack) {
        logError('Reckoner', err, stack);
      }
    },
  );
}

Future<bool> yesNoDialog(
  final BuildContext context,
  final String alert, {
  final FutureOr<void> Function()? callback,
  final String? continueText,
  final String? cancelText,
}) async {
  final localization = ReckonerLocalizations.of(context);
  final String yesText = continueText ?? localization.continueButton;
  final String noText = cancelText ?? localization.cancel;
  final success = await showDialog<bool>(
    context: context,
    builder:
        (final context) => AlertDialog(
          content: Text(alert),
          actions: [
            TextButton(
              onPressed: () async {
                await callback?.call();
                if (context.mounted) Navigator.pop(context, true);
              },
              child: Text(yesText),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text(noText),
            ),
          ],
        ),
  );
  return success ?? false;
}

Future<Currency?> selectCurrency(
  final BuildContext context,
  final Iterable<Currency?>? excludeList,
) async {
  Currency? value;
  final List<Currency> currencyList = List.from(
    di<DataCache>().currencyMap.value.values,
  );
  if (excludeList != null) {
    currencyList.removeWhere((final cur) => excludeList.contains(cur));
  }

  return showDialog<Currency?>(
    context: context,
    builder:
        (final context) => AlertDialog(
          content: ListSearchFormField<Currency>(
            decoration: const InputDecoration(
              suffixIcon: Icon(Icons.arrow_drop_down_rounded),
              label: Text('Currency'),
            ),
            itemToDisplayString: (final Currency? item) => item?.name ?? ' ',
            searchItems: currencyList,
            onChanged: (final val) => value = val,
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, value),
              child: const Text('Accept'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, null),
              child: const Text('Cancel'),
            ),
          ],
        ),
  );
}

Future<bool> deleteTxEntityPopup(
  final BuildContext context,
  final TransactionEntity item,
) {
  final itemName = item.isTransfer ? 'Transfer' : 'Transaction';
  return yesNoDialog(
    context,
    ReckonerLocalizations.of(context).deleteMsg(itemName),
    callback: () async {
      try {
        if (item.transfer != null) {
          await _deleteItem(item.transfer!.source);
          await _deleteItem(item.transfer!.destination);
        } else {
          await _deleteItem(item.transaction!);
        }
      } catch (err, stack) {
        logError('Reckoner', err, stack);
      }
    },
  );
}

Future<IconData?> pickIcon(
  final BuildContext context,
  final List<IconData> icons,
) async {
  return showDialog<IconData>(
    context: context,
    builder: (final ctx) {
      return AlertDialog(
        content: SizedBox(
          width: 200,
          child: GridView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(20),
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              maxCrossAxisExtent: 50,
              mainAxisExtent: 50,
            ),
            children:
                icons
                    .map(
                      (final e) => DefaultIconButton(
                        icon: Icon(e),
                        onPressed: () => Navigator.pop(ctx, e),
                      ),
                    )
                    .toList(),
          ),
        ),
      );
    },
  );
}

Future<bool> editDiscardPopup(
  final BuildContext context,
  final GlobalKey<FormState> formKey,
  final FutureOr<dynamic> Function() saveFn,
) async {
  final localization = ReckonerLocalizations.of(context);
  final theme = Theme.of(context);
  final success = await showDialog<bool>(
    context: context,
    builder:
        (final context) => AlertDialog(
          content: Text('Discard changes?'),
          actions: [
            DefaultTextButton(
              onPressed: () async {
                bool saved = false;
                if (formKey.currentState!.validate()) {
                  formKey.currentState!.save();
                  if (await saveFn() != null) saved = true;
                }
                if (context.mounted) {
                  Navigator.pop(context, saved ? true : null);
                }
              },
              child: Text(localization.save),
            ),
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: theme.colorScheme.error,
                foregroundColor: theme.colorScheme.onError,
              ),
              onPressed: () async {
                if (context.mounted) Navigator.pop(context, true);
              },
              child: Text('Discard'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text(localization.cancel),
            ),
          ],
        ),
  );
  return success ?? false;
}
