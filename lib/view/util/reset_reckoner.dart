import 'package:flutter/material.dart';

import '../../database/reckoner_db.dart';
import '../../model/secure_storage.dart';
import '../../service/logger.dart';
import '../reckoner.dart';

Future<void> resetReckoner(final BuildContext context) async {
  final logList = await getErrorLogs();
  await Future.wait(logList.map((final e) => e.delete()));
  await ReckonerDb.clear();
  await SecureStorage.clear();
  if (context.mounted) Reckoner.restartApp(context);
}
