import 'package:intl/intl.dart';

String datetimeDisplay(final DateTime inst, {final bool short = false}) {
  if (short) {
    return DateFormat.yMd()
        .format(inst)
        .replaceAll(inst.year.toString(), inst.year.toString().substring(2));
  }
  return DateFormat.yMMMd().format(inst);
}
