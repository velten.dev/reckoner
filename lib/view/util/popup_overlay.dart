import 'package:flutter/material.dart';

// Created this after having trouble with bugs from the dropdown_search package.
// Breaking changes in addition to the bug
// https://github.com/salim-lachdhaf/searchable_dropdown/issues/498
// caused me to write code it instead of adding a dependency. The benefit is
// that the code is more concise due to being target to the application's needs.
//
// Shamelessly ripped code from Flutter Material PopupMenu and from
// dropdown_search 5.0.3 (https://pub.dev/packages/dropdown_search),
// both MIT licensed.

Future<T?> popupOverlay<T>(
  final BuildContext context,
  final Widget child,
) async {
  final RenderBox button = context.findRenderObject()! as RenderBox;
  final RenderBox overlay =
      Navigator.of(context).overlay!.context.findRenderObject()! as RenderBox;
  final RelativeRect position = RelativeRect.fromRect(
    Rect.fromPoints(
      button.localToGlobal(Offset.zero, ancestor: overlay),
      button.localToGlobal(
        button.size.bottomRight(Offset.zero),
        ancestor: overlay,
      ),
    ),
    Offset.zero & overlay.size,
  );
  final NavigatorState navigator = Navigator.of(context);

  return navigator.push(
    _PopupRoute<T>(
      position: position,
      child: child,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      capturedThemes: InheritedTheme.capture(
        from: context,
        to: navigator.context,
      ),
    ),
  );
}

class _PopupRoute<T> extends PopupRoute<T> {
  final RelativeRect position;
  final double? elevation;
  final String? semanticLabel;
  final CapturedThemes capturedThemes;
  final Widget child;

  _PopupRoute({
    required this.barrierLabel,
    required this.position,
    required this.child,
    this.elevation,
    this.semanticLabel,
    required this.capturedThemes,
  });

  @override
  Color? get barrierColor => null;

  @override
  bool get barrierDismissible => true;

  @override
  final String barrierLabel;

  @override
  Widget buildPage(
    final BuildContext context,
    final Animation<double> animation,
    final Animation<double> secondaryAnimation,
  ) {
    final PopupMenuThemeData popupMenuTheme = PopupMenuTheme.of(context);
    final menu = Material(
      shape: popupMenuTheme.shape,
      color: popupMenuTheme.color,
      type: MaterialType.card,
      elevation: popupMenuTheme.elevation ?? 8.0,
      child: Padding(padding: const EdgeInsets.all(5), child: child),
    );

    return CustomSingleChildLayout(
      delegate: _PopupMenuRouteLayout(context, position),
      child: capturedThemes.wrap(menu),
    );
  }

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);
}

// Positioning of the menu on the screen.
class _PopupMenuRouteLayout extends SingleChildLayoutDelegate {
  // Rectangle of underlying button, relative to the overlay's dimensions.
  final RelativeRect position;
  final BuildContext context;

  _PopupMenuRouteLayout(this.context, this.position);

  @override
  BoxConstraints getConstraintsForChild(final BoxConstraints constraints) {
    final parentRenderBox = context.findRenderObject() as RenderBox;
    //keyBoardHeight is height of keyboard if showing
    final double keyBoardHeight = MediaQuery.of(context).viewInsets.bottom;
    final double safeAreaTop = MediaQuery.of(context).padding.top;
    final double safeAreaBottom = MediaQuery.of(context).padding.bottom;
    final double totalSafeArea = safeAreaTop + safeAreaBottom;
    final double maxHeight =
        constraints.minHeight - keyBoardHeight - totalSafeArea;
    return BoxConstraints.loose(
      Size(
        parentRenderBox.size.width - position.right - position.left,
        maxHeight,
      ),
    );
  }

  @override
  Offset getPositionForChild(final Size size, final Size childSize) {
    // size: The size of the overlay.
    // childSize: The size of the menu, when fully open, as determined by
    // getConstraintsForChild.

    //keyBoardHeight is height of keyboard if showing
    final double keyBoardHeight = MediaQuery.of(context).viewInsets.bottom;

    final double x = position.left;

    // Find the ideal vertical position.
    double y = position.top;
    // check if we are in the bottom
    if (y + childSize.height > size.height - keyBoardHeight) {
      y = size.height - childSize.height - keyBoardHeight;
    }

    return Offset(x, y);
  }

  @override
  bool shouldRelayout(final _PopupMenuRouteLayout oldDelegate) {
    return true;
  }
}
