enum TimeRange { year, month, week, day }

String relativeRangeLabel(
  final TimeRange range,
  final int offset, [
  final bool hidePrefix = false,
]) {
  final plural = offset.abs() > 1;
  final String timeLabel = switch (range) {
    TimeRange.year => plural ? 'years' : 'year',
    TimeRange.month => plural ? 'months' : 'month',
    TimeRange.week => plural ? 'weeks' : 'week',
    TimeRange.day => plural ? 'days' : 'day',
  };
  String numLabel = '';
  if (offset == 0) {
    if (range == TimeRange.day) return 'today';
    numLabel = 'this';
  } else if (offset < 0) {
    if (range == TimeRange.day && offset.abs() == 1) {
      return 'yesterday';
    }
    numLabel = 'last';
  } else {
    if (range == TimeRange.day && offset.abs() == 1) {
      return 'tomorrow';
    }
    numLabel = 'next';
  }

  if (offset.abs() > 1) {
    if (hidePrefix) {
      numLabel = offset.abs().toString();
    } else {
      numLabel += ' ${offset.abs()}';
    }
  }

  return '$numLabel $timeLabel';
}
