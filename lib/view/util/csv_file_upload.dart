import 'dart:io';

import 'package:collection/collection.dart';
import 'package:csv/csv.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/platform.dart';
import '../../model/account.dart';
import '../../model/currency.dart';
import '../../model/transaction.dart';
import '../../model/tz_date_time.dart';
import '../../service/data_cache.dart';
import '../widgets/form/list_search_input.dart';
import '../widgets/form/validators.dart';
import '../widgets/toast_message.dart';
import 'save_item.dart';

typedef _ImportData = ({Currency? currency, Account? account});

List<Transaction>? csvToTransaction(
  final String rawCsv,
  final Currency currency,
  final Account account,
  final void Function(String) errorHandler,
) {
  final List<List<String>> rows = const CsvToListConverter().convert(
    rawCsv,
    eol: '\n',
  );

  if (rows.length < 2) {
    errorHandler('CSV does not contain data');
    return null;
  }

  if (!const ListEquality<String>().equals(rows.first, [
    'Description',
    'Date',
    'Amount',
  ])) {
    errorHandler('CSV header not formatted correctly');
    return null;
  }

  int rowInd = 1;
  final List<Transaction> data = [];

  try {
    for (rowInd = 1; rowInd < rows.length; rowInd++) {
      data.add(_csvRowToTxSave(rows[rowInd], currency, account));
    }
  } catch (e) {
    errorHandler('Error importing row: $rowInd. Check the row for errors.');
    return null;
  }

  return data;
}

Transaction _csvRowToTxSave(
  final List<String> row,
  final Currency currency,
  final Account account,
) =>
    Transaction()
      ..currency = currency
      ..account = account
      ..name = row[0]
      ..date = tzParse(row[1])
      ..amount = currency.toDatabase(row[2]);

Future<void> pickFileAndUpload(final BuildContext context) async {
  final DataCache cache = di();
  final CurrencyMap currencyMap = cache.currencyMap.value;

  final result = await FilePicker.platform.pickFiles(
    allowedExtensions: ['csv'],
    type: FileType.custom,
    allowMultiple: false,
    lockParentWindow: true,
    withData: isWeb,
  );

  if (result == null || result.files.isEmpty || result.files.length > 1) {
    return;
  }

  if (context.mounted) {
    final importData = await showDialog<_ImportData>(
      context: context,
      builder:
          (final _) => _AccountPicker(
            accountInfos: cache.accounts.value,
            currencies: currencyMap.values.toList(),
          ),
    );
    if (importData == null ||
        importData.currency == null ||
        importData.account == null) {
      return;
    }
    final Account account = importData.account!;
    final Currency currency = importData.currency!;

    String rawCsv;
    if (isWeb) {
      rawCsv = String.fromCharCodes(result.files.first.bytes!);
    } else {
      final file = File(result.files.first.path!);
      rawCsv = await file.readAsString();
    }
    final txSaveList = csvToTransaction(
      rawCsv,
      currency,
      account,
      (final error) => context.mounted ? showError(error) : null,
    );
    if (txSaveList == null) return;
    for (final txSave in txSaveList) {
      await saveItem(txSave);
    }
  }
}

class _AccountPicker extends StatefulWidget {
  final List<Account> accountInfos;
  final List<Currency> currencies;

  const _AccountPicker({required this.accountInfos, required this.currencies});

  @override
  State<StatefulWidget> createState() => _AccountPickerState();
}

class _AccountPickerState extends State<_AccountPicker> {
  Account? account;
  Currency? currency;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(final BuildContext context) {
    return AlertDialog(
      title: const Text('Select Account'),
      content: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListSearchFormField<Account>(
              decoration: const InputDecoration(
                label: Text('Account'),
                suffixIcon: Icon(Icons.arrow_drop_down_rounded),
              ),
              searchItems: widget.accountInfos,
              itemToDisplayString: (final info) => info.displayName,
              onChanged: (final val) => account = val,
              validator: (final val) => nonNullValidator(context, val),
            ),
            if (account != null && account!.isMultiCurrency)
              ListSearchFormField<Currency>(
                decoration: const InputDecoration(
                  label: Text('Currency'),
                  suffixIcon: Icon(Icons.arrow_drop_down_rounded),
                ),
                searchItems: widget.currencies,
                itemToDisplayString: (final info) => info.displayName,
                onChanged: (final val) => currency = val,
                validator: (final val) => nonNullValidator(context, val),
              ),
          ],
        ),
      ),
      actionsPadding: const EdgeInsets.symmetric(horizontal: 8.0),
      actions: [
        TextButton(
          onPressed: () {
            if (!_formKey.currentState!.validate()) return;
            Navigator.pop(context, (
              currency: currency ?? account?.currency,
              account: account,
            ));
          },
          child: const Text('Done'),
        ),
      ],
    );
  }
}
