# Reckoner

Privacy first financial tracking, with radical customizability!

---

## Audience

This tool is designed for both average end users and powers users interested tracking their finances and keeping that information private. Most other tracking tools include the ability for the company to aggregate and use your personal data for marketing and advertisements to you. This app is for those who don't want to give these companies access to some of your most private data and want a polished and customized view of their finances.

This tool is <ins>**not**</ins> designed to convert people who are happy with their financial tracking solution. If you are happy with YNAB, Personal Capital, Tiller, etc. then keep on keeping on. I designed this tool when I couldn't find an open source financial tracking tool that met my need for private financial tracking and customizability for all the operating systems I use.

## Demo

If you want to try this out before installing I have a demo site at [demo.reckoner.finance](https://demo.reckoner.finance). You can do anything in the demo except device syncing. The demo site uses an in-memory database which re-adds all data on refresh. If you setup device syncing, then every time you would start getting duplicate data every time the browser refreshed.

## Features

- Data is stored locally in a SQLite database
- Multi-paradigm tracking support
- Ability to add multiple user defined category groups
- Create custom report layouts to view relevant data at a glance
- Native support for Linux, Android, and Web
- Multi-currency support (including custom user currencies)
- Splitting of transactions to add tags and categories based on the split

### Planned Features

- Rules engine for automation
- Recurring transaction handling
- Marketplace for custom designed financial tracking 'dogmas' (use of categories, tags, and reports to support a financial tracking methodology)
- Downloading of transactions from financial aggregators

## Installing

Reckoner is available on the following platforms.

<a href="https://apt.izzysoft.de/fdroid/index/apk/finance.reckoner.app">
    <img alt="Get it on IzzyOnDroid" height="60" src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid2.png" />
</a>
<a href='https://flathub.org/apps/finance.reckoner.Reckoner'>
    <img alt='Download on Flathub' height="60" src='https://flathub.org/api/badge?locale=en' />
</a>
<a href="https://app.reckoner.finance/" target="_blank">
    <img alt="Launch as Web App" height="60" src="https://reckoner.finance/PWA-dark-en.svg" />
</a>
<a href="https://codeberg.org/Reckoner/app/releases/latest" target="_blank">
    <img src="https://reckoner.finance/codeberg-get-it.svg" height="60" alt="Get it on Codeberg" />
</a>

<a href="https://apps.apple.com/us/app/reckoner-finance/id6505043355" >
    <img src="https://toolbox.marketingtools.apple.com/api/v2/badges/download-on-the-app-store/black/en-us" alt="Download on the App Store" height="60" />
</a>

### Windows

Currently there isn't a plan to support Windows. I did a quick experiment with compiling to windows in the past and the database helper I am using, [Drift](https://pub.dev/packages/drift), [doesn't have great support for Windows](https://github.com/simolus3/sqlite3.dart/issues/177).

### Google Play

In order to release an app on Google Play, you need [20 testers to install your app in closed testing for 14 days continuously](https://support.google.com/googleplay/android-developer/answer/14151465?hl=en). Of course there are services you can pay for to, for a lack of a better word, hack the system and get your 20 testers. I am not willing to pay for these services for a free app. If interested in having this released on Google Play, you can [email me](mailto:victor@velten.dev) and I can include you as a tester.

## Building

This relies on the latest release of the flutter framework. Consult the relevant [installation guide](https://docs.flutter.dev/get-started/install) for your operating system and the platforms you are trying to build. Most of the builds can be invoked by calling the relevant make command. The commands below assume a bash like shell with cmake installed.

```bash
# Android APK and appbundle
make android

# Web
make web

# Demo web page
make demo
```

### Linux

On linux you need to install the required dependencies. I've included a script to do so on debian based distributions.

```bash
# Should only need to do this once
./install.sh

# Build it!
make linux

# Build it for both x86 and arm64 using docker
make docker_linux
```

### iOS and MacOS

For iOS and MacOS, you only need to have the flutter requirements installed. Actually follow Flutter documentation to get developer keys and setup your system for iOS development on macOS.

```bash
# Build for iOS
make ios

# Create a DMG image for use in MacOS, required appdmg installed
make dmg
```

## Credit

Before building this tool I used [Firefly III](https://www.firefly-iii.org/) for my personal finance tracking. It is a great tool and some of the user interface design for this app borrows heavily from that project. However, as James Cole states in his documentation ["It's opinionated, which means it follows the mantras that I, the developer like."](https://docs.firefly-iii.org/firefly-iii/faq/general/#what-is-firefly-iii). That is his prerogative, but I found myself bumping into these limitations imposed by the design and wanted to build not just a tool for myself, but one which could be molded for any mantra. Thus this tool.
