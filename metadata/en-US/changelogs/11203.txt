Enhancements:
 * Add calculator on amount fields
 * Add ability to save on discard popups

Fixes:
 * Properly detect if there are changes in a transaction edit so popup is not shown if no changes
 * Only change sync date on bulk sync to prevent sync drift