Fixes
* Properly check for transaction changes in merchants and tags
* Update merchant page to direct user to create a transaction to add a merchant
* Update transaction edit to add icon on merchant field when adding a new merchant