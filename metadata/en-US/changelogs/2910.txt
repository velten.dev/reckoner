Fixes
 * Fix sync disconnect after first connect
 * Fix sync save regressions
 * Fix report group sync saving issue