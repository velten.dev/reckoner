Fixes
* Fix issue with Pocketbase sync where too many requests were made
* Properly send schemaVersion in settings sync bundle