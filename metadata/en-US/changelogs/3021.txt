Major Update:
* Modified handling of Merchants. They are now just a free text field on transactions instead of being their own table.
* Added location handling to transactions from the merchant table